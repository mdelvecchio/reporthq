﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportHq.IncidentReports.Entities
{
	/// <summary>
	/// The base entity class for the Property and Evidence -Item objects.
	/// </summary>
	public class PropertyEvidenceItemEntity
    {
		public int ID { get; set; }

		public int ReportID { get; set; }

		public int LossTypeID { get; set; }

		public string LossType { get; set; }

		public decimal Quantity { get; set; }

		public int TypeID { get; set; }

		public string Type { get; set; }

		public int NarcoticWeightTypeID { get; set; }

		public string NarcoticWeightUnit { get; set; }

		public int NarcoticTypeID { get; set; }

		public string NarcoticType { get; set; }

		public string Description { get; set; }

		public string Brand { get; set; }

		public string SerialNumber { get; set; }

		public int Value { get; set; }
    }
}
