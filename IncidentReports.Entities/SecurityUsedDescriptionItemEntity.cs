﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportHq.IncidentReports.Entities
{
	/// <summary>
	/// The base entity class for the M.O. DescriptionItem domain object. Used to pass between BAL & DAL.
	/// </summary>
	public class SecurityUsedDescriptionItemEntity
    {
		/// <summary>
		/// Our unique SQL Server ID.
		/// </summary>
		public int ID { get; set; }

		/// <summary>
		/// A legacy mainframe value used by the data entry people and for backwards compatibility w/ the official mainframe stats (we will be doing an EPR nightly dump into it).
		/// </summary>
		public string MainframeCode { get; set; }

		/// <summary>
		/// Human readble description.
		/// </summary>
		public string Description { get; set; }

		/// <summary>
		/// Indicates whether this security-item was defeated by the baddie.
		/// </summary>
		public bool WasDefeated { get; set; }
    }
}
