﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportHq.IncidentReports.Entities
{
	/// <summary>
	/// The base entity class for the Weapon object. Used to pass between BAL & DAL.
	/// </summary>
	public class WeaponEntity
    {
		public int ID { get; set; }

		public int ReportID { get; set; }

		public int TypeID { get; set; }

		public string Type { get; set; }

		public int MakeTypeID { get; set; }

		public string Make { get; set; }

		public string Model { get; set; }

		public int CaliberTypeID { get; set; }

		public string Caliber { get; set; }

		public string SerialNumber { get; set; }

		public bool NcicIsStolen { get; set; }

		public string NcicContactName { get; set; }

		public bool HasPawnshopRecord { get; set; }

		public string PawnshopContactName { get; set; }

		public string AdditionalInfo { get; set; }
    }
}
