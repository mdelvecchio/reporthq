﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportHq.IncidentReports.Entities
{
	/// <summary>
	/// The base entity class for the Vehicle object. Used to pass between BAL & DAL.
	/// </summary>
	public class VehicleEntity
    {
		public int ID { get; set; }

		public int ReportID { get; set; }

		public int StatusTypeID { get; set; }

		public string Status { get; set; }

		public int Year { get; set; }

		public int MakeTypeID { get; set; }

		public string Make { get; set; }

		public int ModelTypeID { get; set; }

		public string Model { get; set; }

		public int ColorTypeID { get; set; }

		public string Color { get; set; }

		public int StyleTypeID { get; set; }

		public string Style { get; set; }

		public int Value { get; set; }

		public string LicensePlate { get; set; }

		public string LicensePlateState { get; set; }

		public int LicensePlateYear { get; set; }

		public string Vin { get; set; }

		public int RecoveredTypeID { get; set; }

		public string RecoveredType { get; set; }

		public string RecoveryLocation { get; set; }

		public int RecoveryDistrict { get; set; }

		public string RecoveryZone { get; set; }

		public string RecoverySubZone { get; set; }

		public string AdditionalDescription { get; set; }

		public int DamageLevelTypeID { get; set; }

		public string DamageLevel { get; set; }

		public List<DescriptionItemEntity> DamageZoneItems { get; set; }

		public List<DescriptionItemEntity> RecoveredVehicleMoItems { get; set; }
    }
}
