﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportHq.IncidentReports.Entities
{
	/// <summary>
	/// The base entity class for the M.O. DescriptionItem object. Used to pass between BAL & DAL.
	/// </summary>
	public class DescriptionItemEntity
    {
		public int ID { get; set; }

		public string Description { get; set; }
    }
}
