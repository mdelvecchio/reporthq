﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportHq.IncidentReports.Entities
{
	/// <summary>
	/// The base entity class for the Report object. Used to pass between BAL & DAL.
	/// </summary>
    public class ReportEntity
    {
		#region Enums

		public enum ApprovalStatusTypes
		{
			Approved = 1,
			Rejected = 2,
			Pending = 3,
			Incomplete = 4,
			DataEntry = 5
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion 

		#region REQUIRED

		/// <summary>
		/// The db-issued unique ID value for this report.
		/// </summary>
		public int ID { get; set; }

		/// <summary>
		/// The type ID for this report (Initial, or Supplementary)
		/// </summary>
		public int ReportTypeID { get; set; }

		public int ApprovalStatusTypeID { get; set; }

		/// <summary>
		/// The current approval-workflow status of this report.
		/// </summary>
		public ApprovalStatusTypes ApprovalStatusType { get; set; }

		/// <summary>
		/// The unique CAD-issued incident number.
		/// </summary>
		public string ItemNumber { get; set; }

		#endregion

		#region EVENT

		public int SignalTypeID { get; set; }

		public string Signal { get; set; }

		public string IncidentDescription { get; set; }

		public string Location { get; set; }

		public int District { get; set; }

		public string Zone { get; set; }

		public string SubZone { get; set; }

		public DateTime OccurredDateTime { get; set; }

		public DateTime ReportDateTime { get; set; }

		public int LightingConditionTypeID { get; set; }

		public string LightingCondition { get; set; }

		public int StatusTypeID { get; set; }

		public string Status { get; set; }

		public bool BulletinRequired { get; set; }

		public int BulletinNumber { get; set; }

		#endregion

		#region UCR

		public int UcrCategoryID { get; set; }

		public string UcrCategory { get; set; }

		public int UcrOffenseTypeID { get; set; }

		public string UcrOffense { get; set; }

		#endregion

		#region ADMIN

		public string Officer1Name { get; set; }

		public int Officer1Badge { get; set; }

		public int Officer1EmployeeID { get; set; }

		public int Officer1RankTypeID { get; set; }

		public string Officer1Rank { get; set; }

		public string Officer2Name { get; set; }

		public int Officer2Badge { get; set; }

		public int Officer2RankTypeID { get; set; }

		public string Officer2Rank { get; set; }

		public int ReportingCarNumber { get; set; }

		public string ReportingCarPlatoon { get; set; }

		public int ReportingAssignmentUnitTypeID { get; set; }

		public string DetectiveName { get; set; }

		public string CrimeLabName { get; set; }

		public string OtherName { get; set; }

		#endregion

		#region SUPERVISOR

		public string SupervisorName { get; set; }

		public int SupervisorBadge { get; set; }

		public int SupervisorRankTypeID { get; set; }

		public string SupervisorRank { get; set; }

		public string SupervisorComments { get; set; }

		public DateTime SupervisorLastModifiedDate { get; set; }

		#endregion

		#region BOILER

		public string CreatedBy { get; set; }

		public DateTime CreatedDate { get; set; }

		public string LastModifiedBy { get; set; }

		public DateTime LastModifiedDate { get; set; }

		public bool IsActive { get; set; }

		#endregion

		#region INCIDENT MO - MOTIVATION

		public List<DescriptionItemEntity> MotivationCriminalActivityItems { get; set; }

		public List<DescriptionItemEntity> MotivationMotiveItems { get; set; }

		public List<DescriptionItemEntity> MotivationTargetItems { get; set; }

		#endregion

		#region INCIDENT MO - MEANS OF ENTRY

		public List<DescriptionItemEntity> MeansOfEntryPointOfEntryItems { get; set; }

		public List<DescriptionItemEntity> MeansOfEntryMethodOfEntryItems { get; set; }

		public List<SecurityUsedDescriptionItemEntity> MeansOfEntrySecurityUsedItems { get; set; }

		#endregion

		#region INCIDENT MO - CRIME LOCATION

		public List<DescriptionItemEntity> CrimeLocationResidentialItems { get; set; }

		public List<DescriptionItemEntity> CrimeLocationCommercialItems { get; set; }

		public List<DescriptionItemEntity> CrimeLocationOutdoorAreaItems { get; set; }

		public List<DescriptionItemEntity> CrimeLocationPublicAccessItems { get; set; }

		public List<DescriptionItemEntity> CrimeLocationMovableItems { get; set; }

		public List<DescriptionItemEntity> CrimeLocationStructureTypeItems { get; set; }

		public List<DescriptionItemEntity> CrimeLocationStructureStatusItems { get; set; }

		#endregion

		#region INCIDENT MO - OFFENDERS ACTIONS

		public List<DescriptionItemEntity> OffendersActionsApproachItems { get; set; }

		public List<DescriptionItemEntity> OffendersActionsImpersonatedItems { get; set; }

		public List<DescriptionItemEntity> OffendersActionsWeaponItems { get; set; }

		public List<DescriptionItemEntity> OffendersActionsFirearmFeatureItems { get; set; }

		public List<DescriptionItemEntity> OffendersActionsPropertyCrimeItems { get; set; }

		public List<DescriptionItemEntity> OffendersActionsPersonCrimeItems { get; set; }

		public List<DescriptionItemEntity> OffendersActionsSexCrimeItems { get; set; }

		#endregion

		#region DESCRIPTIONS

		public string Narrative { get; set; }

		public string MotivationDescription { get; set; }

		public string MeansOfEntryDescription { get; set; }

		public string CrimeLocationDescription { get; set; }

		public string OffendersActionsDescription { get; set; }

		#endregion

		#region COLLECTIONS

		public List<VictimPersonEntity> VictimPersons { get; set; }

		public List<OffenderEntity> Offenders { get; set; }

		public List<WeaponEntity> Weapons { get; set; }

		public List<VehicleEntity> Vehicles { get; set; }

		public List<PropertyItemEntity> Property { get; set; }

		public List<EvidenceItemEntity> Evidence { get; set; }

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		public bool IsSupplemental { get; set; }

		public bool IsDataEntry { get; set; }

		public string PropertyEvidenceReceiptNumber { get; set; }

		/// <summary>
		/// Gets sums of Property values for types Stolen and Recovered.
		/// </summary>
		public Dictionary<string, double> PropertySums { get; set; }

		/// <summary>
		/// Gets count of Offenders set as arrested.
		/// </summary>
		public int OffendersArrested { get; set; }

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    }
}
