﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportHq.IncidentReports.Entities
{
	/// <summary>
	/// The base entity class for the OffenderCharge object. Used to pass between BAL & DAL.
	/// </summary>
	public class OffenderChargeEntity
    {
		public int ID { get; set; }

		public int OffenderID { get; set; }

		public int ChargePrefixTypeID { get; set; }

		public string Prefix { get; set; }

		public int ChargeTypeID { get; set; }

		public string Code { get; set; }

		/// <summary>
		/// A description of the ChargeTypeID from ChargeTypes table. Read-only.
		/// </summary>
		public string Charge { get; set; }

		/// <summary>
		/// Optional additional details the officer may record.
		/// </summary>
		public string ChargeDetails { get; set; }

		public int VictimPersonID { get; set; }

		public int VictimNumber { get; set; }

		public int VictimOffenderRelationshipTypeID { get; set; }

		public string VictimsRelationship { get; set; }
    }
}
