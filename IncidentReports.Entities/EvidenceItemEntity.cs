﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportHq.IncidentReports.Entities
{
	/// <summary>
	/// The base entity class for the EvidenceItem object. Used to pass between BAL & DAL.
	/// </summary>
	public class EvidenceItemEntity : PropertyEvidenceItemEntity
    {
		//app properties are inherited from base
    }
}
