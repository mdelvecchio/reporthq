﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportHq.IncidentReports.Entities
{
	/// <summary>
	/// The base entity class for the VictimPerson object. Used to pass between BAL & DAL.
	/// </summary>
	public class VictimPersonEntity
    {
		public int ID { get; set; }

		public int ReportID { get; set; }

		public int VictimNumber { get; set; }

		public int VictimPersonTypeID { get; set; }

		public string VictimPersonType { get; set; }

		public int VictimTypeID { get; set; }

		public string VictimType { get; set; }

		public string LastName { get; set; }

		public string FirstName { get; set; }

		public int RaceTypeID { get; set; }

		public string Race { get; set; }

		public int GenderTypeID { get; set; }

		public string Gender { get; set; }

		public DateTime DateOfBirth { get; set; }

		public string StreetAddress { get; set; }

		public string City { get; set; }

		public string State { get; set; }

		public int ZipCode { get; set; }

		public string PhoneNumber { get; set; }

		public string SocialSecurityNumber { get; set; }

		public string DriversLicenseNumber { get; set; }

		public int SobrietyTypeID { get; set; }

		public string Sobriety { get; set; }

		public int InjuryTypeID { get; set; }

		public string Injury { get; set; }

		public int TreatedTypeID { get; set; }

		public string Treated { get; set; }

		public string Occupation { get; set; }

		public string WorkStreetAddress { get; set; }

		public string WorkCity { get; set; }

		public string WorkState { get; set; }

		public int WorkZipCode { get; set; }

		public string WorkPhoneNumber { get; set; }

		public string EmailAddress { get; set; }
    }
}
