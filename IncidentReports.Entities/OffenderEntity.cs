﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportHq.IncidentReports.Entities
{
	/// <summary>
	/// The base entity class for the Offender object. Used to pass between BAL & DAL.
	/// </summary>
	public class OffenderEntity
    {
		public int ID { get; set; }

		public int ReportID { get; set; }

		public int OffenderNumber { get; set; }

		public int StatusTypeID { get; set; }

		public string Status { get; set; }

		public string LastName { get; set; }

		public string FirstName { get; set; }

		public string Nickname { get; set; }

		public int RaceTypeID { get; set; }

		public string Race { get; set; }

		public int GenderTypeID { get; set; }

		public string Gender { get; set; }

		public DateTime DateOfBirth { get; set; }

		public int Height { get; set; }

		public int Weight { get; set; }

		public string StreetAddress { get; set; }

		public string City { get; set; }

		public string State { get; set; }

		public int ZipCode { get; set; }

		public string SocialSecurityNumber { get; set; }

		public string DriversLicenseNumber { get; set; }

		public int SobrietyTypeID { get; set; }

		public string Sobriety { get; set; }

		public int InjuryTypeID { get; set; }

		public string Injury { get; set; }

		public int TreatedTypeID { get; set; }

		public string Treated { get; set; }

		public int ArrestTypeID { get; set; }

		public string ArrestType { get; set; }

		public string ArrestLocation { get; set; }

		public DateTime ArrestDateTime { get; set; }

		public string ArrestCredit { get; set; }

		public string TransportedBy { get; set; }

		public string TransportUnit { get; set; }

		public int RightsWaivedFormNumber { get; set; }

		public int ResidentTypeID { get; set; }

		public string ResidentType { get; set; }

		public int JuvenileDispositionTypeID { get; set; }

		public string JuvenileDisposition { get; set; }

		public int District { get; set; }

		public string Zone { get; set; }

		public string SubZone { get; set; }

		public string AdditionalDescription { get; set; }

		public List<OffenderChargeEntity> Charges { get; set; }
    }
}
