﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReportHq.IncidentReports.Controls;
using Telerik.Web.UI;

namespace ReportHq.IncidentReports.Tools
{
	public partial class PlotReports : Bases.NormalPage
	{
		#region Variables

		private string _username;
		private const string _RESULTS_KEY = "dtPlotReportsSearch";

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Event Handlers

		protected void Page_Load(object sender, EventArgs e)
		{
			this.BreadCrumbs.Add((new BreadCrumb("Tools", ResolveUrl("~/Tools/"), "to Tools")));
			this.HelpUrl = "~/Tools/Help/PlotReports.html";
			this.Heading = "Plot Reports";

			_username = this.Username;

			if (!IsPostBack)
			{
				//rmtbItemNumber.Focus();
				gridReports.Visible = false;
				
				#region set defaults

				//districts
				rcbDistrict.DataSource = GenericLookups.GetDistricts();
				rcbDistrict.DataBind();
				rcbDistrict.Items.Insert(0, new RadComboBoxItem());

				//platoons
				rcbPlatoon.DataSource = Report.GetPlatoonTypes();
				rcbPlatoon.DataBind();
				rcbPlatoon.Items.Insert(0, new RadComboBoxItem());

				//zones
				rcbZone.DataSource = GenericLookups.GetZones();
				rcbZone.DataBind();
				rcbZone.Items.Insert(0, new RadComboBoxItem());

				//subzones
				rcbSubzone.DataSource = GenericLookups.GetSubZones();
				rcbSubzone.DataBind();
				rcbSubzone.Items.Insert(0, new RadComboBoxItem());

				//default daterange is 3-months
				//rdpStartDate.SelectedDate = DateTime.Now.AddMonths(-3);
				//rdpEndDate.SelectedDate = DateTime.Now;

				//TEMP: just for demos since data is old
				System.Globalization.CultureInfo provider = System.Globalization.CultureInfo.InvariantCulture;
				rdpStartDate.SelectedDate = DateTime.ParseExact("20130701", "yyyyMMdd", provider);
				rdpEndDate.SelectedDate = DateTime.ParseExact("20130801", "yyyyMMdd", provider);

				#endregion
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Fired when grid needs a datasource. eg, after editing or deleting.
		/// </summary>
		protected void gridReports_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
		{
			//when resorting grid or expanding child tables, this page re-ran entire query, which was too slow. now we cache the user's last-results in Session in  whenever search criteria are acted on in btnSubmit_OnClick; it gets used by grid here.

			//use cached version instead of hitting db
			DataTable results = (DataTable)Session[_RESULTS_KEY];

			gridReports.DataSource = results;

			lblResultCount.Visible = true;
			lblResultCount.Text = string.Format("There are <b>{0:N0}</b> results matching your query:<br/><br/>", results.Rows.Count);

			//if (results != null && results.Rows.Count > 0)
			//{
			//	lblResultCount.Visible = true;
			//	lblResultCount.Text = string.Format("There are <b>{0:N0}</b> results matching your query:<br/><br/>", results.Rows.Count);
			//}			

			//divResults.Visible = true;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Fired when grid item's child-table icon is expanded.
		/// </summary>
		protected void gridReports_DetailTableDataBind(object source, GridDetailTableDataBindEventArgs e)
		{
			GridDataItem parentItem = e.DetailTableView.ParentItem as GridDataItem;

			//came from sdk example. if parent is in edit-mode dont update this
			if (parentItem.Edit)
				return;

			int reportID = (int)parentItem.GetDataKeyValue("ReportID");

			DataTable dt = null;

			//get appropiate child-table data
			switch (e.DetailTableView.Name)
			{
				case "persons":
					dt = VictimPerson.GetVictimPersonsByReport(reportID);
					break;

				case "offenders":
					dt = Offender.GetOffendersByReport(reportID);
					break;
			}

			e.DetailTableView.DataSource = dt;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void lbSubmit_Click(object sender, EventArgs e)
		{
			//when resorting grid or expanding child tables, this page re-ran entire query, which was too slow. now we cache the user's last-results in Session here whenever search criteria are acted on; it gets used by grid in gridResults_NeedDataSource.

			Session[_RESULTS_KEY] = null;
			Session[_RESULTS_KEY] = GetData();

			gridReports.Rebind();
			gridReports.Visible = true;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Private Methods

		private DataTable GetData()
		{
			string itemNumber = string.Empty;
			int district = -1;
			string zone = string.Empty;
			string subzone = string.Empty;
			int startCarNumber = -1;
			int endCarNumber = -1;
			string platoonType = string.Empty;
			int badgeNumber = -1;
			DateTime startDate = new DateTime(1900, 1, 1);
			DateTime endDate = new DateTime(1900, 1, 1);


			//get form values
			if (!string.IsNullOrEmpty(rmtbItemNumber.Text))
				itemNumber = rmtbItemNumber.TextWithLiterals;

			if (!string.IsNullOrEmpty(rcbDistrict.SelectedValue))
				district = Int32.Parse(rcbDistrict.SelectedValue);

			if (!string.IsNullOrEmpty(rcbZone.SelectedValue))
				zone = rcbZone.SelectedValue;

			if (!string.IsNullOrEmpty(rcbSubzone.SelectedValue))
				subzone = rcbSubzone.SelectedValue;

			if (!string.IsNullOrEmpty(rcbPlatoon.SelectedValue))
				platoonType = rcbPlatoon.SelectedValue;

			if (rntbStartCarNumber.Value > 0)
				startCarNumber = Convert.ToInt32(rntbStartCarNumber.Value);

			if (rntbEndCarNumber.Value > 0)
				endCarNumber = Convert.ToInt32(rntbEndCarNumber.Value);

			if (rdpStartDate.SelectedDate != null)
				startDate = (DateTime)rdpStartDate.SelectedDate;

			if (rdpEndDate.SelectedDate != null)
				endDate = (DateTime)rdpEndDate.SelectedDate;

			if (!string.IsNullOrEmpty(rntbBadgeNumber.Text))
				badgeNumber = Convert.ToInt32(rntbBadgeNumber.Text);

			//do it
			return Report.GetPlotReportsByCriteria(itemNumber,
												   district,
												   zone,
												   subzone,
												   platoonType,
												   startCarNumber,
												   endCarNumber,
												   startDate,
												   endDate,
												   badgeNumber);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}