﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReportHq.IncidentReports.Controls;
using Telerik.Web.UI;

namespace ReportHq.IncidentReports.Tools
{
	public partial class FindReports : Bases.NormalPage
	{
		#region Variables

		private string _username;
		private const string _RESULTS_KEY = "dtReportsSearch";

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Event Handlers

		protected void Page_Load(object sender, EventArgs e)
		{
			this.BreadCrumbs.Add((new BreadCrumb("Tools", ResolveUrl("~/Tools/"), "to Tools")));
			this.HelpUrl = "~/Tools/Help/FindReports.html";
			this.Heading = "Find Reports";

			_username = this.Username;

			if (!IsPostBack)
			{
				//rmtbItemNumber.Focus();
				
				#region set defaults

				//districts
				rcbDistrict.DataSource = GenericLookups.GetDistricts();
				rcbDistrict.DataBind();
				rcbDistrict.Items.Insert(0, new RadComboBoxItem());

				//platoons
				rcbPlatoons.DataSource = Report.GetPlatoonTypes();
				rcbPlatoons.DataBind();
				rcbPlatoons.Items.Insert(0, new RadComboBoxItem());

				//default daterange is 1-year
				rdpStartDate.SelectedDate = DateTime.Now.AddYears(-1);
				rdpEndDate.SelectedDate = DateTime.Now;

				#endregion
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Fired when grid needs a datasource. eg, after editing or deleting.
		/// </summary>
		protected void gridReports_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
		{
			//when resorting grid or expanding child tables, this page re-ran entire query, which was too slow. now we cache the user's last-results in Session in  whenever search criteria are acted on in btnSubmit_OnClick; it gets used by grid here.

			//use cached version instead of hitting db
			DataTable results = (DataTable)Session[_RESULTS_KEY];

			gridReports.DataSource = results;

			lblResultCount.Text = string.Format("There are <b>{0:N0}</b> results matching your query:<br/><br/>", results.Rows.Count);

			divResults.Visible = true;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Fired when grid item's child-table icon is expanded.
		/// </summary>
		protected void gridReports_DetailTableDataBind(object source, GridDetailTableDataBindEventArgs e)
		{
			GridDataItem parentItem = e.DetailTableView.ParentItem as GridDataItem;

			//came from sdk example. if parent is in edit-mode dont update this
			if (parentItem.Edit)
				return;

			int reportID = (int)parentItem.GetDataKeyValue("ReportID");

			DataTable dt = null;

			//get appropiate child-table data
			switch (e.DetailTableView.Name)
			{
				case "persons":
					dt = VictimPerson.GetVictimPersonsByReport(reportID);
					break;

				case "offenders":
					dt = Offender.GetOffendersByReport(reportID);
					break;
			}

			e.DetailTableView.DataSource = dt;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void lbSubmit_Click(object sender, EventArgs e)
		{
			//when resorting grid or expanding child tables, this page re-ran entire query, which was too slow. now we cache the user's last-results in Session here whenever search criteria are acted on; it gets used by grid in gridResults_NeedDataSource.

			Session[_RESULTS_KEY] = null;
			Session[_RESULTS_KEY] = GetData();

			gridReports.Rebind();
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Private Methods

		private DataTable GetData()
		{
			string itemNumber = string.Empty;
			int district = -1;
			int startCarNumber = -1;
			int endCarNumber = -1;
			string platoonType = string.Empty;
			int badgeNumber = -1;
			DateTime startDate = new DateTime(1900, 1, 1);
			DateTime endDate = new DateTime(1900, 1, 1);


			//get form values

			if (!string.IsNullOrEmpty(rmtbItemNumber.Text))
				itemNumber = rmtbItemNumber.TextWithLiterals;

			if (!string.IsNullOrEmpty(rcbDistrict.SelectedValue))
				district = Int32.Parse(rcbDistrict.SelectedValue);

			if (!string.IsNullOrEmpty(rcbPlatoons.SelectedValue))
				platoonType = rcbPlatoons.SelectedValue;

			if (rntbStartCarNumber.Value > 0)
				startCarNumber = Convert.ToInt32(rntbStartCarNumber.Value);

			if (rntbEndCarNumber.Value > 0)
				endCarNumber = Convert.ToInt32(rntbEndCarNumber.Value);

			if (rdpStartDate.SelectedDate != null)
				startDate = (DateTime)rdpStartDate.SelectedDate;

			if (rdpEndDate.SelectedDate != null)
				endDate = (DateTime)rdpEndDate.SelectedDate;

			if (!string.IsNullOrEmpty(rntbBadgeNumber.Text))
				badgeNumber = Convert.ToInt32(rntbBadgeNumber.Text);

			//do it
			return Report.GetReportsByCriteria(itemNumber,
											   -1,
											   district,
											   platoonType,
											   startCarNumber,
											   endCarNumber,
											   startDate,
											   endDate,
											   badgeNumber);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}