﻿<%@ Page Title="Find Offenders" Language="C#" MasterPageFile="~/IncidentReports.Master" AutoEventWireup="true" CodeBehind="FindOffenders.aspx.cs" Inherits="ReportHq.IncidentReports.Tools.FindOffenders" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<link rel="stylesheet" href="../css/boxes.css" type="text/css" />
	
	<script type="text/javascript" src="../js/radComboBoxHelpers.js"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="phContent" runat="server">

	<style type="text/css">
		.itemBox {
			float: left;
			margin-right:10px;
			margin-bottom:10px;
			min-width:155px;
			min-height:245px;
			background-color:#dddce1;
			padding:4px;
			padding-right:8px;
			border-radius:4px;
		}
	</style>

	<telerik:RadFormDecorator ID="radFormDecorator" DecoratedControls="CheckBoxes" Skin="Metro" runat="server" />
	
	<dl class="box search" style="width:700px; margin-bottom:30px;">
		<dt><span class="label">SEARCH CRITERIA</span></dt>
		<dd>

			<table border="0">
				<tr>
					<td class="label">Race:</td>
					<td><telerik:RadComboBox ID="rcbRace" DataValueField="RaceTypeID" DataTextField="Description" Width="100" ShowToggleImage="true" AllowCustomText="False" MarkFirstMatch="true" OnClientFocus="showDropDown" TabIndex="1" runat="server" /></td>
					<td style="width:25px;"></td>
					<td class="label">Height:</td>
					<td><telerik:RadMaskedTextBox ID="rmtbHeight" Mask="#'##" Width="50" TabIndex="4" runat="server" /></td>
					<td style="width: 25px;"></td>
					<td class="label">First Name:</td>
					<td><asp:TextBox ID="tbFirstName" Width="96" TabIndex="7" runat="server" /></td>
				</tr>
				<tr>
					<td class="label">Sex:</td>
					<td><telerik:RadComboBox ID="rcbGender" DataValueField="GenderTypeID" DataTextField="Description" Width="100" ShowToggleImage="true" AllowCustomText="False" MarkFirstMatch="true" TabIndex="2" runat="server" /></td>
					<td></td>
					<td class="label">Weight:</td>
					<td><telerik:RadNumericTextBox ID="rntbWeight" Type="Number" MaxLength="5" NumberFormat-DecimalDigits="0" NumberFormat-GroupSeparator="" Width="50" TabIndex="5" runat="server" /></td>
					<td></td>					
					<td class="label">Last Name:</td>
					<td><asp:TextBox ID="tbLastName" Width="96" TabIndex="8" runat="server" /></td>
				</tr>
				<tr>
					<td class="label">Drivers Lic #:</td>
					<td><asp:TextBox ID="tbDriversLicense" Width="96" TabIndex="3" runat="server" /></td>
					<td></td>
					<td class="label">SSN:</td>
					<td><telerik:RadMaskedTextBox ID="rmtbSsn" Mask="###-##-####" Width="100" TabIndex="6" runat="server" /></td>
					<td></td>
					<td class="label">Nickname:</td>
					<td><asp:TextBox ID="tbNickname" Width="96" TabIndex="9" runat="server" /></td>
				</tr>
			</table><br/>

			<asp:LinkButton ID="lbSubmit" CssClass="flatButton" OnClick="lbSubmit_Click" TabIndex="10" runat="server">
				<i class="fa fa-search fa-fw"></i> Find Offenders
			</asp:LinkButton>

		</dd>
	</dl>

	
	<dl id="TestPanel" class="box search collapsed" style="width:100%; margin-bottom: 30px;">
		<dt onclick="toggleCollapseState('#DescriptorsPanel');">
			<span class="label">DESCRIPTORS</span> <i class="fa fa-chevron-circle-down"></i>
		</dt>

		<dd id="DescriptorsPanel">

			<div class="itemBox">
				<div class="checkBoxListLabel">Hair Color</div>
				<asp:CheckBoxList ID="cblHairColors" DataValueField="OffenderDescriptorTypeID" DataTextField="Description" RepeatLayout="Flow" runat="server" />
			</div>
			<div class="itemBox">
				<div class="checkBoxListLabel">Eyes</div>
				<asp:CheckBoxList ID="cblEyes" DataValueField="OffenderDescriptorTypeID" DataTextField="Description" RepeatLayout="Flow" runat="server" />
			</div>
			<div class="itemBox">
				<div class="checkBoxListLabel">Build</div>
				<asp:CheckBoxList ID="cblBuilds" DataValueField="OffenderDescriptorTypeID" DataTextField="Description" RepeatLayout="Flow" runat="server" />
			</div>
			<div class="itemBox">
				<div class="checkBoxListLabel">Complexion</div>
				<asp:CheckBoxList ID="cblComplexion" DataValueField="OffenderDescriptorTypeID" DataTextField="Description" RepeatLayout="Flow" runat="server" />
			</div>

			<div class="itemBox">
				<div class="checkBoxListLabel">Hair Style</div>
				<asp:CheckBoxList ID="cblHairStyles" DataValueField="OffenderDescriptorTypeID" DataTextField="Description" RepeatLayout="Flow" runat="server" />
			</div>
			<div class="itemBox">
				<div class="checkBoxListLabel">Facial Hair</div>
				<asp:CheckBoxList ID="cblFacialHair" DataValueField="OffenderDescriptorTypeID" DataTextField="Description" RepeatLayout="Flow" runat="server" />
			</div>
			<div class="itemBox">
				<div class="checkBoxListLabel">Tattoos</div>
				<asp:CheckBoxList ID="cblTattoos" DataValueField="OffenderDescriptorTypeID" DataTextField="Description" RepeatLayout="Flow" runat="server" />
			</div>
			<div class="itemBox">
				<div class="checkBoxListLabel">Apparel</div>
				<asp:CheckBoxList ID="cblApparel" DataValueField="OffenderDescriptorTypeID" DataTextField="Description" RepeatLayout="Flow" runat="server" />
			</div>

			<div class="itemBox">
				<div class="checkBoxListLabel">Nose</div>
				<asp:CheckBoxList ID="cblNose" DataValueField="OffenderDescriptorTypeID" DataTextField="Description" RepeatLayout="Flow" runat="server" />
			</div>
			<div class="itemBox">
				<div class="checkBoxListLabel">Teeth</div>
				<asp:CheckBoxList ID="cblTeeth" DataValueField="OffenderDescriptorTypeID" DataTextField="Description" RepeatLayout="Flow" runat="server" />
			</div>
			<div class="itemBox">
				<div class="checkBoxListLabel">Speech</div>
				<asp:CheckBoxList ID="cblSpeech" DataValueField="OffenderDescriptorTypeID" DataTextField="Description" RepeatLayout="Flow" runat="server" />
			</div>
			<div class="itemBox">
				<div class="checkBoxListLabel">Accent</div>
				<asp:CheckBoxList ID="cblAccent" DataValueField="OffenderDescriptorTypeID" DataTextField="Description" RepeatLayout="Flow" runat="server" />
			</div>

			<div class="itemBox">
				<div class="checkBoxListLabel">Scars</div>
				<asp:CheckBoxList ID="cblScars" DataValueField="OffenderDescriptorTypeID" DataTextField="Description" RepeatColumns="2" CellPadding="0" CellSpacing="0" Width="100%" runat="server" />
			</div>
			<div class="itemBox">
				<div class="checkBoxListLabel">Oddities</div>
				<asp:CheckBoxList ID="cblOddities" DataValueField="OffenderDescriptorTypeID" DataTextField="Description" RepeatLayout="Flow" runat="server" />
			</div>
			<div class="itemBox">
				<div class="checkBoxListLabel">Facial Oddities</div>
				<asp:CheckBoxList ID="cblFacialOddities" DataValueField="OffenderDescriptorTypeID" DataTextField="Description" RepeatLayout="Flow" runat="server" />
			</div>

			<div style="clear: both;">&nbsp;</div>
		
			<asp:LinkButton ID="lbSubmit2" CssClass="flatButton" OnClick="lbSubmit_Click" runat="server">
				<i class="fa fa-search fa-fw"></i> Find Offenders
			</asp:LinkButton>

		</dd>
	</dl>
	

	<div id="divResults" visible="false" runat="server">

		<asp:Label ID="lblResultCount" runat="server" />

		<telerik:RadAjaxPanel ID="updResults" LoadingPanelID="ajaxLoadingPanel" ClientEvents-OnRequestStart="onRequestStart" runat="server">
					
			<telerik:RadGrid ID="gridOffenders"
				Width="95%"
				AutoGenerateColumns="False"
				AllowPaging="true"
				PageSize="25"
				AllowSorting="true"
				ShowGroupPanel="false"
				AllowFilteringByColumn="false"
				AllowMultiRowSelection="false"
				AllowMultiRowEdit="false"
				EnableHeaderContextMenu="true"
				OnNeedDataSource="gridOffenders_NeedDataSource"
				OnItemDataBound="gridOffenders_ItemDataBound"
				runat="server">

				<ClientSettings 
					EnableRowHoverStyle="true"
					AllowDragToGroup="false" 
					AllowColumnsReorder="false" 
					AllowKeyboardNavigation="true"
					ReorderColumnsOnClient="false" >
					<Resizing AllowColumnResize="false" AllowRowResize="false" />
					<Selecting AllowRowSelect="false" EnableDragToSelectRows="false" />
				</ClientSettings>

				<ExportSettings FileName="offenders" ExportOnlyData="true" IgnorePaging="true" HideStructureColumns="true" OpenInNewWindow="true">
					<Excel Format="ExcelML"></Excel>
				</ExportSettings>
			
				<MasterTableView 
					CommandItemDisplay="Bottom"
					DataKeyNames="ReportID" 
					NoMasterRecordsText="No Offenders to display." 
					CommandItemStyle-Font-Bold="true" 
					HeaderStyle-ForeColor="#191970" 
					ItemStyle-CssClass="item" 
					AlternatingItemStyle-CssClass="item">
				
					<PagerStyle PageSizes="25,50,100" Position="TopAndBottom" />
					<CommandItemSettings ShowRefreshButton="false" ShowAddNewRecordButton="false" ShowExportToExcelButton="true" />
				
					<Columns>				
						<Telerik:GridTemplateColumn UniqueName="PdfIcon" ItemStyle-HorizontalAlign="center">
							<ItemTemplate>
								<a href='../print/ReportPdf.aspx?rid=<%# Eval("ReportID") %>'><img src="../images/pdf.gif" width="16" height="16" border="0" alt="View PDF" /></a>
							</ItemTemplate>
						</Telerik:GridTemplateColumn>
						<Telerik:GridHyperLinkColumn UniqueName="View" DataNavigateUrlFields="ReportID" DataNavigateUrlFormatString="../People/Offenders.aspx?rid={0}" DataTextField="ReportID" DataTextFormatString="View" ItemStyle-HorizontalAlign="center" ItemStyle-Wrap="false" ItemStyle-CssClass="commandItem" />
						<Telerik:GridBoundColumn DataField="ReportID" Visible="false" />
						<Telerik:GridBoundColumn DataField="LastName" UniqueName="LastName" HeaderText="Last Name" ItemStyle-Wrap="false" />
						<Telerik:GridBoundColumn DataField="FirstName" UniqueName="FirstName" HeaderText="First Name" ItemStyle-Wrap="false" />
						<Telerik:GridBoundColumn DataField="Nickname" UniqueName="Nickname" HeaderText="Nickname" ItemStyle-Wrap="false" />
						<Telerik:GridBoundColumn DataField="RaceType" UniqueName="RaceType" HeaderText="Race" />
						<Telerik:GridBoundColumn DataField="GenderType" UniqueName="GenderType" HeaderText="Gender" />						
						<Telerik:GridBoundColumn DataField="Height" UniqueName="Height" HeaderText="Height" />
						<Telerik:GridBoundColumn DataField="Weight" UniqueName="Weight" HeaderText="Weight" />
						<Telerik:GridBoundColumn DataField="DateOfBirth" UniqueName="DateOfBirth" HeaderText="DOB" DataFormatString="{0:MM/dd/yyyy}" />
						<Telerik:GridBoundColumn DataField="SocialSecurityNumber" UniqueName="SocialSecurityNumber" DataFormatString="{0:000-00-0000}" HeaderText="SSN" />						
					</Columns>
				
				</MasterTableView>
			</Telerik:RadGrid>

		</telerik:RadAjaxPanel>
				
	</div>
	
</asp:Content>
