﻿<%@ Page Title="Plot Reports" Language="C#" MasterPageFile="~/IncidentReports.Master" AutoEventWireup="true" CodeBehind="PlotReports.aspx.cs" Inherits="ReportHq.IncidentReports.Tools.PlotReports" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<link rel="stylesheet" href="../css/boxes.css" type="text/css" />
	<script type="text/javascript" src="../js/radComboBoxHelpers.js"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="phContent" runat="server">
	
	<style>
		.RadMap.MyMap .k-widget.k-attribution { display:none; }
		.MyMap { border: 1px solid #DBE3E4; }
		.MyMap, .RadMap .km-scroll-wrapper { 
			border-radius: 10px; 
			/* fix website corner radius bug: */
			outline:none;
			-webkit-mask-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAIAAACQd1PeAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAA5JREFUeNpiYGBgAAgwAAAEAAGbA+oJAAAAAElFTkSuQmCC);
		}
		.tipLink {
			color:red; font-weight:bold;
		}
	</style>

	<!-- used by multiple RadDatePickers -->
	<telerik:RadCalendar ID="sharedCalendar" ShowRowHeaders="false" UseColumnHeadersAsSelectors="false" runat="server" />

	<dl id="TestPanel" class="box search" style="width: 1000px; margin-bottom: 30px;">
		<dt onclick="toggleCollapseState('#CriteriaPanel');">
			<span class="label">SEARCH CRITERIA</span> <i class="fa fa-chevron-circle-up"></i>
		</dt>
		<dd id="CriteriaPanel">

			<table border="0">
				<tr>
					<td class="label">Item Number:</td>
					<td>
						<Telerik:RadMaskedTextBox id="rmtbItemNumber" Mask="L-#####-##" ClientEvents-OnBlur="padNumberSection" Width="85" TabIndex="1" runat="server" />
						<asp:RegularExpressionValidator 
							id="revItemNumber" 
							ControlToValidate="rmtbItemNumber" 
							ErrorMessage="Invalid" 
							ValidationExpression="[A-L]-[0-9]{5}-[0-9]{2}" 
							Display="Dynamic" CssClass="error"
							runat="server" />
					</td>

					<td style="width: 25px;"></td>
					<td class="label">Zone:</td>
					<td><telerik:RadComboBox ID="rcbZone" DataValueField="Zone" DataTextField="Zone" Width="85" AllowCustomText="False" MarkFirstMatch="true" TabIndex="3" runat="server" /></td>

					<td style="width:25px;"></td>
					<td class="label">Platoon:</td>
					<td><telerik:RadComboBox ID="rcbPlatoon" DataValueField="PlatoonType" DataTextField="PlatoonType" Width="75" MarkFirstMatch="true" OnClientFocus="showDropDown" TabIndex="5" runat="server" /></td>

					<td style="width: 25px;"></td>
					<td class="label">Car # Between:</td>
					<td nowrap="true"><telerik:RadNumericTextBox ID="rntbStartCarNumber" Type="Number" MinValue="1" MaxValue="10999" MaxLength="5" NumberFormat-DecimalDigits="0" NumberFormat-GroupSeparator="" Width="75" TabIndex="7" runat="server" />&nbsp;and &nbsp;<telerik:RadNumericTextBox ID="rntbEndCarNumber" Type="Number" MinValue="1" MaxValue="10999" MaxLength="5" NumberFormat-DecimalDigits="0" NumberFormat-GroupSeparator="" Width="75" TabIndex="8" runat="server" /></td>

				</tr>


				<tr>
					<td class="label">District:</td>
					<td><telerik:RadComboBox ID="rcbDistrict" DataValueField="District" DataTextField="District" Width="85" AllowCustomText="False" MarkFirstMatch="true" TabIndex="2" runat="server" /></td>

					<td></td>
					<td class="label">Subzone:</td>
					<td><telerik:RadComboBox ID="rcbSubzone" DataValueField="SubZone" DataTextField="SubZone" Width="85" MarkFirstMatch="true" OnClientFocus="showDropDown" TabIndex="4" runat="server" /></td>

					<td></td>
					<td class="label">Badge Number:</td>
					<td><telerik:RadNumericTextBox ID="rntbBadgeNumber" Type="Number" MaxLength="5" NumberFormat-DecimalDigits="0" NumberFormat-GroupSeparator="" Width="75" TabIndex="6" runat="server" /></td>

					<td></td>
					<td class="label">Dates Between:</td>
					<td nowrap="true"><telerik:RadDatePicker ID="rdpStartDate" Width="101" SharedCalendarID="sharedCalendar" TabIndex="9" runat="server" />&nbsp;and &nbsp;<telerik:RadDatePicker ID="rdpEndDate" Width="101" SharedCalendarID="sharedCalendar" TabIndex="10" runat="server" />
					</td>
				</tr>

			</table><br/>

			<asp:LinkButton ID="lbSubmit" CssClass="flatButton" OnClick="lbSubmit_Click" runat="server"><i class="fa fa-search fa-fw"></i> Plot Reports</asp:LinkButton>

		</dd>
	</dl>

	<asp:Label ID="lblResultCount" Visible="false" runat="server" />

	<!-- TODO: replace this w/ css layout. try display:table and cells -->
	<table width="100%" cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td width="49%" valign="top">

				<telerik:RadMap ID="radMap" Zoom="12" Height="654" CssClass="MyMap" runat="server">
					<CenterSettings Latitude="29.9667" Longitude="-90.0500" />
					<LayersCollection>
						<telerik:MapLayer Type="Tile" UrlTemplate="http://a.tile.opencyclemap.org/transport/#= zoom #/#= x #/#= y #.png" />
					</LayersCollection>

					<MarkerDefaultsSettings Shape="pinTarget">
						<TooltipSettings Template="<a href='../Event/BasicInfo.aspx?rid=#= marker.options.info.ID #' class='tipLink'>#= marker.options.info.itemNumber #</a><br/> #= marker.options.info.status #<br/> #= marker.options.info.signal #<br/> #= marker.options.info.reportDate #<br/> #= marker.options.info.officer #<br/>">
							<AnimationSettings>
							   <OpenSettings Duration="150" Effects="fade:in" />
							   <CloseSettings Duration="150" Effects="fade:out" />
						  </AnimationSettings>
						</TooltipSettings>
					</MarkerDefaultsSettings>
				</telerik:RadMap>

			</td>
			<td width="2%">&nbsp;</td>
			<td width="49%" valign="top">

				<telerik:RadAjaxPanel ID="updResults" LoadingPanelID="ajaxLoadingPanel" ClientEvents-OnRequestStart="onRequestStart" runat="server">
					<telerik:RadGrid ID="gridReports"
						AutoGenerateColumns="False"
						AllowPaging="true"
						PageSize="25"
						AllowSorting="true"
						AllowMultiRowSelection="false"
						AllowMultiRowEdit="false"
						EnableHeaderContextMenu="true"
						OnNeedDataSource="gridReports_NeedDataSource"
						OnDetailTableDataBind="gridReports_DetailTableDataBind"
						ClientSettings-ClientEvents-OnGridCreated="GridCreated"
						ClientSettings-ClientEvents-OnRowMouseOver="ShowMarkerTooltip"
						runat="server">

						<ClientSettings 
							EnableRowHoverStyle="true"
							AllowDragToGroup="false" 
							AllowColumnsReorder="false" 
							AllowKeyboardNavigation="true"
							ReorderColumnsOnClient="false" >
							<Resizing AllowColumnResize="false" AllowRowResize="false" />
							<Selecting AllowRowSelect="false" EnableDragToSelectRows="false" />
						</ClientSettings>

						<ExportSettings FileName="plottedReports" ExportOnlyData="true" IgnorePaging="true" HideStructureColumns="true" OpenInNewWindow="true">
							<Excel Format="ExcelML"></Excel>
						</ExportSettings>

						<MasterTableView
							CommandItemDisplay="Bottom"
							DataKeyNames="ReportID"
							NoMasterRecordsText="No Reports to display."
							CommandItemStyle-Font-Bold="true"
							HeaderStyle-ForeColor="#191970"
							ItemStyle-CssClass="item"
							AlternatingItemStyle-CssClass="item">
				
							<PagerStyle PageSizes="25,50,100" Position="TopAndBottom" />
							<CommandItemSettings ShowRefreshButton="false" ShowAddNewRecordButton="false" ShowExportToExcelButton="true" />
				
							<Columns>				
								<Telerik:GridTemplateColumn UniqueName="PdfIcon" AllowFiltering="false" ItemStyle-HorizontalAlign="center">
									<ItemTemplate>
										<a href='../print/ReportPdf.aspx?rid=<%# Eval("ReportID") %>'><img src="../images/pdf.gif" width="16" height="16" border="0" alt="View PDF" /></a>
									</ItemTemplate>
								</Telerik:GridTemplateColumn>
								<Telerik:GridHyperLinkColumn UniqueName="ViewReport" DataNavigateUrlFields="ReportID" DataNavigateUrlFormatString="../Event/BasicInfo.aspx?rid={0}" DataTextField="ReportID" DataTextFormatString="View" AllowFiltering="false" ItemStyle-HorizontalAlign="center" ItemStyle-Wrap="false" ItemStyle-CssClass="commandItem" />
								<Telerik:GridBoundColumn DataField="ReportID" Display="false" />
								<Telerik:GridBoundColumn HeaderText="Item #" UniqueName="ItemNumber" DataField="ItemNumber" ShowFilterIcon="false" ItemStyle-Wrap="false" />
								<Telerik:GridBoundColumn HeaderText="Type" UniqueName="ReportType" DataField="ReportType" ItemStyle-Wrap="false" />
								<Telerik:GridBoundColumn HeaderText="Status" UniqueName="Status" DataField="ApprovalStatus" ItemStyle-Wrap="false" />
								<Telerik:GridBoundColumn HeaderText="Signal" UniqueName="Signal" DataField="Signal" HeaderStyle-Wrap="false" />
								<Telerik:GridBoundColumn HeaderText="Report Date" UniqueName="ReportDate" DataField="ReportDate" DataFormatString="{0:MM/dd/yyyy}" HeaderStyle-Wrap="false" />
								<Telerik:GridBoundColumn HeaderText="Officer1Name" UniqueName="Officer1Name" DataField="Officer1Name" Display="false" />
								<Telerik:GridBoundColumn HeaderText="District" UniqueName="District" DataField="District" HeaderStyle-HorizontalAlign="center" HeaderStyle-Wrap="false" ItemStyle-HorizontalAlign="center" />
								<telerik:GridBoundColumn HeaderText="Zone" UniqueName="Zone" DataField="Zone" HeaderStyle-HorizontalAlign="center" HeaderStyle-Wrap="false" ItemStyle-HorizontalAlign="center" />
								<telerik:GridBoundColumn HeaderText="Subzone" UniqueName="Subzone" DataField="SubZone" HeaderStyle-HorizontalAlign="center" HeaderStyle-Wrap="false" ItemStyle-HorizontalAlign="center" />
								<Telerik:GridBoundColumn HeaderText="Location" UniqueName="Location" DataField="Location" Display="false" />
							</Columns>
				
							<DetailTables>
		
								<%-- PERSONS --%>
								<Telerik:GridTableView 
									Name="persons" 
									DataKeyNames="VictimPersonID" 
									AutoGenerateColumns="false" 
									AllowSorting="false" 
									Caption="VICTIM/PERSONS" 
									Width="100%" 
									NoDetailRecordsText="No Victims/Persons to display">
						
									<ParentTableRelation>
										<Telerik:GridRelationFields DetailKeyField="ReportID" MasterKeyField="ReportID" />
									</ParentTableRelation>
						
									<Columns>
										<Telerik:GridBoundColumn HeaderText="ReportID" DataField="ReportID" Visible="false" />
										<Telerik:GridBoundColumn HeaderText="PersonID" DataField="PersonID" Visible="false" />
										<Telerik:GridBoundColumn HeaderText="Victim #" DataField="VictimNumber" ItemStyle-Width="70" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="center" />
										<Telerik:GridBoundColumn HeaderText="Last Name" DataField="LastName" />
										<Telerik:GridBoundColumn HeaderText="First Name" DataField="FirstName" />
										<Telerik:GridBoundColumn HeaderText="Race" DataField="Race" />
										<Telerik:GridBoundColumn HeaderText="Gender" DataField="Gender" />
										<Telerik:GridBoundColumn HeaderText="DOB" DataField="DateOfBirth" DataFormatString="{0:MM/dd/yyyy}" />
									</Columns>
								</Telerik:GridTableView>
					
								<%-- OFFENDERS --%>
								<Telerik:GridTableView 
									Name="offenders" 
									DataKeyNames="OffenderID" 
									AutoGenerateColumns="false" 
									AllowSorting="false" 
									Caption="OFFENDERS" 
									Width="100%" 
									NoDetailRecordsText="No Offenders to display" 
									CssClass="setMargin">
						
									<ParentTableRelation>
										<Telerik:GridRelationFields DetailKeyField="ReportID" MasterKeyField="ReportID" />
									</ParentTableRelation>
									<Columns>
										<Telerik:GridBoundColumn HeaderText="ReportID" DataField="ReportID" Visible="false" />
										<Telerik:GridBoundColumn HeaderText="OffenderID" DataField="OffenderID" Visible="false" />
										<Telerik:GridBoundColumn HeaderText="Offender #" DataField="OffenderNumber" ItemStyle-Width="70" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="center" />
										<Telerik:GridBoundColumn HeaderText="Last Name" DataField="LastName" />
										<Telerik:GridBoundColumn HeaderText="First Name" DataField="FirstName" />
										<Telerik:GridBoundColumn HeaderText="Race" DataField="Race" />
										<Telerik:GridBoundColumn HeaderText="Gender" DataField="Gender" />
										<Telerik:GridBoundColumn HeaderText="DOB " DataField="DateOfBirth" DataFormatString="{0:MM/dd/yyyy}" />
									</Columns>
								</Telerik:GridTableView>

							</DetailTables>
				
						</MasterTableView>
					</Telerik:RadGrid>
				</Telerik:RadAjaxPanel>
			
			</td>
		</tr>
	</table>

	
	
	<telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">

		<script type="text/javascript">
			var grid;
			var map;
			var key = 'Fmjtd%7Cluur256ynd%2C82%3Do5-9w72h6'; //issued by MapQuest
		
			function GridCreated(sender, eventArgs)
			{
				grid = $find("<%=gridReports.ClientID %>");

				//set RadMap
				map = $find("<%=radMap.ClientID %>").kendoWidget; //the Kendo widget that RadMap is based on
				map.markers.clear();
					
				//set grid data
				var masterTableView = sender.get_masterTableView();
				var dataItems = masterTableView.get_dataItems();
				var item;

				//loop grid data
				for (var i = 0; i < dataItems.length; i++) {
					item = dataItems[i];
					CreateMapMarker(item.get_cell('Location').innerHTML, item.get_cell('ReportID').innerHTML, item.get_cell('ItemNumber').innerHTML, item.get_cell('Status').innerHTML, item.get_cell('Signal').innerHTML, item.get_cell('ReportDate').innerHTML, item.get_cell('Officer1Name').innerHTML);
				}
			}


			function CreateMapMarker(location, reportID, itemNumber, status, signal, reportDate, officer)
			{
				var mapQuestUri = 'http://open.mapquestapi.com/geocoding/v1/address?key=' + key + '&inFormat=kvp&outFormat=json&maxResults=1&thumbMaps=false&location=' + location; //append incident location to uri

				//make ajax request to uri
				$.getJSON(mapQuestUri)

					.done(function (data) {
						//alert('got data'); //success; 'data' now contains JSON-formatted geodata
				
						$.each(data, function (key, item) { //"results", "options", "info"
							if (key == 'results') {
								var objCoords = item[0].locations[0].latLng; //this is where MapQuest stores it in its JSON schema: "results[0].location[0].latLng"

								//TODO: save coords for this record to SQL. then, return coords for record from SQL proc. then, in GridCreated, when looping grid data, check for existing value first, then hit MapQuest only if needed 

								map.markers.add({
									location: [objCoords.lat, objCoords.lng],
									info: {
										ID: reportID,
										itemNumber: itemNumber,
										status: status,
										signal: signal,
										reportDate: reportDate,
										officer: officer
									}
								});
							}
						});					
					})

					.fail(function () {
						alert('request failed');
					})
				//.always(function () { alert('getJSON request ended'); })
			}


			function ShowMarkerTooltip(sender, eventArgs)
			{			
				var index = eventArgs.get_itemIndexHierarchical();			
				var gridRow = grid.get_masterTableView().get_dataItems()[index];
				var cell = grid.MasterTableView.getCellByColumnUniqueName(gridRow, "ReportID");
				var reportID = cell.innerHTML;
				
				var markers = map.markers.items;

				//hide any open
				for (var i = 0; i < markers.length; i++) {
					markers[i].tooltip.hide();					
				}
			
				//show desried
				for (var i = 0; i < markers.length; i++) {
					var markerId = markers[i].options.info.ID;

					if (markerId == reportID) {
						markers[i].tooltip.show();
						break;
					}
				}
			}
		</script>

	</telerik:RadCodeBlock>
	
	
</asp:Content>
