﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReportHq.Common;
using ReportHq.IncidentReports.Controls;
using Telerik.Web.UI;

namespace ReportHq.IncidentReports.Tools
{
	public partial class FindOffenders : Bases.NormalPage
	{
		#region Variables

		private string _username;
		private const string _RESULTS_KEY = "dtOffendersSearch";

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Event Handlers

		protected void Page_Load(object sender, EventArgs e)
		{
			this.BreadCrumbs.Add((new BreadCrumb("Tools", ResolveUrl("~/Tools/"), "to Tools")));
			this.HelpUrl = "~/Tools/Help/FindOffenders.html";
			this.Heading = "Find Offenders";
			
			_username = this.Username;

			//BindFormLists();

			if (!IsPostBack)
			{
				//rcbRace.Focus();
				
				#region set defaults

				//race
				rcbRace.DataSource = GenericLookups.GetPersonRaces();
				rcbRace.DataBind();
				rcbRace.Items.Insert(0, new RadComboBoxItem());

				//gender
				rcbGender.DataSource = GenericLookups.GetPersonGenders();
				rcbGender.DataBind();
				rcbGender.Items.Insert(0, new RadComboBoxItem());

				#endregion

				BindFormLists();
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Fired when grid needs a datasource. eg, after editing or deleting.
		/// </summary>
		protected void gridOffenders_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
		{
			//cache the user's last-results in Session in whenever search criteria are acted on in btnSubmit_OnClick; it gets used by grid here for sorts, etc

			//use cached version instead of hitting db
			DataTable results = (DataTable)Session[_RESULTS_KEY];

			gridOffenders.DataSource = results;

			lblResultCount.Text = string.Format("There are <b>{0:N0}</b> results matching your query:<br/><br/>", results.Rows.Count);

			divResults.Visible = true;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void gridOffenders_ItemDataBound(object sender, GridItemEventArgs e)
		{
			if (e.Item is GridDataItem)
			{
				GridDataItem dataBoundItem = e.Item as GridDataItem;

				if (dataBoundItem["Height"].Text != "&nbsp;")
					dataBoundItem["Height"].Text = Conversions.GetHeightFromInches(Int32.Parse(dataBoundItem["Height"].Text));
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void lbSubmit_Click(object sender, EventArgs e)
		{
			//when resorting grid or expanding child tables, this page re-ran entire query, which was too slow. now we cache the user's last-results in Session here whenever search criteria are acted on; it gets used by grid in gridResults_NeedDataSource.

			Session[_RESULTS_KEY] = GetData();

			gridOffenders.Rebind();
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Private Methods

		private DataTable GetData()
		{
			string userStringValue;
			double? userDoubleValue;

			int raceTypeID = -1;
			int genderTypeID = -1;
			int height = -1;
			int weight = -1;


			//race
			userStringValue = rcbRace.SelectedValue;
			if (!string.IsNullOrEmpty(userStringValue))
				raceTypeID = Int32.Parse(userStringValue);

			//gender
			userStringValue = rcbGender.SelectedValue;
			if (!string.IsNullOrEmpty(userStringValue))
				genderTypeID = Int32.Parse(userStringValue);
			
			//height
			userStringValue = rmtbHeight.TextWithLiterals;
			if (userStringValue != "'") //if field is not filled in, a single "'" is present from our mask
				height = Conversions.GetInchesFromHeight(userStringValue);
			
			//weight
			userDoubleValue = rntbWeight.Value;
			if (userDoubleValue > 0)
				weight = Convert.ToInt32(userDoubleValue);
			

			//TODO: 1) build up XML of checkbox items 2) pass into search method 3) pass into DAL 4) if XML exists, use it, otherwise use normal query

			List<DescriptionItem> checkedItems = new List<DescriptionItem>();

			checkedItems.AddRange(Common.GetCheckedItems(cblBuilds));
			checkedItems.AddRange(Common.GetCheckedItems(cblOddities));
			checkedItems.AddRange(Common.GetCheckedItems(cblScars));
			checkedItems.AddRange(Common.GetCheckedItems(cblTattoos));
			checkedItems.AddRange(Common.GetCheckedItems(cblApparel));
			checkedItems.AddRange(Common.GetCheckedItems(cblSpeech));
			checkedItems.AddRange(Common.GetCheckedItems(cblAccent));
			checkedItems.AddRange(Common.GetCheckedItems(cblFacialOddities));
			checkedItems.AddRange(Common.GetCheckedItems(cblEyes));
			checkedItems.AddRange(Common.GetCheckedItems(cblNose));
			checkedItems.AddRange(Common.GetCheckedItems(cblTeeth));
			checkedItems.AddRange(Common.GetCheckedItems(cblHairColors));
			checkedItems.AddRange(Common.GetCheckedItems(cblHairStyles));
			checkedItems.AddRange(Common.GetCheckedItems(cblFacialHair));
			checkedItems.AddRange(Common.GetCheckedItems(cblComplexion));

			string descriptorsXml = Common.GetXmlStringOfIds(checkedItems);

			//do it
			return Offender.GetOffendersByCriteria(tbFirstName.Text,
												   tbLastName.Text,
												   tbNickname.Text,
												   tbDriversLicense.Text,
												   rmtbSsn.Text,
												   raceTypeID,
												   genderTypeID,
												   height,
												   weight,
												   descriptorsXml);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Bind the checkboxlists.
		/// </summary>
		private void BindFormLists()
		{
			//usually a webform's dropdownlists are populated in the Page_Load in the !IsPostBack() check. however the viewstate hidden HTML variable is getting too fat w/ all these in it, 
			//slows things down. so instead we're binding these dropdownlists in the Page_Init *every* page load. awful, right? well the good news is the BAL caches these datatables on the 
			//web server, so it doesnt actually have to hit the db every time. also, the db server is likely near the web server so even on refreshes the hit isnt too bad. and because
			//they are rendered as normal HTML controls, the selected state of these lists is remembered on postback and doesnt need the viewstate collection.

			//build
			cblBuilds.DataSource = Offender.GetOffenderBuildTypes();
			cblBuilds.DataBind();

			//oddities
			cblOddities.DataSource = Offender.GetOffenderOddityTypes();
			cblOddities.DataBind();

			//scars
			cblScars.DataSource = Offender.GetOffenderScarTypes();
			cblScars.DataBind();

			//tattoos
			cblTattoos.DataSource = Offender.GetOffenderTattooTypes();
			cblTattoos.DataBind();

			//apparel
			cblApparel.DataSource = Offender.GetOffenderApparelTypes();
			cblApparel.DataBind();

			//speech
			cblSpeech.DataSource = Offender.GetOffenderSpeechTypes();
			cblSpeech.DataBind();

			//accent
			cblAccent.DataSource = Offender.GetOffenderAccentTypes();
			cblAccent.DataBind();

			//facialOddities
			cblFacialOddities.DataSource = Offender.GetOffenderFacialOddityTypes();
			cblFacialOddities.DataBind();

			//eyes
			cblEyes.DataSource = Offender.GetOffenderEyeTypes();
			cblEyes.DataBind();

			//nose
			cblNose.DataSource = Offender.GetOffenderNoseTypes();
			cblNose.DataBind();

			//teeth
			cblTeeth.DataSource = Offender.GetOffenderTeethTypes();
			cblTeeth.DataBind();

			//hair colors
			cblHairColors.DataSource = Offender.GetOffenderHairColorTypes();
			cblHairColors.DataBind();

			//hair styles
			cblHairStyles.DataSource = Offender.GetOffenderHairStyleTypes();
			cblHairStyles.DataBind();

			//facial hair
			cblFacialHair.DataSource = Offender.GetOffenderFacialHairTypes();
			cblFacialHair.DataBind();

			//complexion
			cblComplexion.DataSource = Offender.GetOffenderComplexionTypes();
			cblComplexion.DataBind();
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}