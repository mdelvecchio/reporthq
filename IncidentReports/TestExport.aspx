﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TestExport.aspx.cs" Inherits="ReportHq.IncidentReports.TestExport" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">

		<telerik:RadScriptManager ID="radScriptManager" runat="server">
			<Scripts>
				<asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.Core.js" />
				<asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQuery.js" />
				<asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQueryInclude.js" />
			</Scripts>
		</telerik:RadScriptManager>


		<telerik:RadClientExportManager ID="radClientExportManager" runat="server">
			<PdfSettings FileName="MyChart.pdf" />
		</telerik:RadClientExportManager>


		<h1>Test Export3</h1>

		<div id="exportArea">
			text XXX
		</div>

		<p>
			<input type="button" value="Export" onclick="exportRadHtmlChart('exportArea', 'charts1')" />
		</p>

	
		<script type="text/javascript">

			function exportRadHtmlChart(targetName, filename)
			{
				var actualName = $('div[id$="' + targetName + '"]')[0].id;
				//alert(actualName);

				var radClientExportManager = $find('<%= radClientExportManager.ClientID %>');

				var pdfSettings = {
					fileName: filename + ".pdf"
				};

				//var pdfSettings = {
				//	fileName: filename + ".pdf",
				//	proxyURL: "/Services/TelerikPdfExport.aspx"
				//};

				//uncommenting this causes Safari to reload page instead of get pdf
				radClientExportManager.set_pdfSettings(pdfSettings); 

				//alert(radClientExportManager);
				radClientExportManager.exportPDF($("#" + actualName));
			}

		</script>

    </form>
</body>
</html>
