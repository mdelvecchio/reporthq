﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TestService.aspx.cs" Inherits="TestService" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <telerik:RadStyleSheetManager ID="RadStyleSheetManager1" runat="server" />
</head>
<body>
    <form id="form1" runat="server">

        <telerik:RadScriptManager ID="radScriptManager" runat="server">
            <Scripts>
                <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.Core.js" />
                <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQuery.js" />
                <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQueryInclude.js" />
            </Scripts>
        </telerik:RadScriptManager>


		<telerik:RadClientExportManager ID="radClientExportManager" runat="server">
			<PdfSettings FileName="MyChart.pdf" />
		</telerik:RadClientExportManager>


        <h1>Test Export</h1>
			
		<div id="exportArea">
			text XXX 
		</div>
			
		<p>
			<input type="button" value="Export" onclick="exportRadHtmlChart('exportArea', 'charts.pdf')" />
		</p>
			


		<script type="text/javascript">

			function exportRadHtmlChart(targetName, filename)
			{
				var actualName = $('div[id$="' + targetName + '"]')[0].id;
				alert(actualName);

				var radClientExportManager = $find('<%= radClientExportManager.ClientID %>');

				//var pdfSettings = {
				//	fileName: filename,
				//	proxyURL: "FixesDebug2.aspx"						
				//};

				//var pdfSettings = {
				//	fileName: filename + ".pdf"
				//};
					
				//uncommenting this causes Safari to reload page instead of get pdf
				//radClientExportManager.set_pdfSettings(pdfSettings); 

				alert(radClientExportManager);
				radClientExportManager.exportPDF($("#" + actualName));
			}

		</script>

        

    </form>
</body>
</html>
