﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReportHq.Common;
using ReportHq.IncidentReports.Controls;

namespace ReportHq.IncidentReports
{
	public partial class SubmitReport : Bases.ReportPopup
	{
		#region Variables

		private Report _report;

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Event Handlers

		protected void Page_Init(object sender, EventArgs e)
		{
			_report = base.Report; //set from base's OnInit
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				ValidateReport();
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// User tries to submit report.
		/// </summary>
		protected void lbSubmit_Click(object sender, EventArgs e)
		{
			//ensure the instantiated report in session is the same ReportID as the one saved in the page's ViewState.
			base.SyncSessionToViewState();

			//SyncSessionToViewState ensures the report is sync'd up w/ the ViewState. now ensure our local copy is the latest instance.
			_report = base.Report;

			//now submit it
			StatusInfo status = _report.SubmitReport(this.Username);

			if (status.Success == true)
			{
				_report.ApprovalStatusType = Report.ApprovalStatusTypes.Pending;
				_report.ApprovalStatusTypeID = (int)Report.ApprovalStatusTypes.Pending;

				base.Report = _report; //update Session copy w/ new values

				string path = Page.ResolveUrl("~/YourDashboard.aspx");

				//redirect
				ClientScript.RegisterClientScriptBlock(GetType(), "CloseScript", "redirectParentPage('" + path + "?submitted=1')", true);
			}
			else //save failed
			{
				base.RenderUserMessage(Enums.UserMessageTypes.Warning, "Sorry, there was a problem: " + status.Message);
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
				
		#region Private Methods

		/// <summary>
		/// Checks that the report isn't missing anything.
		/// </summary>
		private void ValidateReport()
		{
			StatusInfo status = _report.ValidateReport();

			if (status.Success == true)
			{
				base.RenderUserMessage(Enums.UserMessageTypes.Plain, "If finished with this report, you may submit it for supervisor review.");
				lbSubmit.Visible = true;
			}
			else
			{
				StringBuilder sb = new StringBuilder(400);

				sb.Append(status.Message + ":<br/><br/>");
				sb.Append("<ul>");

				foreach (string item in status.SimpleLogs)
					sb.Append(String.Format("<li>{0}</li>", item));

				sb.Append("</ul>");
				sb.Append("...Please correct and re-submit.");

				base.RenderUserMessage(Enums.UserMessageTypes.Warning, sb.ToString());

				lbCancel.Text = "OK";
			}
		}
	
		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}