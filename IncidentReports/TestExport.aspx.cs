﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Telerik.Web.UI;

namespace ReportHq.IncidentReports
{
	public partial class TestExport : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			//setting this server side so its relative per environment
			radClientExportManager.PdfSettings.ProxyURL = Page.ResolveUrl("~/Services/TelerikPdfExport.aspx");
		}
	}
}