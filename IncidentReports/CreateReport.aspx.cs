﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReportHq.Common;

namespace ReportHq.IncidentReports
{
	public partial class CreateReport : Bases.NormalPage
	{
		#region Event Handlers

		protected void Page_Load(object sender, EventArgs e)
		{
			this.Heading = "Create Report";
			this.HelpUrl = "~/Help/CreateReport.html";

			rmtbItemNumber.SelectionOnFocus = Telerik.Web.UI.SelectionOnFocus.CaretToBeginning;
			rmtbItemNumber.Focus();
			
			if (!IsPostBack)
			{
				//report types
				rcbReportTypes.DataSource = Report.GetReportTypes();
				rcbReportTypes.DataBind();

				//default to this year
				//rmtbItemNumber.MaskParts[8].Value = "1"; 
				//rmtbItemNumber.MaskParts[9].Value = "4";

				//default month
				rmtbItemNumber.MaskParts[0].Value = Common.GetLetterForMonth(DateTime.Today.Month);
			}	
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void lbSubmit_Click(object sender, EventArgs e)
		{
			//lblMessage.Text = "postback";
			//lblMessage.Visible = true;

			if (IsValid)
				ProcessForm();

			/*
			below is the regex for Item Number:
			
			[A-L]-[0-9]{5}-[0-9]{2}
			 
			"L" - A-L
			"-"
			"#" - 0-9
			"#" - 0-9
			"#" - 0-9
			"#" - 0-9
			"#" - 0-9
			"-"
			"#" - 0-9
			"#" - 0-9			
			*/
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Private Methods

		private void ProcessForm()
		{
			Report report = new Report();
			report.ItemNumber = rmtbItemNumber.TextWithLiterals;
			report.ReportTypeID = Int32.Parse(rcbReportTypes.SelectedValue);
			report.ApprovalStatusType = Report.ApprovalStatusTypes.Incomplete;
			report.CreatedBy = this.Username;

			//save it!
			StatusInfo status = report.Add(this.Username);

			if (status.Success == true)
			{
				report.ID = status.Code; //need the ReportID for updates to work, comes from proc's out param

				//send user to first section
				Response.Redirect("Event/BasicInfo.aspx?rid=" + report.ID, false); //false = prevents a ThreadAbortException from raising (which is expensive)
				HttpContext.Current.ApplicationInstance.CompleteRequest(); //stops all subsquent code from executing
			}
			else //something failed
			{
				if (status.Message.Contains("already exists"))
					status.Message = "there's already an Incident report for this Item Number. Did you mean a Supplemental?";

				base.RenderUserMessage(Enums.UserMessageTypes.Warning, "Sorry, there was a problem saving: " + status.Message);
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}