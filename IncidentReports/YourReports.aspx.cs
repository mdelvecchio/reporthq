﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Telerik.Web.UI;

namespace ReportHq.IncidentReports
{
	public partial class YourReports : Bases.NormalPage
	{
		#region Variables

		private string _username;
		private const string _RESULTS_KEY = "dtYourReports";

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Event Handlers

		protected void Page_Load(object sender, EventArgs e)
		{
			this.Heading = "Your Reports";
			this.HelpUrl = "~/Help/YourReports.html"; 

			_username = this.Username;

			if (!IsPostBack)
			{
				Session[_RESULTS_KEY] = Report.GetReportsByUsername(_username);

				gridReports.Rebind();
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Fired when grid needs a datasource. eg, after editing or deleting.
		/// </summary>
		protected void gridReports_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
		{
			DataTable results = (DataTable)Session[_RESULTS_KEY];

			gridReports.DataSource = results;

			lblResultCount.Text = string.Format("You have <b>{0:N0}</b> reports:<br/><br/>", results.Rows.Count);

			//if (results.Rows.Count == 0)
			//{
			//	base.RenderUserMessage(Enums.UserMessageTypes.Warning, "Could not locate any Reports for " + _username);	
			//}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Fired when grid item's child-table icon is expanded.
		/// </summary>
		protected void gridReports_DetailTableDataBind(object source, GridDetailTableDataBindEventArgs e)
		{
			GridDataItem parentItem = e.DetailTableView.ParentItem as GridDataItem;

			//came from sdk example. if parent is in edit-mode dont update this
			if (parentItem.Edit)
				return;

			int reportID = (int)parentItem.GetDataKeyValue("ReportID");

			DataTable dt = null;

			//get appropiate child-table data
			switch (e.DetailTableView.Name)
			{
				case "persons":
					dt = VictimPerson.GetVictimPersonsByReport(reportID);
					break;

				case "offenders":
					dt = Offender.GetOffendersByReport(reportID);
					break;
			}

			e.DetailTableView.DataSource = dt;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}