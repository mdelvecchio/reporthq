﻿<%@ Page Title="Signal Types" Language="C#" MasterPageFile="~/IncidentReports.Master" AutoEventWireup="true" CodeBehind="SignalTypes.aspx.cs" Inherits="ReportHq.IncidentReports.Admin.Editors.SignalTypes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"></asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="phContent" runat="server">

	<telerik:RadGrid ID="gridItems"
		AllowPaging="true"
		PageSize="25"
		AllowSorting="true"
		ShowGroupPanel="false"
		AutoGenerateColumns="False"
		AllowMultiRowSelection="false"
		AllowMultiRowEdit="false"
		EnableHeaderContextMenu="true"
		OnNeedDataSource="gridItems_NeedDataSource"
		OnItemCreated="gridItems_ItemCreated"
		OnInsertCommand="gridItems_InsertCommand"
		OnUpdateCommand="gridItems_UpdateCommand" 
		runat="server">

		<ClientSettings
			EnableRowHoverStyle="true"
			AllowDragToGroup="false"
			AllowColumnsReorder="false"
			AllowKeyboardNavigation="true"
			ReorderColumnsOnClient="true">
			<Resizing AllowColumnResize="false" AllowRowResize="false" />
			<Selecting AllowRowSelect="true" EnableDragToSelectRows="true" />
		</ClientSettings>

		<ExportSettings FileName="signalTypes" ExportOnlyData="true" IgnorePaging="true" HideStructureColumns="true" OpenInNewWindow="true">
			<Excel Format="ExcelML"></Excel>
		</ExportSettings>

		<MasterTableView
			CommandItemDisplay="Top"
			DataKeyNames="SignalTypeID"
			EditMode="InPlace"
			NoMasterRecordsText="No items to display."
			CommandItemStyle-Font-Bold="true"
			HeaderStyle-ForeColor="#191970"
			ItemStyle-CssClass="item"
			AlternatingItemStyle-CssClass="item">

			<PagerStyle PageSizes="25,75,300" Mode="NextPrevAndNumeric" Position="TopAndBottom" />

			<CommandItemSettings
				ShowSaveChangesButton="false"
				ShowCancelChangesButton="false"
				ShowRefreshButton="false"
				ShowAddNewRecordButton="true"
				ShowExportToExcelButton="true" />

			<BatchEditingSettings EditType="Cell" />
			

			<Columns>				
				<telerik:GridEditCommandColumn UniqueName="editColumn" ButtonType="LinkButton" HeaderStyle-Width="40" ItemStyle-CssClass="commandItem" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Bold="True" />

				<telerik:GridBoundColumn HeaderText="Code" UniqueName="Code" DataField="Code" MaxLength="50" HeaderStyle-Wrap="false">
					<ColumnValidationSettings EnableRequiredFieldValidation="true">
						<RequiredFieldValidator ErrorMessage="Required" CssClass="required" Display="Dynamic"></RequiredFieldValidator>						
					</ColumnValidationSettings>
				</telerik:GridBoundColumn>

				<telerik:GridBoundColumn HeaderText="Description" UniqueName="Description" DataField="Description" MaxLength="50" HeaderStyle-Wrap="false">
					<ColumnValidationSettings EnableRequiredFieldValidation="true">
						<RequiredFieldValidator ErrorMessage="Required" CssClass="required" Display="Dynamic"></RequiredFieldValidator>
					</ColumnValidationSettings>
				</telerik:GridBoundColumn>

				<telerik:GridCheckBoxColumn HeaderText="IsDomesticViolence" UniqueName="IsDomesticViolence" DataField="IsDomesticViolence" HeaderStyle-Wrap="false" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
				<telerik:GridCheckBoxColumn HeaderText="IsCriminal" UniqueName="IsCriminal" DataField="IsCriminal" HeaderStyle-Wrap="false" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />				
				<telerik:GridDropDownColumn HeaderText="UCR Offense Type" UniqueName="UcrOffenseTypeID" DataField="UcrOffenseTypeID" DataSourceID="odsUcrOffenseTypes" ListValueField="UcrOffenseTypeID" ListTextField="Name" EnableEmptyListItem="true" />
				<telerik:GridCheckBoxColumn HeaderText="Active" UniqueName="Active" DataField="IsActive" HeaderStyle-Wrap="false" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
			</Columns>

		</MasterTableView>
	</telerik:RadGrid>

	<asp:ObjectDataSource ID="odsUcrOffenseTypes" TypeName="ReportHq.IncidentReports.UcrOffense" SelectMethod="GetAllOffenseTypes" runat="server" />


	<telerik:RadAjaxManager ID="radAjaxManager" ClientEvents-OnRequestStart="onRequestStart" runat="server" />
	
</asp:Content>
