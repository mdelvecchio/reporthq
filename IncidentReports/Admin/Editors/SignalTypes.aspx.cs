﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReportHq.Common;
using ReportHq.IncidentReports.Bases;
using ReportHq.IncidentReports.Controls;
using Telerik.Web.UI;

namespace ReportHq.IncidentReports.Admin.Editors
{
	public partial class SignalTypes : Bases.NormalPage
	{
		#region Variables

		NormalPage _page;

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Event Handlers

		protected void Page_Load(object sender, EventArgs e)
		{
			this.Heading = "Signal Types Editor";
			this.BreadCrumbs.Add((new BreadCrumb("Admin Tools", ResolveUrl("~/Admin/"), "to Admin Tools")));
			this.HelpUrl = "~/admin/help/ResetLists.html";			

			_page = (NormalPage)this.Page;
			NormalMasterPage masterPage = (NormalMasterPage)this.Master;

			//ajax mapping
			//radAjaxManager.AjaxSettings.AddAjaxSetting(gridItems, gridItems, masterPage.LoadingPanel); //grid updates selef
			radAjaxManager.AjaxSettings.AddAjaxSetting(gridItems, gridItems, null); //grid updates selef
			radAjaxManager.AjaxSettings.AddAjaxSetting(gridItems, masterPage.LabelMessage, null); //grid updates page label-message
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void gridItems_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
		{
			gridItems.DataSource = GetData();
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void gridItems_InsertCommand(object source, GridCommandEventArgs e)
		{
			GridEditableItem item = e.Item as GridEditableItem;

			Hashtable newValues = new Hashtable();
			item.OwnerTableView.ExtractValuesFromItem(newValues, item);

			SignalType signalType = new SignalType(newValues);
			
			//save it
			StatusInfo status = signalType.Add();

			if (!status.Success)
				_page.RenderUserMessage(Enums.UserMessageTypes.Warning, ("Sorry, there was a problem saving: " + status.Message));
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void gridItems_UpdateCommand(object source, GridCommandEventArgs e)
		{
			GridEditableItem item = e.Item as GridEditableItem;

			int signalTypeID = (int)item.GetDataKeyValue("SignalTypeID");
			
			Hashtable newValues = new Hashtable();
			item.OwnerTableView.ExtractValuesFromItem(newValues, item);

			SignalType signalType = new SignalType(newValues);
			signalType.ID = signalTypeID;

			//save it
			StatusInfo status = signalType.Update();

			if (!status.Success)
				_page.RenderUserMessage(Enums.UserMessageTypes.Warning, ("Sorry, there was a problem saving: " + status.Message));
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		//protected void gridItems_DeleteCommand(object source, GridCommandEventArgs e)
		//{
		//	GridEditableItem item = (GridEditableItem)e.Item;

		//	int signalTypeID = (int)item.GetDataKeyValue("SignalTypeID");

		//	StatusInfo status = SignalType.DeactivateSignalType(signalTypeID);

		//	if (!status.Success)
		//		_page.RenderUserMessage(Enums.UserMessageTypes.Warning, ("Sorry, there was a problem saving: " + status.Message));
		//}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Fix a layout issue.
		/// </summary>
		protected void gridItems_ItemCreated(object sender, GridItemEventArgs e)
		{
			if (e.Item is GridDataItem && e.Item.IsInEditMode)
			{
				ControlCollection controls = (e.Item as GridDataItem)["editColumn"].Controls;

				//space things out a bit (requested by users to make linkbuttons easier to distinguish)
				LiteralControl litSpacer = (LiteralControl)controls[1];
				litSpacer.Text = "&nbsp;&nbsp;&nbsp;";
			}
		}
		
		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Private Methods

		private DataTable GetData()
		{
			return SignalType.GetSignalTypes(false);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}