﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ReportHq.IncidentReports.Admin
{
	public partial class ResetNotifications : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{

		}

		protected void lbResetNotifications_Click(object sender, EventArgs e)
		{
			//GlobalNotification.TempResetNotifications();
			Notification.TempResetNotifications();
		}
	}
}