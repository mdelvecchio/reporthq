﻿<%@ Page Title="Manage Notifications" Language="C#" MasterPageFile="~/IncidentReports.Master" AutoEventWireup="true" CodeBehind="ManageNotifications.aspx.cs" Inherits="ReportHq.IncidentReports.Admin.ManageNotifications" %>

<asp:Content ID="Content2" ContentPlaceHolderID="phContent" runat="server">

	<a href="CreateNotification.aspx" title="Create a new Notification" class="flatButton"><i class="fa fa-plus fa-fw"></i> Create Notification</a><br /><br />
	
	<p><asp:Label ID="lblRecordCount" runat="server" /></p>

	<telerik:RadAjaxPanel ID="updResults" LoadingPanelID="ajaxLoadingPanel" runat="server">

		<telerik:RadGrid ID="gridNotifications"
			Width="95%"
			AutoGenerateColumns="False"
			AllowPaging="true"
			PageSize="25"
			PagerStyle-Mode="NextPrevAndNumeric"
			PagerStyle-Position="TopAndBottom"
			OnNeedDataSource="gridNotifications_NeedDataSource" 
			OnUpdateCommand="gridNotifications_UpdateCommand" 
			OnDeleteCommand="gridNotifications_DeleteCommand"						
			runat="server">

			<ClientSettings
				EnableRowHoverStyle="true"
				AllowDragToGroup="false"
				AllowColumnsReorder="false"
				AllowKeyboardNavigation="true"
				ReorderColumnsOnClient="false">
				<Resizing AllowColumnResize="false" AllowRowResize="false" />
				<Selecting AllowRowSelect="false" EnableDragToSelectRows="false" />
			</ClientSettings>

			<MasterTableView
				CommandItemDisplay="Bottom"
				DataKeyNames="ID"
				NoMasterRecordsText="No Notifications to display."
				CommandItemStyle-Font-Bold="true"
				HeaderStyle-ForeColor="#191970"
				ItemStyle-CssClass="item"
				AlternatingItemStyle-CssClass="item"
				EditMode="InPlace">

				<CommandItemSettings ShowRefreshButton="false" ShowAddNewRecordButton="false" />

				<Columns>
					<telerik:GridEditCommandColumn ButtonType="LinkButton" HeaderStyle-Width="50" ItemStyle-HorizontalAlign="center" ItemStyle-CssClass="commandItem" />
					<telerik:GridButtonColumn CommandName="Delete" Text="Delete" UniqueName="DeleteColumn" ConfirmText="Are you sure you want to delete this?" HeaderStyle-Width="50" ItemStyle-CssClass="commandItem" />

					<telerik:GridBoundColumn DataField="ID" UniqueName="ID" Visible="false" />
					<telerik:GridBoundColumn DataField="Message" UniqueName="Message" HeaderText="Message" ItemStyle-Wrap="false">
						<ColumnValidationSettings EnableRequiredFieldValidation="true">
							<RequiredFieldValidator CssClass="required" ErrorMessage=" Required"></RequiredFieldValidator>
						</ColumnValidationSettings>
					</telerik:GridBoundColumn>
					<telerik:GridBoundColumn DataField="CreatedDate" UniqueName="CreatedDate" ReadOnly="true" HeaderText="Created" DataFormatString="{0:MM/dd/yyyy}" HeaderStyle-Width="75px" />
					<telerik:GridBoundColumn DataField="CreatedBy" UniqueName="CreatedBy" ReadOnly="true" HeaderText="CreatedBy" ItemStyle-Wrap="false" />
				</Columns>

			</MasterTableView>
		</telerik:RadGrid>

	</telerik:RadAjaxPanel>
	
</asp:Content>
