﻿<%@ Page Title="Admin Tools" Language="C#" MasterPageFile="~/IncidentReports.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ReportHq.IncidentReports.Admin.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"></asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="phContent" runat="server">

	<p><a href="ManageNotifications.aspx" class="siteNav">Manage Notifications</a></p>
	<p><a href="ResetLists.aspx" class="siteNav">Reset Cached Lists</a></p>
	<p><a href="#" class="siteNav">Role Manager</a></p>
	<p><a href="#" class="siteNav">Deactivate Reports</a></p>
	<p><a href="#" class="siteNav">Error Log</a></p>
	<p><a href="ResetNotifications.aspx" target="_blank" class="siteNav">Reset Notifications (demo only)</a></p>

	<br />


	<p>List Editors</p>

	<blockquote>

		<a href="editors/SignalTypes.aspx" class="siteNav">Signal Types</a><br /><br />

		<a href="#" class="siteNav">Charge Prefixes</a><br />
		<a href="#" class="siteNav">Charge Types</a><br /><br />

		<a href="#" class="siteNav">Offender Descriptors</a><br />
		<a href="#" class="siteNav">Modus Operandi</a><br /><br />

		<a href="#" class="siteNav">Vehicle Types</a><br />
		<a href="#" class="siteNav">Car Makes</a><br />
		<a href="#" class="siteNav">Car Models</a><br />
		<a href="#" class="siteNav">Car Colors</a><br /><br />

		<a href="#" class="siteNav">Property Types</a><br />
		<a href="#" class="siteNav">Evidence Types</a><br /><br />

		<a href="#" class="siteNav">Weapon Types</a><br />
		<a href="#" class="siteNav">Firearm Makes</a><br />
		<a href="#" class="siteNav">Firearm Models</a><br />
	</blockquote>

</asp:Content>
