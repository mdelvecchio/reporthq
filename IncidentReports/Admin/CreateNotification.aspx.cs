﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReportHq.Common;
using ReportHq.IncidentReports.Controls;

namespace ReportHq.IncidentReports.Admin
{
	public partial class CreateNotification : Bases.NormalPage
	{
		#region Event Handlers

		protected void Page_Load(object sender, EventArgs e)
		{
			this.BreadCrumbs.Add((new BreadCrumb("Admin Tools", ResolveUrl("~/Admin/"), "to Admin Tools")));
			this.HelpUrl = "http://www.google.com"; //temp
			this.Heading = "Create Notification";			
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void lbSend_Click(object sender, EventArgs e)
		{
			Notification notification = new Notification(Notification.Types.System, -1, tbMessage.Text, string.Empty);

			//send it
			StatusInfo status = notification.SendGlobal(this.Username);

			if (status.Success)
			{
				Response.Redirect("ManageNotifications.aspx?created=1", false);  //false = prevents a ThreadAbortException from raising (which is expensive)
				HttpContext.Current.ApplicationInstance.CompleteRequest(); //stops all subsquent code from executing
			}
			else
			{
				base.RenderUserMessage(Enums.UserMessageTypes.Warning, ("Sorry, there was a problem saving: " + status.Message));
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}