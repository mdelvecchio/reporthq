﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReportHq.IncidentReports.Controls;

namespace ReportHq.IncidentReports.Admin
{
	public partial class ResetLists : Bases.NormalPage
	{
		#region Event Handlers

		protected void Page_Load(object sender, EventArgs e)
		{
			this.BreadCrumbs.Add((new BreadCrumb("Admin Tools", ResolveUrl("~/Admin/"), "to Admin Tools")));
			this.HelpUrl = "~/admin/help/ResetLists.html";
			this.Heading = "Reset Cached Lists";
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Removes all cached items out of the Cache collection
		/// </summary>
		protected void btnClearCahe_Click(object sender, EventArgs e)
		{
			IDictionaryEnumerator iterator = Cache.GetEnumerator();

			while (iterator.MoveNext())
			{
				Cache.Remove(iterator.Key.ToString());
			}

			this.RenderUserMessage(Enums.UserMessageTypes.Success, "All items removed from Cache collection.");
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}