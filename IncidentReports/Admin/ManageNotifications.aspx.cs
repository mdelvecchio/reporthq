﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReportHq.Common;
using ReportHq.IncidentReports.Bases;
using ReportHq.IncidentReports.Controls;
using Telerik.Web.UI;

namespace ReportHq.IncidentReports.Admin
{
	public partial class ManageNotifications : Bases.NormalPage
	{
		#region Event Handlers

		protected void Page_Load(object sender, EventArgs e)
		{
			this.BreadCrumbs.Add((new BreadCrumb("Admin Tools", ResolveUrl("~/Admin/"), "to Admin Tools")));
			this.HelpUrl = "http://www.google.com"; //temp
			this.Heading = "Manage Notifications";

			if (!string.IsNullOrEmpty(Request.QueryString["created"]))
				base.RenderUserMessage(Enums.UserMessageTypes.Success, ("Notification created!"));

			if (!string.IsNullOrEmpty(Request.QueryString["saved"]))
				base.RenderUserMessage(Enums.UserMessageTypes.Success, ("Notification saved!"));
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Fired when grid needs a datasource. eg, after editing or deleting.
		/// </summary>
		protected void gridNotifications_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
		{
			DataTable results = Notification.GetNotificationsByType(Notification.Types.System);

			gridNotifications.DataSource = results;

			lblRecordCount.Text = string.Format("There are <b>{0:N0}</b> Bulletins:<br/><br/>", results.Rows.Count);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		
		protected void gridNotifications_UpdateCommand(object sender, GridCommandEventArgs e)
		{
			GridEditableItem editableItem = (e.Item as GridEditableItem);

			int notificationID = (int)editableItem.GetDataKeyValue("ID");

			//get values into hashtable
			Hashtable newValues = new Hashtable();
			e.Item.OwnerTableView.ExtractValuesFromItem(newValues, editableItem);

			string message = newValues["Message"].ToString().Trim();

			//do it
			StatusInfo status = Notification.UpdateNotification(notificationID, message);

			if (!status.Success)
			{
				this.RenderUserMessage(Enums.UserMessageTypes.Warning, ("Sorry, there was a problem saving: " + status.Message));
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void gridNotifications_DeleteCommand(object sender, GridCommandEventArgs e)
		{
			GridEditableItem editableItem = (GridEditableItem)e.Item;
			int notificationID = (int)editableItem.GetDataKeyValue("ID");

			//do it
			StatusInfo status = Notification.DeleteNotification(notificationID);

			if (!status.Success)
			{
				this.RenderUserMessage(Enums.UserMessageTypes.Warning, ("Sorry, there was a problem: " + status.Message));
			}
		}
				
		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		
		#endregion
	}
}