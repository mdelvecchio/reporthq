﻿<%@ Page Title="Create Notification" Language="C#" MasterPageFile="~/IncidentReports.Master" AutoEventWireup="true" CodeBehind="CreateNotification.aspx.cs" Inherits="ReportHq.IncidentReports.Admin.CreateNotification" %>

<asp:Content ID="Content2" ContentPlaceHolderID="phContent" runat="server">

	<p>
		<asp:TextBox ID="tbMessage" MaxLength="100" Width="250" runat="server" />
	</p>
	
	<asp:LinkButton ID="lbSend" ValidationGroup="bulletin" OnClick="lbSend_Click" CssClass="flatButton" runat="server"><i class="fa fa-check fa-fw"></i> Send Notification</asp:LinkButton>

</asp:Content>
