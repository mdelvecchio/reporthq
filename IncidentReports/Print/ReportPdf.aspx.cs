﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReportHq.IncidentReports.Bases;

namespace ReportHq.IncidentReports.Print
{
	public partial class ReportPdf : NormalPage
	{
		#region Event Handlers

		protected void Page_Load(object sender, EventArgs e)
		{
			bool isOfficial = false;

			string sMode = Request.QueryString["m"];

			if (!string.IsNullOrEmpty(sMode) && sMode == "o")
				isOfficial = true;

			Report report = new Report(Int32.Parse(Request.QueryString["rid"]));

			string fileName = report.ItemNumber;

			if (report.IsSupplemental)
				fileName += "_Supplemental";

			fileName += ".pdf";

			string watermarkText = string.Empty;

			if (this.ApplicationEnvironment == Enums.ApplicationEnvironments.Training)
			{
				watermarkText = "TRAINING ONLY";
				isOfficial = false;
			}

			if (report.IsDataEntry)
			{
				watermarkText = "DATA ENTRY:" + Environment.NewLine + report.CreatedBy;
				isOfficial = false;
			}

			byte[] pdf = report.GetPdf(Common.GetPrintReportTemplateUrl(), isOfficial, watermarkText);

			Response.Clear();
			Response.ContentType = "application/pdf";
			Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
			Response.AddHeader("content-length", pdf.Length.ToString());
			Response.BinaryWrite(pdf);
			Response.End();
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}