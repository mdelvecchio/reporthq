﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReportHq.Common;
using ReportHq.IncidentReports;
using ReportHq.IncidentReports.Controls;
using ReportHq.IncidentReports.ModusOperandi.Controls;

namespace ReportHq.IncidentReports.ModusOperandi
{
	public partial class MeansOfEntry : Bases.ReportPage
	{
		#region Variables

		private Report _report;

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Event Handlers

		protected void Page_Init(object sender, EventArgs e)
		{
			_report = base.Report; //set from base's OnInit
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void Page_Load(object sender, EventArgs e)
		{
			this.HelpUrl = "~/ModusOperandi/Help/MeansOfEntry.html";
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void ucMajorSections_NavItemClicked(object sender, EventArgs e)
		{
			base.ProcessNavClick((sender as ReportMainNav).TargetRedirectUrl, this.UserHasWriteAccess);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void ucMeansOfEntry_NavItemClicked(object sender, EventArgs e)
		{
			base.ProcessNavClick((sender as ReportSubNav).TargetRedirectUrl, this.UserHasWriteAccess);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Public Methods

		/// <summary>
		/// Sets the page's form fields to the Report's values. This overrides the method stub in the base ReportPage, and is called by the base's OnLoad() method.
		/// </summary>
		public override void SetFormValues()
		{
			ucMeansOfEntry.PointOfEntryCheckedItems = _report.MeansOfEntryPointOfEntryItems;
			ucMeansOfEntry.MethodOfEntryCheckedItems = _report.MeansOfEntryMethodOfEntryItems;
			ucMeansOfEntry.SecurityUsedCheckedItems = _report.MeansOfEntrySecurityUsedItems;
			ucMeansOfEntry.Description = _report.MeansOfEntryDescription;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Saves the report. On success, updates Session copy. This overrides the method stub in the base ReportPage, and is called by the base's ProcessNavClick() public method.
		/// </summary>
		public override StatusInfo SaveReport()
		{
			_report = base.Report; //the page base ensures the report is sync'd up w/ the ViewState prior to it calling this SaveReport(). now ensure our local copy is the latest instance.

			//set new form values to report
			_report.MeansOfEntryPointOfEntryItems = ucMeansOfEntry.PointOfEntryCheckedItems;
			_report.MeansOfEntryMethodOfEntryItems = ucMeansOfEntry.MethodOfEntryCheckedItems;
			_report.MeansOfEntrySecurityUsedItems = ucMeansOfEntry.SecurityUsedCheckedItems;
			_report.MeansOfEntryDescription = ucMeansOfEntry.Description;

			//update
			StatusInfo status = _report.UpdateMeansOfEntry(this.Username);

			if (status.Success == true)
				base.Report = _report; //update Session copy w/ new values, but only if it saved to db. dont want to mislead the user.

			//base handles rendering errors & redirect
			return status;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}