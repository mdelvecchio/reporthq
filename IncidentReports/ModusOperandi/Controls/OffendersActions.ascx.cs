﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReportHq.IncidentReports.Controls;

namespace ReportHq.IncidentReports.ModusOperandi.Controls
{
	public partial class OffendersActions : System.Web.UI.UserControl
	{
		#region Properties

		public List<DescriptionItem> ApproachCheckedItems
		{
			get { return Common.GetCheckedItems(cblApproaches); }
			set
			{
				if (value.Count > 0)
					Common.SetCheckedItems(value, cblApproaches);
			}
		}

		public List<DescriptionItem> ImpersonationCheckedItems
		{
			get { return Common.GetCheckedItems(cblImpersonationTypes); }
			set
			{
				if (value.Count > 0)
					Common.SetCheckedItems(value, cblImpersonationTypes);
			}
		}

		public List<DescriptionItem> WeaponCheckedItems
		{
			get { return Common.GetCheckedItems(cblWeapons); }
			set
			{
				if (value.Count > 0)
					Common.SetCheckedItems(value, cblWeapons);
			}
		}

		public List<DescriptionItem> FirearmFeaturesCheckedItems
		{
			get { return Common.GetCheckedItems(cblFirearmFeatures); }
			set
			{
				if (value.Count > 0)
					Common.SetCheckedItems(value, cblFirearmFeatures);
			}
		}

		public List<DescriptionItem> PropertyCrimeCheckedItems
		{
			get { return Common.GetCheckedItems(cblPropertyCrimes); }
			set
			{
				if (value.Count > 0)
					Common.SetCheckedItems(value, cblPropertyCrimes);
			}
		}

		public List<DescriptionItem> PersonCrimeCheckedItems
		{
			get { return Common.GetCheckedItems(cblPersonCrimes); }
			set
			{
				if (value.Count > 0)
					Common.SetCheckedItems(value, cblPersonCrimes);
			}
		}

		public List<DescriptionItem> SexCrimeCheckedItems
		{
			get { return Common.GetCheckedItems(cblSexCrimes); }
			set
			{
				if (value.Count > 0)
					Common.SetCheckedItems(value, cblSexCrimes);
			}
		}

		public String Description
		{
			get { return tbOffendersActionsDesc.Text; }
			set { tbOffendersActionsDesc.Text = value; }
		}

		/// <summary>
		/// Whether the user can operate certain form elements.
		/// </summary>
		public bool ReadOnly
		{
			set
			{
				//if (value == true)
				//	radSpell.Enabled = false;
				//else
				//	radSpell.Enabled = true;
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Events

		//public delegate for LinkButton nav-item click
		public delegate void NavItemClick(object sender, EventArgs e);

		//public event for LinkButton click; will be handled by the consuming pages
		public event NavItemClick NavItemClicked;

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Event Handlers

		/// <summary>
		/// Bind form lists.
		/// </summary>
		protected void Page_Init(object sender, EventArgs e)
		{
			//usually a webform's dropdownlists are populated in the Page_Load in the !IsPostBack() check. however the viewstate hidden HTML variable is getting too fat w/ all these in it, 
			//slows things down. so instead we're binding these dropdownlists in the Page_Init *every* page load. awful, right? well the good news is the BAL caches these datatables on the 
			//web server, so it doesnt actually have to hit the db every time. also, the db server is likely near the web server so even on refreshes the hit isnt too bad. and because
			//they are rendered as normal HTML controls, the selected state of these lists is remembered on postback and doesnt need the viewstate collection.

			BindFormLists();
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void Page_Load(object sender, EventArgs e)
		{

		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void ucReportSubNav_NavItemClicked(object sender, EventArgs e)
		{
			//fire event for consuming page to handler
			NavItemClicked(sender, e);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Private Methods

		/// <summary>
		/// Binds Report HQ's MO items to the form controls. Not the actual form values, just the possible items.
		/// </summary>
		private void BindFormLists()
		{
			cblApproaches.DataSource = Report.GetOffenderApproachTypes();
			cblApproaches.DataBind();

			cblImpersonationTypes.DataSource = Report.GetImpersonationTypes();
			cblImpersonationTypes.DataBind();

			cblWeapons.DataSource = Report.GetWeaponTypes();
			cblWeapons.DataBind();

			cblFirearmFeatures.DataSource = Report.GetFirearmFeatureTypes();
			cblFirearmFeatures.DataBind();

			cblPropertyCrimes.DataSource = Report.GetPropertyCrimeTypes();
			cblPropertyCrimes.DataBind();

			cblPersonCrimes.DataSource = Report.GetPersonCrimeTypes();
			cblPersonCrimes.DataBind();

			cblSexCrimes.DataSource = Report.GetSexCrimeTypes();
			cblSexCrimes.DataBind();
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}