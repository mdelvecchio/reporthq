﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReportHq.IncidentReports.Controls;

namespace ReportHq.IncidentReports.ModusOperandi.Controls
{
	public partial class CrimeLocation : System.Web.UI.UserControl
	{
		#region Properties

		public List<DescriptionItem> ResidentialCheckedItems
		{
			get { return Common.GetCheckedItems(cblResidential); }
			set
			{
				if (value.Count > 0)
					Common.SetCheckedItems(value, cblResidential);
			}
		}

		public List<DescriptionItem> CommericalCheckedItems
		{
			get { return Common.GetCheckedItems(cblCommerical); }
			set
			{
				if (value.Count > 0)
					Common.SetCheckedItems(value, cblCommerical);
			}
		}

		public List<DescriptionItem> OutdoorAreaCheckedItems
		{
			get { return Common.GetCheckedItems(cblOutdoorAreas); }
			set
			{
				if (value.Count > 0)
					Common.SetCheckedItems(value, cblOutdoorAreas);
			}
		}

		public List<DescriptionItem> PublicAccessCheckedItems
		{
			get { return Common.GetCheckedItems(cblPublicAccessAreas); }
			set
			{
				if (value.Count > 0)
					Common.SetCheckedItems(value, cblPublicAccessAreas);
			}
		}

		public List<DescriptionItem> MovableCheckedItems
		{
			get { return Common.GetCheckedItems(cblMoveables); }
			set
			{
				if (value.Count > 0)
					Common.SetCheckedItems(value, cblMoveables);
			}
		}

		public List<DescriptionItem> StructureTypeCheckedItems
		{
			get { return Common.GetCheckedItems(cblStructureTypes); }
			set
			{
				if (value.Count > 0)
					Common.SetCheckedItems(value, cblStructureTypes);
			}
		}

		public List<DescriptionItem> StructureStatusCheckedItems
		{
			get { return Common.GetCheckedItems(cblStructureStatuses); }
			set
			{
				if (value.Count > 0)
					Common.SetCheckedItems(value, cblStructureStatuses);
			}
		}

		public String Description
		{
			get { return tbCrimeLocationDesc.Text; }
			set { tbCrimeLocationDesc.Text = value; }
		}

		/// <summary>
		/// Whether the user can operate certain form elements.
		/// </summary>
		public bool ReadOnly
		{
			set
			{
				//if (value == true)
				//	radSpell.Enabled = false;
				//else
				//	radSpell.Enabled = true;
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Events

		//public delegate for LinkButton nav-item click
		public delegate void NavItemClick(object sender, EventArgs e);

		//public event for LinkButton click; will be handled by the consuming pages
		public event NavItemClick NavItemClicked;

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Event Handlers

		/// <summary>
		/// Bind form lists.
		/// </summary>
		protected void Page_Init(object sender, EventArgs e)
		{
			//usually a webform's dropdownlists are populated in the Page_Load in the !IsPostBack() check. however the viewstate hidden HTML variable is getting too fat w/ all these in it, 
			//slows things down. so instead we're binding these dropdownlists in the Page_Init *every* page load. awful, right? well the good news is the BAL caches these datatables on the 
			//web server, so it doesnt actually have to hit the db every time. also, the db server is likely near the web server so even on refreshes the hit isnt too bad. and because
			//they are rendered as normal HTML controls, the selected state of these lists is remembered on postback and doesnt need the viewstate collection.

			BindFormLists();
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void Page_Load(object sender, EventArgs e)
		{

		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void ucReportSubNav_NavItemClicked(object sender, EventArgs e)
		{
			//fire event for consuming page to handler
			NavItemClicked(sender, e);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Private Methods

		/// <summary>
		/// Binds Report HQ's MO items to the form controls. Not the actual form values, just the possible items.
		/// </summary>
		private void BindFormLists()
		{
			cblResidential.DataSource = Report.GetResidentialLocationTypes();
			cblResidential.DataBind();

			cblCommerical.DataSource = Report.GetCommericalLocationTypes();
			cblCommerical.DataBind();

			cblOutdoorAreas.DataSource = Report.GetOutdoorLocationTypes();
			cblOutdoorAreas.DataBind();

			cblPublicAccessAreas.DataSource = Report.GetPublicLocationTypes();
			cblPublicAccessAreas.DataBind();

			cblMoveables.DataSource = Report.GetMovableLocationTypes();
			cblMoveables.DataBind();

			cblStructureTypes.DataSource = Report.GetLocationStructureTypes();
			cblStructureTypes.DataBind();

			cblStructureStatuses.DataSource = Report.GetLocationStructureStatusTypes();
			cblStructureStatuses.DataBind();
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}