﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReportHq.IncidentReports.Controls;

namespace ReportHq.IncidentReports.ModusOperandi.Controls
{
	public partial class MeansOfEntry : System.Web.UI.UserControl
	{
		#region Properties

		public List<DescriptionItem> PointOfEntryCheckedItems
		{
			get { return Common.GetCheckedItems(cblPointsOfEntry); }
			set
			{
				if (value.Count > 0)
					Common.SetCheckedItems(value, cblPointsOfEntry);
			}
		}

		public List<DescriptionItem> MethodOfEntryCheckedItems
		{
			get { return Common.GetCheckedItems(cblMethodsOfEntry); }
			set
			{
				if (value.Count > 0)
					Common.SetCheckedItems(value, cblMethodsOfEntry);
			}
		}

		public List<SecurityUsedDescriptionItem> SecurityUsedCheckedItems
		{
			get { return GetSecurityUsedItems(); }
			set
			{
				if (value.Count > 0)
					SetSecurityUsedItems(value);
			}
		}

		public String Description
		{
			get { return tbMeansOfEntryDesc.Text; }
			set { tbMeansOfEntryDesc.Text = value; }
		}

		/// <summary>
		/// Whether the user can operate certain form elements.
		/// </summary>
		public bool ReadOnly
		{
			set
			{
				//if (value == true)
				//	radSpell.Enabled = false;
				//else
				//	radSpell.Enabled = true;
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Events

		//public delegate for LinkButton nav-item click
		public delegate void NavItemClick(object sender, EventArgs e);

		//public event for LinkButton click; will be handled by the consuming pages
		public event NavItemClick NavItemClicked;

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Event Handlers

		/// <summary>
		/// Bind form lists.
		/// </summary>
		protected void Page_Init(object sender, EventArgs e)
		{
			//usually a webform's dropdownlists are populated in the Page_Load in the !IsPostBack() check. however the viewstate hidden HTML variable is getting too fat w/ all these in it, 
			//slows things down. so instead we're binding these dropdownlists in the Page_Init *every* page load. awful, right? well the good news is the BAL caches these datatables on the 
			//web server, so it doesnt actually have to hit the db every time. also, the db server is likely near the web server so even on refreshes the hit isnt too bad. and because
			//they are rendered as normal HTML controls, the selected state of these lists is remembered on postback and doesnt need the viewstate collection.

			BindFormLists();
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void Page_Load(object sender, EventArgs e)
		{

		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void ucReportSubNav_NavItemClicked(object sender, EventArgs e)
		{
			//fire event for consuming page to handler
			NavItemClicked(sender, e);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Private Methods

		/// <summary>
		/// Binds Report HQ's MO items to the form controls. Not the actual form values, just the possible items.
		/// </summary>
		private void BindFormLists()
		{
			cblPointsOfEntry.DataSource = Report.GetPointOfEntryTypes();
			cblPointsOfEntry.DataBind();

			cblMethodsOfEntry.DataSource = Report.GetMethodOfEntryTypes();
			cblMethodsOfEntry.DataBind();

			rptSecurityTypesUsed.DataSource = Report.GetSecurityTypes();
			rptSecurityTypesUsed.DataBind();
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Sets appropiate checkboxes for the "Security Types Used" repeater, based on db values.
		/// </summary>
		private void SetSecurityUsedItems(List<SecurityUsedDescriptionItem> usedItems)
		{
			//loop thru this report's checked-items
			foreach (SecurityUsedDescriptionItem securityItem in usedItems)
			{
				int itemID = securityItem.ID; //the db MeansOfEntryTypeID of this security-item

				//loop thru form's repeater items
				foreach (RepeaterItem repeaterItem in rptSecurityTypesUsed.Items)
				{
					if (Int32.Parse(((HiddenField)repeaterItem.FindControl("hidMeansOfEntryTypeID")).Value) == itemID) //this type was in use
					{
						((CheckBox)repeaterItem.FindControl("cbWasUsed")).Checked = true; //used, so check

						if (securityItem.WasDefeated) //defeated, so check this repeater's defeated-checkbox
							((CheckBox)repeaterItem.FindControl("cbWasDefeated")).Checked = true;
					}
				}
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		//private DataTable GetSecurityUsedItems()
		private List<SecurityUsedDescriptionItem> GetSecurityUsedItems()
		{
			List<SecurityUsedDescriptionItem> items = new List<SecurityUsedDescriptionItem>();

			//loop thru form's repeater items
			foreach (RepeaterItem item in rptSecurityTypesUsed.Items)
			{
				//if this item is checked, add it to the table
				if (((CheckBox)item.FindControl("cbWasUsed")).Checked)
				{
					SecurityUsedDescriptionItem newItem = new SecurityUsedDescriptionItem();
					newItem.ID = Int32.Parse(((HiddenField)item.FindControl("hidMeansOfEntryTypeID")).Value);
					newItem.WasDefeated = ((CheckBox)item.FindControl("cbWasDefeated")).Checked;

					items.Add(newItem);
				}
			}

			return items;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}