﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReportSubNav.ascx.cs" Inherits="ReportHq.IncidentReports.ModusOperandi.Controls.ReportSubNav" %>

<!--
<div id="ReportSubNav">
	<a href="#">Area 1</a>
	<a href="#">Area 2</a>
	<span class="selected">Area 3</span>
	<a href="#">Area 4</a>				
</div>
-->


<div id="ReportSubNav">
	<asp:Label ID="lblOffendersActions" Text="Offender's Actions" CssClass="selected" Visible="false" runat="server" /><asp:LinkButton ID="lbOffendersActions" Text="Offender's Actions" ToolTip="Save your work and move here" OnClick="lbOffendersActions_Click" runat="server" />
	<asp:Label ID="lblCrimeLocation" Text="Crime Location" CssClass="selected" Visible="false" runat="server" /><asp:LinkButton ID="lbCrimeLocation" Text="Crime Location" ToolTip="Save your work and move here" OnClick="lbCrimeLocation_Click" runat="server" />
	<asp:Label ID="lblMeansOfEntry" Text="Means of Entry" CssClass="selected" Visible="false" runat="server" /><asp:LinkButton ID="lbMeansOfEntry" Text="Means of Entry" ToolTip="Save your work and move here" OnClick="lbMeansOfEntry_Click" runat="server" />
	<asp:Label ID="lblMotivation" Text="Motivation" CssClass="selected" Visible="false" runat="server" /><asp:LinkButton ID="lbMotivation" Text="Motivation" ToolTip="Save your work and move here" OnClick="lbMotivation_Click" runat="server" />
</div>