﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReportHq.IncidentReports;

namespace ReportHq.IncidentReports.ModusOperandi.Controls
{
	public partial class ReportSubNav : System.Web.UI.UserControl
	{
		#region Enums

		public enum NavSections
		{
			Motivation,
			MeansOfEntry,
			CrimeLocation,
			OffendersActions
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Properties

		private NavSections _currentSection;
		private string _targetRedirectUrl;
		private string _validationGroup;
				
		/// <summary>
		/// Which main section link the user is currently at.
		/// </summary>
		public NavSections CurrentSection
		{
			get { return _currentSection; }
			set { _currentSection = value; }
		}

		/// <summary>
		/// Where to bring the user after clicking; used by the consuming .ASPX/ascx page
		/// </summary>
		public string TargetRedirectUrl
		{
			get { return _targetRedirectUrl; }
			set { _targetRedirectUrl = value; }
		}

		public string ValidationGroup
		{
			get { return _validationGroup; }
			set { _validationGroup = value; }
		}

		
		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		
		#endregion

		#region Events

		//public delegate for LinkButton nav-item click
		public delegate void NavItemClick(object sender, EventArgs e);

		//public event for LinkButton click; will be handled by the consuming pages
		public event NavItemClick NavItemClicked;

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
		
		#region Event Handlers

		protected void Page_Load(object sender, EventArgs e)
		{
			//set validation group used by page's validator controls
			if (!string.IsNullOrEmpty(_validationGroup))
			{
				lbMotivation.ValidationGroup = _validationGroup;
				lbMeansOfEntry.ValidationGroup = _validationGroup;
				lbCrimeLocation.ValidationGroup = _validationGroup;
				lbOffendersActions.ValidationGroup = _validationGroup;
			}

			//set selected-item state
			switch (_currentSection)
			{
				case NavSections.Motivation:
					lblMotivation.Visible = true;
					lbMotivation.Visible = false;
					break;

				case NavSections.MeansOfEntry:
					lblMeansOfEntry.Visible = true;
					lbMeansOfEntry.Visible = false;
					break;

				case NavSections.CrimeLocation:
					lblCrimeLocation.Visible = true;
					lbCrimeLocation.Visible = false;
					break;

				case NavSections.OffendersActions:
					lblOffendersActions.Visible = true;
					lbOffendersActions.Visible = false;
					break;
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void lbMotivation_Click(object sender, EventArgs e)
		{
			_targetRedirectUrl = "Motivation.aspx";

			//fire event for consuming page to handler
			NavItemClicked(this, e);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void lbMeansOfEntry_Click(object sender, EventArgs e)
		{
			_targetRedirectUrl = "MeansOfEntry.aspx";

			//fire event for consuming page to handler
			NavItemClicked(this, e);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void lbCrimeLocation_Click(object sender, EventArgs e)
		{
			_targetRedirectUrl = "CrimeLocation.aspx";

			//fire event for consuming page to handler
			NavItemClicked(this, e);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void lbOffendersActions_Click(object sender, EventArgs e)
		{
			_targetRedirectUrl = "OffendersActions.aspx";

			//fire event for consuming page to handler
			NavItemClicked(this, e);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}