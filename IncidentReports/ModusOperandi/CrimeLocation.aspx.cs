﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReportHq.Common;
using ReportHq.IncidentReports;
using ReportHq.IncidentReports.Controls;
using ReportHq.IncidentReports.ModusOperandi.Controls;

namespace ReportHq.IncidentReports.ModusOperandi
{
	public partial class CrimeLocation : Bases.ReportPage
	{
		#region Variables

		private Report _report;

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Event Handlers

		protected void Page_Init(object sender, EventArgs e)
		{
			_report = base.Report; //set from base's OnInit
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void Page_Load(object sender, EventArgs e)
		{
			this.HelpUrl = "~/ModusOperandi/Help/CrimeLocation.html"; 
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void ucMajorSections_NavItemClicked(object sender, EventArgs e)
		{
			base.ProcessNavClick((sender as ReportMainNav).TargetRedirectUrl, this.UserHasWriteAccess);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void ucCrimeLocation_NavItemClicked(object sender, EventArgs e)
		{
			base.ProcessNavClick((sender as ReportSubNav).TargetRedirectUrl, this.UserHasWriteAccess);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Public Methods

		/// <summary>
		/// Sets the page's form fields to the Report's values. This overrides the method stub in the base ReportPage, and is called by the base's OnLoad() method.
		/// </summary>
		public override void SetFormValues()
		{
			ucCrimeLocation.ResidentialCheckedItems = _report.CrimeLocationResidentialItems;
			ucCrimeLocation.CommericalCheckedItems = _report.CrimeLocationCommercialItems;
			ucCrimeLocation.OutdoorAreaCheckedItems = _report.CrimeLocationOutdoorAreaItems;
			ucCrimeLocation.PublicAccessCheckedItems = _report.CrimeLocationPublicAccessItems;
			ucCrimeLocation.MovableCheckedItems = _report.CrimeLocationMovableItems;
			ucCrimeLocation.StructureTypeCheckedItems = _report.CrimeLocationStructureTypeItems;
			ucCrimeLocation.StructureStatusCheckedItems = _report.CrimeLocationStructureStatusItems;
			ucCrimeLocation.Description = _report.CrimeLocationDescription;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Saves the report. On success, updates Session copy. This overrides the method stub in the base ReportPage, and is called by the base's ProcessNavClick() public method.
		/// </summary>
		public override StatusInfo SaveReport()
		{
			_report = base.Report; //the page base ensures the report is sync'd up w/ the ViewState prior to it calling this SaveReport(). now ensure our local copy is the latest instance.

			//set new form values to report
			_report.CrimeLocationResidentialItems = ucCrimeLocation.ResidentialCheckedItems;
			_report.CrimeLocationCommercialItems = ucCrimeLocation.CommericalCheckedItems;
			_report.CrimeLocationOutdoorAreaItems = ucCrimeLocation.OutdoorAreaCheckedItems;
			_report.CrimeLocationPublicAccessItems = ucCrimeLocation.PublicAccessCheckedItems;
			_report.CrimeLocationMovableItems = ucCrimeLocation.MovableCheckedItems;
			_report.CrimeLocationStructureTypeItems = ucCrimeLocation.StructureTypeCheckedItems;
			_report.CrimeLocationStructureStatusItems = ucCrimeLocation.StructureStatusCheckedItems;
			_report.CrimeLocationDescription = ucCrimeLocation.Description;

			//update
			StatusInfo status = _report.UpdateCrimeLocation(this.Username);

			if (status.Success == true)
				base.Report = _report; //update Session copy w/ new values, but only if it saved to db. dont want to mislead the user.

			//base handles rendering errors & redirect
			return status;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}