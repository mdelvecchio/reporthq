﻿<%@ Page Title="Report HQ - Offender's Actions" Language="C#" MasterPageFile="~/IncidentReports.Master" AutoEventWireup="true" CodeBehind="OffendersActions.aspx.cs" Inherits="ReportHq.IncidentReports.ModusOperandi.OffendersActions" %>
<%@ Register TagPrefix="ReportHq" TagName="OffendersActions" src="controls/OffendersActions.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<link rel="stylesheet" href="../css/reportForm.css" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="phContent" runat="server">

	<ReportHq:ReportMainNav id="ucReportMainNav" CurrentSection="ModusOperandi" OnNavItemClicked="ucMajorSections_NavItemClicked" runat="server" />
	
	<ReportHq:OffendersActions ID="ucOffendersActions" OnNavItemClicked="ucOffendersActions_NavItemClicked" runat="server" />

</asp:Content>
