﻿<%@ Page Title="Report HQ - Motivation" Language="C#" MasterPageFile="~/IncidentReports.Master" AutoEventWireup="true" CodeBehind="Motivation.aspx.cs" Inherits="ReportHq.IncidentReports.ModusOperandi.Motivation" %>
<%@ Register TagPrefix="ReportHq" TagName="Motivation" src="controls/Motivation.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<link rel="stylesheet" href="../css/reportForm.css" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="phContent" runat="server">

	<ReportHq:ReportMainNav id="ucReportMainNav" CurrentSection="ModusOperandi" OnNavItemClicked="ucMajorSections_NavItemClicked" runat="server" />
	
	<ReportHq:Motivation ID="ucMotivation" OnNavItemClicked="ucMotivation_NavItemClicked" runat="server" />

</asp:Content>
