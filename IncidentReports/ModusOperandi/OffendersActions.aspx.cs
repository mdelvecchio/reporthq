﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReportHq.Common;
using ReportHq.IncidentReports;
using ReportHq.IncidentReports.Controls;
using ReportHq.IncidentReports.ModusOperandi.Controls;

namespace ReportHq.IncidentReports.ModusOperandi
{
	public partial class OffendersActions : Bases.ReportPage
	{
		#region Variables

		private Report _report;

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Event Handlers

		protected void Page_Init(object sender, EventArgs e)
		{
			_report = base.Report; //set from base's OnInit
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void Page_Load(object sender, EventArgs e)
		{
			this.HelpUrl = "~/ModusOperandi/Help/OffendersActions.html"; 
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void ucMajorSections_NavItemClicked(object sender, EventArgs e)
		{
			base.ProcessNavClick((sender as ReportMainNav).TargetRedirectUrl, this.UserHasWriteAccess);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void ucOffendersActions_NavItemClicked(object sender, EventArgs e)
		{
			base.ProcessNavClick((sender as ReportSubNav).TargetRedirectUrl, this.UserHasWriteAccess);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Public Methods

		/// <summary>
		/// Sets the page's form fields to the Report's values. This overrides the method stub in the base ReportPage, and is called by the base's OnLoad() method.
		/// </summary>
		public override void SetFormValues()
		{
			ucOffendersActions.ApproachCheckedItems = _report.OffendersActionsApproachItems;
			ucOffendersActions.ImpersonationCheckedItems = _report.OffendersActionsImpersonatedItems;
			ucOffendersActions.WeaponCheckedItems = _report.OffendersActionsWeaponItems;
			ucOffendersActions.FirearmFeaturesCheckedItems = _report.OffendersActionsFirearmFeatureItems;
			ucOffendersActions.PropertyCrimeCheckedItems = _report.OffendersActionsPropertyCrimeItems;
			ucOffendersActions.PersonCrimeCheckedItems = _report.OffendersActionsPersonCrimeItems;
			ucOffendersActions.SexCrimeCheckedItems = _report.OffendersActionsSexCrimeItems;
			ucOffendersActions.Description = _report.OffendersActionsDescription;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Saves the report. On success, updates Session copy. This overrides the method stub in the base ReportPage, and is called by the base's ProcessNavClick() public method.
		/// </summary>
		public override StatusInfo SaveReport()
		{
			_report = base.Report; //the page base ensures the report is sync'd up w/ the ViewState prior to it calling this SaveReport(). now ensure our local copy is the latest instance.

			//set new form values to report
			_report.OffendersActionsApproachItems = ucOffendersActions.ApproachCheckedItems;
			_report.OffendersActionsImpersonatedItems = ucOffendersActions.ImpersonationCheckedItems;
			_report.OffendersActionsWeaponItems = ucOffendersActions.WeaponCheckedItems;
			_report.OffendersActionsFirearmFeatureItems = ucOffendersActions.FirearmFeaturesCheckedItems;
			_report.OffendersActionsPropertyCrimeItems = ucOffendersActions.PropertyCrimeCheckedItems;
			_report.OffendersActionsPersonCrimeItems = ucOffendersActions.PersonCrimeCheckedItems;
			_report.OffendersActionsSexCrimeItems = ucOffendersActions.SexCrimeCheckedItems;
			_report.OffendersActionsDescription = ucOffendersActions.Description;

			//update
			StatusInfo status = _report.UpdateOffendersActions(this.Username);

			if (status.Success == true)
				base.Report = _report; //update Session copy w/ new values, but only if it saved to db. dont want to mislead the user.

			//base handles rendering errors & redirect
			return status;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}