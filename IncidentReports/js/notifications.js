﻿var uri = '/api/notifications';
var username;

//for tracking notificationIDs to mark as read
var apbNotificationItemIDs;
var systemNotificationItemIDs;
var reportNotificationItemIDs;

//counts for badge display
var newApbNotifications;
var newSystemNotifications;
var newReportNotifications;

//dunno why but these dont work when polled in functions.
//var ApbNotifications = $('#ApbNotifications');
//var ReportNotifications = $('#ReportNotifications');

//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

//on document ready, create a new function to hit our service

$(document).ready(getNotifications);

//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

function getNotifications() {

	username = document.getElementById('hidUsername').value;
	//alert(username);

	apbNotificationItemIDs = '';
	systemNotificationItemIDs = '';
	reportNotificationItemIDs = '';

	newApbNotifications = 0;
	newSystemNotifications = 0;
	newReportNotifications = 0;

	//alert($('#Notifications').is(":visible"));

	//remove old notifications
	$('#ApbNotifications').empty();
	$('#SystemNotifications').empty();
	$('#ReportNotifications').empty();
	
	getApbNotifications();
	getSystemNotifications();
	getReportNotifications();

	//repeat every X seconds (10000 = 10s)
	setTimeout(getNotifications, 30000);
}

//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

function getSystemNotifications() {

	//get ajax request to uri
	$.getJSON(uri + '/system/' + username)

		.done(function (data) {
			//alert('got data');
			//success; 'data' now contains a typed list of notifications

			var SystemNotifications = $('#SystemNotifications'); //dunno why by a global var fails, must retrieve here
			var SystemNotificationsHeader = $('#SystemNotificationsHeader')

			if (data != null && data.length > 0) //got results, render
			{
				SystemNotificationsHeader.show();
				SystemNotifications.show();

				//newSystemNotifications = data.length; //not doing since we only want non-viewed System notices. viewed-but-under-24-hours are included in results

				$.each(data, function (key, item) {

					//we do different things for viewed/unviewed notifs
					if (item.Viewed == false) {
						//only badge non-viewed items. this allows viewed-but-fresh APBs to be rendered but not badged.
						newSystemNotifications = newSystemNotifications + 1;

						//manage a list for mark-as-read. dont track .Viewed == true items.
						systemNotificationItemIDs = systemNotificationItemIDs + item.ID + ',';
					}

					//add a list item for each data item to the HTML element
					var aTag;
					if (item.url == null) {
						aTag = '<a href="#">';
					}
					else {
						aTag = '<a href="' + item.Url + '">';
					}

					$(aTag + item.Message + '</a>').appendTo(SystemNotifications);
				});

				$('#SystemNotificationItemIds').val(systemNotificationItemIDs);
				//alert($('#SystemNotificationItemIds').val());

				updateNotificationsCount();
			}
			else {
				//alert('no system results found')
				SystemNotificationsHeader.hide();
				SystemNotifications.hide();
			}
		})

	.fail(function () {
		//alert('request failed');
		$('#SystemNotifications').hide();
		$('#SystemNotificationsHeader').hide();
	})
	//.always(function () { alert('getJSON request ended'); })
	;
}

//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

function getApbNotifications() {

	//get ajax request to uri
	$.getJSON(uri + '/apb/' + username)

		.done(function (data) {
			//alert('got data');
			//success; 'data' now contains a typed list of notifications

			var ApbNotifications = $('#ApbNotifications'); //dunno why by a global var fails, must retrieve here
			var ApbNotificationsHeader = $('#ApbNotificationsHeader')

			if (data != null && data.length > 0) //got results, render
			{
				ApbNotificationsHeader.show();
				ApbNotifications.show();

				//newApbNotifications = data.length; //not doing since we only want non-viewed APBs. viewed-but-under-24-hours are included in results

				$.each(data, function (key, item) {

					//we do different things for viewed/unviewed notifs
					if (item.Viewed == false) {
						//only badge non-viewed items. this allows viewed-but-fresh APBs to be rendered but not badged.
						newApbNotifications = newApbNotifications + 1;

						//manage a list for mark-as-read. dont track .Viewed == true items.
						apbNotificationItemIDs = apbNotificationItemIDs + item.ID + ',';
					}
					
					//add a list item for each data item to the HTML element
					$('<a href="' + item.Url + '">' + item.Message + '</a>').appendTo(ApbNotifications);

				});

				$('#apbNotificationItemIDs').val(apbNotificationItemIDs);
				//alert($('#apbNotificationItemIDs').val());

				updateNotificationsCount();
			}
			else {
				//alert('no apb results found')
				ApbNotificationsHeader.hide();
				ApbNotifications.hide();
			}
		})

	.fail(function () {
		//alert('request failed');
		$('#ApbNotifications').hide();
		$('#ApbNotificationsHeader').hide();
	})
	//.always(function () { alert('getJSON request ended'); })
	;
}

//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

function getReportNotifications() {
	
	//alert(uri + '/report/' + username);

	//get ajax request to uri
	$.getJSON(uri + '/report/' + username)

		.done(function (data) {
			//alert('got data');
			//success; 'data' now contains a typed list of notifications

			var ReportNotifications = $('#ReportNotifications');
			var EmptyNotifications = $('#EmptyNotifications');

			if (data != null && data.length > 0) //got results, render
			{
				ReportNotifications.show();
				EmptyNotifications.hide();

				newReportNotifications = data.length;

				$.each(data, function (key, item) {
					
					//add a list item for each data item to the HTML element
					$('<a href="' + item.Url + '">' + item.Message + '</a>').appendTo(ReportNotifications);

					//manage a list for mark-as-read
					reportNotificationItemIDs = reportNotificationItemIDs + item.ID + ',';
				});

				$('#hidReportNotificationItemIds').val(reportNotificationItemIDs);
				//alert($('#ReportNotificationItemIds').val());

				updateNotificationsCount();
			}
			else {
				//alert('no results found')
				ReportNotifications.hide();
				EmptyNotifications.show();
			}
		})

		.fail(function () {
			//alert('request failed');
			$('#ReportNotifications').hide();
		})
		//.always(function () { alert('getJSON request ended'); })
	;
}

//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

//fired when user clicks notifications bell icon
function showNotifications()
{
	toggleMenu("#Notifications"); //toggle menu
	
	markNotificationsAsRead(3, apbNotificationItemIDs);
	markNotificationsAsRead(4, systemNotificationItemIDs);
	markNotificationsAsRead(1, reportNotificationItemIDs);
}

//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

function markNotificationsAsRead(mode, items) {
	
	var serviceMethod;

	switch (mode) {
		case 1: //report
			serviceMethod = '/setAsRead/';
			break;
		case 3: //apb
		case 4: //system
			serviceMethod = '/setGlobalAsRead/';
			break;
	}

	if (items.length > 0)
	{
		$.ajax({
			type: "POST",
			url: uri + serviceMethod + username,
			data: '=' + items, //for some reason Web API only maps complex objects, not text..unless prefixed w/ "="
			dataType: 'text',
			success: function (jqXHR, textStatus, errorThrown) {

				switch (mode) {
					case 1 : //report
						newReportNotifications = 0;
						break;
					case 3 : //apb
						newApbNotifications = 0;
						break;
					case 4 : //system
						systemNotificationItemIDs = 0;
						break;
				}

				updateNotificationsCount();
			},
			error: function (jqXHR, textStatus, errorThrown) {
				//alert('global mark as read failed');
			}
		});
	}
}

//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

function updateNotificationsCount() {
	var total = newReportNotifications + newApbNotifications + newSystemNotifications;

	var spanNotificationsCount = $('#NotificationsCount');

	if (total > 0) {
		spanNotificationsCount.text(total);
		spanNotificationsCount.show();
	} else {
		spanNotificationsCount.hide();
	}
}