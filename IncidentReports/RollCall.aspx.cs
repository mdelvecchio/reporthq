﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ReportHq.IncidentReports
{
	public partial class RollCall : Bases.NormalPage
	{
		#region Event Handlers

		protected void Page_Load(object sender, EventArgs e)
		{
			this.Heading = "Roll Call";
			this.HelpUrl = "~/Help/RollCall.html";

			//if (this.UserIsSupervisor)
			//	lnkCreateBulletin.Visible = true;

			SetBulletinTables();
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Private Methods

		private void SetBulletinTables()
		{
			//TODO: whats faster -- 1) a dataset of child tables, or 2) a collection of objects and using LINQ to filter sub-groups on bind? ask stackoverflow
			DataSet results = Bulletin.GetBulletinsByUsername(this.Username);

			if (results.Tables.Count > 0)
			{
				DataTable dtBolo = results.Tables[0];
				DataTable dtPersonnel = results.Tables[1];
				DataTable dtPolicy = results.Tables[2];
				DataTable dtMemo = results.Tables[3];

				
				//bolo
				if (dtBolo.Rows.Count > 0)
				{
					rptBolo.DataSource = dtBolo;
					rptBolo.DataBind();
				}
				else
				{
					litNoneBolo.Visible = true;
				}


				//personnel
				if (dtPersonnel.Rows.Count > 0)
				{
					rptPersonnel.DataSource = dtPersonnel;
					rptPersonnel.DataBind();
				}
				else
				{
					litNonePersonnel.Visible = true;
				}


				//policy
				if (dtPolicy.Rows.Count > 0)
				{
					rptPolicy.DataSource = dtPolicy;
					rptPolicy.DataBind();
				}
				else
				{
					litNonePolicy.Visible = true;
				}


				//memo
				if (dtMemo.Rows.Count > 0)
				{
					rptMemo.DataSource = dtMemo;
					rptMemo.DataBind();
				}
				else
				{
					litNoneMemo.Visible = true;
				}

			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}