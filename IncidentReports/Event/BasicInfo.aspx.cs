﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReportHq.Common;
using ReportHq.IncidentReports;
using ReportHq.IncidentReports.Controls;
using ReportHq.IncidentReports.Event.Controls;

namespace ReportHq.IncidentReports.Event
{
	public partial class BasicInfo : Bases.ReportPage
	{
		#region Variables

		private Report _report;

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Event Handlers

		protected void Page_Init(object sender, EventArgs e)
		{
			_report = base.Report; //set from base's OnInit
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void Page_Load(object sender, EventArgs e)
		{
			this.HelpUrl = "~/Event/Help/BasicInfo.html"; 

			if (!base.UserHasWriteAccess)
				ucEvent.DisableValidators = true;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void ucMajorSections_NavItemClicked(object sender, EventArgs e)
		{
			base.ProcessNavClick((sender as ReportMainNav).TargetRedirectUrl, this.UserHasWriteAccess);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void ucBasicInfo_NavItemClicked(object sender, EventArgs e)
		{
			base.ProcessNavClick((sender as ReportSubNav).TargetRedirectUrl, this.UserHasWriteAccess);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		
		#endregion

		#region Public Methods

		/// <summary>
		/// Sets the page's form fields to the Report's values. This overrides the method stub in the base ReportPage, and is called by the base's OnLoad() method.
		/// </summary>
		public override void SetFormValues()
		{
			//if user has changed the signaltype for this report, reset its ucr offense type too.
			//if (_report.SignalTypeID != ucEvent.SignalTypeID)
			//	_report.UcrOffenseTypeID = -1;

			ucEvent.SignalTypeID = _report.SignalTypeID;
			ucEvent.IncidentDescription = _report.IncidentDescription;
			ucEvent.Location = _report.Location;
			ucEvent.District = _report.District;
			ucEvent.Zone = _report.Zone;
			ucEvent.SubZone = _report.SubZone;
			ucEvent.OccurredDateTime = _report.OccurredDateTime;
			ucEvent.ReportDateTime = _report.ReportDateTime;
			ucEvent.LightingConditionTypeID = _report.LightingConditionTypeID;
			ucEvent.StatusTypeID = _report.StatusTypeID;
			//ucEvent.BulletinRequired = _report.BulletinRequired;
			//ucEvent.BulletinNumber = _report.BulletinNumber;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Saves the report. On success, updates Session copy. This overrides the method stub in the base ReportPage, and is called by the base's ProcessNavClick() public method.
		/// </summary>
		public override StatusInfo SaveReport()
		{
			_report = base.Report; //the page base ensures the report is sync'd up w/ the ViewState prior to it calling this SaveReport(). now ensure our local copy is the latest instance.

			//set new form values
			_report.SignalTypeID = ucEvent.SignalTypeID;
			_report.IncidentDescription = ucEvent.IncidentDescription;
			_report.Location = ucEvent.Location;
			_report.District = ucEvent.District;
			_report.Zone = ucEvent.Zone;
			_report.SubZone = ucEvent.SubZone;
			_report.OccurredDateTime = ucEvent.OccurredDateTime;
			_report.ReportDateTime = ucEvent.ReportDateTime;
			_report.LightingConditionTypeID = ucEvent.LightingConditionTypeID;
			_report.StatusTypeID = ucEvent.StatusTypeID;

			//update
			StatusInfo status = _report.UpdateEvent(this.Username);

			if (status.Success == true)
				base.Report = _report; //update Session copy w/ new values, but only if it saved to db. dont want to mislead the user.
			
			return status;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}