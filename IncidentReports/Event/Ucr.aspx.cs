﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReportHq.Common;
using ReportHq.IncidentReports;
using ReportHq.IncidentReports.Bases;
using ReportHq.IncidentReports.Controls;
using ReportHq.IncidentReports.Event.Controls;

namespace ReportHq.IncidentReports.Event
{
	public partial class Ucr : Bases.ReportPage
	{
		#region Variables

		private Report _report;

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Event Handlers

		protected void Page_Init(object sender, EventArgs e)
		{
			_report = base.Report; //set from base's OnInit
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void Page_Load(object sender, EventArgs e)
		{
			this.HelpUrl = "~/Event/Help/Ucr.html"; 

			//if (!base.UserHasWriteAccess)
			//	ucEvent.DisableValidators = true;

			ucUcr.OffenseType = (UcrOffense.OffenseTypes)_report.UcrOffenseTypeID;
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void ucMajorSections_NavItemClicked(object sender, EventArgs e)
		{
			base.ProcessNavClick((sender as ReportMainNav).TargetRedirectUrl, this.UserHasWriteAccess);
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void ucUcr_NavItemClicked(object sender, EventArgs e)
		{
			base.ProcessNavClick((sender as ReportSubNav).TargetRedirectUrl, this.UserHasWriteAccess);
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		
		#endregion

		#region Public Methods

		/// <summary>
		/// Sets the page's form fields to the Report's values. This overrides the method stub in the base ReportPage, and is called by the base's OnLoad() method.
		/// </summary>
		public override void SetFormValues()
		{
			UcrOffense ucrOffense = new UcrOffense(_report.ID);

			//burglary
			ucUcr.BurglaryCrimeLocation = ucrOffense.BurglaryCrimeLocation;
			ucUcr.BurglaryForceUsed = ucrOffense.BurglaryForceUsed;

			//larceny
			ucUcr.LarcenyNature = ucrOffense.LarcenyNature;
			
			//these arent form values -- and since SetFormValues() only gets called !IsPostBack, these will lose their state every time unless stored in a form field (like a hidden field)
			//ucUcr.OffenseType = (UcrOffense.OffenseTypes)_report.UcrOffenseTypeID;
			//ucUcr.CategoryID = _report.UcrCategoryID; //currently not form set; could store in hidden
			//ucUcr.OffenseTypeID = _report.UcrOffenseTypeID; //currently not form set; could store in hidden
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Saves the report. On success, updates Session copy. This overrides the method stub in the base ReportPage, and is called by the base's ProcessNavClick() public method.
		/// </summary>
		public override StatusInfo SaveReport()
		{
			//_report = base.Report; //the page base ensures the report is sync'd up w/ the ViewState prior to it calling this SaveReport(). now ensure our local copy is the latest instance.

			//set new form values

			if (ucUcr.OffenseType > 0)
			{
				//UcrOffense ucrOffense = new UcrOffense(_report.ID);
				UcrOffense ucrOffense = new UcrOffense(); //instantiating new causes any other db-saved values not of this-ucr-offense-type to become null when saved

				switch (ucUcr.OffenseType)
				{
					case UcrOffense.OffenseTypes.Burglary:
						ucrOffense.BurglaryTimeOfDay = ucUcr.BurglaryTimeOfDay;
						ucrOffense.BurglaryCrimeLocation = ucUcr.BurglaryCrimeLocation;
						ucrOffense.BurglaryForceUsed = ucUcr.BurglaryForceUsed;

						//TODO? set calculated items into the UcrOffense object? or have db do that in update proc? db.

						//old idea.
						//UcrBurglary ucrOffense = new UcrBurglary();
						//ucrOffense.TimeOfDay = ucUcr.BurglaryTimeOfDay;
						//ucrOffense.CrimeLocation = ucUcr.BurglaryCrimeLocation;
						//ucrOffense.ForceUsed = ucUcr.BurglaryForceUsed;

						//status = ucrOffense.Update(_report.ID, this.Username);
						break;

					case UcrOffense.OffenseTypes.LarcenyTheft:
						ucrOffense.LarcenyNature = ucUcr.LarcenyNature;
						break;
				}

				//save it
				return ucrOffense.Update(_report.ID, this.Username);

				//not needed since ucr details arent stored in session report copy
				//if (status.Success == true)
				//	base.Report = _report; //update Session copy w/ new values
				//return status;
			}
			else
			{
				//this report's Signal doesn't correspond to a UCR offense type, so no save.
				return new StatusInfo(true);
			}

			
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}