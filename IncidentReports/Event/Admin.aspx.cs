﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReportHq.Common;
using ReportHq.IncidentReports;
using ReportHq.IncidentReports.Controls;
using ReportHq.IncidentReports.Event.Controls;

namespace ReportHq.IncidentReports.Event
{
	public partial class Admin : Bases.ReportPage
	{
		#region Variables

		private Report _report;

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Event Handlers

		protected void Page_Init(object sender, EventArgs e)
		{
			_report = base.Report; //set from base's OnInit
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void Page_Load(object sender, EventArgs e)
		{
			this.HelpUrl = "~/Event/Help/Admin.html"; 

			if (!base.UserHasWriteAccess)
				ucAdmin.DisableValidators = true;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void ucMajorSections_NavItemClicked(object sender, EventArgs e)
		{
			base.ProcessNavClick((sender as ReportMainNav).TargetRedirectUrl, this.UserHasWriteAccess);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void ucAdmin_NavItemClicked(object sender, EventArgs e)
		{
			base.ProcessNavClick((sender as ReportSubNav).TargetRedirectUrl, this.UserHasWriteAccess);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Public Methods

		/// <summary>
		/// Sets the page's form fields to the Report's values. This overrides the method stub in the base ReportPage, and is called by the base's OnLoad() method.
		/// </summary>
		public override void SetFormValues()
		{
			//bool hitAdForMetaData = (base.UserHasWriteAccess && _report.IsDataEntry == false); //we only want to hit AD if user has write-access and report isnt a Data Entry (since DE persons aren't the actual report author)
			bool hitAdForMetaData = base.UserHasWriteAccess; //we only want to hit AD if user has write-access. per NOPD do for DE now too
			string userAdEmail = ActiveDirectory.GetAdEmail();

			//officer name
			if (!string.IsNullOrEmpty(_report.Officer1Name))
			{
				ucAdmin.Officer1Name = _report.Officer1Name;
			}
			else
			{
				if (hitAdForMetaData)
				{
					//no Officer1Name set, so get it from AD
					ucAdmin.Officer1Name = ActiveDirectory.GetAdDisplayName(userAdEmail).ToUpper();
				}
			}

			//officer ID
			if (_report.Officer1EmployeeID > 0)
			{
				ucAdmin.Officer1EmployeeID = _report.Officer1EmployeeID;
			}
			else
			{
				if (hitAdForMetaData)
				{
					//no Officer1EmployeeID set, so get it from AD
					string employeeID = ActiveDirectory.GetAdEmployeeID(userAdEmail);

					if (!string.IsNullOrEmpty(employeeID))
						ucAdmin.Officer1EmployeeID = Int32.Parse(employeeID);
				}
			}

			ucAdmin.Officer1Badge = _report.Officer1Badge;
			ucAdmin.Officer1Rank = _report.Officer1RankTypeID;
			ucAdmin.Officer2Name = _report.Officer2Name;
			ucAdmin.Officer2Badge = _report.Officer2Badge;
			ucAdmin.Officer2Rank = _report.Officer2RankTypeID;

			//in cases where there is no ReportingCarNumber, NOPD represents this w/ an officer's Badge Number preceeded w/ a "B".
			if (_report.ReportingCarNumber < 1 && _report.Officer1Badge > 0)
				ucAdmin.ReportingCarNumber = "B" + _report.Officer1Badge;
			else if (_report.ReportingCarNumber > 0) //dont set if the ReportingCarNumber hasnt been defined (it renders as "0")
				ucAdmin.ReportingCarNumber = _report.ReportingCarNumber.ToString();

			ucAdmin.ReportingCarPlatoon = _report.ReportingCarPlatoon;
			ucAdmin.ReportingAssignmentUnit = _report.ReportingAssignmentUnitTypeID;
			ucAdmin.DetectiveName = _report.DetectiveName;
			ucAdmin.CrimeLabName = _report.CrimeLabName;
			ucAdmin.OtherName = _report.OtherName;			
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Saves the report. On success, updates Session copy. This overrides the method stub in the base ReportPage, and is called by the base's ProcessNavClick() public method.
		/// </summary>
		public override StatusInfo SaveReport()
		{
			_report = base.Report; //the page base ensures the report is sync'd up w/ the ViewState prior to it calling this SaveReport(). now ensure our local copy is the latest instance.

			//set new form values
			_report.Officer1Name = ucAdmin.Officer1Name;
			_report.Officer1Badge = ucAdmin.Officer1Badge;
			_report.Officer1EmployeeID = ucAdmin.Officer1EmployeeID;
			_report.Officer1RankTypeID = ucAdmin.Officer1Rank;
			_report.Officer2Name = ucAdmin.Officer2Name;
			_report.Officer2Badge = ucAdmin.Officer2Badge;
			_report.Officer2RankTypeID = ucAdmin.Officer2Rank;

			//a badge-as-car-number (no car number) is represented by "B" and the BadgeNumber. not really the car num so dont update the car field in db.			
			if (ucAdmin.ReportingCarNumber.Substring(0, 1).ToUpper() == "B")
				_report.ReportingCarNumber = -1; //badge-as-carnumber; clear CarNumber in db
			else
				_report.ReportingCarNumber = Int32.Parse(ucAdmin.ReportingCarNumber); //normal CarNumber
		
			_report.ReportingCarPlatoon = ucAdmin.ReportingCarPlatoon;
			_report.ReportingAssignmentUnitTypeID = ucAdmin.ReportingAssignmentUnit;
			_report.DetectiveName = ucAdmin.DetectiveName;
			_report.CrimeLabName = ucAdmin.CrimeLabName;
			_report.OtherName = ucAdmin.OtherName;

			//update
			StatusInfo status = _report.UpdateAdmin(this.Username);
			
			if (status.Success == true)
				base.Report = _report; //update Session copy w/ new values

			return status;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}