﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReportSubNav.ascx.cs" Inherits="ReportHq.IncidentReports.Event.Controls.ReportSubNav" %>

<div id="ReportSubNav">
	<asp:Label ID="lblAdmin" Text="Admin" CssClass="selected" Visible="false" runat="server" /><asp:LinkButton ID="lbAdmin" Text="Admin" ToolTip="Save your work and move here" OnClick="lbAdmin_Click" runat="server" />
	<asp:Label ID="lblUcr" Text="UCR" CssClass="selected" Visible="false" runat="server" /><asp:LinkButton ID="lbUcr" Text="UCR" ToolTip="Save your work and move here" OnClick="lbUcr_Click" runat="server" />
	<asp:Label ID="lblBasicInfo" Text="Basic Info" CssClass="selected" Visible="false" runat="server" /><asp:LinkButton ID="lbBasicInfo" Text="Basic Info" ToolTip="Save your work and move here" OnClick="lbBasicInfo_Click" runat="server" />
</div>