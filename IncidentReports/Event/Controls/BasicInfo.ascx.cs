﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReportHq.IncidentReports.Controls;
using Telerik.Web.UI;

namespace ReportHq.IncidentReports.Event.Controls
{
	public partial class BasicInfo : System.Web.UI.UserControl
	{
		#region Properties

		private bool _disableValidators = false;

		public int SignalTypeID
		{
			get
			{
				if (!string.IsNullOrEmpty(rcbSignals.SelectedValue))
					return Int32.Parse(rcbSignals.SelectedValue);
				else
					return -1;
			}
			set
			{
				if (value > 0)
					rcbSignals.SelectedValue = value.ToString();
			}
		}

		public string IncidentDescription
		{
			get { return tbIncidentDescription.Text; }
			set { tbIncidentDescription.Text = value; }
		}

		public string Location
		{
			get { return tbLocation.Text; }
			set { tbLocation.Text = value; }
		}

		public int District
		{
			get
			{
				if (!string.IsNullOrEmpty(rcbDistricts.SelectedValue))
					return Int32.Parse(rcbDistricts.SelectedValue);
				else
					return -1;
			}
			set
			{
				if (value > 0)
					rcbDistricts.SelectedValue = value.ToString();
			}
		}

		public string Zone
		{
			get { return rcbZones.SelectedValue; }
			set { rcbZones.SelectedValue = value; }
		}

		public string SubZone
		{
			get { return rcbSubZones.SelectedValue; }
			set { rcbSubZones.SelectedValue = value; }
		}

		public DateTime OccurredDateTime
		{
			get
			{
				string userDate = "1/1/1900";
				string userTime = "12:00:00 AM";

				//set the date portion
				if (rdpOccurredDate.SelectedDate != null)
					userDate = ((DateTime)rdpOccurredDate.SelectedDate).ToShortDateString();

				//set the time portion
				if (rdiOccurredTime.SelectedDate != null)
					userTime = ((DateTime)rdiOccurredTime.SelectedDate).ToShortTimeString();

				return DateTime.Parse(userDate + " " + userTime);
			}

			set
			{
				if (value > new DateTime(1900, 1, 1))
				{
					rdpOccurredDate.SelectedDate = value;
					rdiOccurredTime.SelectedDate = value;
				}
			}
		}

		public DateTime ReportDateTime
		{
			get
			{
				string userDate = "1/1/1900";
				string userTime = "12:00:00 AM";

				//set the date portion
				if (rdpReportDate.SelectedDate != null)
					userDate = ((DateTime)rdpReportDate.SelectedDate).ToShortDateString();

				//set the time portion
				if (rdiReportTime.SelectedDate != null)
					userTime = ((DateTime)rdiReportTime.SelectedDate).ToShortTimeString();

				return DateTime.Parse(userDate + " " + userTime);
			}

			set
			{
				if (value > new DateTime(1900, 1, 1))
				{
					rdpReportDate.SelectedDate = value;
					rdiReportTime.SelectedDate = value;
				}
			}
		}

		public int LightingConditionTypeID
		{
			get
			{
				if (!string.IsNullOrEmpty(rcbLightingTypes.SelectedValue))
					return Int32.Parse(rcbLightingTypes.SelectedValue);
				else
					return -1;
			}
			set
			{
				if (value > 0)
					rcbLightingTypes.SelectedValue = value.ToString();
			}
		}

		public int StatusTypeID
		{
			get
			{
				if (!string.IsNullOrEmpty(rcbReportStatusTypes.SelectedValue))
					return Int32.Parse(rcbReportStatusTypes.SelectedValue);
				else
					return -1;
			}
			set
			{
				if (value > 0)
					rcbReportStatusTypes.SelectedValue = value.ToString();
			}
		}

		//public bool BulletinRequired
		//{
		//	get { return Convert.ToBoolean(rblBulletinRequired.SelectedValue); }
		//	set
		//	{
		//		rblBulletinRequired.SelectedValue = value.ToString();

		//		rntbBulletinNumber.Enabled = value;
		//	}
		//}

		//public int BulletinNumber
		//{
		//	get
		//	{
		//		if (rntbBulletinNumber.Value > 0)
		//			return Convert.ToInt32(rntbBulletinNumber.Value);
		//		else
		//			return -1;
		//	}
		//	set
		//	{
		//		if (value > 0)
		//			rntbBulletinNumber.Value = value;
		//	}
		//}


		public bool DisableValidators
		{
			get { return _disableValidators; }
			set { _disableValidators = value; }
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Events

		//public delegate for LinkButton nav-item click
		public delegate void NavItemClick(object sender, EventArgs e);

		//public event for LinkButton click; will be handled by the consuming pages
		public event NavItemClick NavItemClicked;

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Event Handlers

		protected void Page_Init(object sender, EventArgs e)
		{
			//usually a webform's dropdownlists are populated in the Page_Load in the !IsPostBack() check. however the viewstate hidden HTML variable is getting too fat w/ all these in it, 
			//slows things down. so instead we're binding these dropdownlists in the Page_Init *every* page load. awful, right? well the good news is the BAL caches these datatables on the 
			//web server, so it doesnt actually have to hit the db every time. also, the db server is likely near the web server so even on refreshes the hit isnt too bad. and because
			//they are rendered as normal HTML controls, the selected state of these lists is remembered on postback and doesnt need the viewstate collection.

			BindFormLists();
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void Page_Load(object sender, EventArgs e)
		{

		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void ucReportSubNav_NavItemClicked(object sender, EventArgs e)
		{
			//raise event which the page will handle. (TODO: is there a better way to bubble-up downstream control events?)

			//fire event for consuming page to handler
			NavItemClicked(sender, e);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		
		/// <summary>
		/// Format the RadCombBox's items with multiple columns.
		/// </summary>
		protected void rcbSignals_ItemDataBound(object sender, RadComboBoxItemEventArgs e)
		{
			DataRowView rowView = (DataRowView)e.Item.DataItem;

			e.Item.Text = String.Format("{0} - {1}", rowView["Code"], rowView["Description"]);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Private Methods

		/// <summary>
		/// Binds Report HQ's list items to the form controls. Not the actual form values, just the list items.
		/// </summary>
		private void BindFormLists()
		{
			//signals
			rcbSignals.DataSource = SignalType.GetSignalTypes(true);
			rcbSignals.DataBind();
			rcbSignals.Items.Insert(0, new RadComboBoxItem());

			//districts
			rcbDistricts.DataSource = GenericLookups.GetDistricts();
			rcbDistricts.DataBind();
			rcbDistricts.Items.Insert(0, new RadComboBoxItem());

			//zones
			rcbZones.DataSource = GenericLookups.GetZones();
			rcbZones.DataBind();
			rcbZones.Items.Insert(0, new RadComboBoxItem());

			//subzones
			rcbSubZones.DataSource = GenericLookups.GetSubZones();
			rcbSubZones.DataBind();
			rcbSubZones.Items.Insert(0, new RadComboBoxItem());

			//lighting types
			rcbLightingTypes.DataSource = Report.GetLightingTypes();
			rcbLightingTypes.DataBind();
			rcbLightingTypes.Items.Insert(0, new RadComboBoxItem());

			//status types
			rcbReportStatusTypes.DataSource = Report.GetReportStatusTypes();
			rcbReportStatusTypes.DataBind();
			rcbReportStatusTypes.Items.Insert(0, new RadComboBoxItem());
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}