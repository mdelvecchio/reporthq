﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Admin.ascx.cs" Inherits="ReportHq.IncidentReports.Event.Controls.Admin" %>
<%@ Register TagPrefix="ReportHq" TagName="ReportSubNav" src="ReportSubNav.ascx" %>

<ReportHq:ReportSubNav ID="ucReportSubNav" CurrentSection="Admin" OnNavItemClicked="ucReportSubNav_NavItemClicked" runat="server" />

<table class="formBox" border="0">
	<tr>
		<td class="label">Rank:<span class="required">*</span></td>
		<td>
			<Telerik:RadComboBox ID="rcbOfficer1Rank" DataValueField="RankTypeID" DataTextField="Description" Width="180" EnableViewState="false" ShowToggleImage="true" MarkFirstMatch="true" OnClientFocus="showDropDown" TabIndex="1" runat="server" />
			<asp:RequiredFieldValidator ID="rfvOfficer1Rank" ControlToValidate="rcbOfficer1Rank" ValidationGroup="admin" ErrorMessage="Reporting Officer Rank" Display="None" runat="server" />
		</td>
		<td></td>
		<td class="label">Rank:</td>
		<td><Telerik:RadComboBox ID="rcbOfficer2Rank" DataValueField="RankTypeID" DataTextField="Description" Width="180" EnableViewState="false" ShowToggleImage="true" MarkFirstMatch="true" OnClientFocus="showDropDown" TabIndex="4" runat="server" /></td>
	</tr>
	<tr>
		<td class="label">Officer: <span class="required">*</span></td>
		<td>
			<asp:TextBox ID="tbOfficer1Name" MaxLength="50" Width="176" TabIndex="2" runat="server" />
			<asp:RequiredFieldValidator ID="rfvOfficer1Name" ControlToValidate="tbOfficer1Name" ValidationGroup="admin" ErrorMessage="Reporting Officer Name" Display="None" runat="server" />
		</td>
		<td><img src="/1.gif" width="25" height="1" /></td>
		<td class="label">Officer 2:</td>
		<td><asp:TextBox ID="tbOfficer2Name" MaxLength="50" runat="server" Width="176" TabIndex="5" /></td>
	</tr>	
	<tr>
		<td class="label">Badge #: <span class="required">*</span></td>
		<td>
			<Telerik:RadNumericTextBox ID="rntbOfficer1Badge" type="Number" MaxLength="5" MinValue="1" MaxValue="10999" NumberFormat-DecimalDigits="0" NumberFormat-GroupSeparator="" Width="75" TabIndex="3" runat="server" />
			<asp:RequiredFieldValidator ID="rfvOfficer1Badge" ControlToValidate="rntbOfficer1Badge" ValidationGroup="admin" ErrorMessage="Reporting Officer Badge" Display="None" runat="server" />
		</td>
		<td></td>
		<td class="label">Badge #:</td>
		<td><Telerik:RadNumericTextBox ID="rntbOfficer2Badge" type="Number" MinValue="1" MaxValue="10999" NumberFormat-DecimalDigits="0" NumberFormat-GroupSeparator="" Width="75" TabIndex="6" runat="server" /></td>
	</tr>
	<tr>
		<td class="label">Employee #: <span class="required">*</span></td>
		<td colspan="4">
			<Telerik:RadNumericTextBox ID="rntbOfficer1EmployeeID" type="Number" MaxLength="5" MinValue="1" MaxValue="99999" NumberFormat-DecimalDigits="0" NumberFormat-GroupSeparator="" Width="75" ReadOnly="false" Enabled="true" TabIndex="4" runat="server" />
			<asp:RequiredFieldValidator ID="rfvEmployeeID" ControlToValidate="rntbOfficer1EmployeeID" ValidationGroup="admin" ErrorMessage="Employee #" Display="None" runat="server" />		
		</td>
	</tr>
	<tr>
		<td class="label">Car #: <span class="required">*</span></td>
		<td colspan="4">
			<asp:TextBox ID="tbReportingCar" MaxLength="6" Width="71" TabIndex="7" runat="server" />
			<asp:RequiredFieldValidator ID="rfvReportingCar" ControlToValidate="tbReportingCar" ValidationGroup="admin" ErrorMessage="Reporting Car #" Display="None" runat="server" />		
			<asp:CustomValidator ID="cvReportingCar" ControlToValidate="tbReportingCar" ValidationGroup="admin" ErrorMessage="Invalid Reporting Car #" Display="None" OnServerValidate="ValidateCarNumber" ClientValidationFunction="validateCarNumber" runat="server" />
		</td>
	</tr>
	<tr>
		<td class="label">Platoon: <span class="required">*</span></td>
		<td colspan="4">
			<Telerik:RadComboBox ID="rcbPlatoons" DataValueField="PlatoonType" DataTextField="PlatoonType" Width="75" EnableViewState="false" ShowToggleImage="true" MarkFirstMatch="true" OnClientFocus="showDropDown" TabIndex="8" runat="server" />
			<asp:RequiredFieldValidator ID="rfvPlatoon" ControlToValidate="rcbPlatoons" ValidationGroup="admin" ErrorMessage="Platoon" Display="None" runat="server" />
		</td>
	</tr>	
	<tr>
		<td class="label">Assignment: <span class="required">*</span></td>
		<td colspan="4">
			<Telerik:RadComboBox ID="rcbReportingAssignment" DataValueField="AssignmentUnitTypeID" DataTextField="Description" Width="180" EnableViewState="false" ShowToggleImage="true" MarkFirstMatch="true" OnClientFocus="showDropDown" TabIndex="9" runat="server" />
			<asp:RequiredFieldValidator ID="rfvReportingAssignment" ControlToValidate="rcbReportingAssignment" ValidationGroup="admin" ErrorMessage="Reporting Assignment" Display="None" runat="server" />
		</td>
	</tr>	
	<tr>
		<td class="label">Detective:</td>
		<td colspan="4"><asp:TextBox ID="tbDetectiveName" MaxLength="50" Width="176" TabIndex="10" runat="server" /></td>
	</tr>	
	<tr>
		<td class="label">Crime Lab:</td>
		<td colspan="4"><asp:TextBox ID="tbCrimeLabName" MaxLength="50" Width="176" TabIndex="11" runat="server" /></td>
	</tr>
	<tr>
		<td class="label">Other:</td>
		<td colspan="4"><asp:TextBox ID="tbOtherName" MaxLength="50" Width="176" TabIndex="12" runat="server" /></td>
	</tr>	
</table>

<asp:ValidationSummary ID="validationSummary" ValidationGroup="admin" DisplayMode="bulletList" CssClass="validationSummary" HeaderText="Required Fields:" EnableClientScript="true" runat="server" />


<script>
	//provides client-side validation for CustomValidator "cvReportingCar"
	function validateCarNumber(sender, e)
	{
		var carNumber = e.Value;
		var isValid = false;
		var isBadgeAsCarNumber = false;
		
		//valid Car Numbers are between 1-10999. if there is no car number, users can optionally use a "B" in front 
		//of their Badge Number to designate this.
		
		//strip out a possible preceeding "B"
		if (carNumber.indexOf('B') > -1)
		{
			isBadgeAsCarNumber = true;
			carNumber = replace(carNumber, 'B', '');			
		}
		
		var theNum = carNumber;		
		
		if (theNum >= 1 && theNum <= 10999) {
			isValid = true;
		}
		
		//if a Bxxxxx value (badge-as-carnumber), make sure the number matches the page's BadgeNumber field
		if (isBadgeAsCarNumber)
		{
			var rntbOfficer1Badge = $find('<%= rntbOfficer1Badge.ClientID %>');
			
			if (theNum != rntbOfficer1Badge.get_value()) {
				isValid = false;
			}
		}
	
		e.IsValid = isValid;		
	}	
</script>