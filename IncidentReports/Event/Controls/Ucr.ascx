﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Ucr.ascx.cs" Inherits="ReportHq.IncidentReports.Event.Controls.Ucr" %>
<%@ Register TagPrefix="ReportHq" TagName="ReportSubNav" src="ReportSubNav.ascx" %>

<ReportHq:ReportSubNav ID="ucReportSubNav" CurrentSection="Ucr" OnNavItemClicked="ucReportSubNav_NavItemClicked" runat="server" />

<div class="formBox">

	<telerik:RadAjaxPanel ID="radAjaxPanel" runat="server">

		<asp:Panel ID="panelCriminalHomicide" runat="server" Visible="false">
			<table border="0">
				<tr>
					<td class="label" style="width: 100px;">Homicide 1:</td>
					<td>
						<asp:TextBox ID="TextBox1" runat="server" />
					</td>
				</tr>
				<tr>
					<td class="label">Homicide 2:</td>
					<td>
						<asp:TextBox ID="TextBox2" runat="server" />
					</td>
				</tr>
				<tr>
					<td class="label">Homicide 3:</td>
					<td>
						<asp:TextBox ID="TextBox3" runat="server" />
					</td>
				</tr>
			</table>
		</asp:Panel>


		<asp:Panel ID="panelForcibleRape" runat="server" Visible="false">
			<table border="0">
				<tr>
					<td class="label" style="width: 100px;">Rape 1:</td>
					<td>
						<asp:TextBox ID="TextBox4" runat="server" />
					</td>
				</tr>
				<tr>
					<td class="label">Rape 2:</td>
					<td>
						<asp:TextBox ID="TextBox5" runat="server" />
					</td>
				</tr>
				<tr>
					<td class="label">Rape 3:</td>
					<td>
						<asp:TextBox ID="TextBox6" runat="server" />
					</td>
				</tr>
			</table>
		</asp:Panel>


		<asp:Panel ID="panelRobbery" runat="server" Visible="false">
			<table border="0">
				<tr>
					<td class="label" style="width: 100px;">Robbery 1:</td>
					<td>
						<asp:TextBox ID="TextBox7" runat="server" />
					</td>
				</tr>
				<tr>
					<td class="label">Robbery 2:</td>
					<td>
						<asp:TextBox ID="TextBox8" runat="server" />
					</td>
				</tr>
				<tr>
					<td class="label">Robbery 3:</td>
					<td>
						<asp:TextBox ID="TextBox9" runat="server" />
					</td>
				</tr>
			</table>
		</asp:Panel>


		<asp:Panel ID="panelAggravatedAssault" runat="server" Visible="false">
			<table border="0">
				<tr>
					<td class="label" style="width: 100px;">Assault 1:</td>
					<td>
						<asp:TextBox ID="TextBox10" runat="server" />
					</td>
				</tr>
				<tr>
					<td class="label">Assault 2:</td>
					<td>
						<asp:TextBox ID="TextBox11" runat="server" />
					</td>
				</tr>
				<tr>
					<td class="label">Assault 3:</td>
					<td>
						<asp:TextBox ID="TextBox12" runat="server" />
					</td>
				</tr>
			</table>
		</asp:Panel>


		<!-- burglarly -->
		<asp:Panel ID="panelBurglary" runat="server" Visible="false">
			<table border="0">
				<tr>
					<td class="label" style="width: 100px;">UCR Offense Type:</td>
					<td><asp:Literal ID="litBurglaryType" runat="server" /></td>
				</tr>
				<tr>
					<td class="label" style="width: 100px;">Time of Day:</td>
					<td><asp:Literal ID="litTimeOfDay" runat="server" /></td>
				</tr>
				<tr>
					<td class="label">Crime Location:</td>
					<td>
						<telerik:RadComboBox ID="rcbBurglaryCrimeLocation" Width="150" EnableViewState="true" ShowToggleImage="true" AllowCustomText="False" MarkFirstMatch="true" runat="server">
							<Items>
								<telerik:RadComboBoxItem />
								<telerik:RadComboBoxItem Value="RESIDENCE" Text="RESIDENCE" />
								<telerik:RadComboBoxItem Value="NON-RESIDENCE" Text="NON-RESIDENCE" />
							</Items>
						</telerik:RadComboBox>
					</td>
				</tr>
				<tr>
					<td class="label">Force:</td>
					<td>
						<telerik:RadComboBox ID="rcbBurglaryForceUsed" Width="150" EnableViewState="true" ShowToggleImage="true" AllowCustomText="False" MarkFirstMatch="true" runat="server">
							<Items>
								<telerik:RadComboBoxItem />
								<telerik:RadComboBoxItem Value="FORCE USED" Text="FORCE USED" />
								<telerik:RadComboBoxItem Value="NO FORCE USED" Text="NO FORCE USED" />
								<telerik:RadComboBoxItem Value="ATTEMPTED FORCE" Text="ATTEMPTED FORCE" />
							</Items>
						</telerik:RadComboBox>
					</td>
				</tr>
				<tr>
					<td class="label" style="width: 100px;">Stolen Value:</td>
					<td><asp:Literal ID="litBurglaryStolenValue" runat="server" /></td>
				</tr>
				<tr>
					<td class="label" style="width: 100px;">Recovered Value:</td>
					<td><asp:Literal ID="litBurglaryRecoveredValue" runat="server" /></td>
				</tr>
				<tr>
					<td class="label" style="width: 100px;">Offenders Arrested:</td>
					<td><asp:Literal ID="litBurglaryOffenders" runat="server" /></td>
				</tr>
			</table>

			<p>See Property secection to modify values, and People section to modify offenders.</p>

		</asp:Panel>


		<!-- larceny-theft -->
		<asp:Panel ID="panelLarcenyTheft" runat="server" Visible="false">
			<table border="0">
				<tr>
					<td class="label" style="width: 100px;">UCR Offense Type:</td>
					<td><asp:Literal ID="litLarcenyType" runat="server" /></td>
				</tr>
				<tr>
					<td class="label">Larceny Nature:</td>
					<td>
						<telerik:RadComboBox ID="rcbLarcenyNature" Width="300" EnableViewState="true" ShowToggleImage="true" AllowCustomText="False" MarkFirstMatch="true" runat="server">
							<Items>
								<telerik:RadComboBoxItem />
								<telerik:RadComboBoxItem Value="POCKET-PICKING" Text="POCKET-PICKING" />
								<telerik:RadComboBoxItem Value="PURSE-SNATCHING" Text="PURSE-SNATCHING" />
								<telerik:RadComboBoxItem Value="SHOPLIFTING" Text="SHOPLIFTING" />
								<telerik:RadComboBoxItem Value="FROM MOTOR VEHICLE" Text="FROM MOTOR VEHICLE" />
								<telerik:RadComboBoxItem Value="MOTOR VEHICLE PARTS & ACCESSORIES" Text="MOTOR VEHICLE PARTS & ACCESSORIES" />
								<telerik:RadComboBoxItem Value="BICYCLES" Text="BICYCLES" />
								<telerik:RadComboBoxItem Value="FROM BUILDING" Text="FROM BUILDING" />
								<telerik:RadComboBoxItem Value="FROM COIN-OPERATED DEVICE OR MACHINE" Text="FROM COIN-OPERATED DEVICE OR MACHINE" />
								<telerik:RadComboBoxItem Value="ALL OTHER" Text="ALL OTHER" />
							</Items>
						</telerik:RadComboBox>
					</td>
				</tr>
				<tr>
					<td class="label" style="width: 100px;">Stolen Value:</td>
					<td><asp:Literal ID="litLarcenyStolenValue" runat="server" /></td>
				</tr>
				<tr>
					<td class="label" style="width: 100px;">Recovered Value:</td>
					<td><asp:Literal ID="litLarcenyRecoveredValue" runat="server" /></td>
				</tr>
				<tr>
					<td class="label" style="width: 100px;">Offenders Arrested:</td>
					<td><asp:Literal ID="litLarcenyOffenders" runat="server" /></td>
				</tr>
			</table>

			<p>See Property secection to modify values, and People section to modify offenders.</p>
		</asp:Panel>


		<asp:Panel ID="panelMotorVehicleTheft" runat="server" Visible="false">
			<table border="0">
				<tr>
					<td class="label" style="width: 100px;">MV Theft 1:</td>
					<td>
						<asp:TextBox ID="TextBox19" runat="server" />
					</td>
				</tr>
				<tr>
					<td class="label">MV Theft 2:</td>
					<td>
						<asp:TextBox ID="TextBox20" runat="server" />
					</td>
				</tr>
				<tr>
					<td class="label">MV Theft 3:</td>
					<td>
						<asp:TextBox ID="TextBox21" runat="server" />
					</td>
				</tr>
			</table>
		</asp:Panel>


		<asp:Panel ID="panelArson" runat="server" Visible="false">
			<table border="0">
				<tr>
					<td class="label" style="width: 100px;">Arson 1:</td>
					<td>
						<asp:TextBox ID="TextBox22" runat="server" />
					</td>
				</tr>
				<tr>
					<td class="label">Arson 2:</td>
					<td>
						<asp:TextBox ID="TextBox23" runat="server" />
					</td>
				</tr>
				<tr>
					<td class="label">Arson 3:</td>
					<td>
						<asp:TextBox ID="TextBox24" runat="server" />
					</td>
				</tr>
			</table>
		</asp:Panel>


		<asp:Panel ID="panelNone" runat="server" Visible="false">
			<br/>This report's Signal does not correspond to a UCR offense type.
		</asp:Panel>

	</telerik:RadAjaxPanel>
	
</div>


<%--<telerik:RadAjaxManager ID="radAjaxManager" runat="server">

	<AjaxSettings>
		<telerik:AjaxSetting AjaxControlID="rcbCategories">
			<UpdatedControls>
				<telerik:AjaxUpdatedControl ControlID="rcbOffenseTypes" />
			</UpdatedControls>
		</telerik:AjaxSetting>
	</AjaxSettings>

</telerik:RadAjaxManager>--%>