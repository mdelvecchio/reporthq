﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BasicInfo.ascx.cs" Inherits="ReportHq.IncidentReports.Event.Controls.BasicInfo" %>
<%@ Register TagPrefix="ReportHq" TagName="ReportSubNav" src="ReportSubNav.ascx" %>

<ReportHq:ReportSubNav ID="ucReportSubNav" CurrentSection="BasicInfo" OnNavItemClicked="ucReportSubNav_NavItemClicked" runat="server" />
	
<table class="formBox" border="0">
	<tr>
		<td class="label">Signal: </td>
		<td>
			<Telerik:RadComboBox ID="rcbSignals" 
				DataValueField="SignalTypeID" 
				DataTextField="Code" 
				Width="280" 
				DropDownWidth="400" 
				EnableViewState="false" 
				ShowToggleImage="true" 				
				Filter="Contains" 
				AllowCustomText="True" 
				MarkFirstMatch="false" 
				HighlightTemplatedItems="true"
				OnItemDataBound="rcbSignals_ItemDataBound"
				OnClientFocus="showDropDown" 
				OnClientBlur="processSignal"
				runat="server" />
			<asp:RequiredFieldValidator ID="rfvSignal" ControlToValidate="rcbSignals" ValidationGroup="event" ErrorMessage="Signal" Display="None" runat="server" />
		</td>
	</tr>
	<tr>
		<td class="label">Incident: </td>
		<td>
			<asp:TextBox ID="tbIncidentDescription" Width="276" MaxLength="100" runat="server" />
			<asp:RequiredFieldValidator ID="rfvIncident" ControlToValidate="tbIncidentDescription" ValidationGroup="event" ErrorMessage="Incident" Display="None" runat="server" />
		</td>
	</tr>
	<tr>
		<td class="label">Location: </td>
		<td>
			<asp:TextBox ID="tbLocation" Width="276" MaxLength="100" runat="server" />
			<asp:RequiredFieldValidator ID="rfvLocation" ControlToValidate="tbLocation" ValidationGroup="event" ErrorMessage="Location" Display="None" runat="server" />
		</td>
	</tr>
	<tr>
		<td class="label">District: </td>
		<td>
			<Telerik:RadComboBox ID="rcbDistricts" DataValueField="District" DataTextField="District" Width="75" EnableViewState="false" ShowToggleImage="true" AllowCustomText="False" MarkFirstMatch="true" runat="server" />
			<asp:RequiredFieldValidator ID="rfvDistrict" ControlToValidate="rcbDistricts" ValidationGroup="event" ErrorMessage="District" Display="None" runat="server" />
		</td>
	</tr>
	<tr>
		<td class="label">Zone: </td>
		<td>
			<Telerik:RadComboBox ID="rcbZones" DataValueField="Zone" DataTextField="Zone" Width="75" EnableViewState="false" ShowToggleImage="true" AllowCustomText="False" MarkFirstMatch="true" runat="server" />
			<asp:RequiredFieldValidator ID="rfvZone" ControlToValidate="rcbZones" ValidationGroup="event" ErrorMessage="Zone" Display="None" runat="server" />
		</td>
	</tr>
	<tr>
		<td class="label">Subzone: </td>
		<td>
			<Telerik:RadComboBox ID="rcbSubZones" DataValueField="SubZone" DataTextField="SubZone" Width="75" EnableViewState="false" ShowToggleImage="true" AllowCustomText="False" MarkFirstMatch="true" OnClientFocus="showDropDown" runat="server" />
			<asp:RequiredFieldValidator ID="rfvSubZone" ControlToValidate="rcbSubZones" ValidationGroup="event" ErrorMessage="Subzone" Display="None" runat="server" />
		</td>
	</tr>
	<tr>
		<td class="label">Occured Date: </td>
		<td>
			<telerik:RadDatePicker ID="rdpOccurredDate" MinDate="1900-01-01" Width="101" SharedCalendarID="sharedCalendar" PopupDirection="BottomLeft" runat="server" /> <Telerik:RadDateInput ID="rdiOccurredTime" DateFormat="HH:mm" Width="50" runat="server" />
			<asp:RequiredFieldValidator ID="rfvOccurredDate" ControlToValidate="rdpOccurredDate" ValidationGroup="event" ErrorMessage="Occurred Date" Display="None" runat="server" />
			<asp:RequiredFieldValidator ID="rfvOccurredTime" ControlToValidate="rdiOccurredTime" ValidationGroup="event" ErrorMessage="Occurred Time" Display="None" runat="server" />
		</td>
	</tr>
	<tr>
		<td class="label">Report Date: </td>
		<td>
			<Telerik:RadDatePicker id="rdpReportDate" MinDate="1900-01-01" Width="101" SharedCalendarID="sharedCalendar" PopupDirection="BottomLeft" Runat="server" /> <Telerik:RadDateInput ID="rdiReportTime" DateFormat="HH:mm" Width="50" runat="server" />
			<asp:RequiredFieldValidator ID="rfvReportDate" ControlToValidate="rdpReportDate" ValidationGroup="event" ErrorMessage="Report Date" Display="None" runat="server" />
			<asp:RequiredFieldValidator ID="rfvReportTime" ControlToValidate="rdiReportTime" ValidationGroup="event" ErrorMessage="Report Time" Display="None" runat="server" />
			
		</td>
	</tr>
	<tr>
		<td class="label">Lighting: </td>
		<td>
			<Telerik:RadComboBox ID="rcbLightingTypes" DataValueField="LightingConditionTypeID" DataTextField="Description" Width="75" EnableViewState="false" ShowToggleImage="true" AllowCustomText="False" MarkFirstMatch="true" OnClientFocus="showDropDown" runat="server" />
			<asp:RequiredFieldValidator ID="rfvLighting" ControlToValidate="rcbLightingTypes" ValidationGroup="event" ErrorMessage="Lighting" Display="None" runat="server" />
		</td>
	</tr>

	<tr>
		<td class="label">Status: </td>
		<td>
			<Telerik:RadComboBox ID="rcbReportStatusTypes" DataValueField="ReportStatusTypeID" DataTextField="Description" EnableViewState="false" ShowToggleImage="true" AllowCustomText="False" MarkFirstMatch="true" Width="180" OnClientFocus="showDropDown" runat="server" />
			<asp:RequiredFieldValidator ID="rfvStatus" ControlToValidate="rcbReportStatusTypes" ValidationGroup="event" ErrorMessage="Status" Display="None" runat="server" />
		</td>
	</tr>
	<%--<tr>
		<td class="label">Bulletin: </td>
		<td><input type="text" style="width:280px;"></td>
	</tr>--%>
</table>


<asp:ValidationSummary ID="validationSummary" ValidationGroup="event" DisplayMode="bulletList" CssClass="validationSummary" HeaderText="Required Fields:" EnableClientScript="true" runat="server" />


<!-- used by multiple RadDatePickers -->
<telerik:RadCalendar ID="sharedCalendar" ShowRowHeaders="false" UseColumnHeadersAsSelectors="false" runat="server" />


<script type="text/javascript">
	//takes the desc of a Signal and sets it to a textbox
	function processSignal(sender)
	{
		var enteredText = sender.get_text();
		var item = sender.findItemByText(enteredText);
	
		var rcbVehicleModels = $get('<%= tbIncidentDescription.ClientID %>');
		
		//if text is an actual item
		if (item)
		{
			if (enteredText != '')
			{
				var splitArray = enteredText.split(' - ');
				rcbVehicleModels.value = splitArray[1];
			}
			else
			{
				rcbVehicleModels.value = '';
			}
		}
		else //no item found for text
		{
			sender.set_text('');
			rcbVehicleModels.value = '';
		}
	}
</script>