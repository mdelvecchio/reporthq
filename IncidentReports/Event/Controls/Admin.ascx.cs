﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReportHq.IncidentReports.Controls;
using Telerik.Web.UI;

namespace ReportHq.IncidentReports.Event.Controls
{
	public partial class Admin : System.Web.UI.UserControl
	{
		#region Properties

		private bool _disableValidators = false;

		public string Officer1Name
		{
			get { return tbOfficer1Name.Text; }
			set { tbOfficer1Name.Text = value; }
		}

		public int Officer1Badge
		{
			get
			{
				return Int32.Parse(rntbOfficer1Badge.Text);
			}
			set
			{
				if (value > 0)
					rntbOfficer1Badge.Text = value.ToString();
			}
		}

		public int Officer1EmployeeID
		{
			get
			{
				return Int32.Parse(rntbOfficer1EmployeeID.Text);
			}
			set
			{
				if (value > 0)
					rntbOfficer1EmployeeID.Text = value.ToString();
			}
		}

		public int Officer1Rank
		{
			get
			{
				return Int32.Parse(rcbOfficer1Rank.SelectedValue);
			}
			set
			{
				if (value > 0)
					rcbOfficer1Rank.SelectedValue = value.ToString();
			}
		}

		public string Officer2Name
		{
			get { return tbOfficer2Name.Text; }
			set { tbOfficer2Name.Text = value; }
		}

		public int Officer2Badge
		{
			get
			{
				if (!string.IsNullOrEmpty(rntbOfficer2Badge.Text))
					return Int32.Parse(rntbOfficer2Badge.Text);
				else
					return -1;
			}
			set
			{
				if (value > 0)
					rntbOfficer2Badge.Text = value.ToString();
			}
		}

		public int Officer2Rank
		{
			get
			{
				if (!string.IsNullOrEmpty(rcbOfficer2Rank.SelectedValue))
					return Int32.Parse(rcbOfficer2Rank.SelectedValue);
				else
					return -1;
			}
			set
			{
				if (value > 0)
					rcbOfficer2Rank.SelectedValue = value.ToString();
			}
		}

		public string ReportingCarNumber
		{
			get { return tbReportingCar.Text; }
			set { tbReportingCar.Text = value; }
		}

		public string ReportingCarPlatoon
		{
			get { return rcbPlatoons.SelectedValue; }
			set
			{
				rcbPlatoons.SelectedValue = value;
			}
		}

		public int ReportingAssignmentUnit
		{
			get
			{
				if (!string.IsNullOrEmpty(rcbReportingAssignment.SelectedValue))
					return Int32.Parse(rcbReportingAssignment.SelectedValue);
				else
					return -1;
			}
			set
			{
				if (value > 0)
					rcbReportingAssignment.SelectedValue = value.ToString();
			}
		}

		public string DetectiveName
		{
			get { return tbDetectiveName.Text; }
			set { tbDetectiveName.Text = value; }
		}

		public string CrimeLabName
		{
			get { return tbCrimeLabName.Text; }
			set { tbCrimeLabName.Text = value; }
		}

		public string OtherName
		{
			get { return tbOtherName.Text; }
			set { tbOtherName.Text = value; }
		}

		public bool DisableValidators
		{
			get { return _disableValidators; }
			set { _disableValidators = value; }
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Events

		//public delegate for LinkButton nav-item click
		public delegate void NavItemClick(object sender, EventArgs e);

		//public event for LinkButton click; will be handled by the consuming pages
		public event NavItemClick NavItemClicked;

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Event Handlers

		protected void Page_Init(object sender, EventArgs e)
		{
			/// in an effort to shrink the rising ViewState & thus page size, ive decided to do something a bit different. typically,
			/// one does a "if (!IsPostBack)" and then populates lists w/ data -- idea being that you only go to the database the first 
			/// time the form is hit. on postbacks the ViewState then remembers the list options. works great for small stuff, but its 
			/// really starting to fatten the ViewState since it stores each ListItem inside. so, considering that our lists are all 
			/// cached in the app, and that the database is only a few feet away from the webserver, i think its best to yank these 
			/// lists OUT of the ViewState, and bind them each page hit.
			/// 
			/// fearnot, the controls will still "remember" their values on postback, as thats part of their HTML behavior, and is not
			/// dependent on ViewState to do.

			//platoons
			rcbPlatoons.DataSource = Report.GetPlatoonTypes();
			rcbPlatoons.DataBind();
			rcbPlatoons.Items.Insert(0, new RadComboBoxItem());

			//ranks
			rcbOfficer1Rank.DataSource = Report.GetRankTypes();
			rcbOfficer1Rank.DataBind();
			rcbOfficer1Rank.Items.Insert(0, new RadComboBoxItem());

			rcbOfficer2Rank.DataSource = Report.GetRankTypes();
			rcbOfficer2Rank.DataBind();
			rcbOfficer2Rank.Items.Insert(0, new RadComboBoxItem());

			//assignment unit
			rcbReportingAssignment.DataSource = Report.GetAssignmentUnitTypes();
			rcbReportingAssignment.DataBind();
			rcbReportingAssignment.Items.Insert(0, new RadComboBoxItem());
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void Page_Load(object sender, EventArgs e)
		{
			rcbOfficer1Rank.Focus();

			if (_disableValidators)
			{
				rfvEmployeeID.Enabled = false;
				rfvOfficer1Name.Enabled = false;
				rfvOfficer1Badge.Enabled = false;
				rfvOfficer1Rank.Enabled = false;
				rfvReportingCar.Enabled = false;
				cvReportingCar.Enabled = false;
				rfvPlatoon.Enabled = false;
				rfvReportingAssignment.Enabled = false;
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void ucReportSubNav_NavItemClicked(object sender, EventArgs e)
		{
			//raise event which the page will handle. (TODO: is there a better way to bubble-up downstream control events?)

			//fire event for consuming page to handler
			NavItemClicked(sender, e);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Private Methods

		/// <summary>
		/// A manual ReportingCarNumber validation function, used by the CustomValidator control "cvReportingCar"
		/// </summary>
		protected void ValidateCarNumber(object sender, ServerValidateEventArgs e)
		{
			string carNumber = e.Value;
			bool isBadgeAsCarNumber = false;
			bool isValid = false;

			//valid Car Numbers are between 1-10999. if there is no car number, users can optionally use a "B" in front 
			//of their Badge Number to designate this.

			//strip out a possible preceeding "B"
			if (carNumber.IndexOf("B") > -1)
			{
				isBadgeAsCarNumber = true;
				carNumber = carNumber.Replace("B", string.Empty);
			}

			int theNum = Int32.Parse(carNumber);

			if (theNum < 1 || theNum > 10999)
				isValid = false;
			else
				isValid = true;

			//if a Bxxxxx value (badge-as-carnumber), make sure the number matches the page's BadgeNumber field
			if (isBadgeAsCarNumber)
				if (theNum != rntbOfficer1Badge.Value)
					isValid = false;


			e.IsValid = isValid;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}