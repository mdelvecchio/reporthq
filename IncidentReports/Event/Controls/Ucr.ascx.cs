﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReportHq.Common;
using ReportHq.IncidentReports;
using ReportHq.IncidentReports.Bases;
using ReportHq.IncidentReports.Controls;
using Telerik.Web.UI;

namespace ReportHq.IncidentReports.Event.Controls
{
	public partial class Ucr : System.Web.UI.UserControl
	{
		#region Variables

		Report _report;
		//string _summary;

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Properties

		#region GENERAL

		/// <summary>
		/// The item to select in the dropdownlist of UCR offense categories. (Not using for now)
		/// </summary>
		public int CategoryID { get; set; }
		
		//public int CategoryID
		//{
		//	get
		//	{
		//		if (!string.IsNullOrEmpty(rcbCategories.SelectedValue))
		//			return Int32.Parse(rcbCategories.SelectedValue);
		//		else
		//			return -1;
		//	}
		//	set
		//	{
		//		if (value > 0)
		//			rcbCategories.SelectedValue = value.ToString();
		//	}
		//}

		/// <summary>
		/// The UCR offense type. These correspond to the UCR offense category.
		/// </summary>
		public UcrOffense.OffenseTypes OffenseType { get; set; }

		/// <summary>
		/// The UCR offense type. These correspond to the UCR offense category.
		/// </summary>
		public int OffenseTypeID { get; set; }

		//public int OffenseTypeID
		//{
		//	get
		//	{
		//		if (!string.IsNullOrEmpty(rcbOffenseTypes.SelectedValue))
		//			return Int32.Parse(rcbOffenseTypes.SelectedValue);
		//		else
		//			return -1;
		//	}
		//	set
		//	{
		//		if (value > 0)
		//			rcbOffenseTypes.SelectedValue = value.ToString();
		//	}
		//}

		#endregion

		#region BURGLARY

		public string BurglaryTimeOfDay
		{
			get { return litTimeOfDay.Text;	}
		}

		public string BurglaryCrimeLocation
		{
			get { return rcbBurglaryCrimeLocation.SelectedValue; }
			set { rcbBurglaryCrimeLocation.SelectedValue = value; }
		}

		public string BurglaryForceUsed
		{
			get	{ return rcbBurglaryForceUsed.SelectedValue; }
			set { rcbBurglaryForceUsed.SelectedValue = value; }
		}

		//public double BurglaryPropertyValue
		//{
		//	set { litBurglaryValue.Text = value.ToString("C"); }			
		//}

		#endregion

		#region LARCENY

		public string LarcenyNature
		{
			get { return rcbLarcenyNature.SelectedValue; }
			set { rcbLarcenyNature.SelectedValue = value; }
		}

		#endregion

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Events

		//public delegate for LinkButton nav-item click
		public delegate void NavItemClick(object sender, EventArgs e);

		//public event for LinkButton click; will be handled by the consuming pages
		public event NavItemClick NavItemClicked;

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Event Handlers

		protected void Page_Load(object sender, EventArgs e)
		{
			//if (!IsPostBack)
			//	BindFormLists();

			_report = ((ReportPage)this.Page).Report;

			//litSummary.Text = String.Format("Signal {0} - UCR crime of {1}", _report.Signal, _report.UcrOffense);
			
			SetOffenseTypePanels();
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		
		//protected void rcbCategories_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
		//{
		//	int categoryID = Int32.Parse(rcbCategories.SelectedValue);

		//	rcbOffenseTypes.DataSource = ReportHq.IncidentReports.Ucr.GetOffenseTypesByCategoryID(categoryID);
		//	rcbOffenseTypes.DataBind();

		//	this.CategoryID = categoryID;

		//	SetOffenseTypePanels();
		//}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		
		//protected void rcbOffenseTypes_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
		//{
		//	int offenseTypeID = Int32.Parse(rcbOffenseTypes.SelectedValue);

		//	this.OffenseTypeID = offenseTypeID;
		//}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		
		protected void ucReportSubNav_NavItemClicked(object sender, EventArgs e)
		{
			//raise event which the page will handle. (TODO: is there a better way to bubble-up downstream control events?)

			//fire event for consuming page to handler
			NavItemClicked(sender, e);
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Private Methods

		/// <summary>
		/// Binds Report HQ's list items to the form controls. Not the actual form values, just the list items.
		/// </summary>
		private void BindFormLists()
		{
			
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Turns on proper UCR input panel, turning off others. Called via Page_Load
		/// </summary>
		private void SetOffenseTypePanels()
		{
			//UcrOffense.OffenseTypes offenseType = (UcrOffense.OffenseTypes)this.OffenseTypeID;
			//Ucr.OffenseTypes offenseType = (ReportHq.IncidentReports.Ucr.OffenseTypes)Int32.Parse(rcbOffenseTypes.SelectedValue);

			//values used by multiple
			string typeSummary = String.Format("{0} (Signal {1})", _report.UcrOffense, _report.Signal);
			Dictionary<string, double> propertySums = _report.PropertySums;

			switch (this.OffenseType)
			{
				case UcrOffense.OffenseTypes.AggravatedAssault:
					panelAggravatedAssault.Visible = true; //ON
					panelArson.Visible = false;
					panelBurglary.Visible = false;
					panelCriminalHomicide.Visible = false;
					panelForcibleRape.Visible = false;
					panelLarcenyTheft.Visible = false;
					panelMotorVehicleTheft.Visible = false;
					panelRobbery.Visible = false;
					break;

				case UcrOffense.OffenseTypes.Arson:
					panelAggravatedAssault.Visible = false;
					panelArson.Visible = true; //ON
					panelBurglary.Visible = false;
					panelCriminalHomicide.Visible = false;
					panelForcibleRape.Visible = false;
					panelLarcenyTheft.Visible = false;
					panelMotorVehicleTheft.Visible = false;
					panelRobbery.Visible = false;
					break;

				case UcrOffense.OffenseTypes.Burglary:
					panelAggravatedAssault.Visible = false;
					panelArson.Visible = false;
					panelBurglary.Visible = true; //ON
					panelCriminalHomicide.Visible = false;
					panelForcibleRape.Visible = false;
					panelLarcenyTheft.Visible = false;
					panelMotorVehicleTheft.Visible = false;
					panelRobbery.Visible = false;

					litBurglaryType.Text = typeSummary;

					if (DateBuddy.GetDayOrNight(_report.OccurredDateTime) == DateBuddy.DayOrNight.Day)
						litTimeOfDay.Text = "Day";
					else
						litTimeOfDay.Text = "Night";
				
					if (propertySums.Count > 0)
					{
						litBurglaryStolenValue.Text = propertySums["ValueStolen"].ToString("C");
						litBurglaryRecoveredValue.Text = propertySums["ValueRecovered"].ToString("C");
					}

					litBurglaryOffenders.Text = _report.OffendersArrested.ToString();

					break;

				case UcrOffense.OffenseTypes.CriminalHomicide:
					panelAggravatedAssault.Visible = false;
					panelArson.Visible = false;
					panelBurglary.Visible = false;
					panelCriminalHomicide.Visible = true; //ON
					panelForcibleRape.Visible = false;
					panelLarcenyTheft.Visible = false;
					panelMotorVehicleTheft.Visible = false;
					panelRobbery.Visible = false;
					break;

				case UcrOffense.OffenseTypes.ForcibleRape:
					panelAggravatedAssault.Visible = false;
					panelArson.Visible = false;
					panelBurglary.Visible = false;
					panelCriminalHomicide.Visible = false;
					panelForcibleRape.Visible = true; //ON
					panelLarcenyTheft.Visible = false;
					panelMotorVehicleTheft.Visible = false;
					panelRobbery.Visible = false;
					break;

				case UcrOffense.OffenseTypes.LarcenyTheft:
					panelAggravatedAssault.Visible = false;
					panelArson.Visible = false;
					panelBurglary.Visible = false;
					panelCriminalHomicide.Visible = false;
					panelForcibleRape.Visible = false;
					panelLarcenyTheft.Visible = true; //ON
					panelMotorVehicleTheft.Visible = false;
					panelRobbery.Visible = false;

					litLarcenyType.Text = typeSummary;

					if (propertySums.Count > 0)
					{
						litLarcenyStolenValue.Text = propertySums["ValueStolen"].ToString("C");
						litLarcenyRecoveredValue.Text = propertySums["ValueRecovered"].ToString("C");
					}

					litLarcenyOffenders.Text = _report.OffendersArrested.ToString();

					break;

				case UcrOffense.OffenseTypes.MotorVehicleTheft:
					panelAggravatedAssault.Visible = false;
					panelArson.Visible = false;
					panelBurglary.Visible = false;
					panelCriminalHomicide.Visible = false;
					panelForcibleRape.Visible = false;
					panelLarcenyTheft.Visible = false;
					panelMotorVehicleTheft.Visible = true; //ON
					panelRobbery.Visible = false;
					break;

				case UcrOffense.OffenseTypes.Robbery:
					panelAggravatedAssault.Visible = false;
					panelArson.Visible = false;
					panelBurglary.Visible = false;
					panelCriminalHomicide.Visible = false;
					panelForcibleRape.Visible = false;
					panelLarcenyTheft.Visible = false;
					panelMotorVehicleTheft.Visible = false;
					panelRobbery.Visible = true; //ON
					break;

				default:
					panelAggravatedAssault.Visible = false;
					panelArson.Visible = false;
					panelBurglary.Visible = false;
					panelCriminalHomicide.Visible = false;
					panelForcibleRape.Visible = false;
					panelLarcenyTheft.Visible = false;
					panelMotorVehicleTheft.Visible = false;
					panelRobbery.Visible = false;
					panelNone.Visible = true; //ON
					break;
			}
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion	
	}
}