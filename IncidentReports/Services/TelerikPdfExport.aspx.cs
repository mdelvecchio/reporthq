﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReportHq.IncidentReports.DataAccess;
using ReportHq.Common;

namespace ReportHq.IncidentReports.Services
{
	public partial class TelerikPdfExport : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack && Request.Form["fileName"] != null)
			{
				//get our details
				string filename = Request.Form["fileName"].ToString();
				string contentType = Request.Form["contentType"].ToString();
				byte[] data = Convert.FromBase64String(Request.Form["base64"].ToString());

				//push it back
				Export(filename, contentType, data);
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public void Export(string filename, string contentType, byte[] data)
		{
			Response.ClearContent();
			Response.ContentType = contentType;
			Response.AddHeader("Content-Disposition", "attachment; filename=" + filename);
			Response.AddHeader("Content-Length", data.Length.ToString());
			Response.BinaryWrite(data);
			Response.End();
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	}

}