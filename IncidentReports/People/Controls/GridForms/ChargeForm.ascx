﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ChargeForm.ascx.cs" Inherits="ReportHq.IncidentReports.People.Controls.GridForms.ChargeForm" %>

<div class="gridForm">

	<table style="margin-bottom:10px;">	
		<tr>
			<td class="label">Charge Prefix:</td>
			<td>
				<Telerik:RadComboBox ID="rcbChargePrefixes" 
					DataValueField="ChargePrefixTypeID" 
					DataTextField="Name" 
					ShowToggleImage="true" 
					OnClientFocus="showDropDown"
					OnClientSelectedIndexChanging="loadCharges"
					TabIndex="1" 
					runat="server" />
			</td>
		</tr>
		<tr>
			<td class="label">Charge: <span class="required">*</span></td>
			<td>
				<Telerik:RadComboBox ID="rcbCharges" 
					DataValueField="ChargeTypeID" 
					DataTextField="Code" 
					Width="381" 
					ShowToggleImage="true"					
					Filter="Contains" 
					AllowCustomText="false" 
					MarkFirstMatch="false" 					
					OnItemsRequested="rcbCharges_ItemsRequested" 
					OnItemDataBound="rcbCharges_ItemDataBound"									
					OnClientItemsRequested="itemsLoaded" 
					OnClientFocus="showDropDown"
					TabIndex="2" 
					runat="server" />
				<asp:RequiredFieldValidator ID="rfvCharges" ControlToValidate="rcbCharges" ValidationGroup="charge" ErrorMessage="Charge Code" Display="none" runat="server" />
			</td>
		</tr>
		<tr>
			<td class="label">Details:</td>
			<td>
				<asp:TextBox ID="tbChargeDetails" Text='<%# Eval("ChargeDetails") %>' MaxLength="200" Width="375" TabIndex="3" runat="server" />
			</td>
		</tr>
		<tr>
			<td class="label">Victim #:</td>
			<td><Telerik:RadComboBox ID="rcbVictims" DataValueField="VictimPersonID" DataTextField="Name" DropDownWidth="300" ShowToggleImage="false" AllowCustomText="False" MarkFirstMatch="true" OnItemDataBound="rcbVictims_ItemDataBound" OnClientFocus="showDropDown" TabIndex="4" runat="server" /></td>
		</tr>
		<tr>
			<td class="label">Relationsip:</td>
			<td><Telerik:RadComboBox ID="rcbVictimOffenderRelationshipTypes" DataValueField="VictimOffenderRelationshipTypeID" DataTextField="Description" ShowToggleImage="false" AllowCustomText="False" MarkFirstMatch="true" OnClientFocus="showDropDown" TabIndex="5" runat="server" />&nbsp;<span class="small" style="vertical-align:sub;">(Victim was...)</span></td>
		</tr>
	</table>
	
	<asp:ValidationSummary ID="validationSummary" ValidationGroup="charge" DisplayMode="bulletList" CssClass="validationSummary" HeaderText="Required Fields:" EnableClientScript="true" runat="server" />
		
	<asp:LinkButton ID="lbInsert" CommandName="PerformInsert" Text="Save Charge" ValidationGroup="property" Visible='<%# (DataItem is Telerik.Web.UI.GridInsertionObject) %>' TabIndex="6" CssClass="flatButton" runat="server" />
	<asp:LinkButton ID="lbUpdate" CommandName="Update" Text="Update" ValidationGroup="property" Visible='<%# !(DataItem is Telerik.Web.UI.GridInsertionObject) %>' TabIndex="6" CssClass="flatButton" runat="server" />
	<asp:LinkButton ID="lbCancel" CommandName="Cancel" Text="Cancel" CausesValidation="false" TabIndex="7" CssClass="flatButton" runat="server" />

</div>


<Telerik:RadScriptBlock ID="radScriptBlock" runat="server">	
	<script type="text/javascript">
		//called when ChargePrefixes changes. kicks off data collection for rcbCharges.
		function loadCharges(combo, eventargs) {
			var item = eventargs.get_item();

			var rcbCharges = $find('<%= rcbCharges.ClientID %>');

			if (item.get_index() > 0) //Charge Prefix selected, get Charges
			{
				//alert(item.get_value()); //ChargePrefixeTypeID value from rcbChargePrefixes

				rcbCharges.set_text("Loading...");
				rcbCharges.requestItems(item.get_value(), false); //TODO: BUG: this is applying a filter on the description portion. in case of ChargePrefixeTypeID 4, no results. i dont want an initial filter..
			}
			else //empty item selected, reset Charges
			{
				rcbCharges.set_text(" ");
				rcbCharges.clearItems();
			}
		}

		//called when rcbCharges loads its items. shows & selects the first item.
		function itemsLoaded(combo, eventarqs) {
			var comboItems = combo.get_items();

			if (comboItems.get_count() > 0) {
				combo.set_text(comboItems.getItem(0).get_text());
				//comboItems.getItem(0).highlight(); // ?
			}

			combo.showDropDown();
		}
	</script>
</Telerik:RadScriptBlock>