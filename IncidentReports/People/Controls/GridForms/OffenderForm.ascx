﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OffenderForm.ascx.cs" Inherits="ReportHq.IncidentReports.People.Controls.GridForms.OffenderForm" %>

<div class="gridForm">

	<table border="0" style="margin-bottom:10px;">
		<tr>
			<td class="label">Offender #: <span class="required">*</span></td>
			<td>
				<Telerik:RadNumericTextBox id="rntbOffenderNumber" Text='<%# Eval("OffenderNumber") %>' Type="Number" MaxLength="2" NumberFormat-DecimalDigits="0" NumberFormat-GroupSeparator="" Width="50" TabIndex="1" runat="server" />
				<asp:RequiredFieldValidator ID="rfvOffenderNumber" ControlToValidate="rntbOffenderNumber" ValidationGroup="offender" ErrorMessage="Offender Number" Display="none" runat="server" />
			</td>
			<td style="width:25px;"></td>
			<td class="label">Sobriety:</td>
			<td><Telerik:RadComboBox ID="rcbSobrietyTypes" DataValueField="SobrietyTypeID" DataTextField="Description" ShowToggleImage="true" AllowCustomText="False" MarkFirstMatch="true" OnClientFocus="showDropDown" Width="180" TabIndex="16" runat="server" /></td>
		</tr>
		<tr>
			<td class="label">Status: <span class="required">*</span></td>
			<td>
				<Telerik:RadComboBox ID="rcbStatusTypes" DataValueField="OffenderStatusTypeID" DataTextField="Description" ShowToggleImage="true" AllowCustomText="False" MarkFirstMatch="true" OnClientFocus="showDropDown" Width="180" TabIndex="1" runat="server" />
				<asp:RequiredFieldValidator ID="rfvStatusType" ControlToValidate="rcbStatusTypes" ValidationGroup="offender" ErrorMessage="Status" Display="none" runat="server" />
			</td>
			<td></td>
			<td class="label">Injury:</td>
			<td><Telerik:RadComboBox ID="rcbInjuryTypes" DataValueField="InjuryTypeID" DataTextField="Description" ShowToggleImage="true" AllowCustomText="False" MarkFirstMatch="true" OnClientFocus="showDropDown" Width="180" TabIndex="17" runat="server" /></td>
		</tr>
		<tr>
			<td class="label">Last Name:</td>
			<td><asp:TextBox ID="tbLastName" Text='<%# Eval("LastName") %>' Width="176" TabIndex="2" runat="server" /></td>
			<td></td>
			<td class="label">Treated:</td>
			<td><Telerik:RadComboBox ID="rcbTreatedTypes" DataValueField="TreatedTypeID" DataTextField="Description" ShowToggleImage="true" AllowCustomText="False" MarkFirstMatch="true" OnClientFocus="showDropDown" Width="180" TabIndex="18" runat="server" /></td>
		</tr>
		<tr>
			<td class="label">First Name:</td>
			<td><asp:TextBox ID="tbFirstName" Text='<%# Eval("FirstName") %>' Width="96" TabIndex="3" runat="server" /></td>
			<td></td>
			<td class="label">Arrest Type:</td>
			<td><Telerik:RadComboBox ID="rcbArrestTypes" DataValueField="ArrestTypeID" DataTextField="Description" ShowToggleImage="true" AllowCustomText="False" MarkFirstMatch="true" OnClientFocus="showDropDown" Width="180" TabIndex="19" runat="server" /></td>
		</tr>
		<tr>
			<td class="label">Nickname:</td>
			<td><asp:TextBox ID="tbNickname" Text='<%# Eval("Nickname") %>' Width="96" TabIndex="4" runat="server" /></td>
			<td></td>
			<td class="label">Arrest Location:</td>
			<td><asp:TextBox ID="tbArrestLocation" Text='<%# Eval("ArrestLocation") %>' Width="176" TabIndex="20" runat="server" /></td>
		</tr>
		<tr>
			<td class="label">Race:</td>
			<td><Telerik:RadComboBox ID="rcbRace" DataValueField="RaceTypeID" DataTextField="Description" Width="100" ShowToggleImage="true" AllowCustomText="False" MarkFirstMatch="true" OnClientFocus="showDropDown" TabIndex="5" runat="server" /></td>
			<td></td>
			<td class="label">Arrest Date:</td>
			<td>
				<Telerik:RadDateInput ID="rdiArrestDate" DbSelectedDate='<%# Eval("ArrestDate") %>' MinDate="01/01/1900" Width="75" TabIndex="21" runat="server" />
				<%--
				TODO: figure out how to use this instead of above

				<br />
				<Telerik:RadDatePicker id="rdpArrestDate" SelectedDate='<%# Eval("ArrestDate") %>' MinDate="1900-01-01" Width="100" SharedCalendarID="sharedCalendar" Runat="server" /> <Telerik:RadDateInput ID="rdiOccurredTime" SelectedDate='<%# Eval("ArrestDate") %>' DateFormat="HH:mm" Width="50" runat="server" />
				--%>
			</td>
		</tr>
		<tr>
			<td class="label">Sex:</td>
			<td><Telerik:RadComboBox ID="rcbGender" DataValueField="GenderTypeID" DataTextField="Description" Width="100" ShowToggleImage="true" AllowCustomText="False" MarkFirstMatch="true" TabIndex="6" runat="server" /></td>
			<td></td>
			<td class="label">Arrest Time:</td>
			<td><Telerik:RadDateInput ID="rdiArrestTime" DbSelectedDate='<%# Eval("ArrestDate") %>' DateFormat="HH:mm" MinDate="01/01/1900" Width="75" TabIndex="22" runat="server" /></td>
		</tr>
		<tr>
			<td class="label">DOB:</td>
			<td><Telerik:RadDateInput ID="rdiDateOfBirth" DbSelectedDate='<%# Eval("DateOfBirth") %>' MinDate="01/01/1900" Width="100" TabIndex="7" runat="server" /></td>
			<td></td>
			<td class="label">Arrest Credit:</td>
			<td><asp:TextBox ID="tbArrestCredit" Text='<%# Eval("ArrestCredit") %>' Width="176" TabIndex="23" runat="server" /></td>
		</tr>
		<tr>
			<td class="label">Height:</td>
			<td><Telerik:RadMaskedTextBox id="rmtbHeight" Text='<%# ReportHq.Common.RadGrid.ConvertHeight(Eval("Height")) %>' Mask="#'##" Width="50" TabIndex="8" runat="server" /></td>
			<td></td>
			<td class="label">Transported By:</td>
			<td><asp:TextBox ID="tbTransportedBy" Text='<%# Eval("TransportedBy") %>' Width="176" TabIndex="24" runat="server" /></td>
		</tr>
		<tr>
			<td class="label">Weight:</td>
			<td><Telerik:RadNumericTextBox id="rntbWeight" Type="Number" MaxLength="4" NumberFormat-DecimalDigits="0" NumberFormat-GroupSeparator="" Text='<%# Eval("Weight") %>' Width="50" TabIndex="9" runat="server" /></td>
			<td></td>
			<td class="label">Unit:</td>
			<td><asp:TextBox ID="tbTransportUnit" Text='<%# Eval("TransportUnit") %>' Width="176" TabIndex="25" runat="server" /></td>
		</tr>
		<tr>
			<td class="label">Home Address:</td>
			<td><asp:TextBox ID="tbStreetAddress" Text='<%# Eval("StreetAddress") %>' Width="176" TabIndex="10" runat="server" /></td>
			<td></td>
			<td class="label">Rights Waive Frm:</td>
			<td><Telerik:RadNumericTextBox id="rntbRightsWaivedForm" Text='<%# Eval("RightsWaivedFormNumber") %>' Type="Number" MaxLength="6" NumberFormat-DecimalDigits="0" NumberFormat-GroupSeparator="" Width="75" TabIndex="26" runat="server" /></td>
		</tr>
		<tr>
			<td class="label">City:</td>
			<td><asp:TextBox ID="tbCity" Text='<%# Eval("City") %>' Width="176" TabIndex="11" runat="server" /></td>
			<td></td>
			<td class="label">Resident Type:</td>
			<td><Telerik:RadComboBox ID="rcbResidentTypes" DataValueField="ResidentTypeID" DataTextField="Description" ShowToggleImage="true" AllowCustomText="False" MarkFirstMatch="true" OnClientFocus="showDropDown" Width="180" TabIndex="27" runat="server" /></td>
		</tr>
		<tr>
			<td class="label">State:</td>
			<td><Telerik:RadComboBox ID="rcbState" DataValueField="Abbreviation" DataTextField="State" ShowToggleImage="true" AllowCustomText="False" MarkFirstMatch="true" Width="180" TabIndex="12" runat="server" /></td>
			<td></td>
			<td class="label">Juvenile Disp:</td>
			<td><Telerik:RadComboBox ID="rcbJuvenileDispositionTypes" DataValueField="JuvenileDispositionTypeID" DataTextField="Description" ShowToggleImage="true" AllowCustomText="False" MarkFirstMatch="true" OnClientFocus="showDropDown" Width="180" TabIndex="28" runat="server" /></td>
		</tr>
		<tr>
			<td class="label">Zip Code:</td>
			<td><Telerik:RadNumericTextBox id="rmtbZipCode" Type="Number" MaxLength="5" NumberFormat-DecimalDigits="0" NumberFormat-GroupSeparator="" Text='<%# Eval("ZipCode") %>' Width="100" TabIndex="13" runat="server" /></td>
			<td></td>
			<td class="label">District:</td>
			<td><Telerik:RadComboBox ID="rcbDistricts" DataValueField="District" DataTextField="District" Width="75" ShowToggleImage="true" AllowCustomText="False" MarkFirstMatch="true" TabIndex="29" runat="server" /></td>
		</tr>
		<tr>
			<td class="label">SSN:</td>
			<td><Telerik:RadMaskedTextBox id="rmtbSsn" Mask="###-##-####" Text='<%# Eval("SocialSecurityNumber") %>' Width="100" TabIndex="14" runat="server" /></td>
			<td></td>
			<td class="label">Zone:</td>
			<td><Telerik:RadComboBox ID="rcbZones" DataValueField="Zone" DataTextField="Zone" Width="75" ShowToggleImage="true" AllowCustomText="False" MarkFirstMatch="true" TabIndex="30" runat="server" /></td>
		</tr>
		<tr>
			<td class="label">Drivers Lic #:</td>
			<td><asp:TextBox ID="tbDriversLicense" Text='<%# Eval("DriversLicenseNumber") %>' Width="96" TabIndex="15" runat="server" /></td>
			<td></td>
			<td class="label">Subzone:</td>
			<td><Telerik:RadComboBox ID="rcbSubZones" DataValueField="SubZone" DataTextField="SubZone" Width="75" ShowToggleImage="true" AllowCustomText="False" MarkFirstMatch="true" OnClientFocus="showDropDown" TabIndex="31" runat="server" /></td>
		</tr>
		<tr>
			<td class="label">Drivers State:</td>
			<td><Telerik:RadComboBox ID="rcbDriversState" DataValueField="Abbreviation" DataTextField="State" ShowToggleImage="true" AllowCustomText="False" MarkFirstMatch="true" Width="180" TabIndex="15" runat="server" /></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>

	</table>

	<asp:ValidationSummary ID="validationSummary" ValidationGroup="offender" DisplayMode="bulletList" CssClass="validationSummary" HeaderText="Required Fields:" EnableClientScript="true" runat="server" />
		
	<asp:LinkButton ID="lbInsert" CommandName="PerformInsert" Text="Save Offender" ValidationGroup="offender" Visible='<%# (DataItem is Telerik.Web.UI.GridInsertionObject) %>' TabIndex="27" CssClass="flatButton" runat="server" />
	<asp:LinkButton ID="lbUpdate" CommandName="Update" Text="Update" ValidationGroup="offender" Visible='<%# !(DataItem is Telerik.Web.UI.GridInsertionObject) %>' TabIndex="27" CssClass="flatButton" runat="server" />
	<asp:LinkButton ID="lbCancel" CommandName="Cancel" Text="Cancel" CausesValidation="false" TabIndex="28" CssClass="flatButton" runat="server" />

	<!-- used by multiple RadDatePickers -->
	<%--<telerik:RadCalendar ID="sharedCalendar" ShowRowHeaders="false" UseColumnHeadersAsSelectors="false" runat="server" />--%>

</div>