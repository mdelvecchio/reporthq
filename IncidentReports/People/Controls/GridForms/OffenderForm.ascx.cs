﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReportHq.Common;
using ReportHq.IncidentReports.Bases;
using Telerik.Web.UI;

namespace ReportHq.IncidentReports.People.Controls.GridForms
{
	public partial class OffenderForm : System.Web.UI.UserControl
	{
		#region Properties

		private object _dataItem;

		public object DataItem
		{
			get { return _dataItem; }
			set { _dataItem = value; }
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Event Handlers

		override protected void OnInit(EventArgs e)
		{
			//needed to initiate special event handler for dropdownlists bindings
			this.DataBinding += new EventHandler(OffendersForm_DataBinding);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void Page_Load(object sender, EventArgs e)
		{
			rntbOffenderNumber.Focus();
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		private void Page_PreRender(object sender, EventArgs e)
		{
			if (!((ReportPage)this.Page).UserHasWriteAccess)
			{
				lbInsert.Visible = false;
				lbUpdate.Visible = false;
			}

			//the Arrest Date & Arrest Time fields share the same datasource ("ArrestDate" in db). so if the user only enters time, remove the default value for date.
			if (rdiArrestDate.SelectedDate != null)
			{
				if (((DateTime)rdiArrestDate.SelectedDate).Year == 1900)
				{
					rdiArrestDate.SelectedDate = null;
				}
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Was told by Telerik that any list-binding for this control must happen in this custom event handler.
		/// </summary>
		private void OffendersForm_DataBinding(object sender, System.EventArgs e)
		{
			//offender status types
			rcbStatusTypes.DataSource = Offender.GetStatusTypes(); ;
			rcbStatusTypes.DataBind();
			rcbStatusTypes.Items.Insert(0, new RadComboBoxItem());

			//race
			rcbRace.DataSource = GenericLookups.GetPersonRaces();
			rcbRace.DataBind();
			rcbRace.Items.Insert(0, new RadComboBoxItem());

			//gender
			rcbGender.DataSource = GenericLookups.GetPersonGenders();
			rcbGender.DataBind();
			rcbGender.Items.Insert(0, new RadComboBoxItem());

			DataTable dtStates = UsStates.GetStateNamesAndAbbreviations(true);

			//residence state
			rcbState.DataSource = dtStates;
			rcbState.DataBind();
			rcbState.Items.Insert(0, new RadComboBoxItem());
			
			//drivers license state
			rcbDriversState.DataSource = dtStates;
			rcbDriversState.DataBind();
			rcbDriversState.Items.Insert(0, new RadComboBoxItem());
			
			//sobriety
			rcbSobrietyTypes.DataSource = GenericLookups.GetSobrietyTypes();
			rcbSobrietyTypes.DataBind();
			rcbSobrietyTypes.Items.Insert(0, new RadComboBoxItem());

			//injured
			rcbInjuryTypes.DataSource = GenericLookups.GetInjuryTypes();
			rcbInjuryTypes.DataBind();
			rcbInjuryTypes.Items.Insert(0, new RadComboBoxItem());

			//treated
			rcbTreatedTypes.DataSource = GenericLookups.GetTreatedTypes();
			rcbTreatedTypes.DataBind();
			rcbTreatedTypes.Items.Insert(0, new RadComboBoxItem());

			//arrest types
			rcbArrestTypes.DataSource = Offender.GetArrestTypes();
			rcbArrestTypes.DataBind();
			rcbArrestTypes.Items.Insert(0, new RadComboBoxItem());

			//resident types
			rcbResidentTypes.DataSource = GenericLookups.GetResidentTypes();
			rcbResidentTypes.DataBind();
			rcbResidentTypes.Items.Insert(0, new RadComboBoxItem());

			//juvenile disposition types
			rcbJuvenileDispositionTypes.DataSource = Offender.GetJuvenileDispositionTypes();
			rcbJuvenileDispositionTypes.DataBind();
			rcbJuvenileDispositionTypes.Items.Insert(0, new RadComboBoxItem());

			//districts
			rcbDistricts.DataSource = GenericLookups.GetDistricts();
			rcbDistricts.DataBind();
			rcbDistricts.Items.Insert(0, new RadComboBoxItem());

			//zones
			rcbZones.DataSource = GenericLookups.GetZones();
			rcbZones.DataBind();
			rcbZones.Items.Insert(0, new RadComboBoxItem());

			//subzones
			rcbSubZones.DataSource = GenericLookups.GetSubZones();
			rcbSubZones.DataBind();
			rcbSubZones.Items.Insert(0, new RadComboBoxItem());


			//set grid item's values
			rcbStatusTypes.Items.FindItemByValue(DataBinder.Eval(this.DataItem, "OffenderStatusTypeID").ToString()).Selected = true;
			rcbRace.Items.FindItemByValue(DataBinder.Eval(this.DataItem, "RaceTypeID").ToString()).Selected = true;
			rcbGender.Items.FindItemByValue(DataBinder.Eval(this.DataItem, "GenderTypeID").ToString()).Selected = true;
			rcbState.Items.FindItemByValue(DataBinder.Eval(this.DataItem, "State").ToString()).Selected = true;
			//rcbDriversState.Items.FindItemByValue(DataBinder.Eval(this.DataItem, "DriversLicenseState").ToString()).Selected = true; //TODO: include this
			rcbSobrietyTypes.Items.FindItemByValue(DataBinder.Eval(this.DataItem, "SobrietyTypeID").ToString()).Selected = true;
			rcbInjuryTypes.Items.FindItemByValue(DataBinder.Eval(this.DataItem, "InjuryTypeID").ToString()).Selected = true;
			rcbTreatedTypes.Items.FindItemByValue(DataBinder.Eval(this.DataItem, "TreatedTypeID").ToString()).Selected = true;
			rcbArrestTypes.Items.FindItemByValue(DataBinder.Eval(this.DataItem, "ArrestTypeID").ToString()).Selected = true;
			rcbResidentTypes.Items.FindItemByValue(DataBinder.Eval(this.DataItem, "ResidentTypeID").ToString()).Selected = true;
			rcbJuvenileDispositionTypes.Items.FindItemByValue(DataBinder.Eval(this.DataItem, "JuvenileDispositionTypeID").ToString()).Selected = true;
			rcbDistricts.Items.FindItemByValue(DataBinder.Eval(this.DataItem, "District").ToString()).Selected = true;
			rcbZones.Items.FindItemByValue(DataBinder.Eval(this.DataItem, "Zone").ToString()).Selected = true;
			rcbSubZones.Items.FindItemByValue(DataBinder.Eval(this.DataItem, "Subzone").ToString()).Selected = true;
			
			//deselects the first item in the dropdown lists (keeps blank)
			rcbStatusTypes.DataSource = null;
			rcbRace.DataSource = null;
			rcbGender.DataSource = null;
			rcbState.DataSource = null;
			rcbDriversState.DataSource = null;
			rcbSobrietyTypes.DataSource = null;
			rcbInjuryTypes.DataSource = null;
			rcbTreatedTypes.DataSource = null;
			rcbArrestTypes.DataSource = null;
			rcbResidentTypes.DataSource = null;
			rcbJuvenileDispositionTypes.DataSource = null;
			rcbDistricts.DataSource = null;
			rcbZones.DataSource = null;
			rcbSubZones.DataSource = null;

			//http://www.telerik.com/help/aspnet-ajax/grdcustomeditforms.html
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}