﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReportHq.Common;
using ReportHq.IncidentReports.Bases;
using Telerik.Web.UI;

namespace ReportHq.IncidentReports.People.Controls.GridForms
{
	public partial class VictimForm : System.Web.UI.UserControl
	{
		#region Properties

		private object _dataItem;

		public object DataItem
		{
			get { return _dataItem; }
			set { _dataItem = value; }
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Event Handlers

		override protected void OnInit(EventArgs e)
		{
			//needed to initiate special event handler for dropdownlists bindings
			this.DataBinding += new EventHandler(VictimsForm_DataBinding);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void Page_Load(object sender, EventArgs e)
		{
			rntbVictimNumber.Focus();
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		private void Page_PreRender(object sender, EventArgs e)
		{
			if (!(this.Page as ReportPage).UserHasWriteAccess)
			{
				lbInsert.Visible = false;
				lbUpdate.Visible = false;
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Was told by Telerik that any list-binding for this control must happen in this custom event handler.
		/// </summary>
		private void VictimsForm_DataBinding(object sender, System.EventArgs e)
		{
			//victim/person types
			rcbVictimPersonType.DataSource = VictimPerson.GetVictimPersonTypes();
			rcbVictimPersonType.DataBind();
			rcbVictimPersonType.Items.Insert(0, new RadComboBoxItem());

			//victim types
			rcbVictimTypes.DataSource = VictimPerson.GetVictimTypes();
			rcbVictimTypes.DataBind();
			rcbVictimTypes.Items.Insert(0, new RadComboBoxItem());

			//race
			rcbRace.DataSource = GenericLookups.GetPersonRaces();
			rcbRace.DataBind();
			rcbRace.Items.Insert(0, new RadComboBoxItem());

			//gender
			rcbGender.DataSource = GenericLookups.GetPersonGenders();
			rcbGender.DataBind();
			rcbGender.Items.Insert(0, new RadComboBoxItem());

			DataTable dtStates = UsStates.GetStateNamesAndAbbreviations(true);

			//residence state
			rcbState.DataSource = dtStates;
			rcbState.DataBind();
			rcbState.Items.Insert(0, new RadComboBoxItem());

			//work state
			rcbWorkState.DataSource = dtStates;
			rcbWorkState.DataBind();
			rcbWorkState.Items.Insert(0, new RadComboBoxItem());

			//sobriety
			rcbSobrietyTypes.DataSource = GenericLookups.GetSobrietyTypes();
			rcbSobrietyTypes.DataBind();
			rcbSobrietyTypes.Items.Insert(0, new RadComboBoxItem());

			//injured
			rcbInjuryTypes.DataSource = GenericLookups.GetInjuryTypes();
			rcbInjuryTypes.DataBind();
			rcbInjuryTypes.Items.Insert(0, new RadComboBoxItem());

			//treated
			rcbTreatedTypes.DataSource = GenericLookups.GetTreatedTypes();
			rcbTreatedTypes.DataBind();
			rcbTreatedTypes.Items.Insert(0, new RadComboBoxItem());


			//set grid item's values
			rcbVictimPersonType.Items.FindItemByValue(DataBinder.Eval(this.DataItem, "VictimPersonTypeID").ToString()).Selected = true;
			rcbVictimTypes.Items.FindItemByValue(DataBinder.Eval(this.DataItem, "VictimTypeID").ToString()).Selected = true;
			rcbRace.Items.FindItemByValue(DataBinder.Eval(this.DataItem, "RaceTypeID").ToString()).Selected = true;
			rcbGender.Items.FindItemByValue(DataBinder.Eval(this.DataItem, "GenderTypeID").ToString()).Selected = true;
			rcbState.Items.FindItemByValue(DataBinder.Eval(this.DataItem, "State").ToString()).Selected = true;
			rcbWorkState.Items.FindItemByValue(DataBinder.Eval(this.DataItem, "WorkState").ToString()).Selected = true;
			rcbSobrietyTypes.Items.FindItemByValue(DataBinder.Eval(this.DataItem, "SobrietyTypeID").ToString()).Selected = true;
			rcbInjuryTypes.Items.FindItemByValue(DataBinder.Eval(this.DataItem, "InjuryTypeID").ToString()).Selected = true;
			rcbTreatedTypes.Items.FindItemByValue(DataBinder.Eval(this.DataItem, "TreatedTypeID").ToString()).Selected = true;
			
			//deselects the first item in the dropdown lists (keeps blank)
			rcbVictimPersonType.DataSource = null;
			rcbVictimTypes.DataSource = null;
			rcbRace.DataSource = null;
			rcbGender.DataSource = null;
			rcbState.DataSource = null;
			rcbWorkState.DataSource = null;
			rcbSobrietyTypes.DataSource = null;
			rcbInjuryTypes.DataSource = null;
			rcbTreatedTypes.DataSource = null;
			//http://www.telerik.com/help/aspnet-ajax/grdcustomeditforms.html
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}