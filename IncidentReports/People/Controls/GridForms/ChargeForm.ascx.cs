﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReportHq.Common;
using ReportHq.IncidentReports.Bases;
using Telerik.Web.UI;

namespace ReportHq.IncidentReports.People.Controls.GridForms
{
	public partial class ChargeForm : System.Web.UI.UserControl
	{
		#region Properties

		private object _dataItem;

		public object DataItem
		{
			get { return _dataItem; }
			set { _dataItem = value; }
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Event Handlers

		override protected void OnInit(EventArgs e)
		{
			//needed to initiate special event handler for dropdownlists bindings
			this.DataBinding += new EventHandler(ChargeForm_DataBinding);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void Page_Load(object sender, EventArgs e)
		{
			
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Was told by Telerik that any list-binding for this control must happen in this custom event handler.
		/// </summary>
		private void ChargeForm_DataBinding(object sender, System.EventArgs e)
		{
			//charges prefixes
			rcbChargePrefixes.DataSource = OffenderCharge.GetChargePrefixTypes(true);
			rcbChargePrefixes.DataBind();
			rcbChargePrefixes.Items.Insert(0, new RadComboBoxItem());

			//victim numbers
			rcbVictims.DataSource = VictimPerson.GetVictimsByReport((this.Page as ReportPage).Report.ID);
			rcbVictims.DataBind();
			rcbVictims.Items.Insert(0, new RadComboBoxItem());

			//relationships
			rcbVictimOffenderRelationshipTypes.DataSource = VictimPerson.GetVictimOffenderRelationshipTypes();
			rcbVictimOffenderRelationshipTypes.DataBind();
			rcbVictimOffenderRelationshipTypes.Items.Insert(0, new RadComboBoxItem());


			//set grid item's values
			rcbChargePrefixes.Items.FindItemByValue(DataBinder.Eval(this.DataItem, "ChargePrefixTypeID").ToString()).Selected = true;
			rcbVictims.Items.FindItemByValue(DataBinder.Eval(this.DataItem, "VictimPersonID").ToString()).Selected = true;
			rcbVictimOffenderRelationshipTypes.Items.FindItemByValue(DataBinder.Eval(this.DataItem, "VictimOffenderRelationshipTypeID").ToString()).Selected = true;


			//rcbCharges.Items.FindItemByValue(DataBinder.Eval(this.DataItem, "ChargeTypeID").ToString()).Selected = true;

			//set Charges list - have to do after Charge Prefix is set by the above
			if (!string.IsNullOrEmpty(rcbChargePrefixes.SelectedValue))
			{
				rcbCharges.DataSource = OffenderCharge.GetChargeTypesByChargePrefix(Int32.Parse(rcbChargePrefixes.SelectedValue), true);
				rcbCharges.DataBind();
				rcbCharges.Items.Insert(0, new RadComboBoxItem());

				rcbCharges.Items.FindItemByValue(DataBinder.Eval(this.DataItem, "ChargeTypeID").ToString()).Selected = true;
			}
			
			//deselects the first item in the dropdown lists (keeps blank)
			rcbChargePrefixes.DataSource = null;
			rcbCharges.DataSource = null;
			rcbVictims.DataSource = null;
			rcbVictimOffenderRelationshipTypes.DataSource = null;

			//http://www.telerik.com/help/aspnet-ajax/grdcustomeditforms.html
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Makes it so the RadComboBox's .Text shows multiple columns when selected.
		/// </summary>
		protected void rcbChargePrefixes_ItemDataBound(object sender, RadComboBoxItemEventArgs e)
		{
			DataRowView rowView = (DataRowView)e.Item.DataItem;

			e.Item.Text = String.Format("{0} - {1}", rowView["Code"], rowView["Description"]);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		
		/// <summary>
		/// Sets the Charges for a given Charge Prefix. Called by ASPX's javascript's callback method.
		/// </summary>
		protected void rcbCharges_ItemsRequested(object o, RadComboBoxItemsRequestedEventArgs e)
		{
			SetCharge(e.Text);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Makes it so the RadComboBox's .Text shows multiple columns when selected.
		/// </summary>
		protected void rcbCharges_ItemDataBound(object sender, RadComboBoxItemEventArgs e)
		{
			DataRowView rowView = (DataRowView)e.Item.DataItem;

			e.Item.Text = String.Format("{0} - {1}", rowView["Code"], rowView["Description"]);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Makes it so the RadComboBox's .Text shows multiple columns when selected.
		/// </summary>
		protected void rcbVictims_ItemDataBound(object sender, RadComboBoxItemEventArgs e)
		{
			DataRowView rowView = (DataRowView)e.Item.DataItem;

			e.Item.Text = String.Format("{0} - {1}", rowView["VictimNumber"], rowView["Name"]);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Private Methods

		private void SetCharge(string sChargePrefixTypeID)
		{
			int chargePrefixTypeID = Int32.Parse(sChargePrefixTypeID);

			rcbCharges.DataSource = OffenderCharge.GetChargeTypesByChargePrefix(chargePrefixTypeID, true);
			rcbCharges.DataBind();
			rcbCharges.Items.Insert(0, new RadComboBoxItem());
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}