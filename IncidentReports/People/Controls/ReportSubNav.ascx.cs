﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReportHq.IncidentReports;

namespace ReportHq.IncidentReports.People.Controls
{
	public partial class ReportSubNav : System.Web.UI.UserControl
	{
		#region Enums

		public enum NavSections
		{
			Victims,
			Offenders,
			Descriptors
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Properties

		private NavSections _currentSection;
		private string _targetRedirectUrl;
		private string _validationGroup;
				
		/// <summary>
		/// Which main section link the user is currently at.
		/// </summary>
		public NavSections CurrentSection
		{
			get { return _currentSection; }
			set { _currentSection = value; }
		}

		/// <summary>
		/// Where to bring the user after clicking; used by the consuming .ASPX/ascx page
		/// </summary>
		public string TargetRedirectUrl
		{
			get { return _targetRedirectUrl; }
			set { _targetRedirectUrl = value; }
		}

		public string ValidationGroup
		{
			get { return _validationGroup; }
			set { _validationGroup = value; }
		}

		
		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		
		#endregion

		#region Events

		//public delegate for LinkButton nav-item click
		public delegate void NavItemClick(object sender, EventArgs e);

		//public event for LinkButton click; will be handled by the consuming pages
		public event NavItemClick NavItemClicked;

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
		
		#region Event Handlers

		protected void Page_Load(object sender, EventArgs e)
		{
			//set validation group used by page's validator controls
			if (!string.IsNullOrEmpty(_validationGroup))
			{
				lbVictims.ValidationGroup = _validationGroup;
				lbOffenders.ValidationGroup = _validationGroup;
				lbDescriptors.ValidationGroup = _validationGroup;
			}

			//set selected-item state
			switch (_currentSection)
			{
				case NavSections.Victims:
					lblVictims.Visible = true;
					lbVictims.Visible = false;
					break;

				case NavSections.Offenders:
					lblOffenders.Visible = true;
					lbOffenders.Visible = false;
					break;

				case NavSections.Descriptors:
					lblDescriptors.Visible = true;
					lbDescriptors.Visible = false;
					break;
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void lbVictims_Click(object sender, EventArgs e)
		{
			_targetRedirectUrl = "Victims.aspx";

			//fire event for consuming page to handler
			NavItemClicked(this, e);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void lbOffenders_Click(object sender, EventArgs e)
		{
			_targetRedirectUrl = "Offenders.aspx";

			//fire event for consuming page to handler
			NavItemClicked(this, e);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void lbDescriptors_Click(object sender, EventArgs e)
		{
			_targetRedirectUrl = "OffenderDescriptors.aspx";

			//fire event for consuming page to handler
			NavItemClicked(this, e);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Private Methods

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

	}
}