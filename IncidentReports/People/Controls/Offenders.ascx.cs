﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReportHq.Common;
using ReportHq.IncidentReports.Bases;
using ReportHq.IncidentReports.Controls;
using Telerik.Web.UI;

namespace ReportHq.IncidentReports.People.Controls
{
	public partial class Offenders : System.Web.UI.UserControl
	{
		#region Variables

		private int _reportID;
		ReportPage _reportPage;
		Report _report;

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Event Handlers

		protected void Page_Load(object sender, EventArgs e)
		{
			_reportPage = (ReportPage)this.Page;
			_reportPage.SyncSessionToViewState();
			_report = _reportPage.Report;			
			_reportID = _report.ID;

			if (!_reportPage.UserHasWriteAccess)
			{
				//change "Edit" to "View"
				((GridEditCommandColumn)gridOffenders.Columns[1]).EditText = "View";

				//hide "Delete"
				gridOffenders.Columns[2].Visible = false;

				//disable Add command
				GridTableView masterTableView = gridOffenders.MasterTableView;
				masterTableView.CommandItemDisplay = GridCommandItemDisplay.None;

				//disable Charges child table stuff
				GridTableView chargesTable = masterTableView.DetailTables[0];
				chargesTable.Columns[0].Visible = false;
				chargesTable.Columns[1].Visible = false;
				chargesTable.CommandItemDisplay = GridCommandItemDisplay.None;
			}

			//setting definitions in code-behind so we can get the master page's label
			ajaxManager.AjaxSettings.AddAjaxSetting(gridOffenders, gridOffenders); //grid updates itself
			ajaxManager.AjaxSettings.AddAjaxSetting(gridOffenders, _reportPage.Master.FindControl("lblMessage")); //grid updates label
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void ucReportSubNav_NavItemClicked(object sender, EventArgs e)
		{
			_reportPage.ProcessNavClick((sender as ReportSubNav).TargetRedirectUrl, false);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Fired when grid needs a datasource. eg, after editing or deleting. This event fails if a databind is specified - it has to do its own databind.
		/// </summary>
		protected void gridOffenders_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
		{
			//get table of current offenders from db
			gridOffenders.DataSource = GetData();
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Fired when grid needs a datasource. eg, after editing or deleting. This event fails if a databind is specified - it has to do its own databind.
		/// </summary>
		protected void gridOffenders_DetailTableDataBind(object source, GridDetailTableDataBindEventArgs e)
		{			
			GridDataItem parentItem = e.DetailTableView.ParentItem as GridDataItem;
						
			//came from sdk example. if parent is in edit-mode dont update this
			if (parentItem.Edit)
				return;

			int offenderID = (int)parentItem.GetDataKeyValue("OffenderID");
			
			e.DetailTableView.DataSource = OffenderCharge.GetOffenderChargesByOffender(offenderID);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Hide the "Add/Show Charges" column when in edit-mode, as it takes up too much space and awkwardly pushes the edit form area to the right.
		/// </summary>
		protected void gridOffenders_ItemCreated(object sender, GridItemEventArgs e)
		{
			if (e.Item is GridEditFormItem && e.Item.IsInEditMode)
			{
				GridEditFormItem item = (GridEditFormItem)e.Item;
				item.Cells[0].Visible = false;
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void gridOffenders_InsertCommand(object source, GridCommandEventArgs e)
		{
			GridEditableItem editableItem = e.Item as GridEditableItem;
			GridTableView tableView = editableItem.OwnerTableView;

			switch (tableView.Name)
			{
				case "offenders":
					AddOffender(e, editableItem);
					break;

				case "offenderCharges":
					AddCharge(e, editableItem, (int)tableView.ParentItem.GetDataKeyValue("OffenderID"));
					break;
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void gridOffenders_UpdateCommand(object sender, GridCommandEventArgs e)
		{
			GridEditableItem editableItem = e.Item as GridEditableItem;
			GridTableView tableView = editableItem.OwnerTableView;

			switch (tableView.Name)
			{
				case "offenders":
					UpdateOffender(e, editableItem);
					break;

				case "offenderCharges":
					UpdateCharge(e, editableItem);
					break;
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void gridOffenders_DeleteCommand(object sender, GridCommandEventArgs e)
		{
			GridEditableItem editableItem = e.Item as GridEditableItem;
			GridTableView tableView = editableItem.OwnerTableView;

			switch (tableView.Name)
			{
				case "offenders":
					DeleteOffender(editableItem);
					break;

				case "offenderCharges":
					DeleteCharge(editableItem);
					break;
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void gridOffenders_CancelCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
		{
			//restore the "Add/Show Charges" column, which was hidden on Edit
			gridOffenders.Columns[0].Visible = true;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// We can set a custom Exapand/Collaspe link here.
		/// </summary>
		protected void gridOffenders_PreRender(object sender, EventArgs e)
		{
			foreach (GridDataItem dataItem in gridOffenders.MasterTableView.Items)
				(dataItem["ExpandCollapse"].FindControl("lbExpandCollaspe") as LinkButton).Text = dataItem.Expanded ? "Hide Charges" : "Add/Show Charges";
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Private Methods

		private DataTable GetData()
		{
			return Offender.GetOffendersByReport(_reportID);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		
		private void AddOffender(GridCommandEventArgs e, GridEditableItem editableItem)
		{
			//get grid's user control
			GridForms.OffenderForm userControl = (GridForms.OffenderForm)e.Item.FindControl(GridEditFormItem.EditFormUserControlID);

			//get values into hashtable
			Hashtable newValues = GetOffenderFormValues(userControl);

			Offender offender = new Offender(newValues);
			offender.ReportID = _reportID;

			StatusInfo status = offender.Add(_reportPage.Username);

			if (status.Success)
			{
				_report.Offenders = null; //invalidate cached collection, used by other pages
			}
			else
			{
				_reportPage.RenderUserMessage(Enums.UserMessageTypes.Warning, "Sorry, there was a problem: " + status.Message);
			}
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		private void AddCharge(GridCommandEventArgs e, GridEditableItem editableItem, int offenderID)
		{
			//get grid's user control
			GridForms.ChargeForm userControl = (GridForms.ChargeForm)e.Item.FindControl(GridEditFormItem.EditFormUserControlID);

			//get values into hashtable
			Hashtable newValues = GetChargeFormValues(userControl);

			OffenderCharge charge = new OffenderCharge(newValues);

			charge.OffenderID = offenderID;
			//charge.OffenderID = (int)tableView.ParentItem.GetDataKeyValue("OffenderID");

			StatusInfo status = charge.Add(_reportPage.Username);

			if (!status.Success)
			{
				_reportPage.RenderUserMessage(Enums.UserMessageTypes.Warning, "Sorry, there was a problem: " + status.Message);
			}
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		private void UpdateOffender(GridCommandEventArgs e, GridEditableItem editableItem)
		{
			int offenderID = (int)editableItem.GetDataKeyValue("OffenderID");

			//get values into hashtable
			Hashtable newValues = GetOffenderFormValues((GridForms.OffenderForm)e.Item.FindControl(GridEditFormItem.EditFormUserControlID));

			Offender offender = new Offender(newValues);
			offender.ID = offenderID;

			StatusInfo status = offender.Update(_reportPage.Username);

			if (status.Success)
			{
				gridOffenders.Columns[0].Visible = true; //restore grid's hidden "Add/Show Charges" link column
				_report.Offenders = null; //invalidate cached collection, used by other pages
			}
			else
			{
				_reportPage.RenderUserMessage(Enums.UserMessageTypes.Warning, "Sorry, there was a problem: " + status.Message);
			}
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		private void UpdateCharge(GridCommandEventArgs e, GridEditableItem editableItem)
		{
			int offenderChargeID = (int)editableItem.GetDataKeyValue("ID");

			//get values into hashtable
			Hashtable newValues = GetChargeFormValues((GridForms.ChargeForm)e.Item.FindControl(GridEditFormItem.EditFormUserControlID));

			OffenderCharge charge = new OffenderCharge(offenderChargeID, newValues);

			StatusInfo status = charge.Update(_reportPage.Username);

			if (!status.Success)
			{
				_reportPage.RenderUserMessage(Enums.UserMessageTypes.Warning, "Sorry, there was a problem: " + status.Message);
			}
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		private void DeleteOffender(GridEditableItem editableItem)
		{
			int offenderID = (int)editableItem.GetDataKeyValue("OffenderID");

			StatusInfo status = Offender.DeleteOffender(offenderID);

			if (status.Success)
			{
				_report.Offenders = null; //invalidate cached collection, used by other pages
			}
			else
			{
				_reportPage.RenderUserMessage(Enums.UserMessageTypes.Warning, "Sorry, there was a problem: " + status.Message);
			}
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		private void DeleteCharge(GridEditableItem editableItem)
		{
			int offenderChargeID = (int)editableItem.GetDataKeyValue("ID");

			StatusInfo status = OffenderCharge.DeleteOffenderCharge(offenderChargeID);

			if (!status.Success)
			{
				_reportPage.RenderUserMessage(Enums.UserMessageTypes.Warning, "Sorry, there was a problem: " + status.Message);
			}
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		private Hashtable GetOffenderFormValues(GridForms.OffenderForm userControl)
		{
			Hashtable newValues = new Hashtable();
			double? userDoubleValue;
			string userStringValue;

			////status type
			//userStringValue = (userControl.FindControl("rcbStatusTypes") as RadComboBox).SelectedValue;
			//if (!string.IsNullOrEmpty(userStringValue))
			//    newValues["OffenderStatusTypeID"] = Int32.Parse(userStringValue);
			//else
			//    newValues["OffenderStatusTypeID"] = -1;

			newValues["OffenderNumber"] = Convert.ToInt32((userControl.FindControl("rntbOffenderNumber") as RadNumericTextBox).Value);
			newValues["OffenderStatusTypeID"] = Int32.Parse((userControl.FindControl("rcbStatusTypes") as RadComboBox).SelectedValue);
			newValues["LastName"] = (userControl.FindControl("tbLastName") as TextBox).Text;
			newValues["FirstName"] = (userControl.FindControl("tbFirstName") as TextBox).Text;
			newValues["Nickname"] = (userControl.FindControl("tbNickname") as TextBox).Text;

			//race
			userStringValue = (userControl.FindControl("rcbRace") as RadComboBox).SelectedValue;
			if (!string.IsNullOrEmpty(userStringValue))
				newValues["RaceTypeID"] = Int32.Parse(userStringValue);
			else
				newValues["RaceTypeID"] = -1;

			//gender
			userStringValue = (userControl.FindControl("rcbGender") as RadComboBox).SelectedValue;
			if (!string.IsNullOrEmpty(userStringValue))
				newValues["GenderTypeID"] = Int32.Parse(userStringValue);
			else
				newValues["GenderTypeID"] = -1;

			newValues["DateOfBirth"] = (userControl.FindControl("rdiDateOfBirth") as RadDateInput).SelectedDate;

			//height
			userStringValue = (userControl.FindControl("rmtbHeight") as RadMaskedTextBox).TextWithLiterals;
			if (userStringValue != "'") //if field is not filled in, a single "'" is present from our mask
				newValues["Height"] = Conversions.GetInchesFromHeight(userStringValue);
			else
				newValues["Height"] = -1;

			//weight
			userDoubleValue = (userControl.FindControl("rntbWeight") as RadNumericTextBox).Value;
			if (userDoubleValue > 0)
				newValues["Weight"] = Convert.ToInt32(userDoubleValue);
			else
				newValues["Weight"] = -1;

			newValues["StreetAddress"] = (userControl.FindControl("tbStreetAddress") as TextBox).Text;
			newValues["City"] = (userControl.FindControl("tbCity") as TextBox).Text;
			newValues["State"] = (userControl.FindControl("rcbState") as RadComboBox).SelectedValue;

			//zip
			userDoubleValue = (userControl.FindControl("rmtbZipCode") as RadNumericTextBox).Value;
			if (userDoubleValue > 0)
				newValues["ZipCode"] = Convert.ToInt32(userDoubleValue);
			else
				newValues["ZipCode"] = -1;

			newValues["SocialSecurityNumber"] = (userControl.FindControl("rmtbSsn") as RadMaskedTextBox).Text;
			newValues["DriversLicenseNumber"] = (userControl.FindControl("tbDriversLicense") as TextBox).Text;

			//sobriety
			userStringValue = (userControl.FindControl("rcbSobrietyTypes") as RadComboBox).SelectedValue;
			if (!string.IsNullOrEmpty(userStringValue))
				newValues["SobrietyTypeID"] = Int32.Parse(userStringValue);
			else
				newValues["SobrietyTypeID"] = -1;

			//injury
			userStringValue = (userControl.FindControl("rcbInjuryTypes") as RadComboBox).SelectedValue;
			if (!string.IsNullOrEmpty(userStringValue))
				newValues["InjuryTypeID"] = Int32.Parse(userStringValue);
			else
				newValues["InjuryTypeID"] = -1;

			//treated
			userStringValue = (userControl.FindControl("rcbTreatedTypes") as RadComboBox).SelectedValue;
			if (!string.IsNullOrEmpty(userStringValue))
				newValues["TreatedTypeID"] = Int32.Parse(userStringValue);
			else
				newValues["TreatedTypeID"] = -1;

			//arrest type
			userStringValue = (userControl.FindControl("rcbArrestTypes") as RadComboBox).SelectedValue;
			if (!string.IsNullOrEmpty(userStringValue))
				newValues["ArrestTypeID"] = Int32.Parse(userStringValue);
			else
				newValues["ArrestTypeID"] = -1;


			//arrest date & time (make new date/string from two parts)

			//if user selects a date but no time, it will default to 12:00:00 AM.
			string datePortion = "1/1/1900";
			string timePortion = "12:00:00 AM";

			//set the date portion
			if ((userControl.FindControl("rdiArrestDate") as RadDateInput).SelectedDate != null)
				datePortion = ((DateTime)(userControl.FindControl("rdiArrestDate") as RadDateInput).SelectedDate).ToShortDateString();

			//set the time portion
			if ((userControl.FindControl("rdiArrestTime") as RadDateInput).SelectedDate != null)
				timePortion = ((DateTime)(userControl.FindControl("rdiArrestTime") as RadDateInput).SelectedDate).ToShortTimeString();

			newValues["ArrestDateTime"] = DateTime.Parse(datePortion + " " + timePortion);

			newValues["ArrestLocation"] = (userControl.FindControl("tbArrestLocation") as TextBox).Text;
			newValues["ArrestCredit"] = (userControl.FindControl("tbArrestCredit") as TextBox).Text;
			newValues["TransportedBy"] = (userControl.FindControl("tbTransportedBy") as TextBox).Text;
			newValues["TransportUnit"] = (userControl.FindControl("tbTransportUnit") as TextBox).Text;

			//rights waived form
			userDoubleValue = (userControl.FindControl("rntbRightsWaivedForm") as RadNumericTextBox).Value;
			if (userDoubleValue > 0)
				newValues["RightsWaivedFormNumber"] = Convert.ToInt32(userDoubleValue);
			else
				newValues["RightsWaivedFormNumber"] = -1;

			//resident type
			userStringValue = (userControl.FindControl("rcbResidentTypes") as RadComboBox).SelectedValue;
			if (!string.IsNullOrEmpty(userStringValue))
				newValues["ResidentTypeID"] = Int32.Parse(userStringValue);
			else
				newValues["ResidentTypeID"] = -1;

			//juv disposition
			userStringValue = (userControl.FindControl("rcbJuvenileDispositionTypes") as RadComboBox).SelectedValue;
			if (!string.IsNullOrEmpty(userStringValue))
				newValues["JuvenileDispositionTypeID"] = Int32.Parse(userStringValue);
			else
				newValues["JuvenileDispositionTypeID"] = -1;

			//district
			userStringValue = (userControl.FindControl("rcbDistricts") as RadComboBox).SelectedValue;
			if (!string.IsNullOrEmpty(userStringValue))
				newValues["District"] = Int32.Parse(userStringValue);
			else
				newValues["District"] = -1;

			newValues["Zone"] = (userControl.FindControl("rcbZones") as RadComboBox).SelectedValue;
			newValues["SubZone"] = (userControl.FindControl("rcbSubZones") as RadComboBox).SelectedValue;


			return newValues;
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		private Hashtable GetChargeFormValues(GridForms.ChargeForm userControl)
		{
			Hashtable newValues = new Hashtable();
			string userStringValue;

			//charge
			newValues["ChargeTypeID"] = Int32.Parse((userControl.FindControl("rcbCharges") as RadComboBox).SelectedValue);
			newValues["ChargeDetails"] = (userControl.FindControl("tbChargeDetails") as TextBox).Text;

			//victim
			userStringValue = (userControl.FindControl("rcbVictims") as RadComboBox).SelectedValue;
			if (!string.IsNullOrEmpty(userStringValue))
				newValues["VictimPersonID"] = Int32.Parse(userStringValue);
			else
				newValues["VictimPersonID"] = -1;

			//relationship
			userStringValue = (userControl.FindControl("rcbVictimOffenderRelationshipTypes") as RadComboBox).SelectedValue;

			if (!string.IsNullOrEmpty(userStringValue))
				newValues["VictimOffenderRelationshipTypeID"] = Int32.Parse(userStringValue);
			else
				newValues["VictimOffenderRelationshipTypeID"] = -1;


			return newValues;
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}