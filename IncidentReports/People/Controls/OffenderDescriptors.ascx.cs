﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReportHq.Common;
using ReportHq.IncidentReports.Bases;
using ReportHq.IncidentReports.Controls;
using Telerik.Web.UI;

namespace ReportHq.IncidentReports.People.Controls
{
	public partial class OffenderDescriptors : System.Web.UI.UserControl
	{
		#region Global Variables

		ReportPage _reportPage;

		#endregion

		#region Properties

		int _offenderID;

		public int OffenderID
		{
			get { return _offenderID; }
			set { _offenderID = value; }
		}

		public string AdditionalDescription
		{
			get { return tbAdditionalDesc.Text; }
			set { tbAdditionalDesc.Text = value; }
		}

		public List<DescriptionItem> BuildCheckedItems
		{
			get { return Common.GetCheckedItems(cblBuilds); }
			set
			{
				cblBuilds.ClearSelection();
				if (value.Count > 0)
					Common.SetCheckedItems(value, cblBuilds);
			}
		}

		public List<DescriptionItem> OddityCheckedItems
		{
			get { return Common.GetCheckedItems(cblOddities); }
			set
			{
				cblOddities.ClearSelection();
				if (value.Count > 0)
					Common.SetCheckedItems(value, cblOddities);
			}
		}

		public List<DescriptionItem> ScarCheckedItems
		{
			get { return Common.GetCheckedItems(cblScars); }
			set
			{
				cblScars.ClearSelection();
				if (value.Count > 0)
					Common.SetCheckedItems(value, cblScars);
			}
		}

		public List<DescriptionItem> TattooCheckedItems
		{
			get { return Common.GetCheckedItems(cblTattoos); }
			set
			{
				cblTattoos.ClearSelection();
				if (value.Count > 0)
					Common.SetCheckedItems(value, cblTattoos);
			}
		}

		public List<DescriptionItem> ApparelCheckedItems
		{
			get { return Common.GetCheckedItems(cblApparel); }
			set
			{
				cblApparel.ClearSelection();
				if (value.Count > 0)
					Common.SetCheckedItems(value, cblApparel);
			}
		}

		public List<DescriptionItem> SpeechCheckedItems
		{
			get { return Common.GetCheckedItems(cblSpeech); }
			set
			{
				cblSpeech.ClearSelection();
				if (value.Count > 0)
					Common.SetCheckedItems(value, cblSpeech);
			}
		}

		public List<DescriptionItem> AccentCheckedItems
		{
			get { return Common.GetCheckedItems(cblAccent); }
			set
			{
				cblAccent.ClearSelection();
				if (value.Count > 0)
					Common.SetCheckedItems(value, cblAccent);
			}
		}

		public List<DescriptionItem> FacialOddityCheckedItems
		{
			get { return Common.GetCheckedItems(cblFacialOddities); }
			set
			{
				cblFacialOddities.ClearSelection();
				if (value.Count > 0)
					Common.SetCheckedItems(value, cblFacialOddities);
			}
		}

		public List<DescriptionItem> EyeCheckedItems
		{
			get { return Common.GetCheckedItems(cblEyes); }
			set
			{
				cblEyes.ClearSelection();
				if (value.Count > 0)
					Common.SetCheckedItems(value, cblEyes);
			}
		}

		public List<DescriptionItem> NoseCheckedItems
		{
			get { return Common.GetCheckedItems(cblNose); }
			set
			{
				cblNose.ClearSelection();
				if (value.Count > 0)
					Common.SetCheckedItems(value, cblNose);
			}
		}

		public List<DescriptionItem> TeethCheckedItems
		{
			get { return Common.GetCheckedItems(cblTeeth); }
			set
			{
				cblTeeth.ClearSelection();
				if (value.Count > 0)
					Common.SetCheckedItems(value, cblTeeth);
			}
		}

		public List<DescriptionItem> HairColorCheckedItems
		{
			get { return Common.GetCheckedItems(cblHairColors); }
			set
			{
				cblHairColors.ClearSelection();
				if (value.Count > 0)
					Common.SetCheckedItems(value, cblHairColors);
			}
		}

		public List<DescriptionItem> HairStyleCheckedItems
		{
			get { return Common.GetCheckedItems(cblHairStyles); }
			set
			{
				cblHairStyles.ClearSelection();
				if (value.Count > 0)
					Common.SetCheckedItems(value, cblHairStyles);
			}
		}

		public List<DescriptionItem> FacialHairCheckedItems
		{
			get { return Common.GetCheckedItems(cblFacialHair); }
			set
			{
				cblFacialHair.ClearSelection();
				if (value.Count > 0)
					Common.SetCheckedItems(value, cblFacialHair);
			}
		}

		public List<DescriptionItem> ComplexionCheckedItems
		{
			get { return Common.GetCheckedItems(cblComplexion); }
			set
			{
				cblComplexion.ClearSelection();
				if (value.Count > 0)
					Common.SetCheckedItems(value, cblComplexion);
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Events

		//public delegate for LinkButton nav-item click
		public delegate void NavItemClick(object sender, EventArgs e);

		//public event for LinkButton click; will be handled by the consuming pages
		public event NavItemClick NavItemClicked;

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Event Handlers

		/// <summary>
		/// Bind form lists.
		/// </summary>
		protected void Page_Init(object sender, EventArgs e)
		{
			//usually a webform's dropdownlists are populated in the Page_Load in the !IsPostBack() check. however the viewstate hidden HTML variable is getting too fat w/ all these in it, 
			//slows things down. so instead we're binding these dropdownlists in the Page_Init *every* page load. awful, right? well the good news is the BAL caches these datatables on the 
			//web server, so it doesnt actually have to hit the db every time. also, the db server is likely near the web server so even on refreshes the hit isnt too bad. and because
			//they are rendered as normal HTML controls, the selected state of these lists is remembered on postback and doesnt need the viewstate collection.

			BindFormLists();			
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void Page_Load(object sender, EventArgs e)
		{
			_reportPage = (ReportPage)this.Page;

			if (!string.IsNullOrEmpty(rcbOffenders.SelectedValue))
				_offenderID = Int32.Parse(rcbOffenders.SelectedValue);

			//on first load, set the hidden value to the first offender from dropdroplist
			if (!IsPostBack)
				hidPreviousOffenderID.Value = rcbOffenders.SelectedValue;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Format multiple columns for display in RadComboBox's .Text.
		/// </summary>
		protected void rcbOffenders_ItemDataBound(object sender, RadComboBoxItemEventArgs e)
		{
			DataRowView rowView = (DataRowView)e.Item.DataItem;

			e.Item.Text = String.Format("{0} - {1}, {2}", rowView["OffenderNumber"], rowView["LastName"], rowView["FirstName"]);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// User changed the Offender, so load its descriptors into form controls.
		/// </summary>
		protected void rcbOffenders_SelectedIndexChanged(object o, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
		{
			int previouslySelectedOffenderID = 0;

			if (!string.IsNullOrEmpty(hidPreviousOffenderID.Value))
				previouslySelectedOffenderID = Int32.Parse(hidPreviousOffenderID.Value);


			//if appropriate, save previously-selected offender
			if (_reportPage.UserHasWriteAccess && previouslySelectedOffenderID > 0)
			{
				StatusInfo status = SaveOffender(previouslySelectedOffenderID); //TODO: take in an offenderID param and have the .aspx use this as a public method?

				if (status.Success == false)
				{
					_reportPage.RenderUserMessage(Enums.UserMessageTypes.Warning, "Sorry, there was a problem saving: " + status.Message);

					//NEW - set listbox back to failed item.
					rcbOffenders.SelectedValue = previouslySelectedOffenderID.ToString();
				}
				else //saved. change UI
				{
					SetFormValues();

					//set hidden var to this offenderID
					hidPreviousOffenderID.Value = rcbOffenders.SelectedValue;
				}
			}
			else //not a save, so just change UI to new offender
			{
				SetFormValues();

				//set hidden var to this offenderID
				hidPreviousOffenderID.Value = rcbOffenders.SelectedValue;
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void ucReportSubNav_NavItemClicked(object sender, EventArgs e)
		{
			//fire event for consuming page to handler
			NavItemClicked(sender, e);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Public Methods

		/// <summary>
		/// Binds the "Offender:" dropdownlist with the Offenders from this Report.
		/// </summary>
		public void BindOffendersList(int reportID)
		{
			rcbOffenders.DataSource = Offender.GetOffendersByReport(reportID);
			rcbOffenders.DataBind();

			if (!string.IsNullOrEmpty(rcbOffenders.SelectedValue))
				_offenderID = Int32.Parse(rcbOffenders.SelectedValue);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Sets the various checkboxlists on this form with values from the currently-selected OffenderID.
		/// </summary>
		public void SetFormValues()
		{
			Offender offender = new Offender(_offenderID);

			//pass report's select-items datatables to control. controls sets checks
			this.BuildCheckedItems = offender.DescriptionBuildItems;
			this.OddityCheckedItems = offender.DescriptionOddityItems;
			this.ScarCheckedItems = offender.DescriptionScarItems;
			this.TattooCheckedItems = offender.DescriptionTattooItems;
			this.ApparelCheckedItems = offender.DescriptionApparelItems;
			this.SpeechCheckedItems = offender.DescriptionSpeechItems;
			this.AccentCheckedItems = offender.DescriptionAccentItems;
			this.FacialOddityCheckedItems = offender.DescriptionFacialOddityItems;
			this.EyeCheckedItems = offender.DescriptionEyeItems;
			this.NoseCheckedItems = offender.DescriptionNoseItems;
			this.TeethCheckedItems = offender.DescriptionTeethItems;
			this.HairColorCheckedItems = offender.DescriptionHairColorItems;
			this.HairStyleCheckedItems = offender.DescriptionHairStyleItems;
			this.FacialHairCheckedItems = offender.DescriptionFacialHairItems;
			this.ComplexionCheckedItems = offender.DescriptionComplexionItems;

			this.AdditionalDescription = offender.AdditionalDescription;
		}
		
		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Saves the current checkbox form items to the supplied OffenderID. Be sure this is called before binding a new offender's values to the form controls.
		/// </summary>
		public StatusInfo SaveOffender(int offenderID)
		{
			Offender offender = new Offender(); //just using an empty offender for the update-offender-description only.
			offender.ID = offenderID;

			//2) get form values from control
			offender.DescriptionBuildItems = Common.GetCheckedItems(cblBuilds);
			offender.DescriptionOddityItems = Common.GetCheckedItems(cblOddities);
			offender.DescriptionScarItems = Common.GetCheckedItems(cblScars);
			offender.DescriptionTattooItems = Common.GetCheckedItems(cblTattoos);
			offender.DescriptionApparelItems = Common.GetCheckedItems(cblApparel);
			offender.DescriptionSpeechItems = Common.GetCheckedItems(cblSpeech);
			offender.DescriptionAccentItems = Common.GetCheckedItems(cblAccent);
			offender.DescriptionFacialOddityItems = Common.GetCheckedItems(cblFacialOddities);
			offender.DescriptionEyeItems = Common.GetCheckedItems(cblEyes);
			offender.DescriptionNoseItems = Common.GetCheckedItems(cblNose);
			offender.DescriptionTeethItems = Common.GetCheckedItems(cblTeeth);
			offender.DescriptionHairColorItems = Common.GetCheckedItems(cblHairColors);
			offender.DescriptionHairStyleItems = Common.GetCheckedItems(cblHairStyles);
			offender.DescriptionFacialHairItems = Common.GetCheckedItems(cblFacialHair);
			offender.DescriptionComplexionItems = Common.GetCheckedItems(cblComplexion);

			offender.AdditionalDescription = tbAdditionalDesc.Text;

			return offender.UpdateOffenderDescription(_reportPage.Username);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
		
		#region Private Methods

		/// <summary>
		/// Binds all of Report HQ's offender descriptor items to the form controls. Not the Offender's, just the possible descriptor items.
		/// </summary>
		private void BindFormLists()
		{			
			//build
			cblBuilds.DataSource = Offender.GetOffenderBuildTypes();
			cblBuilds.DataBind();

			//oddities
			cblOddities.DataSource = Offender.GetOffenderOddityTypes();
			cblOddities.DataBind();

			//scars
			cblScars.DataSource = Offender.GetOffenderScarTypes();
			cblScars.DataBind();

			//tattoos
			cblTattoos.DataSource = Offender.GetOffenderTattooTypes();
			cblTattoos.DataBind();

			//apparel
			cblApparel.DataSource = Offender.GetOffenderApparelTypes();
			cblApparel.DataBind();

			//speech
			cblSpeech.DataSource = Offender.GetOffenderSpeechTypes();
			cblSpeech.DataBind();

			//accent
			cblAccent.DataSource = Offender.GetOffenderAccentTypes();
			cblAccent.DataBind();

			//facialOddities
			cblFacialOddities.DataSource = Offender.GetOffenderFacialOddityTypes();
			cblFacialOddities.DataBind();

			//eyes
			cblEyes.DataSource = Offender.GetOffenderEyeTypes();
			cblEyes.DataBind();

			//nose
			cblNose.DataSource = Offender.GetOffenderNoseTypes();
			cblNose.DataBind();

			//teeth
			cblTeeth.DataSource = Offender.GetOffenderTeethTypes();
			cblTeeth.DataBind();

			//hair colors
			cblHairColors.DataSource = Offender.GetOffenderHairColorTypes();
			cblHairColors.DataBind();

			//hair styles
			cblHairStyles.DataSource = Offender.GetOffenderHairStyleTypes();
			cblHairStyles.DataBind();

			//facial hair
			cblFacialHair.DataSource = Offender.GetOffenderFacialHairTypes();
			cblFacialHair.DataBind();

			//complexion
			cblComplexion.DataSource = Offender.GetOffenderComplexionTypes();
			cblComplexion.DataBind();
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		
		#endregion
	}
}