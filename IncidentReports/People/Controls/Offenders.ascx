﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Offenders.ascx.cs" Inherits="ReportHq.IncidentReports.People.Controls.Offenders" %>
<%@ Register TagPrefix="ReportHq" TagName="ReportSubNav" src="ReportSubNav.ascx" %>

<ReportHq:ReportSubNav ID="ucReportSubNav" CurrentSection="Offenders" OnNavItemClicked="ucReportSubNav_NavItemClicked" runat="server" />

<div class="formBox" style="width:640px;">

	<Telerik:RadGrid ID="gridOffenders" 
		AllowPaging="false"
		AllowSorting="true" 				
		AutoGenerateColumns="False"
		AllowMultiRowSelection="true"
		OnNeedDataSource="gridOffenders_NeedDataSource"
		OnDetailTableDataBind="gridOffenders_DetailTableDataBind" 
		OnItemCreated="gridOffenders_ItemCreated"
		OnInsertCommand="gridOffenders_InsertCommand"
		OnUpdateCommand="gridOffenders_UpdateCommand" 
		OnDeleteCommand="gridOffenders_DeleteCommand" 
		OnCancelCommand="gridOffenders_CancelCommand"
		
		OnPreRender="gridOffenders_PreRender"	
		runat="server">

		<ClientSettings 
			EnableRowHoverStyle="true"
			AllowDragToGroup="false" 
			AllowColumnsReorder="false" 
			AllowKeyboardNavigation="true"
			ReorderColumnsOnClient="false" >
			<Resizing AllowColumnResize="false" AllowRowResize="false" />
			<Selecting AllowRowSelect="false" />
		</ClientSettings>
			
		<MasterTableView 
			Name="offenders"				
			CommandItemDisplay="Top" 
			DataKeyNames="OffenderID" 
			EditMode="EditForms" 
			NoMasterRecordsText="No Offenders to display." 
			CommandItemStyle-Font-Bold="true" 
			HeaderStyle-ForeColor="#191970" 
			ItemStyle-ForeColor="black" 
			ExpandCollapseColumn-Display="false"
			AlternatingItemStyle-ForeColor="black">
				
			<CommandItemSettings AddNewRecordText="Add Offender" ShowRefreshButton="false" />
			<CommandItemStyle CssClass="commandItem" />
				
			<Columns>
				<Telerik:GridTemplateColumn UniqueName="ExpandCollapse" ItemStyle-Width="134" ItemStyle-HorizontalAlign="center" >
					<ItemTemplate>
						<asp:LinkButton CommandName="ExpandCollapse" ID="lbExpandCollaspe" CssClass="commandItem" runat="server" />
					</ItemTemplate>
				</Telerik:GridTemplateColumn>				
				<Telerik:GridEditCommandColumn ButtonType="LinkButton" UniqueName="editColumn" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="center" ItemStyle-CssClass="commandItem" />
				<Telerik:GridButtonColumn CommandName="Delete" ButtonType="LinkButton" Text="Delete" UniqueName="deleteColumn" ConfirmText="Delete Offender? (This will also remove Charges, Description, Gists, and Domestic Violence details for Offender)" ConfirmTitle="Delete" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="center" ItemStyle-CssClass="commandItem" />
				<Telerik:GridBoundColumn HeaderText="OffenderID" UniqueName="OffenderID" DataField="OffenderID" ReadOnly="true" Visible="false"/>
				<Telerik:GridBoundColumn HeaderText="Number" UniqueName="OffenderNumber" DataField="OffenderNumber" HeaderStyle-Wrap="false" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="center" />
				<Telerik:GridBoundColumn HeaderText="Status" UniqueName="Status" DataField="Status" HeaderStyle-Wrap="false" />
				<Telerik:GridBoundColumn HeaderText="Last Name" UniqueName="LastName" DataField="LastName" HeaderStyle-Wrap="false" />
				<Telerik:GridBoundColumn HeaderText="First Name" UniqueName="FirstName" DataField="FirstName" HeaderStyle-Wrap="false" />
				<Telerik:GridBoundColumn HeaderText="Nickname" UniqueName="Nickname" DataField="Nickname" Visible="false" />
				<Telerik:GridBoundColumn HeaderText="Race" UniqueName="Race" DataField="Race" HeaderStyle-Wrap="false" />
				<Telerik:GridBoundColumn HeaderText="Sex" UniqueName="Gender" DataField="Gender" HeaderStyle-Wrap="false" />
				<Telerik:GridDateTimeColumn HeaderText="DOB" UniqueName="DateOfBirth" DataField="DateOfBirth" DataFormatString="{0:d}" />
			</Columns>
				
			<EditFormSettings EditFormType="WebUserControl" UserControlName="controls/GridForms/OffenderForm.ascx" />
				
			<%-- CHARGES --%>
			<DetailTables>			
				<Telerik:GridTableView 
					Width="100%"
					Name="offenderCharges" 
					DataKeyNames="ID" 
					AutoGenerateColumns="false" 
					EditMode="EditForms" 
					Caption="&nbsp;" 
					CommandItemDisplay="Top" 
					NoDetailRecordsText="No Charges" 
					CssClass="setMargin">
						
					<ParentTableRelation>
						<Telerik:GridRelationFields MasterKeyField="OffenderID" DetailKeyField="OffenderID"/>
					</ParentTableRelation>
						
					<CommandItemSettings AddNewRecordText="Add Charge" ShowRefreshButton="false" />
					<CommandItemStyle CssClass="commandItem" />
											
					<Columns>
						<Telerik:GridEditCommandColumn ButtonType="LinkButton" UniqueName="editColumn" ItemStyle-HorizontalAlign="center" ItemStyle-CssClass="commandItem" />
						<Telerik:GridButtonColumn CommandName="Delete" ButtonType="LinkButton" Text="Delete" UniqueName="deleteColumn" ConfirmText="Delete charge?" ConfirmTitle="Delete" ItemStyle-HorizontalAlign="center" ItemStyle-CssClass="commandItem" />
						<Telerik:GridBoundColumn HeaderText="ID" UniqueName="ID" DataField="ID" Visible="False" ReadOnly="true"/>
						<Telerik:GridBoundColumn HeaderText="OffenderID" UniqueName="OffenderID" DataField="OffenderID" ReadOnly="true" Visible="false"/>
						<Telerik:GridBoundColumn HeaderText="Charge" UniqueName="Charge" DataField="Charge" />
						<Telerik:GridBoundColumn HeaderText="Victim #" UniqueName="VictimNumber" DataField="VictimNumber" HeaderStyle-Wrap="false" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="center" />
						<Telerik:GridBoundColumn HeaderText="Name" UniqueName="Name" DataField="Name" />
						<Telerik:GridBoundColumn HeaderText="Vic. Relationship" UniqueName="Relationsip" DataField="VictimsRelationship" HeaderStyle-Wrap="false" />
					</Columns>
						
					<EditFormSettings EditFormType="WebUserControl" UserControlName="controls/GridForms/ChargeForm.ascx" />
						
				</Telerik:GridTableView>
					
			</DetailTables>
				
		</MasterTableView>
	</Telerik:RadGrid>
	
</div>

<telerik:RadAjaxManager ID="ajaxManager" DefaultLoadingPanelID="ajaxLoadingPanel" runat="server" />