﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Victims.ascx.cs" Inherits="ReportHq.IncidentReports.People.Controls.Admin" %>
<%@ Register TagPrefix="ReportHq" TagName="ReportSubNav" src="ReportSubNav.ascx" %>

<ReportHq:ReportSubNav ID="ucReportSubNav" CurrentSection="Victims" OnNavItemClicked="ucReportSubNav_NavItemClicked" runat="server" />

<div class="formBox" style="width:640px;">

	<Telerik:RadGrid ID="gridVictims" 
		AllowPaging="false"
		AllowSorting="true" 				
		AutoGenerateColumns="False"
		AllowMultiRowSelection="true"
		OnNeedDataSource="gridVictims_NeedDataSource" 
		OnInsertCommand="gridVictims_InsertCommand"		
		OnUpdateCommand="gridVictims_UpdateCommand" 
		OnDeleteCommand="gridVictims_DeleteCommand"	
		runat="server">

		<ClientSettings 
			EnableRowHoverStyle="true"
			AllowDragToGroup="false" 
			AllowColumnsReorder="false" 
			AllowKeyboardNavigation="true"
			ReorderColumnsOnClient="false" >
			<Resizing AllowColumnResize="false" AllowRowResize="false" />
			<Selecting AllowRowSelect="false" />
		</ClientSettings>
			
		<MasterTableView 
			CommandItemDisplay="Top" 
			DataKeyNames="VictimPersonID" 
			EditMode="EditForms" 
			NoMasterRecordsText="No Victims/Persons to display." 
			CommandItemSettings-AddNewRecordText="Add Victim" 
			CommandItemStyle-Font-Bold="true" 
			HeaderStyle-ForeColor="#191970" 
			ItemStyle-ForeColor="black" 
			AlternatingItemStyle-ForeColor="black">
				
			<CommandItemSettings ShowRefreshButton="false" />
				
			<Columns>
				<Telerik:GridEditCommandColumn ButtonType="LinkButton" UniqueName="editColumn" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="center" ItemStyle-CssClass="commandItem" />
				<Telerik:GridButtonColumn CommandName="Delete" ButtonType="LinkButton" Text="Delete" UniqueName="deleteColumn" ConfirmText="Delete Victim/Person?" ConfirmTitle="Delete" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="center" ItemStyle-CssClass="commandItem" />
				<Telerik:GridBoundColumn HeaderText="VictimPersonID" UniqueName="VictimPersonID" DataField="VictimPersonID" ReadOnly="true" Visible="false"/>
				<Telerik:GridBoundColumn HeaderText="Number" UniqueName="VictimNumber" DataField="VictimNumber" HeaderStyle-Wrap="false" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="center"/>
				<Telerik:GridBoundColumn HeaderText="Type" UniqueName="VictimPersonType" DataField="VictimPersonType" HeaderStyle-Wrap="false"/>
				<Telerik:GridBoundColumn HeaderText="Last Name" UniqueName="LastName" DataField="LastName" HeaderStyle-Wrap="false"/>
				<Telerik:GridBoundColumn HeaderText="First Name" UniqueName="FirstName" DataField="FirstName" HeaderStyle-Wrap="false"/>
				<Telerik:GridBoundColumn HeaderText="Race" UniqueName="Race" DataField="Race" HeaderStyle-Wrap="false"/>
				<Telerik:GridBoundColumn HeaderText="Sex" UniqueName="Gender" DataField="Gender" HeaderStyle-Wrap="false"/>
				<Telerik:GridDateTimeColumn HeaderText="DOB" UniqueName="DateOfBirth" DataField="DateOfBirth" DataFormatString="{0:d}" />
			</Columns>
				
			<EditFormSettings EditFormType="WebUserControl" UserControlName="Controls/GridForms/VictimForm.ascx" />
				
		</MasterTableView>
	</Telerik:RadGrid>
	
</div>

<telerik:RadAjaxManager ID="ajaxManager" DefaultLoadingPanelID="ajaxLoadingPanel" runat="server" />