﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReportHq.Common;
using ReportHq.IncidentReports.Bases;
using ReportHq.IncidentReports.Controls;
using Telerik.Web.UI;

namespace ReportHq.IncidentReports.People.Controls
{
	public partial class Admin : System.Web.UI.UserControl
	{
		#region Variables

		private int _reportID;
		ReportPage _reportPage;

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Event Handlers

		protected void Page_Load(object sender, EventArgs e)
		{
			_reportPage = (ReportPage)this.Page;
			_reportPage.SyncSessionToViewState();
			
			_reportID = _reportPage.Report.ID;

			if (!_reportPage.UserHasWriteAccess)
			{
				//change "Edit" to "View"
				((GridEditCommandColumn)gridVictims.Columns[0]).EditText = "View";

				//hide "Delete"
				gridVictims.Columns[1].Visible = false;

				//disable Add command
				gridVictims.MasterTableView.CommandItemDisplay = GridCommandItemDisplay.None;
			}

			//setting definitions in code-behind so we can get the master page's label
			ajaxManager.AjaxSettings.AddAjaxSetting(gridVictims, gridVictims); //grid updates itself
			ajaxManager.AjaxSettings.AddAjaxSetting(gridVictims, _reportPage.Master.FindControl("lblMessage")); //grid updates label
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		
		/// <summary>
		/// Gets Victims for this report. Note that this does not do any caching, so will hit db every time. This is fine for small grids but larger grids should cache locally.
		/// </summary>
		protected void gridVictims_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
		{
			gridVictims.DataSource = GetData();
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void gridVictims_InsertCommand(object source, GridCommandEventArgs e)
		{
			//get grid's user control
			GridForms.VictimForm userControl = (GridForms.VictimForm)e.Item.FindControl(GridEditFormItem.EditFormUserControlID);

			//get user values from control
			Hashtable newValues = GetFormValues(userControl);

			VictimPerson victim = new VictimPerson(newValues);
			victim.ReportID = _reportID;

			StatusInfo status = victim.Add(_reportPage.Username);

			if (!status.Success)
				_reportPage.RenderUserMessage(Enums.UserMessageTypes.Warning, ("Sorry, there was a problem saving: " + status.Message));
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void gridVictims_UpdateCommand(object sender, GridCommandEventArgs e)
		{
			GridEditableItem editableItem = (GridEditableItem)e.Item;
			int victimPersonID = (int)editableItem.GetDataKeyValue("VictimPersonID");
			
			//get user values from control
			Hashtable newValues = GetFormValues((GridForms.VictimForm)e.Item.FindControl(GridEditFormItem.EditFormUserControlID));

			VictimPerson victim = new VictimPerson(newValues);
			victim.ID = victimPersonID;

			StatusInfo status = victim.Update(_reportPage.Username);

			if (!status.Success)
				_reportPage.RenderUserMessage(Enums.UserMessageTypes.Warning, ("Sorry, there was a problem saving: " + status.Message));
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void gridVictims_DeleteCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
		{
			GridEditableItem editableItem = (GridEditableItem)e.Item;
			int victimPersonID = (int)editableItem.GetDataKeyValue("VictimPersonID");

			StatusInfo status = VictimPerson.DeleteVictimPerson(victimPersonID);

			if (!status.Success)
				_reportPage.RenderUserMessage(Enums.UserMessageTypes.Warning, ("Sorry, there was a problem saving: " + status.Message));
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void ucReportSubNav_NavItemClicked(object sender, EventArgs e)
		{
			_reportPage.ProcessNavClick((sender as ReportSubNav).TargetRedirectUrl, false);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Private Methods

		private DataTable GetData()
		{
			return VictimPerson.GetVictimPersonsByReport(_reportID);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Gets all the user-inputted values from the form UI, and puts into a hastable.
		/// </summary>
		private Hashtable GetFormValues(GridForms.VictimForm userControl)
		{
			Hashtable newValues = new Hashtable();
			string userStringValue;
			double? userDoubleValue;

			newValues["VictimNumber"] = Convert.ToInt32((userControl.FindControl("rntbVictimNumber") as RadNumericTextBox).Value);

			//victim-person type
			userStringValue = (userControl.FindControl("rcbVictimPersonType") as RadComboBox).SelectedItem.Value;

			if (!string.IsNullOrEmpty(userStringValue))
				newValues["VictimPersonTypeID"] = Int32.Parse(userStringValue);
			else
				newValues["VictimPersonTypeID"] = -1;			
				

			//victim type
			userStringValue = (userControl.FindControl("rcbVictimTypes") as RadComboBox).SelectedItem.Value;
			if (!string.IsNullOrEmpty(userStringValue))
				newValues["VictimTypeID"] = Int32.Parse(userStringValue);
			else
				newValues["VictimTypeID"] = -1;

			newValues["LastName"] = (userControl.FindControl("tbLastName") as TextBox).Text;
			newValues["FirstName"] = (userControl.FindControl("tbFirstName") as TextBox).Text;

			//race
			userStringValue = (userControl.FindControl("rcbRace") as RadComboBox).SelectedItem.Value;
			if (!string.IsNullOrEmpty(userStringValue))
				newValues["RaceTypeID"] = Int32.Parse(userStringValue);
			else
				newValues["RaceTypeID"] = -1;

			//gender
			userStringValue = (userControl.FindControl("rcbGender") as RadComboBox).SelectedItem.Value;
			if (!string.IsNullOrEmpty(userStringValue))
				newValues["GenderTypeID"] = Int32.Parse(userStringValue);
			else
				newValues["GenderTypeID"] = -1;

			newValues["DateOfBirth"] = (userControl.FindControl("rdiDateOfBirth") as RadDateInput).SelectedDate;
			newValues["StreetAddress"] = (userControl.FindControl("tbStreetAddress") as TextBox).Text;
			newValues["City"] = (userControl.FindControl("tbCity") as TextBox).Text;
			newValues["State"] = (userControl.FindControl("rcbState") as RadComboBox).SelectedItem.Value;

			//zip
			userDoubleValue = (userControl.FindControl("rmtbZipCode") as RadNumericTextBox).Value;
			if (userDoubleValue > 0)
				newValues["ZipCode"] = Convert.ToInt32(userDoubleValue);
			else
				newValues["ZipCode"] = -1;

			newValues["PhoneNumber"] = (userControl.FindControl("rmtbPhone") as RadMaskedTextBox).Text;
			newValues["SocialSecurityNumber"] = (userControl.FindControl("rmtbSsn") as RadMaskedTextBox).Text;
			newValues["DriversLicenseNumber"] = (userControl.FindControl("tbDriversLicense") as TextBox).Text;

			//sobriety
			userStringValue = (userControl.FindControl("rcbSobrietyTypes") as RadComboBox).SelectedItem.Value;
			if (!string.IsNullOrEmpty(userStringValue))
				newValues["SobrietyTypeID"] = Int32.Parse(userStringValue);
			else
				newValues["SobrietyTypeID"] = -1;

			//injury
			userStringValue = (userControl.FindControl("rcbInjuryTypes") as RadComboBox).SelectedItem.Value;
			if (!string.IsNullOrEmpty(userStringValue))
				newValues["InjuryTypeID"] = Int32.Parse(userStringValue);
			else
				newValues["InjuryTypeID"] = -1;

			//treated
			userStringValue = (userControl.FindControl("rcbTreatedTypes") as RadComboBox).SelectedItem.Value;
			if (!string.IsNullOrEmpty(userStringValue))
				newValues["TreatedTypeID"] = Int32.Parse(userStringValue);
			else
				newValues["TreatedTypeID"] = -1;

			newValues["Occupation"] = (userControl.FindControl("tbOccupation") as TextBox).Text;
			newValues["WorkStreetAddress"] = (userControl.FindControl("tbWorkStreetAddress") as TextBox).Text;
			newValues["WorkCity"] = (userControl.FindControl("tbWorkCity") as TextBox).Text;
			newValues["WorkState"] = (userControl.FindControl("rcbWorkState") as RadComboBox).SelectedItem.Value;

			//work zip
			userDoubleValue = (userControl.FindControl("rntbWorkZipCode") as RadNumericTextBox).Value;
			if (userDoubleValue > 0)
				newValues["WorkZipCode"] = Convert.ToInt32(userDoubleValue);
			else
				newValues["WorkZipCode"] = -1;

			newValues["WorkPhoneNumber"] = (userControl.FindControl("rmtbWorkPhone") as RadMaskedTextBox).Text;
			newValues["EmailAddress"] = (userControl.FindControl("tbEmail1") as TextBox).Text;


			//finished! send back
			return newValues;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		
		#endregion
	}
}