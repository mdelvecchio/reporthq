﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OffenderDescriptors.ascx.cs" Inherits="ReportHq.IncidentReports.People.Controls.OffenderDescriptors" %>
<%@ Register TagPrefix="ReportHq" TagName="ReportSubNav" src="ReportSubNav.ascx" %>

<Telerik:RadFormDecorator id="radFormDecorator" DecoratedControls="CheckBoxes" Skin="Metro" runat="server" />

<ReportHq:ReportSubNav ID="ucReportSubNav" CurrentSection="Descriptors" OnNavItemClicked="ucReportSubNav_NavItemClicked" runat="server" />
	
<div class="formBox" style="width:650px;">

	<asp:HiddenField ID="hidPreviousOffenderID" runat="server" />

	<span class="label">Offender:</span> <Telerik:RadComboBox ID="rcbOffenders" Width="200" DataValueField="OffenderID" DataTextField="LastName" OnItemDataBound="rcbOffenders_ItemDataBound" OnSelectedIndexChanged="rcbOffenders_SelectedIndexChanged" AutoPostBack="true" runat="server" /><br /><br />

	<table width="100%" border="0" class="bordered">
		<tr>
			<td valign="top" class="bordered">
				<div class="checkBoxListLabel">Hair Color</div>
				<asp:CheckBoxList ID="cblHairColors" DataValueField="OffenderDescriptorTypeID" DataTextField="Description" RepeatLayout="Flow" EnableViewState="false" runat="server" />
			</td>
			<td valign="top" class="bordered">
				<div class="checkBoxListLabel">Eyes</div>
				<asp:CheckBoxList ID="cblEyes" DataValueField="OffenderDescriptorTypeID" DataTextField="Description" RepeatLayout="Flow" EnableViewState="false" runat="server" />
			</td>
			<td valign="top" class="bordered">
				<div class="checkBoxListLabel">Build</div>
				<asp:CheckBoxList ID="cblBuilds" DataValueField="OffenderDescriptorTypeID" DataTextField="Description" RepeatLayout="Flow" EnableViewState="false" runat="server" />
			</td>
			<td valign="top" class="bordered">
				<div class="checkBoxListLabel">Complexion</div>
				<asp:CheckBoxList ID="cblComplexion" DataValueField="OffenderDescriptorTypeID" DataTextField="Description" RepeatLayout="Flow" EnableViewState="false" runat="server" />
			</td>
		</tr>
		<tr>
			<td valign="top" class="bordered">
				<div class="checkBoxListLabel">Hair Style</div>
				<asp:CheckBoxList ID="cblHairStyles" DataValueField="OffenderDescriptorTypeID" DataTextField="Description" RepeatLayout="Flow" EnableViewState="false" runat="server" />
			</td>
			<td valign="top" class="bordered">
				<div class="checkBoxListLabel">Facial Hair</div>
				<asp:CheckBoxList ID="cblFacialHair" DataValueField="OffenderDescriptorTypeID" DataTextField="Description" RepeatLayout="Flow" EnableViewState="false" runat="server" />
			</td>
			<td valign="top" class="bordered">
				<div class="checkBoxListLabel">Tattoos</div>
				<asp:CheckBoxList ID="cblTattoos" DataValueField="OffenderDescriptorTypeID" DataTextField="Description" RepeatLayout="Flow" EnableViewState="false" runat="server" />
			</td>
			<td valign="top" class="bordered">
				<div class="checkBoxListLabel">Apparel</div>
				<asp:CheckBoxList ID="cblApparel" DataValueField="OffenderDescriptorTypeID" DataTextField="Description" RepeatLayout="Flow" EnableViewState="false" runat="server" />
			</td>
		</tr>
		<tr>
			<td valign="top" class="bordered">
				<div class="checkBoxListLabel">Nose</div>
				<asp:CheckBoxList ID="cblNose" DataValueField="OffenderDescriptorTypeID" DataTextField="Description" RepeatLayout="Flow" EnableViewState="false" runat="server" />
			</td>
			<td valign="top" class="bordered">
				<div class="checkBoxListLabel">Teeth</div>
				<asp:CheckBoxList ID="cblTeeth" DataValueField="OffenderDescriptorTypeID" DataTextField="Description" RepeatLayout="Flow" EnableViewState="false" runat="server" />
			</td>
			<td valign="top" class="bordered">
				<div class="checkBoxListLabel">Speech</div>
				<asp:CheckBoxList ID="cblSpeech" DataValueField="OffenderDescriptorTypeID" DataTextField="Description" RepeatLayout="Flow" EnableViewState="false" runat="server" />
			</td>
			<td valign="top" class="bordered">
				<div class="checkBoxListLabel">Accent</div>
				<asp:CheckBoxList ID="cblAccent" DataValueField="OffenderDescriptorTypeID" DataTextField="Description" RepeatLayout="Flow" EnableViewState="false" runat="server" />
			</td>
		</tr>
		<tr>
			<td colspan="2" valign="top" class="bordered">
				<div class="checkBoxListLabel">Scars</div>
				<asp:CheckBoxList ID="cblScars" DataValueField="OffenderDescriptorTypeID" DataTextField="Description" RepeatColumns="2" CellPadding="0" CellSpacing="0" Width="100%" EnableViewState="false" runat="server" />
			</td>
			<td valign="top" class="bordered">
				<div class="checkBoxListLabel">Oddities</div>
				<asp:CheckBoxList ID="cblOddities" DataValueField="OffenderDescriptorTypeID" DataTextField="Description" RepeatLayout="Flow" EnableViewState="false" runat="server" />
			</td>
			<td valign="top" class="bordered">
				<div class="checkBoxListLabel">Facial Oddities</div>
				<asp:CheckBoxList ID="cblFacialOddities" DataValueField="OffenderDescriptorTypeID" DataTextField="Description" RepeatLayout="Flow" EnableViewState="false" runat="server" />
			</td>
		</tr>
	</table><br />

	<div class="label">Additional Info:</div>
	<asp:TextBox ID="tbAdditionalDesc" TextMode="MultiLine" Rows="5" Columns="50" onkeypress="return isMaxLength(this, 200);" onblur="trimSize(this, 200);" runat="server"/>

</div>