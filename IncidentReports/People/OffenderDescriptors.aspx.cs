﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReportHq.Common;
using ReportHq.IncidentReports;
using ReportHq.IncidentReports.Controls;
using ReportHq.IncidentReports.People.Controls;
using Telerik.Web.UI;

namespace ReportHq.IncidentReports.People
{
	public partial class OffenderDescriptors : Bases.ReportPage
	{
		#region Variables

		private Report _report;

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Event Handlers

		protected void Page_Init(object sender, EventArgs e)
		{
			_report = base.Report; //set from base's OnInit
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void Page_Load(object sender, EventArgs e)
		{
			this.HelpUrl = "~/People/Help/OffenderDescriptors.html"; 

			//if (!base.UserHasWriteAccess)
			//	ucDescriptors.DisableValidators = true;

			//TODO: BUG: why doesnt loading panel show?

			RadAjaxLoadingPanel loadingPanel = (RadAjaxLoadingPanel)(this.Master.FindControl("ajaxLoadingPanel"));

			//setting definitions in code-behind so we can get the master page's label
			ajaxManager.AjaxSettings.AddAjaxSetting(ucOffenderDescriptors, ucOffenderDescriptors, loadingPanel); //ucDescriptors updates itself
			ajaxManager.AjaxSettings.AddAjaxSetting(ucOffenderDescriptors, this.Master.FindControl("lblMessage"), loadingPanel); //ucDescriptors updates label
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void ucMajorSections_NavItemClicked(object sender, EventArgs e)
		{
			base.ProcessNavClick((sender as ReportMainNav).TargetRedirectUrl, this.UserHasWriteAccess);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void OffenderDescriptors_NavItemClicked(object sender, EventArgs e)
		{
			base.ProcessNavClick((sender as ReportSubNav).TargetRedirectUrl, this.UserHasWriteAccess);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
				
		#endregion

		#region Private Methods

		/// <summary>
		/// Sets the page's form fields to the Report's values. This overrides the method stub in the base ReportPage, and is called by the base's OnLoad() method.
		/// </summary>
		public override void SetFormValues()
		{
			ucOffenderDescriptors.BindOffendersList(_report.ID);
			ucOffenderDescriptors.SetFormValues();
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Saves the report. On success, updates Session copy. This overrides the method stub in the base ReportPage, and is called by the base's ProcessNavClick() public method.
		///</summary>
		public override StatusInfo SaveReport()
		{
			_report = base.Report; //the page base ensures the report is sync'd up w/ the ViewState prior to it calling this SaveReport(). now ensure our local copy is the latest instance.

			int offenderID = ucOffenderDescriptors.OffenderID;

			StatusInfo status;

			if (offenderID > 0)
			{
				//update
				status = ucOffenderDescriptors.SaveOffender(ucOffenderDescriptors.OffenderID);
			}
			else //offenders list is empty; dont save, just navigate by returning a successful status to the page base
			{
				status = new StatusInfo(true);
			}
			
			//not needed since we're not changing report data
			//if (status.Success == true)
			//	base.Report = _report; //update Session copy w/ new values, but only if it saved to db. dont want to mislead the user.

			return status; //used by base for error handling and nav flow
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}