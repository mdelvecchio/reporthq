﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReportHq.Common;
using ReportHq.IncidentReports.Controls;

namespace ReportHq.IncidentReports
{
	public partial class Narrative : Bases.ReportPage
	{
		#region Variables

		private Report _report;

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Event Handlers

		protected void Page_Init(object sender, EventArgs e)
		{
			_report = base.Report; //set from base's OnInit
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void Page_Load(object sender, EventArgs e)
		{
			this.HelpUrl = "~/Help/Narrative.html";
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void ucMajorSections_NavItemClicked(object sender, EventArgs e)
		{
			base.ProcessNavClick((sender as ReportMainNav).TargetRedirectUrl, this.UserHasWriteAccess);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void ucNarrative_SaveButtonClicked(object sender, EventArgs e)
		{
			StatusInfo status = SaveReport();

			if (status.Success == true)
			{
				base.RenderUserMessage(Enums.UserMessageTypes.Success, "Narrative saved.");
			}
			else //something failed
			{
				base.RenderUserMessage(Enums.UserMessageTypes.Warning, "Sorry, there was a problem saving: " + status.Message);
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Public Methods

		/// <summary>
		/// Sets the page's form fields to the Report's values. This overrides the method stub in the base ReportPage, and is called by the base's OnLoad() method.
		/// </summary>
		public override void SetFormValues()
		{
			ucNarrative.ReportID = _report.ID;
			ucNarrative.NarrativeText = _report.Narrative;

			if (this.UserHasWriteAccess == false)
				ucNarrative.ReadOnly = true;

			//TODO: enhancement: (idea) to improve performance, dont include Narrative in Report obj. have .Narrative call a DAL method on a need-only basis.
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Saves the report. On success, updates Session copy. This overrides the method stub in the base ReportPage, and is called by the base's ProcessNavClick() public method.
		/// </summary>
		public override StatusInfo SaveReport()
		{
			_report = base.Report; //the page base ensures the report is sync'd up w/ the ViewState prior to it calling this SaveReport(). now ensure our local copy is the latest instance.

			//set new form values to report
			_report.Narrative = ucNarrative.NarrativeText;

			//update
			StatusInfo status = _report.UpdateNarrative(this.Username);

			if (status.Success == true)
				base.Report = _report; //update Session copy w/ new values, but only if it saved to db. dont want to mislead the user.

			//base handles rendering errors & redirect
			return status;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}