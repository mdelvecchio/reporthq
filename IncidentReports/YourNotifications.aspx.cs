﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReportHq.IncidentReports.DataAccess;

namespace ReportHq.IncidentReports
{
	public partial class YourNotifications : Bases.NormalPage
	{
		#region Event Handlers

		protected void Page_Load(object sender, EventArgs e)
		{
			this.Heading = "Your Notifications";
			this.HelpUrl = "help/YourNotifications.html";

			SetNotificationTables();
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Private Methods

		private void SetNotificationTables()
		{
			DataSet results = Notification.GetNotifications(this.Username);

			if (results.Tables.Count > 0)
			{
				DataTable dtReports = results.Tables[0];
				DataTable dtGlobal = results.Tables[1];
				

				//rejected
				if (dtReports.Rows.Count > 0)
				{
					rptReports.DataSource = dtReports;
					rptReports.DataBind();
				}
				else
				{
					litNoneReports.Visible = true;
				}


				//incomplete
				if (dtGlobal.Rows.Count > 0)
				{
					rptGlobal.DataSource = dtGlobal;
					rptGlobal.DataBind();
				}
				else
				{
					litNoneGlobal.Visible = true;
				}
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}