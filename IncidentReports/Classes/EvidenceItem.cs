using System;
using System.Data;
using System.Collections;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using ReportHq.Common;
using ReportHq.IncidentReports.DataAccess;

namespace ReportHq.IncidentReports
{
	public class EvidenceItem : Bases.PropertyEvidenceItem
	{
		#region Constructors

		/// <summary>
		/// Use this when filling a new evidence from values collected from a Grid. (ex: an insert operation)
		/// </summary>
		public EvidenceItem(Hashtable newValues)
		{
			this.LossTypeID = (int)newValues["LossTypeID"];
			this.Quantity = Convert.ToDecimal(newValues["Quantity"]);
			this.TypeID = (int)newValues["TypeID"];
			this.NarcoticWeightTypeID = (int)newValues["NarcoticWeightTypeID"];
			this.NarcoticTypeID = (int)newValues["NarcoticTypeID"];
			this.Description = (string)newValues["Description"];
			this.Brand = (string)newValues["Brand"];
			this.SerialNumber = (string)newValues["SerialNumber"];
			this.Value = (int)newValues["Value"];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Use this when filling a new evidence from datarow. (ex: record from db)
		/// </summary>
		public EvidenceItem(DataRow row)
		{
			FillObject(this, row);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Public Methods

		public StatusInfo AddEvidence(string createdBy)
		{
			return PropertyEvidenceDA.InsertEvidence(this.ReportID,
													 this.LossTypeID,
													 this.Quantity,
													 this.TypeID,
													 this.NarcoticWeightTypeID,
													 this.NarcoticTypeID,
													 this.Description,
													 this.Brand,
													 this.SerialNumber,
													 this.Value,
													 createdBy);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		
		public StatusInfo UpdateEvidence(string modifiedBy)
		{
			return PropertyEvidenceDA.UpdateEvidence(this.ID,
													 this.LossTypeID,
													 this.Quantity,
													 this.TypeID,
													 this.NarcoticWeightTypeID,
													 this.NarcoticTypeID,
													 this.Description,
													 this.Brand,
													 this.SerialNumber,
													 this.Value,
													 modifiedBy);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Public Static Methods

		public static StatusInfo DeleteEvidence(int evidenceID)
		{
			return PropertyEvidenceDA.DeleteEvidence(evidenceID);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetEvidenceByReport(int reportID)
		{
			return PropertyEvidenceDA.GetEvidenceByReport(reportID);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Private Methods

		/// <summary>
		/// Fills this object for a specific VictimPerson, via constructor.
		/// </summary>
		private void FillObject(EvidenceItem evidence, DataRow row)
		{
			//required
			evidence.ID = (int)row["EvidenceID"];
			evidence.ReportID = (int)row["ReportID"];
			evidence.LossTypeID = (int)row["PropertyEvidenceLossTypeID"];
			evidence.LossType = (string)row["LossType"];

			if (row["Quantity"] != DBNull.Value)
				evidence.Quantity = (decimal)row["Quantity"];

			if (row["PropertyTypeID"] != DBNull.Value)
				evidence.TypeID = (int)row["PropertyTypeID"];

			if (row["Type"] != DBNull.Value)
				evidence.Type = (string)row["Type"];

			if (row["NarcoticWeightTypeID"] != DBNull.Value)
				evidence.NarcoticWeightTypeID = (int)row["NarcoticWeightTypeID"];

			if (row["NarcoticWeightUnit"] != DBNull.Value)
				evidence.NarcoticWeightUnit = (string)row["NarcoticWeightUnit"];

			if (row["NarcoticTypeID"] != DBNull.Value)
				evidence.NarcoticTypeID = (int)row["NarcoticTypeID"];

			if (row["NarcoticType"] != DBNull.Value)
				evidence.NarcoticType = (string)row["NarcoticType"];

			if (row["Description"] != DBNull.Value)
				evidence.Description = (string)row["Description"];

			if (row["Brand"] != DBNull.Value)
				evidence.Brand = (string)row["Brand"];

			if (row["SerialNumber"] != DBNull.Value)
				evidence.SerialNumber = (string)row["SerialNumber"];

			if (row["Value"] != DBNull.Value)
				evidence.Value = (int)row["Value"];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}
