﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReportHq.IncidentReports
{
	/// <summary>
	/// Site-wide typed enums, used for various validations and logic decisions.
	/// </summary>
    public class Enums
    {
		/// <summary>
		/// Which environment the application is running in.
		/// </summary>
		public enum ApplicationEnvironments
		{
			Production = 1,
			Training = 2,
			Test = 3,
			Development = 4
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Types of messages that can be written to the screen.
		/// </summary>
		public enum UserMessageTypes
		{
			Warning,
			Success,
			Plain
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    }	
}