﻿using System;
using System.Web.Caching;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Linq;

using ReportHq.IncidentReports.DataAccess;

namespace ReportHq.IncidentReports
{
    public class GenericLookups
    {
		#region Public Methods

		/// <summary>
		/// The NOPD Districts.
		/// </summary>
		public static DataTable GetDistricts()
		{
			const string KEYNAME = "dtDistricts";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in session, create
			{
				DataTable results = GenericLookupsDA.GetDistricts();

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(7), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// The NOPD Zones.
		/// </summary>
		/// <returns></returns>
		public static DataTable GetZones()
		{
			const string KEYNAME = "dtZones";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in cache, create
			{
				DataTable results = GenericLookupsDA.GetZones();

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(7), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// The NOPD SubZones.
		/// </summary>
		/// <returns></returns>
		public static DataTable GetSubZones()
		{
			const string KEYNAME = "dtSubZones";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in cache, create
			{
				DataTable results = GenericLookupsDA.GetSubZones();

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(7), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Gets automobile car makes.
		/// </summary>
		/// <returns></returns>
		public static DataTable GetVehicleMakes()
		{
			const string KEYNAME = "dtVehicleMakes";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in cache, create
			{
				DataTable results = GenericLookupsDA.GetVehicleMakes();

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(7), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetVehicleModelsByMake(int makeTypeID)
		{
			string KEYNAME = "dtVehicleModels_" + makeTypeID.ToString();

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in cache, create
			{
				DataTable results = GenericLookupsDA.GetVehicleModelsByMake(makeTypeID);

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(7), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetVehicleStyles()
		{
			const string KEYNAME = "dtVehicleStyles";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in cache, create
			{
				DataTable results = GenericLookupsDA.GetVehicleStyles();

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(7), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetVehicleColors()
		{
			const string KEYNAME = "dtVehicleColors";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in cache, create
			{
				DataTable results = GenericLookupsDA.GetVehicleColors();

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(7), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetPersonRaces()
		{
			const string KEYNAME = "dtPersonRaces";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in cache, create
			{
				DataTable results = GenericLookupsDA.GetPersonRaces();

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(7), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetPersonGenders()
		{
			const string KEYNAME = "dtPersonGenders";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in cache, create
			{
				DataTable results = GenericLookupsDA.GetPersonGenders();

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(7), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetPersonEyeColors()
		{
			const string KEYNAME = "dtPersonEyeColors";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in cache, create
			{
				DataTable results = GenericLookupsDA.GetPersonEyeColors();

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(7), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetPersonHairColors()
		{
			const string KEYNAME = "dtPersonHairColors";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in cache, create
			{
				DataTable results = GenericLookupsDA.GetPersonHairColors();

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(7), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}
				
		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetSobrietyTypes()
		{
			const string KEYNAME = "dtSobrietyTypes";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in session, create
			{
				DataTable results = GenericLookupsDA.GetSobrietyTypes();

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetInjuryTypes()
		{
			const string KEYNAME = "dtInjuryTypes";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in session, create
			{
				DataTable results = GenericLookupsDA.GetInjuryTypes();

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetTreatedTypes()
		{
			const string KEYNAME = "dtTreatedTypes";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in session, create
			{
				DataTable results = GenericLookupsDA.GetTreatedTypes();

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetResidentTypes()
		{
			const string KEYNAME = "dtResidentTypes";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in session, create
			{
				DataTable results = GenericLookupsDA.GetResidentTypes();

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		
		#endregion
	}		
}
