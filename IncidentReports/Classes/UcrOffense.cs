using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Caching;

using ReportHq.Common;
using ReportHq.IncidentReports.DataAccess;

namespace ReportHq.IncidentReports
{
	/// <summary>
	/// UCR Section for a Part I offense. NOTE: not sure yet whether to use this big-ass class, or individual classes pertaining to each offense type only.
	/// </summary>
	public class UcrOffense
	{
		#region Enums

		public enum Categories
		{
			PartOne = 1,
			PartTwo = 2
		}

		public enum OffenseTypes
		{
			CriminalHomicide = 1,
			ForcibleRape = 2,
			Robbery = 3,
			AggravatedAssault = 4,
			Burglary = 5,
			LarcenyTheft = 6,
			MotorVehicleTheft = 7,
			Arson = 8
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Properties

		public int ReportID { get; set; }

		public string ItemNumber { get; set; }

		public Categories Category { get; set; }

		public int OffenseTypeID { get; set; }

		public OffenseTypes OffenseType { get; set; }

		public int SeriesCode { get; set; }
				
		public string LastModifiedBy { get; set; }

		public DateTime LastModifiedDate { get; set; }
		
		#region BURGLARGY

		public String BurglaryTimeOfDay { get; set; }

		public String BurglaryCrimeLocation { get; set; }

		public String BurglaryForceUsed { get; set; }

		#endregion

		#region LARCENY	

		public String LarcenyNature { get; set; }
		
		#endregion

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Constructors

		public UcrOffense()
		{
			
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Get a UCR object for an exist report, if exists.
		/// </summary>
		public UcrOffense(int reportID)
		{
			DataSet ds = UcrDA.GetUcrOffenseByReportID(reportID);

			if (ds.Tables[0].Rows.Count > 0)
				FillObject(ds.Tables[0].Rows[0]);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Public Methods

		/// <summary>
		/// Update a UCR Burglary offense.
		/// </summary>
		public StatusInfo Update(int reportID, string modifiedBy)
		{
			return UcrDA.UpdateOffense(reportID,
									   this.BurglaryTimeOfDay,
									   this.BurglaryCrimeLocation,
									   this.BurglaryForceUsed,
									   this.LarcenyNature,
									   modifiedBy);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
				
		#region Public Static Methods

		/// <summary>
		/// Get all UCR offense categories.
		/// </summary>
		/// <returns></returns>
		public static DataTable GetOffenseCategories()
		{
			const string KEYNAME = "dtUcrCategories";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in cache, create
			{
				DataTable results = UcrDA.GetOffenseCategories();

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];

		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Get all UCR offense types for a category.
		/// </summary>
		public static DataTable GetOffenseTypesByCategoryID(int categoryID)
		{
			DataTable dtResults = null;

			switch (categoryID)
			{
				case 1:
					dtResults = GetPartOneOffenseTypes();
					break;

				case 2:
					dtResults = GetPartTwoOffenseTypes();
					break;
			}

			return dtResults;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetPartOneOffenseTypes()
		{
			const string KEYNAME = "dtUcrPartOneOffenseTypes";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in session, create
			{
				DataTable results = UcrDA.GetOffenseTypesByCategoryID(1);

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetPartTwoOffenseTypes()
		{
			const string KEYNAME = "dtUcrPartTwoOffenseTypes";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in session, create
			{
				DataTable results = UcrDA.GetOffenseTypesByCategoryID(2);

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		
		public static DataTable GetAllOffenseTypes()
		{
			const string KEYNAME = "dtUcrAllOffenseTypes";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in session, create
			{
				DataTable results = UcrDA.GetAllOffenseTypes();

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		
		public static object[] GetOffenseTypeForSignal(int signalTypeID)
		{
			return UcrDA.GetOffenseTypeBySignalID(signalTypeID);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Private Methods

		/// <summary>
		/// Filling this object for a specific Report, via constructor.
		/// </summary>
		private void FillObject(DataRow row)
		{
			//required
			this.ReportID = (int)row["ReportID"];
			this.ItemNumber = (string)row["ItemNumber"];
			this.LastModifiedBy = (string)row["LastModifiedBy"];
			this.LastModifiedDate = (DateTime)row["LastModifiedDate"];
			this.OffenseTypeID = (int)row["UcrOffenseTypeID"];
			this.OffenseType = (OffenseTypes)row["UcrOffenseTypeID"];
			this.SeriesCode = (int)row["SeriesCode"];

			//burglary
			if (row["BurglaryTimeOfDay"] != DBNull.Value)
				this.BurglaryTimeOfDay = (string)row["BurglaryTimeOfDay"];

			if (row["BurglaryCrimeLocation"] != DBNull.Value)
				this.BurglaryCrimeLocation = (string)row["BurglaryCrimeLocation"];

			if (row["BurglaryForceUsed"] != DBNull.Value)
				this.BurglaryForceUsed = (string)row["BurglaryForceUsed"];

			//larceny
			if (row["LarcenyNature"] != DBNull.Value)
				this.LarcenyNature = (string)row["LarcenyNature"];


			//TODO: additional sections

		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}
