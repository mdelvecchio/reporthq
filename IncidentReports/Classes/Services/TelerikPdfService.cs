﻿using System;
using System.Collections.Specialized;
using System.IO;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Web;

using Telerik.Web.UI;

/// <summary>
/// This service exists to assist the Telerik RadClientExportManager in exporting DOM element content into PDF format. Normally it does so via the browser's "File API" support, but 
/// Safari and IE9 don't offer this so we must do it here. See http://www.telerik.com/support/code-library/ie9-and-safari-compatibility for more info.
/// </summary>
[ServiceContract(Namespace = "")]
[AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
[ServiceBehavior(IncludeExceptionDetailInFaults = true)]
public class TelerikPdfService
{
	[OperationContract]
	[WebInvoke(Method = "POST")]
	public Stream Export(Stream input)
	{
		//take in content source
		StreamReader sr = new StreamReader(input);
		string streamString = sr.ReadToEnd();
		sr.Dispose();

		//get our stuff
		NameValueCollection qs = HttpUtility.ParseQueryString(streamString);
		//string filename = qs["fileName"];
		string base64 = qs["base64"];
		string contentType = qs["contentType"]; //PDF

		byte[] data = Convert.FromBase64String(base64);

		//return it back
		MemoryStream stream = new MemoryStream(data);
		WebOperationContext.Current.OutgoingResponse.ContentType = contentType;
		stream.Position = 0;
		
		return stream;
	}
}