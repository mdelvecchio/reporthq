﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReportHq.IncidentReports
{
	/// <summary>
	/// A simple object that represents a breadcrumb link in the main nav.
	/// </summary>
	public class BreadCrumb
	{
		#region Properties

		private string _text;
		private string _url;
		private string _title;

		/// <summary>
		/// Visuable text of the breadcrumb. 
		/// </summary>
		public string Text
		{
			get { return _text; }
			set { _text = value; }
		}

		/// <summary>
		/// URL of the breadcrumb, used by a's href.
		/// </summary>
		public string Url
		{
			get { return _url; }
			set { _url = value; }
		}

		/// <summary>
		/// Friendly title of the breadcrumb, used for mouse-over.
		/// </summary>
		public string Title
		{
			get { return _title; }
			set { _title = value; }
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region	Constructors

		public BreadCrumb()
		{

		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public BreadCrumb(string text, string url, string title)
		{
			_text = text;
			_url = url;
			_title = title;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}