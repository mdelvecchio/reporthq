using System;
using System.Collections.Generic;
using System.Data;

using ReportHq.Common;
using ReportHq.IncidentReports.DataAccess;

namespace ReportHq.IncidentReports
{
	/// <summary>
	/// A supervisor-issued Bulletin.
	/// </summary>
	public class Bulletin
	{
		#region Enums

		public enum Types
		{
			MissingPerson = 1,
			SuspectAtLarge = 2,
			StolenVehicle = 3,
			PersonnelNotice = 4,
			PolicyNotice = 5,
			Memo = 6

		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Properties

		public int ID { get; set; }
		public int TypeID { get; set; }

		/// <summary>
		/// Enum for the BulletinTypeID.
		/// </summary>
		public Types Type { get; set; }

		/// <summary>
		/// Friendly name for the BulletinTypeID.
		/// </summary>
		public string TypeName { get; set; }

		public bool IsApb { get; set; }
		public bool IsActive { get; set; }

		public string Title { get; set; }

		/// <summary>
		/// The actual Bulletin; in HTML.
		/// </summary>
		public string BulletinContent { get; set; }

		/// <summary>
		/// Who this Bulletin shows up for.
		/// </summary>
		public List<DescriptionItem> RecipientRoleIDs { get; set; }

		public DateTime CreateDate { get; set; }
		
		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Constructors

		/// <summary>
		/// A new Bulletin
		/// </summary>
		public Bulletin()
		{
			
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Get a Bulletin from the db.
		/// </summary>
		public Bulletin(int bulletinID)
		{
			this.ID = bulletinID;

			DataSet ds = BulletinDA.GetBulletinByID(bulletinID);

			if (ds.Tables[0].Rows.Count > 0)
				FillObject(ds);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Public Methods

		/// <summary>
		/// Write new Bulletin into the database.
		/// </summary>
		public StatusInfo Add(string username)
		{
			//set the type enum based on UI's int
			this.Type = (Types)this.TypeID;

			string useRoleIds = Common.GetXmlStringOfIds(this.RecipientRoleIDs);

			StatusInfo status = BulletinDA.InsertBulletin(this.TypeID,
														  this.IsApb,
														  this.Title,
														  this.BulletinContent,
														  useRoleIds,
														  username);
			
			//TODO: move this into the InsertBulletin proc? save some db trips?
			if (status.Success)
			{
				//set new bulletin ID
				this.ID = status.Code;

				//if APB send notification
				if (this.IsApb)
					SendApbNotification(username);
			}

			return status;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		
		/// <summary>
		/// Save an existing Bulletin in the database.
		/// </summary>
		public StatusInfo Update(string username)
		{
			//set the type enum based on UI's int
			this.Type = (Types)this.TypeID;

			string useRoleIds = Common.GetXmlStringOfIds(this.RecipientRoleIDs);

			StatusInfo status = BulletinDA.UpdateBulletin(this.ID,
														  this.TypeID,
														  this.IsApb,
														  this.Title, 
														  this.IsActive,
														  this.BulletinContent,
														  useRoleIds,
														  username);

			//TODO: move this into the InsertBulletin proc? save some db trips?

			//if APB send notification
			if (status.Success && this.IsApb && this.IsActive)
				SendApbNotification(username);

			return status;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
				
		#region Public Static Methods

		/// <summary>
		/// Gets Bulletins for current user, separated into tables for the various types used by Roll Call.
		/// </summary>
		public static DataSet GetBulletinsByUsername(string username)
		{
			return BulletinDA.GetBulletinsByUsername(username);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetBulletinTypes()
		{
			return BulletinDA.GetBulletinTypes();
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetAllBulletins()
		{
			return BulletinDA.GetAllBulletins();
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		
		/// <summary>
		/// Get All-point-bulletins (APBs).
		/// </summary>
		public static DataTable GetAllPointBulletins()
		{
			return BulletinDA.GetAllPointBulletins();
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Private Methods

		/// <summary>
		/// Fills this object for a specific GlobalNotification, via constructor.
		/// </summary>
		private void FillObject(DataSet ds)
		{
			DataRow row = ds.Tables[0].Rows[0];

			this.TypeID = (int)row["BulletinTypeID"];
			this.Type = (Types)this.TypeID;
			this.TypeName = (string)row["Type"];
			this.IsApb = (bool)row["IsAllPoints"];
			this.IsActive = (bool)row["IsActive"];
			this.Title = (string)row["Title"];
			this.BulletinContent = (string)row["Bulletin"];
			this.CreateDate = (DateTime)row["CreatedDate"];
			
			//fill recipients collection
			List<DescriptionItem> items = new List<DescriptionItem>();

			DescriptionItem newItem;
			foreach (DataRow row2 in ds.Tables[1].Rows)
			{
				newItem = new DescriptionItem();
				newItem.ID = (int)row2["UserRoleID"];

				items.Add(newItem);
			}

			this.RecipientRoleIDs = items;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Sends a notification to all Users.
		/// </summary>
		private void SendApbNotification(string username)
		{
			int bulletinID = this.ID;

			string url = string.Format("javascript:openBulletin({0})", bulletinID);

			Notification notification = new Notification(Notification.Types.APB, bulletinID, this.Title, url);
			notification.SendGlobal(username);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}
