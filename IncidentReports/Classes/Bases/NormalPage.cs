﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Web.UI;

using ReportHq.IncidentReports.Controls;
using Telerik.Web.UI;

namespace ReportHq.IncidentReports.Bases
{
	/// <summary>
	/// Custom ReportHq page class, which offers useful properties and methods.
	/// </summary>
	public class NormalPage : Page
	{
		#region Variables

		protected NormalMasterPage _master;
		protected Header _ucHeader;
		protected RadWindowManager _radWindowManager;

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Properties

		List<BreadCrumb> _breadCrumbs = new List<BreadCrumb>();
		private string _helpUrl;
		private string _heading;

		public List<BreadCrumb> BreadCrumbs
		{
			get { return _breadCrumbs; }
			set { _breadCrumbs = value; }
		}

		/// <summary>
		/// Environment the application is running in, as set by the web.config.
		/// </summary>
		public Enums.ApplicationEnvironments ApplicationEnvironment
		{
			get { return Common.GetApplicationEnvironment(); }
		} 

		/// <summary>
		/// Page path used for Help link.
		/// </summary>
		public string HelpUrl
		{
			get { return _helpUrl; }
			set { _helpUrl = value; }
		}

		/// <summary>
		/// Heading to display in the header's title-bar.
		/// </summary>
		public string Heading
		{
			get { return _heading; }
			set { _heading = value; }
		}

		/// <summary>
		/// Current Windows user's ID.
		/// </summary>
		public string Username
		{
			//get { return GetUsername(); }
			get { return "VILEMOBILE\\Administrator"; }
		}

		/// <summary>
		/// Whether user is entitled to Admin access & tools.
		/// </summary>
		public bool UserIsAdministrator
		{
			//get { return Roles.IsUserInRole("Administrators"); }
			get { return true; } //testing only
		}

		/// <summary>
		/// Whether user is entitled to Supervisor access & tools.
		/// </summary>
		public bool UserIsSupervisor
		{
			//get { return Roles.IsUserInRole("Supervisors"); }
			get { return true; } //testing only
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Event Handlers

		/// <summary>
		/// Set global variables for use by this class and derived classes.
		/// </summary>
		/// <param name="e"></param>
		protected override void OnInit(EventArgs e)
		{
			//get a strongly-typed object of our master page
			_master = (NormalMasterPage)this.Master;

			if (_master != null)
			{
				_ucHeader = (Header)_master.FindControl("ucHeader");
				_radWindowManager = (RadWindowManager)_master.FindControl("radWindowManager");
			}

			//fire base Page's event so actual pages can handle it & do their stuff
			base.OnInit(e);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Set page header items (breadcrumbs, user menu items, page heading).
		/// </summary>
		protected override void OnPreRender(EventArgs e)
		{
			if (_master != null)
			{
				//set username into html for client-side use
				_master.HiddenUsername = this.Username.Replace(@"\", "|"); //json URIs cant handle backslash, using | instead and decoding in before use
			}			
				
			#region set header items

			if (_ucHeader != null)
			{
				switch (this.ApplicationEnvironment)
				{
					case Enums.ApplicationEnvironments.Production:
						_breadCrumbs.Insert(0, new BreadCrumb("Incident Reports", ResolveUrl("~/YourDashboard.aspx"), "to Incident Reports home"));
						_ucHeader.BreadCrumbs.AddRange(_breadCrumbs);
						break;
					case Enums.ApplicationEnvironments.Training:
						_breadCrumbs.Insert(0, new BreadCrumb("Incident Reports (training)", ResolveUrl("~/YourDashboard.aspx"), "to Incident Reports home"));
						_ucHeader.BreadCrumbs.AddRange(_breadCrumbs);
						break;
					case Enums.ApplicationEnvironments.Test:
						_breadCrumbs.Insert(0, new BreadCrumb("Incident Reports (test)", ResolveUrl("~/YourDashboard.aspx"), "to Incident Reports home"));
						_ucHeader.BreadCrumbs.AddRange(_breadCrumbs);
						break;
					case Enums.ApplicationEnvironments.Development:
						_breadCrumbs.Insert(0, new BreadCrumb("Incident Reports (dev)", ResolveUrl("~/YourDashboard.aspx"), "to Incident Reports home"));
						_ucHeader.BreadCrumbs.AddRange(_breadCrumbs);

						break;
					default:
						_breadCrumbs.Insert(0, new BreadCrumb("Incident Reports", ResolveUrl("~/YourDashboard.aspx"), "to Incident Reports home"));
						_ucHeader.BreadCrumbs.AddRange(_breadCrumbs);
						break;
				}
				

				_ucHeader.Username = this.Username;
				_ucHeader.ShowAdminTools = this.UserIsAdministrator;
				_ucHeader.ShowSupervisorTools = this.UserIsSupervisor;
				_ucHeader.Heading = _heading;

			}
		
			#endregion

			#region set RadWindows

			//not all our master templates have the RadWindowManager
			if (_radWindowManager != null)
			{
				RadWindow rwHelp = _radWindowManager.Windows[0];

				//rwHelp - window 0 in the radwindows defined in IncidentReports.Master
				rwHelp.NavigateUrl = _helpUrl;
				rwHelp.Visible = true; //not the visibility of the link, just the visibility of the RadWindow element itself, in HTML
			}

			#endregion
			

			//fire base Page's event so actual pages can handle it & do their stuff
			base.OnPreRender(e);
		}
	
		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Constructor

		public NormalPage()
		{

		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Public Methods

		/// <summary>
		/// Renders an HTML message to the masterpage's lblMessage control.
		/// </summary>
		public void RenderUserMessage(Enums.UserMessageTypes messageType, string message)
		{
			Label lblMessage = (Label)_master.FindControl("lblMessage");

			switch (messageType)
			{
				case Enums.UserMessageTypes.Warning:
					lblMessage.Text = @"<i class=""fa fa-exclamation-triangle""></i> ";
					lblMessage.CssClass = "warningMessage";
					break;

				case Enums.UserMessageTypes.Success:
					lblMessage.Text = @"<i class=""fa fa-check-circle""></i> ";
					lblMessage.CssClass = "successMessage";
					break;

				case Enums.UserMessageTypes.Plain:
					//lblMessage.Text = @"<i class=""fa fa-exclamation-triangle""></i> ";
					lblMessage.CssClass = "plainMessage";
					break;

			}
			
			lblMessage.Text += message;
			lblMessage.Visible = true;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Private Methods

		private string GetUsername()
		{
			string returnValue;

			returnValue = this.User.Identity.Name;

			if (string.IsNullOrEmpty(returnValue))
			{
				returnValue = "User Name Empty";
			}

			return returnValue;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}