using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Caching;

using ReportHq.Common;
using ReportHq.IncidentReports.DataAccess;

namespace ReportHq.IncidentReports.Bases
{
	/// <summary>
	/// UCR Section. Subclasses fill out per-type details.
	/// </summary>
	public class UcrOffense
	{
		#region Enums

		public enum Categoires
		{
			PartOne = 1,
			PartTwo = 2
		}

		public enum OffenseTypes
		{
			CriminalHomicide = 1,
			ForcibleRape = 2,
			Robbery = 3,
			AggravatedAssault = 4,
			Burglary = 5,
			LarcenyTheft = 6,
			MotorVehicleTheft = 7,
			Arson = 8
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Properties

		public Categoires Category { get; set; }

		public OffenseTypes OffenseType { get; set; }

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Constructors

		

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Public Methods

		//public StatusInfo UpdateBurglary(string modifiedBy)
		//{
		//	//return UcrDA.UpdateBurglary(int reportID, 
		//	//							this.UcrOffenseTypeID,
		//	//							modifiedBy);

		//	//TODO: how to implement? here, or in each subclass' .Update() method?

		//	return new StatusInfo(false, "not implemented yet");
		//}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
				
		#region Public Static Methods

		/// <summary>
		/// Get all UCR offense categories.
		/// </summary>
		/// <returns></returns>
		public static DataTable GetOffenseCategories()
		{
			const string KEYNAME = "dtUcrCategories";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in cache, create
			{
				DataTable results = UcrDA.GetOffenseCategories();

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];

		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Get all UCR offense types for a category.
		/// </summary>
		public static DataTable GetOffenseTypesByCategoryID(int categoryID)
		{
			DataTable dtResults = null;

			switch (categoryID)
			{
				case 1:
					dtResults = GetPartOneOffenseTypes();
					break;

				case 2:
					dtResults = GetPartTwoOffenseTypes();
					break;
			}

			return dtResults;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetPartOneOffenseTypes()
		{
			const string KEYNAME = "dtUcrPartOneOffenseTypes";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in session, create
			{
				DataTable results = UcrDA.GetOffenseTypesByCategoryID(1);

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetPartTwoOffenseTypes()
		{
			const string KEYNAME = "dtUcrPartTwoOffenseTypes";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in session, create
			{
				DataTable results = UcrDA.GetOffenseTypesByCategoryID(2);

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		
		public static DataTable GetAllOffenseTypes()
		{
			const string KEYNAME = "dtUcrAllOffenseTypes";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in session, create
			{
				DataTable results = UcrDA.GetAllOffenseTypes();

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		
		public static object[] GetOffenseTypeForSignal(int signalTypeID)
		{
			return UcrDA.GetOffenseTypeBySignalID(signalTypeID);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Private Methods



		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}
