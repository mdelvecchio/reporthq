﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReportHq.Common;
using ReportHq.IncidentReports.Controls;
using Telerik.Web.UI;

namespace ReportHq.IncidentReports.Bases
{
	/// <summary>
	/// Custom ReportHq page class for actual report form pages, which offers useful properties and methods.
	/// </summary>
	public class ReportPage : NormalPage
	{
		#region Variables

		const string _KEYNAME = "theReport";
		private Report _report;
		
		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Properties

		/// <summary>
		/// The report user is working with in this session. 
		/// </summary>
		public Report Report
		{
			//get { return GetThisReport(); }
			get { return _report; } //testing this...set it once in Init and see if we can just refer to it from here
			set { HttpContext.Current.Session[_KEYNAME] = value; }
		}

		/// <summary>
		/// Whether user has writable access to this session's report. 
		/// </summary>
		public bool UserHasWriteAccess
		{
			get { return CheckWriteAccess(); }
			//get { return true; } //testing only
		}
		
		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Event Handlers

		protected override void OnInit(EventArgs e)
		{
			_report = GetThisReport(); //get new report from querystring, or cached from session

			//fire base's event so actual pages can handle it & do their stuff
			base.OnInit(e);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected override void OnLoad(EventArgs e)
		{
			int reportID = _report.ID;

			#region load pageLoad js for report pages

			ScriptReference script = new ScriptReference("~/js/pageLoadReport.js");

			RadScriptManager scriptManager = (RadScriptManager)base._master.FindControl("radScriptManager");
			scriptManager.Scripts.Add(script);

			//set reportID into html for client-side use
			base._master.HiddenReportID = reportID.ToString();

			#endregion			

			#region report state writing

			if (_report != null)
			{

				
				//TODO: BUG: multiple tabs losing sync. it only happens when 1) no rid in querystring. 2) tab A is refreshed after tab B has been loaded. also happens in EPR, doh...

				//i dont know how to fix this, other than to stop using session for report caching. 


				if (!IsPostBack)
				{
					SetFormValues();
					
					//store the reportID within this page's HTML in order to keep page/form sync w/ Session object -- on save, check that Session and ViewState ID's match first. this enables multiple-tabs in single session support.
					ViewState["reportID"] = _report.ID; //dont need this because GetReport() is already doing it.
				}
			}
			else
			{
				base.RenderUserMessage(Enums.UserMessageTypes.Warning, "Sorry, there was a problem: Could not locate this Report.");
			}

			#endregion

			#region header items visbility

			base.Heading = "Item # " + _report.ItemNumber;

			if (_report.ApprovalStatusType == Report.ApprovalStatusTypes.Pending)
			{
				base.Heading += " (Pending)";
			}

			base._ucHeader.ReportID = reportID;
		
			//radwindows defined in IncidentReports.Master
			RadWindow rwSubmit = base._radWindowManager.Windows[1];
			RadWindow rwSupervisorReview = base._radWindowManager.Windows[2];
			RadWindow rwSupervisorNotes = base._radWindowManager.Windows[3];

			//set this reportID into popup URLs
			rwSubmit.NavigateUrl += reportID;
			rwSupervisorReview.NavigateUrl += reportID;
			rwSupervisorNotes.NavigateUrl += reportID;
			base._radWindowManager.Windows[4].NavigateUrl += reportID;


			//submit report popup
			if (this.UserHasWriteAccess)
			{
				base._ucHeader.ShowSubmitReportButton = true;

				//enable radwindow HTML
				rwSubmit.Visible = true;
			}
			else //if user doesnt have write-access, lock page down.
			{
				base.RenderUserMessage(Enums.UserMessageTypes.Warning, " You have read-only access to this report. Changes will not be saved.");
			}

			//supervisor button
			if (CheckSupervisorAccess())
			{
				base._ucHeader.ShowSupervisorButton = true;

				//enable radwindow HTML
				rwSupervisorReview.Visible = true;
			}

			//supervisor notes popup
			if (_report.ApprovalStatusType == Report.ApprovalStatusTypes.Rejected && _report.CreatedBy == this.Username)
			{
				base._ucHeader.ShowSupervisorNotesButton = true;

				//enable radwindow HTML
				rwSupervisorNotes.Visible = true;
								
				//append reportID to the .VisibleCookieName and .CoordsCookieName custom attribute value. picked up by js in radWindowState.js
				rwSupervisorNotes.Attributes["VisibleCookieName"] += reportID;
				rwSupervisorNotes.Attributes["CoordsCookieName"] += reportID;
			}
			
			#endregion
			
			//fire base's event so actual pages can handle it & do their stuff
			base.OnLoad(e);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Public Methods

		/// <summary>
		/// Ensures the report in Session is the same as when when this page's HTML was written. This is possible because the page writes the ReportID into this page's ViewState collection, 
		/// which is stored as a hidden element in the HTML. If the Session ReportID doesn't match ViewState, a new report is instantiated from ViewState and dropped into Session.
		/// </summary>
		/// <returns>Boolean indicator of whether sync was performed or not.</returns>
		public void SyncSessionToViewState()
		{

			//TODO: BUG: multiple tabs losing sync. it only happens when 1) no rid in querystring. 2) tab A is refreshed after tab B has been loaded. also happens in EPR, doh...



			//1) get reportID from viewstate
			//2) compare to reportID from session.
			//3) if not the same, get new report from db using viewstate ID & replace in session
			//
			//...this way we can continue to use just one report in session (which is fine for most people), then only re-load the report from db if user is doing multi-tabs, which is less often.

			int viewStateReportID = (int)ViewState["reportID"];

			if (viewStateReportID != _report.ID) //_report.ID comes from Session stored report
			{
				//ViewState & Session are out of sync (multiple tabs open, etc) -- so update global var & session report copies based on viewstate's saved reportID.
				_report = new Report(viewStateReportID); //global var
				this.Report = _report; //puts in session
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Determines whether to save report, and then performs redirect to desired location.
		/// </summary>
		/// <param name="redirectUrl">The target page to redirect user to.</param>
		/// <param name="saveReport">Whether to attempt a save; this means calling the page class' overridden SaveReport() method.</param>
		public void ProcessNavClick(string redirectUrl, bool saveReport)
		{
			//ensure the instantiated report in session is the same ReportID as the one saved in the page's ViewState. without this multi-tab states get dirty.
			SyncSessionToViewState();

			if (saveReport)
			{
				if (Page.IsValid)
					{
						StatusInfo status = SaveReport();
					
						if (status.Success)
						{
							//now redirect to desired page
							Response.Redirect(redirectUrl, false);  //false = prevents a ThreadAbortException from raising (which is expensive)
							HttpContext.Current.ApplicationInstance.CompleteRequest(); //stops all subsquent code from executing

							//TODO: possible refactor: contemplate setting a "_doingRedirect = true" flag, then in the page lifecyle events look for it and halt execution if true. per this article:
							//
							//http://stackoverflow.com/a/14452208/1284093
						}
						else
						{
							base.RenderUserMessage(Enums.UserMessageTypes.Warning, ("Sorry, there was a problem saving: " + status.Message));
						}
					}
			}
			else //not a save attempt. just move sections.
			{
				Response.Redirect(redirectUrl, false);  //false = prevents a ThreadAbortException from raising (which is expensive)
				HttpContext.Current.ApplicationInstance.CompleteRequest(); //stops all subsquent code from executing
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Saves the report. A stub only, overridden by the actual report pages, which implement their saving particulars.
		/// </summary>
		/// <returns></returns>
		public virtual StatusInfo SaveReport()
		{
			throw new NotImplementedException();
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Sets the page's form fields to the Report's values. A stub only, overridden by the actual report pages, which implement their particulars.
		/// </summary>
		/// <returns></returns>
		public virtual void SetFormValues()
		{
			//throw new NotImplementedException();
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Private Methods

		/// <summary>
		/// Gets the Report the user is viewing. First we check the querystring for requested ReportID. if found, we make sure it's not
		/// the same one already in the Session. if not, we hit the db and drop it into session. (if it is the same as the one in Session
		/// we just give them the one already in Session).
		/// </summary>
		private Report GetThisReport()
		{
			Report report = null;

			HttpContext context = HttpContext.Current;

			int sessionReportID = -1;
			int querystringReportID = -1;

			string rid = context.Request.QueryString["rid"];

			//if user is requesting an existing report via the querystring (eg, from a search result).
			if (!string.IsNullOrEmpty(rid))
			{
				querystringReportID = Int32.Parse(rid);

				//get the ReportID of whatever report may be in Session
				if (context.Session[_KEYNAME] != null)
				{
					sessionReportID = ((Report)context.Session[_KEYNAME]).ID;
				}

				//if the querystring ReportID is different than the Session's ReportID, then we know the user is requesting a different report than the last one seen. go get it
				if (sessionReportID != querystringReportID)
				{
					//clear existing Session-stored report
					context.Session[_KEYNAME] = null;

					//grab requested report from db
					report = new Report(querystringReportID);

					if (report.ID > 0) //unmatched reports come back initialized, but not populated
					{
						//drop into Session for future use
						context.Session[_KEYNAME] = report;

						//drop ReportID into viewstate of page. this is used on report-updates to ensure session report matches page's report (they can get contaiminated if user opens multiple reports in browser tabs, since tabs share one session)
						//ViewState["reportID"] = report.ID;
					}
					else
					{
						report = null;
					}
				}
			}
			

			//finally, grab report from Session -- its either already there, or just placed there from a fresh "rid".
			if (context.Session[_KEYNAME] != null)
			{
				report = (Report)context.Session[_KEYNAME];
			}

			return report;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Determines whether the current User has write-priviledges to this Report.
		/// </summary>
		private bool CheckWriteAccess()
		{
			bool returnValue = false;

			//Report report = this.Report;

			if (_report != null)
				returnValue = _report.UserHasWritePermission(this.Username);

			return returnValue;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Determines whether the current User has supervisor-priviledges for the current report.
		/// </summary>
		private bool CheckSupervisorAccess()
		{
			bool returnValue = base.UserIsSupervisor;
			
			//no officer can approve his own report
			if (_report != null && _report.CreatedBy == this.Username)
				returnValue = false;

			//return returnValue;
			return true; //debug only
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}