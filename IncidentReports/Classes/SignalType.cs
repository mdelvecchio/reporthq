using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Caching;

using ReportHq.Common;
using ReportHq.IncidentReports.DataAccess;

namespace ReportHq.IncidentReports
{
	/// <summary>
	/// A Report's Signal Type.
	/// </summary>
	public class SignalType
	{
		#region Properties

		public int ID { get; set; }		
		public string Code { get; set; }
		public string Description { get; set; }
		public bool IsDomesticViolence { get; set; }
		public bool IsCriminal { get; set; }
		public int UcrOffenseTypeID { get; set; }
		public bool IsActive { get; set; }

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Constructors

		public SignalType()
		{
			
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Get a Signal Type from the db.
		/// </summary>
		public SignalType(int signalTypeID)
		{
			this.ID = signalTypeID;

			//DataSet ds = BulletinDA.GetBulletinByID(bulletinID);

			//if (ds.Tables[0].Rows.Count > 0)
			//	FillObject(ds);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		
		/// <summary>
		/// Get a Signal Type from a RadGrid.
		/// </summary>
		public SignalType(Hashtable newValues)
		{
			foreach (DictionaryEntry entry in newValues)
			{
				switch ((string)entry.Key)
				{
					case "Code":
						this.Code = (string)entry.Value;
						break;

					case "Description":
						this.Description = (string)entry.Value;
						break;

					case "IsDomesticViolence":
						this.IsDomesticViolence = (bool)entry.Value;
						break;

					case "IsCriminal":
						this.IsCriminal = (bool)entry.Value;
						break;

					case "UcrOffenseTypeID":
						if (entry.Value != null)
							this.UcrOffenseTypeID = Int32.Parse((string)entry.Value);
						else
							this.UcrOffenseTypeID = -1;
						
						break;

					case "IsActive":
						this.IsActive = (bool)entry.Value;
						break;
				}
			}			
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Public Methods

		/// <summary>
		/// Write new Signal Type into the database.
		/// </summary>
		public StatusInfo Add()
		{
			StatusInfo status = SignalTypeDA.InsertSignalType(this.Code,
															  this.Description,
															  this.IsCriminal,
															  this.IsDomesticViolence,
															  this.UcrOffenseTypeID,
															  this.IsActive);
			
			if (status.Success)
			{
				//set new SignalTypeID
				this.ID = status.Code;
			}

			return status;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		
		/// <summary>
		/// Save an existing Signal Type in the database.
		/// </summary>
		public StatusInfo Update()
		{
			return SignalTypeDA.UpdateSignalType(this.ID,
												 this.Code,
												 this.Description,
												 this.IsCriminal,
												 this.IsDomesticViolence,
												 this.UcrOffenseTypeID,
												 this.IsActive);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
				
		#region Public Static Methods

		public static StatusInfo DeactivateSignalType(int signalTypeID)
		{
			return SignalTypeDA.DeactivateSignalType(signalTypeID);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetSignalTypes(bool getActiveOnly)
		{
			if (getActiveOnly) //get normal, active signal types from cache
			{
				const string KEYNAME = "dtSignalTypes";

				Cache cache = HttpRuntime.Cache;

				if (cache[KEYNAME] == null) //not in session, create
				{
					DataTable results = SignalTypeDA.GetSignalTypes(true);

					cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
				}

				//return table (which is already in cache or brand new)
				return (DataTable)cache[KEYNAME];
			}
			else //user wants inactive records too, so give them fresh data since we only cache active records
			{
				return SignalTypeDA.GetSignalTypes(false);
			}
		}
		
		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Private Methods

		/// <summary>
		/// Fills this object for a specific GlobalNotification, via constructor.
		/// </summary>
		private void FillObject(DataSet ds)
		{
			//DataRow row = ds.Tables[0].Rows[0];

			//this.TypeID = (int)row["BulletinTypeID"];
			//this.Type = (Types)this.TypeID;
			//this.TypeName = (string)row["Type"];
			//this.IsApb = (bool)row["IsAllPoints"];
			//this.IsActive = (bool)row["IsActive"];
			//this.Title = (string)row["Title"];
			//this.BulletinContent = (string)row["Bulletin"];
			//this.CreateDate = (DateTime)row["CreatedDate"];
			
			////fill recipients collection
			//List<DescriptionItem> items = new List<DescriptionItem>();

			//DescriptionItem newItem;
			//foreach (DataRow row2 in ds.Tables[1].Rows)
			//{
			//	newItem = new DescriptionItem();
			//	newItem.ID = (int)row2["UserRoleID"];

			//	items.Add(newItem);
			//}

			//this.RecipientRoleIDs = items;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}
