using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.Caching;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using ReportHq.Common;
using ReportHq.IncidentReports.DataAccess;
using ReportHq.IncidentReports.Entities;

namespace ReportHq.IncidentReports
{
	/// <summary>
	/// An Offender Charge.
	/// </summary>
	public class OffenderCharge : OffenderChargeEntity
	{
		#region Properties
		
		//these are disabled here because they're inherited from the entity object now

		//private int _ID;
		//private int _offenderID;
		//private int _chargePrefixTypeID;
		//private string _prefix;
		//private int _chargeTypeID;
		//private string _code;
		//private string _charge;
		//private string _chargeDetails;

		//private int _victimPersonID;
		//private int _victimNumber;
		//private int _victimOffenderRelationshipTypeID;
		//private string _victimsRelationship;

		//public int ID
		//{
		//	set { _ID = value; }
		//	get { return _ID; }
		//}

		//public int OffenderID
		//{
		//	set { _offenderID = value; }
		//	get { return _offenderID; }
		//}

		//public int ChargePrefixTypeID
		//{
		//	set { _chargePrefixTypeID = value; }
		//	get { return _chargePrefixTypeID; }
		//}

		//public string Prefix
		//{
		//	set { _prefix = value; }
		//	get { return _prefix; }
		//}

		//public int ChargeTypeID
		//{
		//	set { _chargeTypeID = value; }
		//	get { return _chargeTypeID; }
		//}

		//public string Code
		//{
		//	set { _code = value; }
		//	get { return _code; }
		//}

		///// <summary>
		///// A description of the ChargeTypeID from ChargeTypes table. Read-only.
		///// </summary>
		//public string Charge
		//{
		//	set { _charge = value; }
		//	get { return _charge; }
		//}

		///// <summary>
		///// Optional additional details the officer may record.
		///// </summary>
		//public string ChargeDetails
		//{
		//	set { _chargeDetails = value; }
		//	get { return _chargeDetails; }
		//}
		
		//public int VictimPersonID
		//{
		//	set { _victimPersonID = value; }
		//	get { return _victimPersonID; }
		//}

		//public int VictimNumber
		//{
		//	set { _victimNumber = value; }
		//	get { return _victimNumber; }
		//}
		
		//public int VictimOffenderRelationshipTypeID
		//{
		//	set { _victimOffenderRelationshipTypeID = value; }
		//	get { return _victimOffenderRelationshipTypeID; }
		//}

		//public string VictimsRelationship
		//{
		//	set { _victimsRelationship = value; }
		//	get { return _victimsRelationship; }
		//}

		
		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Constructors

		/// <summary>
		/// Use this when filling a new OffenderCharge from values collected from a Grid. (ex: an insert operation)
		/// </summary>
		public OffenderCharge(Hashtable newValues)
		{
			//NOTE: there is no OffenderID being set because thats not int the form's newValues collection (since it cannot be set by user)
			//		instead the app sets it after instantiating this object.

			this.ChargeTypeID = (int)newValues["ChargeTypeID"];
			//this.Charge = (string)newValues["Charge"]; //now a read-only value, so we wont be setting this from new form values
			this.ChargeDetails = (string)newValues["ChargeDetails"];
			this.VictimPersonID = (int)newValues["VictimPersonID"];
			this.VictimOffenderRelationshipTypeID = (int)newValues["VictimOffenderRelationshipTypeID"];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Use this when filling an existing OffenderCharge from values collected from a Grid. (ex: an update operation)
		/// </summary>
		public OffenderCharge(int offenderChargeID, Hashtable newValues)
		{
			//NOTE: there is no OffenderID being set because that cannot change.

			this.ID = offenderChargeID;
			this.ChargeTypeID = (int)newValues["ChargeTypeID"];
			//this.Charge = (string)newValues["Charge"]; //now a read-only value, so we wont be setting this from new form values
			this.ChargeDetails = (string)newValues["ChargeDetails"];
			this.VictimPersonID = (int)newValues["VictimPersonID"];
			this.VictimOffenderRelationshipTypeID = (int)newValues["VictimOffenderRelationshipTypeID"];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Public Methods

		public StatusInfo Add(string createdBy)
		{
			return OffenderChargeDA.InsertOffenderCharge(this.OffenderID, 
														 this.ChargeTypeID,
														 this.ChargeDetails,
														 this.VictimPersonID,
														 this.VictimOffenderRelationshipTypeID,
														 createdBy);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public StatusInfo Update(string modifiedBy)
		{
			return OffenderChargeDA.UpdateOffenderCharge(this.ID,
														 this.ChargeTypeID,
														 this.ChargeDetails,
														 this.VictimPersonID,
														 this.VictimOffenderRelationshipTypeID,
														 modifiedBy);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static StatusInfo DeleteOffenderCharge(int offenderChargeID)
		{
			return OffenderChargeDA.DeleteOffenderCharge(offenderChargeID);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Public Static Methods

		/// <summary>
		/// New way, using entity objects. Gets a typed list of an Offender's Charges.
		/// </summary>
		public static List<OffenderChargeEntity> GetOffenderChargesByOffender(int offenderID)
		{
			return OffenderChargeDA.GetOffenderChargesByOffender(offenderID);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// The Charge Prefix is a reference to the government document which defines its charges. Can be federal, state, local, etc. 
		/// </summary>
		/// <param name="getActiveOnly">Only returns records which are currently usable by report writers.</param>
		/// <returns></returns>
		public static DataTable GetChargePrefixTypes(bool getActiveOnly)
		{
			DataTable returnData = null;

			if (getActiveOnly) //get from cache, active records only
			{
				const string KEYNAME = "dtChargePrefixTypes";

				Cache cache = HttpRuntime.Cache;

				if (cache[KEYNAME] == null) //not in session, create
				{
					DataTable results = OffenderChargeDA.GetChargePrefixTypes(getActiveOnly);

					cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
				}

				returnData = (DataTable)cache[KEYNAME];

			}
			else //get fresh, w/ all records
			{
				returnData = OffenderChargeDA.GetChargePrefixTypes(false);
			}

			return returnData;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static StatusInfo InsertChargePrefixType(string name, string description, bool isActive)
		{
			return OffenderChargeDA.InsertChargePrefixType(name, description, isActive);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static StatusInfo UpdateChargePrefixType(int chargePrefixTypeID, string name, string description, bool isActive)
		{
			return OffenderChargeDA.UpdateChargePrefixType(chargePrefixTypeID, name, description, isActive);
		}
		
		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static StatusInfo DeactivateChargePrefixType(int chargePrefixTypeID)
		{
			return OffenderChargeDA.DeactivateChargePrefixType(chargePrefixTypeID);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// The Charge Type is a list of all charges in for a given Charge Prefix document.. 
		/// </summary>
		/// <param name="getActiveOnly">Only returns records which are currently usable by report writers.</param>
		public static DataTable GetChargeTypesByChargePrefix(int chargePrefixTypeID, bool getActiveOnly)
		{
			if (getActiveOnly)
			{
				string KEYNAME = "dtChargeTypes_" + chargePrefixTypeID.ToString();

				Cache cache = HttpRuntime.Cache;

				if (cache[KEYNAME] == null) //not in cache, create
				{
					DataTable results = OffenderChargeDA.GetChargeTypesByChargePrefix(chargePrefixTypeID, true);

					cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(7), TimeSpan.Zero);
				}

				//return table (which is already in cache or brand new)
				return (DataTable)cache[KEYNAME];
			}
			else //user wants inactive records too, so give them fresh data since we only cache active records
			{
				return OffenderChargeDA.GetChargeTypesByChargePrefix(chargePrefixTypeID, false);
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static StatusInfo InsertChargeType(int chargePrefixTypeID,
												  string code,
												  string description,
												  string ucrCode,
												  string nibrsCode,
												  string librsCode,
												  bool isActive)
		{
			return OffenderChargeDA.InsertChargeType(chargePrefixTypeID,
													 code,
													 description,
													 ucrCode,
													 nibrsCode,
													 librsCode,
													 isActive);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static StatusInfo UpdateChargeType(int chargeTypeID,
												  int chargePrefixTypeID,
												  string code,
												  string description,
												  string ucrCode,
												  string nibrsCode,
												  string librsCode,
												  bool isActive)
		{
			return OffenderChargeDA.UpdateChargeType(chargeTypeID,
													 chargePrefixTypeID,
													 code,
													 description,
													 ucrCode,
													 nibrsCode,
													 librsCode,
													 isActive);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static StatusInfo DeactivateChargeType(int chargeTypeID)
		{
			return OffenderChargeDA.DeactivateChargeType(chargeTypeID);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Gets a ChargeType row for the given ChargeTypeID.
		/// </summary>
		public static DataTable GetChargeType(int chargeTypeID)
		{
			return OffenderChargeDA.GetChargeType(chargeTypeID);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Private Methods


		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}
