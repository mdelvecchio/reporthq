using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Caching;
using System.Web.Security;

using ReportHq.Common;
using ReportHq.IncidentReports.DataAccess;
//using WebSupergoo.ABCpdf7;

namespace ReportHq.IncidentReports
{
	public class Report
	{
		#region Enums

		public enum ApprovalStatusTypes
		{
			Approved = 1,
			Rejected = 2,
			Pending = 3,
			Incomplete = 4,
			DataEntry = 5
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion 

		#region Constructors

		public Report()
		{
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Use this when requesting a specific Report.
		/// </summary>
		public Report(int reportID)
		{
			DataSet ds = ReportDA.GetReportByID(reportID);

			if (ds.Tables[0].Rows.Count > 0)
				FillObject(this, ds.Tables[0].Rows[0]);
			//else
				//throw new Exception("Could not locate a Report for ReportID " + reportID);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Use this when requesting a specific Report. Filling all collections is slow, probably will only be used for printing a PDF or something.
		/// </summary>
		public Report(int reportID, bool fillCollections)
		{
			DataSet ds = ReportDA.GetReportByID(reportID);

			if (ds.Tables[0].Rows.Count > 0)
			{
				FillObject(this, ds.Tables[0].Rows[0]);

				if (fillCollections)
					FillCollections();
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Properties

		#region REQUIRED

		private int _ID;
		private int _reportTypeID;
		private int _approvalStatusTypeID;
		ApprovalStatusTypes _approvalStatusType;
		private string _itemNumber;

		/// <summary>
		/// The db-issued unique ID value for this report.
		/// </summary>
		public int ID
		{
			get { return _ID; }
			set { _ID = value; }
		}

		/// <summary>
		/// The type ID for this report (Initial, or Supplementary)
		/// </summary>
		public int ReportTypeID
		{
			get { return _reportTypeID; }
			set { _reportTypeID = value; }
		}

		public int ApprovalStatusTypeID
		{
			get { return _approvalStatusTypeID; }
			set { _approvalStatusTypeID = value; }
		}

		/// <summary>
		/// The current approval-workflow status of this report.
		/// </summary>
		public ApprovalStatusTypes ApprovalStatusType
		{
			get { return _approvalStatusType; }
			set { _approvalStatusType = value; }
		}

		/// <summary>
		/// The unique CAD-issued incident number.
		/// </summary>
		public string ItemNumber
		{
			get { return _itemNumber; }
			set { _itemNumber = value; }
		}

		#endregion

		#region EVENT

		private int _signalTypeID;
		private string _signal;
		private string _incidentDescription;
		private string _location;
		private int _district;
		private string _zone;
		private string _subZone;
		private DateTime _occuredDateTime;
		private DateTime _reportDatetTime;
		private int _lightingConditionTypeID;
		private string _lightingCondition;
		private int _statusTypeID;
		private string _status;
		private bool _bulletinRequired;
		private int _bulletinNumber;

		public int SignalTypeID
		{
			get { return _signalTypeID; }
			set { _signalTypeID = value; }
		}

		public string Signal
		{
			get { return _signal; }
			set { _signal = value; }
		}

		public string IncidentDescription
		{
			get { return _incidentDescription; }
			set { _incidentDescription = value; }
		}

		public string Location
		{
			get { return _location; }
			set { _location = value; }
		}

		public int District
		{
			get { return _district; }
			set { _district = value; }
		}

		public string Zone
		{
			get { return _zone; }
			set { _zone = value; }
		}

		public string SubZone
		{
			get { return _subZone; }
			set { _subZone = value; }
		}

		public DateTime OccurredDateTime
		{
			get { return _occuredDateTime; }
			set { _occuredDateTime = value; }
		}

		public DateTime ReportDateTime
		{
			get { return _reportDatetTime; }
			set { _reportDatetTime = value; }
		}

		public int LightingConditionTypeID
		{
			get { return _lightingConditionTypeID; }
			set { _lightingConditionTypeID = value; }
		}

		public string LightingCondition
		{
			get { return _lightingCondition; }
			set { _lightingCondition = value; }
		}

		public int StatusTypeID
		{
			get { return _statusTypeID; }
			set { _statusTypeID = value; }
		}

		public string Status
		{
			get { return _status; }
			set { _status = value; }
		}

		public bool BulletinRequired
		{
			get { return _bulletinRequired; }
			set { _bulletinRequired = value; }
		}

		public int BulletinNumber
		{
			get { return _bulletinNumber; }
			set { _bulletinNumber = value; }
		}
		
		#endregion

		#region UCR

		private int _ucrCategoryID;
		private string _ucrCategory;
		private int _ucrOffenseTypeID;
		private string _ucrOffense;

		public int UcrCategoryID
		{
			get { return _ucrCategoryID; }
			set { _ucrCategoryID = value; }
		}

		public string UcrCategory
		{
			get { return _ucrCategory; }
			set { _ucrCategory = value; }
		}

		public int UcrOffenseTypeID
		{
			get { return _ucrOffenseTypeID; }
			set { _ucrOffenseTypeID = value; }
		}

		public string UcrOffense
		{
			get { return _ucrOffense; }
			set { _ucrOffense = value; }
		}

		#endregion

		#region ADMIN

		private string _officer1Name;
		private int _officer1Badge;
		private int _officer1RankTypeID;
		private string _officer2Name;
		private int _officer2Badge;
		private int _officer1EmployeeID;
		private int _officer2RankTypeID;
		private int _reportingCarNumber;
		private string _reportingCarPlatoon;
		private int _reportingAssignmentUnitTypeID;
		private string _detectiveName;
		private string _crimeLabName;
		private string _otherName;

		public string Officer1Name
		{
			get { return _officer1Name; }
			set { _officer1Name = value; }
		}

		public int Officer1Badge
		{
			get { return _officer1Badge; }
			set { _officer1Badge = value; }
		}

		public int Officer1EmployeeID
		{
			get { return _officer1EmployeeID; }
			set { _officer1EmployeeID = value; }
		}

		public int Officer1RankTypeID
		{
			get { return _officer1RankTypeID; }
			set { _officer1RankTypeID = value; }
		}

		public string Officer1Rank
		{
			get { return GetRankByID(this.Officer1RankTypeID); }
		}

		public string Officer2Name
		{
			get { return _officer2Name; }
			set { _officer2Name = value; }
		}

		public int Officer2Badge
		{
			get { return _officer2Badge; }
			set { _officer2Badge = value; }
		}

		public int Officer2RankTypeID
		{
			get { return _officer2RankTypeID; }
			set { _officer2RankTypeID = value; }
		}

		public string Officer2Rank
		{
			get { return GetRankByID(this.Officer2RankTypeID); }
		}

		public int ReportingCarNumber
		{
			get { return _reportingCarNumber; }
			set { _reportingCarNumber = value; }
		}

		public string ReportingCarPlatoon
		{
			get { return _reportingCarPlatoon; }
			set { _reportingCarPlatoon = value; }
		}

		public int ReportingAssignmentUnitTypeID
		{
			get { return _reportingAssignmentUnitTypeID; }
			set { _reportingAssignmentUnitTypeID = value; }
		}

		public string DetectiveName
		{
			get { return _detectiveName; }
			set { _detectiveName = value; }
		}

		public string CrimeLabName
		{
			get { return _crimeLabName; }
			set { _crimeLabName = value; }
		}

		public string OtherName
		{
			get { return _otherName; }
			set { _otherName = value; }
		}

		#endregion

		#region SUPERVISOR

		private string _supervisorName;
		private int _supervisorBadge;
		private int _supervisorRankTypeID;
		private string _supervisorComments;
		private DateTime _supervisorLastModifiedDate;

		public string SupervisorName
		{
			get { return _supervisorName; }
			set { _supervisorName = value; }
		}

		public int SupervisorBadge
		{
			get { return _supervisorBadge; }
			set { _supervisorBadge = value; }
		}

		public int SupervisorRankTypeID
		{
			get { return _supervisorRankTypeID; }
			set { _supervisorRankTypeID = value; }
		}

		public string SupervisorRank
		{
			get { return GetRankByID(this.SupervisorRankTypeID); }
		}

		public string SupervisorComments
		{
			get { return _supervisorComments; }
			set { _supervisorComments = value; }
		}

		public DateTime SupervisorLastModifiedDate
		{
			get { return _supervisorLastModifiedDate; }
			set { _supervisorLastModifiedDate = value; }
		}

		#endregion

		#region BOILER

		private string _createdBy;
		private DateTime _createdDate;
		private string _lastModifiedBy;
		private DateTime _lastModifiedDate;
		private bool _isActive;

		public string CreatedBy
		{
			get { return _createdBy; }
			set { _createdBy = value; }
		}

		public DateTime CreatedDate
		{
			get { return _createdDate; }
			set { _createdDate = value; }
		}

		public string LastModifiedBy
		{
			get { return _lastModifiedBy; }
			set { _lastModifiedBy = value; }
		}

		public DateTime LastModifiedDate
		{
			get { return _lastModifiedDate; }
			set { _lastModifiedDate = value; }
		}

		public bool IsActive
		{
			get { return _isActive; }
			set { _isActive = value; }
		}

		#endregion

		#region INCIDENT MO - MOTIVATION

		private List<DescriptionItem> _motivationCriminalActivityItems;
		private List<DescriptionItem> _motivationMotiveItems;
		private List<DescriptionItem> _motivationTargetItems;

		public List<DescriptionItem> MotivationCriminalActivityItems
		{
			get {
				if (_motivationCriminalActivityItems == null)
					return GetMotivationsByCategory(1);
				else
					return _motivationCriminalActivityItems;
			}
			set { _motivationCriminalActivityItems = value; }
		}

		public List<DescriptionItem> MotivationMotiveItems
		{
			get
			{
				if (_motivationMotiveItems == null)
					return GetMotivationsByCategory(2);
				else
					return _motivationMotiveItems;
			}
			set { _motivationMotiveItems = value; }
		}

		public List<DescriptionItem> MotivationTargetItems
		{
			get
			{
				if (_motivationTargetItems == null)
					return GetMotivationsByCategory(3);
				else
					return _motivationTargetItems;
			}
			set { _motivationTargetItems = value; }
		}

		#endregion
		
		#region INCIDENT MO - MEANS OF ENTRY

		private List<DescriptionItem> _meansOfEntryPointOfEntryItems;
		private List<DescriptionItem> _meansOfEntryMethodOfEntryItems;
		private List<SecurityUsedDescriptionItem> _meansOfEntrySecurityUsedItems;

		public List<DescriptionItem> MeansOfEntryPointOfEntryItems
		{
			get
			{
				if (_meansOfEntryPointOfEntryItems == null)
					return GetMeansOfEntryByCategory(2);
				else
					return _meansOfEntryPointOfEntryItems;
			}
			set { _meansOfEntryPointOfEntryItems = value; }
		}

		public List<DescriptionItem> MeansOfEntryMethodOfEntryItems
		{
			get
			{
				if (_meansOfEntryMethodOfEntryItems == null)
					return GetMeansOfEntryByCategory(1);
				else
					return _meansOfEntryMethodOfEntryItems;
			}
			set { _meansOfEntryMethodOfEntryItems = value; }
		}

		public List<SecurityUsedDescriptionItem> MeansOfEntrySecurityUsedItems
		{
			get
			{
				if (_meansOfEntrySecurityUsedItems == null)
					return GetMeansOfEntrySecurityUsed();

						
				else
					return _meansOfEntrySecurityUsedItems;
			}
			set { _meansOfEntrySecurityUsedItems = value; }
		}

		#endregion
		
		#region INCIDENT MO - CRIME LOCATION

		private List<DescriptionItem> _crimeLocationResidentialItems;
		private List<DescriptionItem> _crimeLocationCommercialItems;
		private List<DescriptionItem> _crimeLocationOutdoorAreaItems;
		private List<DescriptionItem> _crimeLocationPublicAccessItems;
		private List<DescriptionItem> _crimeLocationMovableItems;
		private List<DescriptionItem> _crimeLocationStructureTypeItems;
		private List<DescriptionItem> _crimeLocationStructureStatusItems;

		public List<DescriptionItem> CrimeLocationResidentialItems
		{
			get
			{
				if (_crimeLocationResidentialItems == null)
					return GetCrimeLocationsByCategory(1);
				else
					return _crimeLocationResidentialItems;
			}
			set { _crimeLocationResidentialItems = value; }
		}

		public List<DescriptionItem> CrimeLocationCommercialItems
		{
			get
			{
				if (_crimeLocationCommercialItems == null)
					return GetCrimeLocationsByCategory(3);
				else
					return _crimeLocationCommercialItems;
			}
			set { _crimeLocationCommercialItems = value; }
		}

		public List<DescriptionItem> CrimeLocationOutdoorAreaItems
		{
			get
			{
				if (_crimeLocationOutdoorAreaItems == null)
					return GetCrimeLocationsByCategory(2);
				else
					return _crimeLocationOutdoorAreaItems;
			}
			set { _crimeLocationOutdoorAreaItems = value; }
		}

		public List<DescriptionItem> CrimeLocationPublicAccessItems
		{
			get
			{
				if (_crimeLocationPublicAccessItems == null)
					return GetCrimeLocationsByCategory(4);
				else
					return _crimeLocationPublicAccessItems;
			}
			set { _crimeLocationPublicAccessItems = value; }
		}

		public List<DescriptionItem> CrimeLocationMovableItems
		{
			get
			{
				if (_crimeLocationMovableItems == null)
					return GetCrimeLocationsByCategory(5);
				else
					return _crimeLocationMovableItems;
			}
			set { _crimeLocationMovableItems = value; }
		}

		public List<DescriptionItem> CrimeLocationStructureTypeItems
		{
			get
			{
				if (_crimeLocationStructureTypeItems == null)
					return GetCrimeLocationsByCategory(6);
				else
					return _crimeLocationStructureTypeItems;
			}
			set { _crimeLocationStructureTypeItems = value; }
		}

		public List<DescriptionItem> CrimeLocationStructureStatusItems
		{
			get
			{
				if (_crimeLocationStructureStatusItems == null)
					return GetCrimeLocationsByCategory(7);
				else
					return _crimeLocationStructureStatusItems;
			}
			set { _crimeLocationStructureStatusItems = value; }
		}

		

		#endregion
		
		#region INCIDENT MO - OFFENDERS ACTIONS

		private List<DescriptionItem> _offendersActionsApproachItems;
		private List<DescriptionItem> _offendersActionsImpersonatedItems;
		private List<DescriptionItem> _offendersActionsWeaponItems;
		private List<DescriptionItem> _offendersActionsFirearmFeatureItems;
		private List<DescriptionItem> _offendersActionsPropertyCrimeItems;
		private List<DescriptionItem> _offendersActionsPersonCrimeItems;
		private List<DescriptionItem> _offendersActionsSexCrimeItems;

		public List<DescriptionItem> OffendersActionsApproachItems
		{
			get
			{
				if (_offendersActionsApproachItems == null)
					return GetOffenderActionsByCategory(1);
				else
					return _offendersActionsApproachItems;
			}
			set { _offendersActionsApproachItems = value; }
		}

		public List<DescriptionItem> OffendersActionsImpersonatedItems
		{
			get
			{
				if (_offendersActionsImpersonatedItems == null)
					return GetOffenderActionsByCategory(2);
				else
					return _offendersActionsImpersonatedItems;
			}
			set { _offendersActionsImpersonatedItems = value; }
		}

		public List<DescriptionItem> OffendersActionsWeaponItems
		{
			get
			{
				if (_offendersActionsWeaponItems == null)
					return GetOffenderActionsByCategory(3);
				else
					return _offendersActionsWeaponItems;
			}
			set { _offendersActionsWeaponItems = value; }
		}

		public List<DescriptionItem> OffendersActionsFirearmFeatureItems
		{
			get
			{
				if (_offendersActionsFirearmFeatureItems == null)
					return GetOffenderActionsByCategory(4);
				else
					return _offendersActionsFirearmFeatureItems;
			}
			set { _offendersActionsFirearmFeatureItems = value; }
		}

		public List<DescriptionItem> OffendersActionsPropertyCrimeItems
		{
			get
			{
				if (_offendersActionsPropertyCrimeItems == null)
					return GetOffenderActionsByCategory(5);
				else
					return _offendersActionsPropertyCrimeItems;
			}
			set { _offendersActionsPropertyCrimeItems = value; }
		}

		public List<DescriptionItem> OffendersActionsPersonCrimeItems
		{
			get
			{
				if (_offendersActionsPersonCrimeItems == null)
					return GetOffenderActionsByCategory(6);
				else
					return _offendersActionsPersonCrimeItems;
			}
			set { _offendersActionsPersonCrimeItems = value; }
		}

		public List<DescriptionItem> OffendersActionsSexCrimeItems
		{
			get
			{
				if (_offendersActionsSexCrimeItems == null)
					return GetOffenderActionsByCategory(7);
				else
					return _offendersActionsSexCrimeItems;
			}
			set { _offendersActionsSexCrimeItems = value; }
		}

		#endregion

		#region DESCRIPTIONS

		private string _narrative;
		private string _motivationDescription;
		private string _meansOfEntryDescription;
		private string _crimeLocationDescription;
		private string _offendersActionsDescription;

		public string Narrative
		{
			get { return _narrative; }
			set { _narrative = value; }
		}

		public string MotivationDescription
		{
			get { return _motivationDescription; }
			set { _motivationDescription = value; }
		}

		public string MeansOfEntryDescription
		{
			get { return _meansOfEntryDescription; }
			set { _meansOfEntryDescription = value; }
		}

		public string CrimeLocationDescription
		{
			get { return _crimeLocationDescription; }
			set { _crimeLocationDescription = value; }
		}

		public string OffendersActionsDescription
		{
			get { return _offendersActionsDescription; }
			set { _offendersActionsDescription = value; }
		}

		#endregion

		#region COLLECTIONS

		private List<VictimPerson> _victimPersons;
		private List<Offender> _offenders;
		private List<Weapon> _weapons;
		private List<Vehicle> _vehicles;
		private List<PropertyItem> _property;
		private List<EvidenceItem> _evidence;
			 
		public List<VictimPerson> VictimPersons
		{
			get { return _victimPersons; }
			set { _victimPersons = value; }
		}

		public List<Offender> Offenders
		{
			get { return _offenders; }
			set { _offenders = value; }
		}

		public List<Weapon> Weapons
		{
			get { return _weapons; }
			set { _weapons = value; }
		}

		public List<Vehicle> Vehicles
		{
			get { return _vehicles; }
			set { _vehicles = value; }
		}

		public List<PropertyItem> Property
		{
			get { return _property; }
			set { _property = value; }
		}

		public List<EvidenceItem> Evidence
		{
			get { return _evidence; }
			set { _evidence = value; }
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		private string _propertyEvidenceReceiptNumber;

		public bool IsSupplemental
		{
			get
			{
				if (_reportTypeID == 2) //db value for Supplmental
					return true;
				else
					return false;
			}
		}

		public bool IsDataEntry
		{
			get
			{
				if (_approvalStatusType == ApprovalStatusTypes.DataEntry) //db value for Data Entry reports (different rules than officer-written reports)
					return true;
				else
					return false;
			}
		}

		public string PropertyEvidenceReceiptNumber
		{
			get { return _propertyEvidenceReceiptNumber; }
			set { _propertyEvidenceReceiptNumber = value; }
		}

		/// <summary>
		/// Gets sums of Property values for types Stolen and Recovered.
		/// </summary>
		public Dictionary<string, double> PropertySums
		{
			get {
				//getting from BAL
				Dictionary<string, double> dictionary = new Dictionary<string, double>();

				if (this.Property == null)
					this.FillProperty();

				if (this.Property.Count > 0)
				{
					double i = 0;
					double j = 0;

					foreach (var item in this.Property)
					{
						if (item.LossType == "STOLEN")
							i += item.Value;

						if (item.LossType == "RECOVERED")
							j += item.Value;
					}

					dictionary.Add("ValueStolen", i);
					dictionary.Add("ValueRecovered", j);
				}

				//getting from a DAL call instead
				/*DataTable dtResults = PropertyEvidenceDA.GetPropertySumsByReport(this.ID);

				if (dtResults.Rows.Count > 0)
				{
					dictionary.Add("ValueStolen", 1111);
					dictionary.Add("ValueRecovered", 2222);
				}*/

				return dictionary;
			}
		}

		/// <summary>
		/// Gets count of Offenders set as arrested.
		/// </summary>
		public int OffendersArrested
		{
			get
			{
				if (this.Offenders == null)
					this.FillOffenders();

				int i = 0;

				if (this.Offenders.Count > 0)
				{
					foreach (var item in this.Offenders)
					{
						if (item.Status == "ARRESTED")
							i += 1;
					}
				}

				return i;
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
				
		#endregion

		#region Public Methods

		/// <summary>
		/// Creates a new incident Report.
		/// </summary>
		public StatusInfo Add(string createdBy)
		{
			return ReportDA.InsertReport(this.ReportTypeID, this.ItemNumber, createdBy);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public StatusInfo UpdateEvent(string modifiedBy)
		{
			//old:	 if the event is updated and there is not UcrOffenseTypeID set (ie, first time report is saved), then it updates the UcrOffenseTypeID w/ the value linked to the SignalTypeID. otherwise 
			//			 if there is a UcrOffenseTypeID set, we honor it, since the user could have gone into the UCR screen and set a specific UCR offense type that is not the default one for the SignalTypeID. 
			//			 is this a good policy? dunno, need to ask george.

			//new:		could make it so if the user changes the SignalTypeID via the BasicInfo.aspx page, it resets the UcrOffenseTypeID, regardless of manually setting the UCR. dunno if that would be goor or annoying.
			//			if going for it, reset the this.UcrOffenseTypeID from within BasicInfo.aspx if the SignalTypeID has changed from the usercontrol vs the in-memory report.


			////if no existing UCR type set, set it based on Signal
			//if (this.UcrOffenseTypeID < 1) 
			//{
			//	//get & set OffenseTypeID for this SignalTypeID. setting ensures session cached copy is up to date.
			//	object[] ucrIDs = Ucr.GetOffenseTypeForSignal(this.SignalTypeID);

			//	if (ucrIDs != null)
			//	{
			//		this.UcrCategoryID = (int)ucrIDs[0];
			//		this.UcrOffenseTypeID = (int)ucrIDs[1];
			//	}
			//}

			//get & set OffenseTypeID based on current Signal. setting ensures session cached copy is up to date.
			object[] ucrIDs = Bases.UcrOffense.GetOffenseTypeForSignal(this.SignalTypeID);

			if (ucrIDs != null)
			{
				this.UcrCategoryID = (int)ucrIDs[0];
				this.UcrOffenseTypeID = (int)ucrIDs[1];
			}
			else //no UCR was found for this signal
			{
				this.UcrCategoryID = -1;
				this.UcrOffenseTypeID = -1;
			}

			return ReportDA.UpdateEvent(this.ID,
										this.SignalTypeID,
										this.IncidentDescription,
										this.Location,
										this.District,
										this.Zone,
										this.SubZone,
										this.OccurredDateTime,
										this.ReportDateTime,
										this.LightingConditionTypeID,
										this.StatusTypeID,
										this.BulletinRequired,
										this.BulletinNumber,
										this.UcrOffenseTypeID,
										modifiedBy);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		//moved to ucr classes
		//public StatusInfo UpdateUcrDummy(string modifiedBy)
		//{
		//	return ReportDA.UpdateUcrDummy(this.ID,
		//								   this.UcrOffenseTypeID,
		//								   modifiedBy);
		//}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public StatusInfo UpdateAdmin(string modifiedBy)
		{
			return ReportDA.UpdateAdmin(this.ID,
										this.Officer1EmployeeID,
										this.Officer1Name,
										this.Officer1Badge,
										this.Officer1RankTypeID,
										this.Officer2Name,
										this.Officer2Badge,
										this.Officer2RankTypeID,
										this.ReportingCarNumber,
										this.ReportingCarPlatoon,
										this.ReportingAssignmentUnitTypeID,
										this.DetectiveName,
										this.CrimeLabName,
										this.OtherName,
										modifiedBy);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public StatusInfo UpdateMotivation(string modifiedBy)
		{
			//merge all the Description items into one list
			List<DescriptionItem> items = this.MotivationCriminalActivityItems;
			items.AddRange(this.MotivationMotiveItems);
			items.AddRange(this.MotivationTargetItems);

			//then produce an xmlstring of 'em ("<Items><id>3</id><id>6</id><id>15</id></Items>")
			string itemIdsXml = Common.GetXmlStringOfIds(items);

			return ReportDA.UpdateMotivation(this.ID, itemIdsXml, this.MotivationDescription, modifiedBy);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public StatusInfo UpdateMeansOfEntry(string modifiedBy)
		{
			//merge all the Description items into one list
			List<DescriptionItem> items = this.MeansOfEntryPointOfEntryItems;
			items.AddRange(this.MeansOfEntryMethodOfEntryItems);
			
			//then produce an xmlstring of em
			string itemIdsXml = Common.GetXmlStringOfIds(items);

			//then produce an xmlstring of security types used ("<Items><id wasDefeated='True'>3</id><id wasDefeated='False'>6</id></Items>")
			string securityUsedIdsXml = GetXmlStringOfSecurityUsedIds(this.MeansOfEntrySecurityUsedItems);

			return ReportDA.UpdateMeansOfEntry(this.ID, 
											   itemIdsXml, 
											   securityUsedIdsXml, 
											   this.MeansOfEntryDescription, 
											   modifiedBy);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public StatusInfo UpdateCrimeLocation(string modifiedBy)
		{
			//merge all the Description items into one list
			List<DescriptionItem> items = this.CrimeLocationResidentialItems;
			items.AddRange(this.CrimeLocationCommercialItems);
			items.AddRange(this.CrimeLocationOutdoorAreaItems);
			items.AddRange(this.CrimeLocationPublicAccessItems);
			items.AddRange(this.CrimeLocationMovableItems);
			items.AddRange(this.CrimeLocationStructureTypeItems);
			items.AddRange(this.CrimeLocationStructureStatusItems);

			//then produce an xmlstring of 'em ("<Items><id>3</id><id>6</id><id>15</id></Items>")
			string itemIdsXml = Common.GetXmlStringOfIds(items);

			return ReportDA.UpdateCrimeLocation(this.ID, itemIdsXml, this.CrimeLocationDescription, modifiedBy);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public StatusInfo UpdateOffendersActions(string modifiedBy)
		{
			//merge all the Description items into one list
			List<DescriptionItem> items = this.OffendersActionsApproachItems;
			items.AddRange(this.OffendersActionsImpersonatedItems);
			items.AddRange(this.OffendersActionsWeaponItems);
			items.AddRange(this.OffendersActionsFirearmFeatureItems);
			items.AddRange(this.OffendersActionsPropertyCrimeItems);
			items.AddRange(this.OffendersActionsPersonCrimeItems);
			items.AddRange(this.OffendersActionsSexCrimeItems);

			//then produce an xmlstring of 'em ("<Items><id>3</id><id>6</id><id>15</id></Items>")
			//string itemIdsXml = Common.GetXmlStringOfIds(mergedIds, 0);
			string itemIdsXml = Common.GetXmlStringOfIds(items);

			return ReportDA.UpdateOffendersActions(this.ID, itemIdsXml, this.OffendersActionsDescription, modifiedBy);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public StatusInfo UpdateNarrative(string modifiedBy)
		{
			StatusInfo success;

			if (this.UserHasWritePermission(modifiedBy))
			{
				success = ReportDA.UpdateNarrative(this.ID, this.Narrative, modifiedBy);
			}
			else
			{
				success = new StatusInfo(false, "User does not have permissions to save report.");
			}

			return success;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public StatusInfo UpdatePropertyEvidence(string modifiedBy)
		{
			return ReportDA.UpdatePropertyEvidence(this.ID, this.PropertyEvidenceReceiptNumber, modifiedBy);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Sets supervisor-related fields on the Report. Also sets the ApprovalStatusTypeID -- approving or disapproving the report.
		/// </summary>
		/// <param name="previousApprovalStatusTypeID">The previous approval status. Used to determine if report is already Approved, in which case this supervisor-user cannot re-submit it.</param>
		public StatusInfo UpdateSupervisor(ApprovalStatusTypes previousApprovalStatusType, string modifiedBy)
		{
			//disallow submission if report was already approved
			if (previousApprovalStatusType == ApprovalStatusTypes.Approved)
			{
				return new StatusInfo(false, "Report was already approved. Reports cannot be changed after approval.");
			}
			else
			{
				StatusInfo status = ReportDA.UpdateSupervisor(this.ID,
															  this.SupervisorName,
															  this.SupervisorBadge,
															  this.SupervisorRankTypeID,
															  this.ApprovalStatusTypeID,
															  this.SupervisorComments,
															  modifiedBy);

				if (status.Success == true) //update successful
				{
					//if rejected, issue new notification to report author
					if (this.ApprovalStatusType == ApprovalStatusTypes.Rejected)
					{
						Notification notification = new Notification(Notification.Types.Report, this.ID, this.ItemNumber + " was rejected", "/event/BasicInfo.aspx?rid=" + this.ID, this.CreatedBy);
						notification.Send(modifiedBy);
					}
					//if approved, process gists, DTS, and alerts
					else if (this.ApprovalStatusType == ApprovalStatusTypes.Approved)
					{
						ArrayList errors = new ArrayList();

						#region TODO: process Gists

						

						#endregion
					
						#region process DTS and special alerts

						//if Production, and if Update was "Approval", then perform further actions (archive to DTS, etc)
						if (Common.GetApplicationEnvironment() == Enums.ApplicationEnvironments.Production)
						{							
							//archive to DTS
							status = ArchivePdf();

							if (status.Success != true)
								errors.Add("PDF could not be archived to DTS folder: " + status.Message);

						
							//send 67A NCIC alert
							if (this.Signal == "67A")
								status = SendNcicAlert();

							if (status.Success == false)
								errors.Add("NCIC 67A Alert failed: " + status.Message);
						}

						#endregion

						//if post-approval errors, return them
						if (errors.Count > 0)
							status = new StatusInfo(false, "Report Approval was successful, but there were errors", errors);
					}		
				}				

				return status;
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public StatusInfo UpdateApprovalStatus(string modifiedBy)
		{
			return ReportDA.UpdateApproval(this.ID, this.ApprovalStatusTypeID, modifiedBy);
		}
		
		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Updates the Incident Number only. User must have write-permission to the Report.
		/// </summary>
		/// <param name="modifiedBy">How we determine write-permission.</param>
		public StatusInfo UpdateItemNumber(string modifiedBy)
		{
			StatusInfo success;

			if (this.UserHasWritePermission(modifiedBy))
				success = ReportDA.UpdateItemNumber(this.ID, this.ItemNumber, modifiedBy);
			else
				success = new StatusInfo(false, "User does not have permissions to save report.");

			return success;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Validates business rules for a finished Report (required fields, etc).
		/// </summary>
		public StatusInfo ValidateReport()
		{
			ArrayList errors = new ArrayList();

			//EVENT - Basic Info section

			//EVENT - Admin section
			if (this.Officer1RankTypeID < 1)
				errors.Add("(Event > Admin) Officer Rank required");

			if (string.IsNullOrEmpty(this.Officer1Name))
				errors.Add("(Event > Admin) Officer Name required");

			if (this.Officer1Badge < 1)
				errors.Add("(Event > Admin) Officer Badge required");

			if (this.Officer1EmployeeID < 1)
				errors.Add("(Event > Admin) Employee # required");

			if (string.IsNullOrEmpty(this.ReportingCarPlatoon))
				errors.Add("(Event > Admin) Officer Platoon required");
			
			if (this.ReportingAssignmentUnitTypeID < 1)
				errors.Add("(Event > Admin) Officer Assignment required");

			//NOTE: even tho CarNumber is required in the UI, we arent requiring it here, because when an officer doesnt have a car
			//		number they just enter "Bxxxxx" (BadgeNumber). we dont really store that, but its what they are used to entering.


			//return status
			if (errors.Count > 0)
				return new StatusInfo(false, "This report has errors", errors);
			else
				return new StatusInfo(true);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Officer is finished, and submits report for Supervisor review.
		/// </summary>
		public StatusInfo SubmitReport(string modifiedBy)
		{
			return ReportDA.UpdateApproval(this.ID, (int)ApprovalStatusTypes.Pending, modifiedBy); //"Pending" for supervisor review is ID 3
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Saves a copy of the Report to the file system.
		/// </summary>
		/// <param name="templatePath">The absolute URL to the .ASPX templates required to build a PDF.  (Not defined internally in case other apps make use of this function)</param>
		/// <param name="path">The file location to save to.</param>
		public void SavePdf(string templatePath, string savePath, bool isOfficial, string watermarkText)
		{
			//Doc doc = GetPdfDoc(templatePath, isOfficial, watermarkText);
			//doc.Save(savePath);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Returns a byte array of the Report. Useful for client streaming or databasing.
		/// </summary>
		/// <param name="templatePath">The absolute URL to the .ASPX templates required to build a PDF. (Not defined internally in case other apps make use of this function)</param>
		public byte[] GetPdf(string templatePath, bool isOfficial, string watermarkText)
		{
			//TEMP: havent implemented this yet.
			//Doc doc = GetPdfDoc(templatePath, isOfficial, watermarkText);			
			//return doc.GetData();

			//get a dummy PDF from file system and load.
			// C:\ReportHqFiles\testReport.pdf

			return FileBuddy.GetFile(@"C:\ReportHqFiles\testReport.pdf");
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Determines whether the supplied User has write-priviledges to this Report.
		/// </summary>
		public bool UserHasWritePermission(string username)
		{
			bool returnValue = false;

			if (this.CreatedBy == username) //this-user must = originating-user
			{
				//user can only edit their report when its a certain status. (DataEntry reports can always be edited, as per nopd)
				switch (this.ApprovalStatusType)
				{
					case ApprovalStatusTypes.Incomplete:
					case ApprovalStatusTypes.Rejected:
					case ApprovalStatusTypes.DataEntry:
						returnValue = true;
						break;
				}
			}

			return returnValue;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Fills this Report instance w/ Property collection.
		/// </summary>
		public void FillProperty()
		{
			List<PropertyItem> property = new List<PropertyItem>();

			DataTable table = PropertyItem.GetPropertyByReport(this.ID);
			foreach (DataRow row in table.Rows)
				property.Add(new PropertyItem(row));

			this.Property = property;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Fills this Report instance w/ Offenders collection.
		/// </summary>
		public void FillOffenders()
		{
			List<Offender> offenders = new List<Offender>();

			DataTable table = Offender.GetOffendersByReport(this.ID);
			foreach (DataRow row in table.Rows)
				offenders.Add(new Offender(row, true));

			this.Offenders = offenders;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Public Static Methods

		public static DataTable GetVictimPersonsAndOffenders(int reportID)
		{
			return ReportDA.GetVictimPersonsAndOffenders(reportID);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetReportsByUsername(string username)
		{
			return ReportDA.GetReportsByUsername(username);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Gets Rejected, Incomplete, and Pending report tables.
		/// </summary>
		public static DataSet GetDashboardReportsByUsername(string username)
		{
			return ReportDA.GetDashboardReportsByUsername(username);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetDisapprovedReportsByUsername(string username)
		{
			return ReportDA.GetReportsByUsernameAndApprovalStatus(username, 2); //2 = Disapproved
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetDeletableReportsByUsername(string username)
		{
			return ReportDA.GetDeletableReportsByUsername(username);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetReportsByCriteria(string itemNumber,
													 int approvalStatusTypeID,
													 int district,
													 string platoon,
													 int startCarNumber,
													 int endCarNumber,
													 DateTime startDate,
													 DateTime endDate,
													 int badgeNumber)
		{
			DateTime emptyDate = new DateTime(1900, 1, 1);

			//if user entered startdate but no enddate, assume today is enddate
			if (startDate != emptyDate && endDate == emptyDate)
				endDate = DateTime.Now;

			//if user entered a start/end car number, but not the other, assume the max/min possible car number
			if (startCarNumber > 0 && endCarNumber < 1)
				endCarNumber = 11000;
			else if (startCarNumber < 1 && endCarNumber > 0)
				startCarNumber = 1; 

			return ReportDA.GetReportsByCriteria(itemNumber,
												 approvalStatusTypeID,
											     district,
												 platoon,
											     startCarNumber,
											     endCarNumber, 
												 startDate,
											     endDate,
												 badgeNumber);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetPlotReportsByCriteria(string itemNumber,
														 int district,
														 string zone,
														 string subzone,
														 string platoon,
														 int startCarNumber,
														 int endCarNumber,
														 DateTime startDate,
														 DateTime endDate,
														 int badgeNumber)
		{
			DateTime emptyDate = new DateTime(1900, 1, 1);

			//if user entered startdate but no enddate, assume today is enddate
			if (startDate != emptyDate && endDate == emptyDate)
				endDate = DateTime.Now;

			//if user entered a start/end car number, but not the other, assume the max/min possible car number
			if (startCarNumber > 0 && endCarNumber < 1)
				endCarNumber = 11000;
			else if (startCarNumber < 1 && endCarNumber > 0)
				startCarNumber = 1;

			return ReportDA.GetPlotReportsByCriteria(itemNumber,
													 district,
													 zone,
													 subzone,
													 platoon,
													 startCarNumber,
													 endCarNumber,
													 startDate,
													 endDate,
													 badgeNumber);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetReportTypes()
		{
			const string KEYNAME = "dtReportTypes";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in session, create
			{
				DataTable results = ReportDA.GetReportTypes();

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetReportStatusTypes()
		{
			const string KEYNAME = "dtReportStatusTypes";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in session, create
			{
				DataTable results = ReportDA.GetReportStatusTypes();

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetLightingTypes()
		{
			const string KEYNAME = "dtLightingTypes";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in session, create
			{
				DataTable results = ReportDA.GetLightingTypes();

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetCriminalActivityTypes()
		{
			const string KEYNAME = "dtCriminalActivityTypes";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in session, create
			{
				DataTable results = ReportDA.GetMotivationTypesByCategory(1);

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetMotiveTypes()
		{
			const string KEYNAME = "dtMotiveTypes";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in session, create
			{
				DataTable results = ReportDA.GetMotivationTypesByCategory(2);

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetTargetTypes()
		{
			const string KEYNAME = "dtTargetTypes";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in session, create
			{
				DataTable results = ReportDA.GetMotivationTypesByCategory(3);

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetMethodOfEntryTypes()
		{
			const string KEYNAME = "dtMethodOfEntryTypes";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in session, create
			{
				DataTable results = ReportDA.GetMeansOfEntryTypesByCategory(1);

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetPointOfEntryTypes()
		{
			const string KEYNAME = "dtPointOfEntryTypes";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in session, create
			{
				DataTable results = ReportDA.GetMeansOfEntryTypesByCategory(2);

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetSecurityTypes()
		{
			const string KEYNAME = "dtSecurityTypes";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in session, create
			{
				DataTable results = ReportDA.GetMeansOfEntryTypesByCategory(3);

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetResidentialLocationTypes()
		{
			const string KEYNAME = "dtResidentialLocationTypes";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in session, create
			{
				DataTable results = ReportDA.GetCrimeLocationTypesByCategory(1);

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetOutdoorLocationTypes()
		{
			const string KEYNAME = "dtOutdoorLocationTypes";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in session, create
			{
				DataTable results = ReportDA.GetCrimeLocationTypesByCategory(2);

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetCommericalLocationTypes()
		{
			const string KEYNAME = "dtCommericalLocationTypes";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in session, create
			{
				DataTable results = ReportDA.GetCrimeLocationTypesByCategory(3);

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetPublicLocationTypes()
		{
			const string KEYNAME = "dtPublicLocationTypes";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in session, create
			{
				DataTable results = ReportDA.GetCrimeLocationTypesByCategory(4);

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetMovableLocationTypes()
		{
			const string KEYNAME = "dtMovableLocationTypes";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in session, create
			{
				DataTable results = ReportDA.GetCrimeLocationTypesByCategory(5);

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetLocationStructureTypes()
		{
			const string KEYNAME = "dtLocationStructureTypes";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in session, create
			{
				DataTable results = ReportDA.GetCrimeLocationTypesByCategory(6);

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetLocationStructureStatusTypes()
		{
			const string KEYNAME = "dtLocationStructureStatusTypes";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in session, create
			{
				DataTable results = ReportDA.GetCrimeLocationTypesByCategory(7);

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetOffenderApproachTypes()
		{
			const string KEYNAME = "dtOffenderApproachTypes";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in session, create
			{
				DataTable results = ReportDA.GetOffenderActionTypesByCategory(1);

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetImpersonationTypes()
		{
			const string KEYNAME = "dtImpersonationTypes";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in session, create
			{
				DataTable results = ReportDA.GetOffenderActionTypesByCategory(2);

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetWeaponTypes()
		{
			const string KEYNAME = "dtWeaponTypes";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in session, create
			{
				DataTable results = ReportDA.GetOffenderActionTypesByCategory(3);

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetFirearmFeatureTypes()
		{
			const string KEYNAME = "dtFirearmFeatureTypes";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in session, create
			{
				DataTable results = ReportDA.GetOffenderActionTypesByCategory(4);

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetPropertyCrimeTypes()
		{
			const string KEYNAME = "dtPropertyCrimeTypes";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in session, create
			{
				DataTable results = ReportDA.GetOffenderActionTypesByCategory(5);

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetPersonCrimeTypes()
		{
			const string KEYNAME = "dtPersonCrimeTypes";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in session, create
			{
				DataTable results = ReportDA.GetOffenderActionTypesByCategory(6);

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetSexCrimeTypes()
		{
			const string KEYNAME = "dtSexCrimeTypes";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in session, create
			{
				DataTable results = ReportDA.GetOffenderActionTypesByCategory(7);

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetApprovalStatusTypes()
		{
			const string KEYNAME = "dtApprovalStatusTypes";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in session, create
			{
				DataTable results = ReportDA.GetApprovalStatusTypes();

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetPlatoonTypes()
		{
			const string KEYNAME = "dtPlatoonTypes";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in session, create
			{
				DataTable results = ReportDA.GetPlatoonTypes();

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetRankTypes()
		{
			const string KEYNAME = "dtRankTypes";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in session, create
			{
				DataTable results = ReportDA.GetRankTypes();

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataView GetSupervisorRankTypes()
		{
			const string KEYNAME = "dtRankTypes";

			Cache cache = HttpRuntime.Cache;
			
			if (cache[KEYNAME] == null) //not in session, create
			{
				DataTable results = ReportDA.GetRankTypes();

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//create a filtered view only supervisors. return it.
			DataView view = new DataView((DataTable)cache[KEYNAME], "IsSupervisor = 1", "", DataViewRowState.CurrentRows);

			//return table (which is already in cache or brand new)
			return view;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetAssignmentUnitTypes()
		{
			const string KEYNAME = "dtAssignmentUnitTypes";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in session, create
			{
				DataTable results = ReportDA.GetAssignmentUnitTypes();

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Sets a Report's Active flag to false, preventing it from being used by EPR. This method is used by Admins
		/// </summary>
		public static StatusInfo DeactivateReport(int reportID, string modifiedBy)
		{
			return ReportDA.DeactivateReport(reportID, modifiedBy);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Creates a new copy of the report PDF and drops to EPR-DTS export folder, where it gets picked up and archived into DTS. EPR-DTS service uses Now() for Year/Month folder names, and also ensures filename uniqueness. Report must be approved and environment Production.
		/// </summary>
		/// <returns></returns>
		public static StatusInfo SendToDts(int reportID)
		{
			StatusInfo status = null;

			Report report = new Report(reportID);

			//ensure is-production and report is approved
			if (Common.GetApplicationEnvironment() == Enums.ApplicationEnvironments.Production && report.ApprovalStatusType == ApprovalStatusTypes.Approved)
			{
				//resend to DTS
				status = report.ArchivePdf();
			}
			else
			{
				status = new StatusInfo(false, string.Format("Environment is not Production or report is not approved. Evironment: {0}. ApprovalStatusTypeID: {1} ", Common.GetApplicationEnvironment(), report.ApprovalStatusTypeID));
			}

			return status;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		
		#endregion

		#region Private Methods

		/// <summary>
		/// Filling this object for a specific Report, via constructor.
		/// </summary>
		private void FillObject(Report report, DataRow row)
		{
			//required
			report.ID = (int)row["ReportID"];
			report.ReportTypeID = (int)row["ReportTypeID"];
			report.ItemNumber = (string)row["ItemNumber"];
			report.ApprovalStatusTypeID = (int)row["ApprovalStatusTypeID"];
			report.ApprovalStatusType = (ApprovalStatusTypes)row["ApprovalStatusTypeID"];
			

			//facesheet - event
			if (row["SignalTypeID"] != DBNull.Value)
				report.SignalTypeID = (int)row["SignalTypeID"];

			if (row["Signal"] != DBNull.Value)
				report.Signal = (string)row["Signal"];

			if (row["IncidentDescription"] != DBNull.Value)
				report.IncidentDescription = (string)row["IncidentDescription"];

			if (row["Location"] != DBNull.Value)
				report.Location = (string)row["Location"];

			if (row["District"] != DBNull.Value)
				report.District = (int)row["District"];

			if (row["Zone"] != DBNull.Value)
				report.Zone = (string)row["Zone"];

			if (row["SubZone"] != DBNull.Value)
				report.SubZone = (string)row["SubZone"];

			if (row["OccurredDate"] != DBNull.Value)
				report.OccurredDateTime = (DateTime)row["OccurredDate"];

			if (row["ReportDate"] != DBNull.Value)
				report.ReportDateTime = (DateTime)row["ReportDate"];

			if (row["LightingConditionTypeID"] != DBNull.Value)
				report.LightingConditionTypeID = (int)row["LightingConditionTypeID"];

			if (row["LightingCondition"] != DBNull.Value)
				report.LightingCondition = (string)row["LightingCondition"];

			if (row["ReportStatusTypeID"] != DBNull.Value)
				report.StatusTypeID = (int)row["ReportStatusTypeID"];

			if (row["Status"] != DBNull.Value)
				report.Status = (string)row["Status"];

			if (row["BulletinRequired"] != DBNull.Value)
				report.BulletinRequired = (bool)row["BulletinRequired"];

			if (row["BulletinNumber"] != DBNull.Value)
				report.BulletinNumber = (int)row["BulletinNumber"];
			
			
			//UR
			if (row["UcrCategoryID"] != DBNull.Value)
				report.UcrCategoryID = (int)row["UcrCategoryID"];

			if (row["UcrCategory"] != DBNull.Value)
				report.UcrCategory = (string)row["UcrCategory"];

			if (row["UcrOffenseTypeID"] != DBNull.Value)
				report.UcrOffenseTypeID = (int)row["UcrOffenseTypeID"];

			if (row["UcrOffense"] != DBNull.Value)
				report.UcrOffense = (string)row["UcrOffense"];


			//facesheet - admin
			if (row["Officer1EmployeeID"] != DBNull.Value)
				report.Officer1EmployeeID = (int)row["Officer1EmployeeID"];

			if (row["Officer1Name"] != DBNull.Value)
				report.Officer1Name = (string)row["Officer1Name"];

			if (row["Officer1Badge"] != DBNull.Value)
				report.Officer1Badge = (int)row["Officer1Badge"];

			if (row["Officer1RankTypeID"] != DBNull.Value)
				report.Officer1RankTypeID = (int)row["Officer1RankTypeID"];

			if (row["Officer2Name"] != DBNull.Value)
				report.Officer2Name = (string)row["Officer2Name"];

			if (row["Officer2Badge"] != DBNull.Value)
				report.Officer2Badge = (int)row["Officer2Badge"];

			if (row["Officer2RankTypeID"] != DBNull.Value)
				report.Officer2RankTypeID = (int)row["Officer2RankTypeID"];

			if (row["ReportingCarNumber"] != DBNull.Value)
				report.ReportingCarNumber = (int)row["ReportingCarNumber"];

			if (row["ReportingCarPlatoon"] != DBNull.Value)
				report.ReportingCarPlatoon = (string)row["ReportingCarPlatoon"];

			if (row["ReportingAssignmentUnitTypeID"] != DBNull.Value)
				report.ReportingAssignmentUnitTypeID = (int)row["ReportingAssignmentUnitTypeID"];

			if (row["DetectiveName"] != DBNull.Value)
				report.DetectiveName = (string)row["DetectiveName"];

			if (row["CrimeLabname"] != DBNull.Value)
				report.CrimeLabName = (string)row["CrimeLabname"];

			if (row["OtherName"] != DBNull.Value)
				report.OtherName = (string)row["OtherName"];
						

			//descriptions
			if (row["Narrative"] != DBNull.Value)
				report.Narrative = (string)row["Narrative"];

			if (row["MotivationDescription"] != DBNull.Value)
				report.MotivationDescription = (string)row["MotivationDescription"];

			if (row["MeansOfEntryDescription"] != DBNull.Value)
				report.MeansOfEntryDescription = (string)row["MeansOfEntryDescription"];

			if (row["CrimeLocationDescription"] != DBNull.Value)
				report.CrimeLocationDescription = (string)row["CrimeLocationDescription"];

			if (row["OffendersActionsDescription"] != DBNull.Value)
				report.OffendersActionsDescription = (string)row["OffendersActionsDescription"];


			//prop/ev receipt
			if (row["PropertyEvidenceReceiptNumber"] != DBNull.Value)
				report.PropertyEvidenceReceiptNumber = (string)row["PropertyEvidenceReceiptNumber"];


			//supervisor
			if (row["SupervisorName"] != DBNull.Value)
				report.SupervisorName = (string)row["SupervisorName"];

			if (row["SupervisorBadge"] != DBNull.Value)
				report.SupervisorBadge = (int)row["SupervisorBadge"];

			if (row["SupervisorRankTypeID"] != DBNull.Value)
				report.SupervisorRankTypeID = (int)row["SupervisorRankTypeID"];

			if (row["SupervisorComments"] != DBNull.Value)
				report.SupervisorComments = (string)row["SupervisorComments"];

			if (row["SupervisorLastModifiedDate"] != DBNull.Value)
				report.SupervisorLastModifiedDate = (DateTime)row["SupervisorLastModifiedDate"];


			//boiler plate
			if (row["CreatedBy"] != DBNull.Value)
				report.CreatedBy = (string)row["CreatedBy"];

			if (row["CreatedDate"] != DBNull.Value)
				report.CreatedDate = (DateTime)row["CreatedDate"];

			if (row["LastModifiedBy"] != DBNull.Value)
				report.LastModifiedBy = (string)row["LastModifiedBy"];

			if (row["LastModifiedDate"] != DBNull.Value)
				report.LastModifiedDate = (DateTime)row["LastModifiedDate"];

			if (row["IsActive"] != DBNull.Value)
				report.IsActive = (bool)row["IsActive"];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Fills all the auxillary collections in a Report (VictimPersons, Offenders, etc..) NOTE: gradually replacing with helper methods since i need them elsewhere.
		/// </summary>
		private void FillCollections()
		{
			int reportID = this.ID;


			//victim-persons
			List<VictimPerson> victimPersons = new List<VictimPerson>();

			DataTable table = VictimPerson.GetVictimPersonsByReport(reportID);
			foreach (DataRow row in table.Rows)
				victimPersons.Add(new VictimPerson(row));

			this.VictimPersons = victimPersons;


			//offenders
			FillOffenders();


			//vehicles
			List<Vehicle> vehicles = new List<Vehicle>();

			table = Vehicle.GetVehiclesByReport(reportID);
			foreach (DataRow row in table.Rows)
				vehicles.Add(new Vehicle(row));

			this.Vehicles = vehicles;


			//weapons
			List<Weapon> weapons = new List<Weapon>();

			table = Weapon.GetWeaponsByReport(reportID);
			foreach (DataRow row in table.Rows)
				weapons.Add(new Weapon(row));

			this.Weapons = weapons;


			//property
			FillProperty();


			//evidence
			List<EvidenceItem> evidence = new List<EvidenceItem>();

			table = EvidenceItem.GetEvidenceByReport(reportID);
			foreach (DataRow row in table.Rows)
				evidence.Add(new EvidenceItem(row));

			this.Evidence = evidence;			
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		private List<DescriptionItem> GetMotivationsByCategory(int categoryID)
		{
			List<DescriptionItem> items = new List<DescriptionItem>();

			DescriptionItem newItem;
			foreach (DataRow row in ReportDA.GetReportMotivationsByCategory(this.ID, categoryID).Rows)
			{
				newItem = new DescriptionItem();
				newItem.ID = (int)row["MotivationTypeID"];
				newItem.Description = (string)row["Description"];

				items.Add(newItem);
			}

			return items;

			//1 = Criminal Activity
			//2	= Motive
			//3 = Targets
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		private List<DescriptionItem> GetMeansOfEntryByCategory(int categoryID)
		{
			List<DescriptionItem> items = new List<DescriptionItem>();

			DescriptionItem newItem;
			foreach (DataRow row in ReportDA.GetReportMeansOfEntryByCategory(this.ID, categoryID).Rows)
			{
				newItem = new DescriptionItem();
				newItem.ID = (int)row["MeansOfEntryTypeID"];
				newItem.Description = (string)row["Description"];

				items.Add(newItem);
			}

			return items;

			//1	= Method of Entry
			//2 = Point of Entry/Exit
			//3	= Security Used/Defeated
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// This is like calling GetMeansOfEntryByCategory(3), but we need to return type List<SecurityUsedDescriptionItem>.
		/// </summary>
		private List<SecurityUsedDescriptionItem> GetMeansOfEntrySecurityUsed()
		{
			List<SecurityUsedDescriptionItem> items = new List<SecurityUsedDescriptionItem>();

			SecurityUsedDescriptionItem newItem;
			foreach (DataRow row in ReportDA.GetSecurityTypesUsedByReport(this.ID).Rows)
			{
				newItem = new SecurityUsedDescriptionItem();
				newItem.ID = (int)row["MeansOfEntryTypeID"];
				newItem.MainframeCode = (string)row["MainframeCode"];
				newItem.Description = (string)row["Description"];
				newItem.WasDefeated = (bool)row["WasDefeated"];

				items.Add(newItem);
			}

			return items;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		private List<DescriptionItem> GetCrimeLocationsByCategory(int categoryID)
		{
			List<DescriptionItem> items = new List<DescriptionItem>();

			DescriptionItem newItem;
			foreach (DataRow row in ReportDA.GetReportCrimeLocationsByCategory(this.ID, categoryID).Rows)
			{
				newItem = new DescriptionItem();
				newItem.ID = (int)row["CrimeLocationTypeID"];
				newItem.Description = (string)row["Description"];

				items.Add(newItem);
			}

			return items;

			//1	= Residential
			//2	= Outdoor Area
			//3	= Commercial Establishment
			//4	= Public Access Area
			//5	= Movable
			//6	= Structure Type
			//7	= Structure Status
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		private List<DescriptionItem> GetOffenderActionsByCategory(int categoryID)
		{
			List<DescriptionItem> items = new List<DescriptionItem>();

			DescriptionItem newItem;
			foreach (DataRow row in ReportDA.GetReportOffenderActionsByCategory(this.ID, categoryID).Rows)
			{
				newItem = new DescriptionItem();
				newItem.ID = (int)row["OffenderActionTypeID"];
				newItem.Description = (string)row["Description"];

				items.Add(newItem);
			}

			return items;

			//1	= Offenders Approach to Victim
			//2	= Impersonated
			//3	= Weapon
			//4	= Firearm Features
			//5	= Property Crimes
			//6	= Person Crimes
			//7	= Sex Crime Specific
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Builds an xml-string of ID values w/ Defeated status. Used exclusively by Means Of Entry's "Security Used/Defeated" section.
		/// </summary>
		private string GetXmlStringOfSecurityUsedIds(DataTable table)
		{
			StringBuilder sb = new StringBuilder(200);

			sb.Append("<Items>");

			foreach (DataRow row in table.Rows)
			{
				sb.Append(String.Format(@"<id wasDefeated=""{0}"">", row[1].ToString()));
				sb.Append(row[0].ToString());
				sb.Append("</id>");
			}

			sb.Append("</Items>");

			return sb.ToString();
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Builds an xml-string of ID values. Used by our screens that insert multiple checkbox values into db.
		/// </summary>
		public static string GetXmlStringOfSecurityUsedIds(List<SecurityUsedDescriptionItem> items)
		{
			StringBuilder sb = new StringBuilder(200);

			sb.Append("<Items>");

			foreach (SecurityUsedDescriptionItem item in items)
			{
				sb.Append(String.Format(@"<id wasDefeated=""{0}"">", item.WasDefeated.ToString()));
				sb.Append(item.ID.ToString());
				sb.Append("</id>");
			}

			sb.Append("</Items>");

			return sb.ToString();
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		
		private string GetRankByID(int rankTypeID)
		{
			DataTable dtRankTypes = GetRankTypes();

			string filter = String.Format("(RankTypeID = {0})", rankTypeID);

			//do filter
			DataView view = new DataView(dtRankTypes, filter, string.Empty, DataViewRowState.OriginalRows);

			string returnValue = string.Empty;

			if (view.Count == 1)
				returnValue = (string)view[0]["Description"];
			//else
			//	returnValue = "Error";

			return returnValue;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Gets an ABCpdf Doc for the Report. This can be streamed or saved to a PDF, XPS, or SWF file.
		/// </summary>
		//private Doc GetPdfDoc(string templatePath, bool isOfficial, string watermarkText)
		//{
		//	//our layout templates
		//	string reportUrl = templatePath + "ReportHtml.aspx?rid=" + this.ID;
		//	string page1FooterUrl = reportUrl.Replace("ReportHtml.aspx", "ReportPage1Footer.aspx");

		//	Doc doc = new Doc();

		//	#region settings

		//	//doc.AddGrid(); //grid for aiding dev/layout

		//	doc.HtmlOptions.PageCacheEnabled = false; //dont left ABCpdf cache urls
		//	doc.HtmlOptions.HideBackground = true; //make our HTML transparent

		//	//we need a login for the component to use when trying to hit the url, since our site is locked down.
		//	doc.HtmlOptions.LogonName = "eprweb";
		//	doc.HtmlOptions.LogonPassword = "3prW3bS3rv1c3";

		//	//lock it down (as much as PDF spec allows; note that its not bullet-proof)
		//	doc.Encryption.Type = 2;
		//	doc.Encryption.CanChange = false;
		//	doc.Encryption.CanEdit = false;
		//	doc.Encryption.OwnerPassword = "MOT99";

		//	#endregion

		//	#region content

		//	//ABCpdf uses points for its size units; there are 72 points in an inch.
		//	//default document size is 612x792 points (an 8.5x11)

		//	doc.Rect.Position(40, 130); //from left-bottom. so 40 over, 130 up -- because first page leaves lots of room for Page1-footer.
		//	doc.Rect.Width = 532; //612-40-40 //(total, left-margin, right-margin)
		//	doc.Rect.Height = 622; //792-40-40-80 (total, bottom-margin, top-margin, and 80 for custom Page1-footer)

		//	//doc.Color.String = "0 255 0"; //green
		//	//doc.FrameRect();

		//	//pass in url's HTML
		//	int pageID = doc.AddImageUrl(reportUrl);

		//	//set non-Page1 pages to normal dimensions
		//	doc.Rect.Position(40, 40); //from left-bottom. so 40 over, 40 up
		//	doc.Rect.Height = 712; //792-40-40 (total, bottom-margin, top-margin)

		//	//chain multiple content pages into one doc
		//	while (doc.Chainable(pageID))
		//	{
		//		doc.Page = doc.AddPage();
		//		pageID = doc.AddImageToChain(pageID);

		//		//doc.FrameRect();
		//	}

		//	#endregion

		//	int pageCount = doc.PageCount;

		//	#region header

		//	doc.Rect.Position(40, 760); //from left-bottom. so 40 over, 760 up (40+712+8)
		//	doc.Rect.Width = 532; //612-40-40 //(total, left-margin, right-margin)
		//	doc.Rect.Height = 20;

		//	StringBuilder sb = new StringBuilder(200);

		//	sb.Append("<html><body>");
		//	sb.Append(@"<style type=""text/css"">body {margin:0px; padding:0px;} td {font-size:15px;}</style>");
		//	sb.Append(String.Format(@"<table cellpadding=""0"" cellspacing=""0"" border=""0"" width=""100%""><tr><td width=""33%"">{0:g}</td><td width=""33%"" align=""center"">{1}</td><td width=""33%"" align=""right"">Page 1 of {2}</td></tr></table>", DateTime.Now, this.ItemNumber, pageCount));
		//	sb.Append("</body></html>");

		//	//string headerHtml = String.Format(@"<table cellpadding=""0"" cellspacing=""0"" border=""0"" width=""100%""><tr><td width=""33%"">{0:g}</td><td width=""33%"" align=""center"">{1}</td><td width=""33%"" align=""right"">Page 1 of {2}</td></tr></table>", DateTime.Now, this.ItemNumber, pageCount);

		//	string headerHtml = sb.ToString();

		//	//add header to pages
		//	for (int i = 1; i <= pageCount; i++)
		//	{
		//		//replace previous Page # w/ current.
		//		if (i > 1)
		//		{
		//			headerHtml = headerHtml.Replace(String.Format("Page {0}", i - 1), "Page " + i.ToString());
		//		}

		//		doc.PageNumber = i;
		//		doc.AddImageHtml(headerHtml);

		//		//doc.Color.String = "255 0 0"; //red
		//		//doc.FrameRect();
		//	}

		//	#endregion

		//	#region footer

		//	doc.Rect.Position(40, 20); //from left-bottom. so 40 over, 1 up (40+712+8)
		//	doc.Rect.Width = 532; //612-40-40 //(total, left-margin, right-margin..using a little more space on right)
		//	doc.Rect.Height = 90;

		//	doc.PageNumber = 1; //footer goes on Page 1 only
		//	doc.AddImageUrl(page1FooterUrl);

		//	//doc.Color.String = "0 0 255"; //blue
		//	//doc.FrameRect();

		//	#endregion

		//	#region watermark & flatten

		//	doc.Layer = doc.LayerCount + 1;

		//	if (isOfficial)
		//	{
		//		#region official seal

		//		doc.Rect.Position(80, 160); //from left-bottom. so 40 over, 40 up
		//		doc.Rect.Width = 440; //image's actual size
		//		doc.Rect.Height = 443; //image's actual size

		//		System.Drawing.Bitmap bitmap = new System.Drawing.Bitmap(HttpContext.Current.Server.MapPath("~/images/pdfWatermark.gif"));

		//		for (int i = 1; i <= pageCount; i++)
		//		{
		//			doc.PageNumber = i;

		//			doc.AddImageBitmap(bitmap, false);

		//			doc.Flatten(); //removes extra layers, decreasing file size
		//		}

		//		bitmap.Dispose();

		//		#endregion
		//	}
		//	else
		//	{
		//		# region use text

		//		doc.Rect.Position(40, 40); //from left-bottom. so 40 over, 40 up
		//		doc.Rect.Width = 532; //612 - 40 - 40
		//		doc.Rect.Height = 712;
		//		doc.HPos = 0.5; //center
		//		doc.VPos = 0.5; //center

		//		//when set below 255 Adobe Reader says: "An error exists on this page...". so using a light gray.
		//		//and the new text is not visible.
		//		//doc.Color.Alpha = 20;

		//		doc.FontSize = 60;
		//		doc.TextStyle.Bold = true;
		//		doc.Color.String = "204 204 204";
		//		doc.Transform.Rotate(45, 302, 396); //what do the 2nd & 3rd params do?

		//		if (!Strings.Exists(watermarkText))
		//			watermarkText = "LAW ENFORCEMENT USE ONLY";

		//		for (int i = 1; i <= pageCount; i++)
		//		{
		//			doc.PageNumber = i;

		//			doc.AddText(watermarkText);

		//			doc.Flatten(); //removes extra layers, decreasing file size
		//		}

		//		#endregion
		//	}

		//	#endregion

		//	return doc;
		//}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Archives an approved PDF. This is done by saving a PDF of report to the server's "outgoing" folder, where it can be picked up by the an exporter service to process accordingly.
		/// </summary>
		private StatusInfo ArchivePdf()
		{
			string folder = ConfigurationManager.AppSettings["ExportPath"];
			string fileName = String.Format("{0}_{1}.pdf", this.ItemNumber, this.ID); //"A-66666-09_3411.pdf"
			string fullPath = Path.Combine(folder, fileName);

			#region ensure filename uniqueness

			//check for existing filename. if exists, modify filename w/ a +1 until unique.
			int i = 1;
			while (File.Exists(fullPath))
			{
				//create unique file: "X-XXXXX-XX_YYYY_Z.pdf" note the _Z.pdf will be unique
				string appendedFileName = String.Format(Path.GetFileNameWithoutExtension(fileName) + "_{0}.pdf", i);

				fullPath = Path.Combine(folder, appendedFileName);

				i++;
			}

			#endregion

			StatusInfo status = new StatusInfo();

			try
			{
				//copy approved gist to the outgoing folder
				this.SavePdf(Common.GetPrintReportTemplateUrl(), fullPath, true, string.Empty);

				status.Success = true;
			}
			catch (Exception ex)
			{
				//if archive fails send back error

				status.Success = false;
				status.Message = ex.Message;
			}

			return status;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		private StatusInfo SendNcicAlert()
		{
			System.Text.StringBuilder sb = new System.Text.StringBuilder(500);
			
			sb.Append("<span style='font:12px verdana'>");
			sb.Append("A new Signal 67A report has been filed and approved. Details:<br/><br/>");
			sb.Append("<b>Item Number:</b> " + this.ItemNumber + "<br/><br/>");
			sb.Append("<b>Officer 1:</b> " + this.Officer1Name + "<br/><br/><br/>");

			HttpContext context = HttpContext.Current;

			string serverUrl = context.Request.Url.GetLeftPart(UriPartial.Authority); // "http://someUrl"
			string appUrl = context.Request.ApplicationPath; // "/appFolder" or "/"
			string pageUrl = (appUrl + "/print/printReport.aspx?rid=" + this.ID).Replace("//", "/");
			
			sb.Append("<b><a href='" + serverUrl + pageUrl + "'>View Report PDF</a><br/><br/>");
			sb.Append("</span>");

			return Email.SendEmail("someRecipient@someDomain.com", ConfigurationManager.AppSettings["NcicContact"], "New 67A Report", sb.ToString(), true);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}
