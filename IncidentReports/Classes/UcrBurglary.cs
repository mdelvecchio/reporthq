using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Caching;

using ReportHq.Common;
using ReportHq.IncidentReports.DataAccess;

namespace ReportHq.IncidentReports
{
	/// <summary>
	/// UCR Burglary offense details. NOTE: not sure yet whether to use these individual classes, or just use one big-ass UCR offense that has a bunch of props w/ most null or a given report.
	/// </summary>
	public class UcrBurglary : Bases.UcrOffense
	{
		#region Properties

		public String TimeOfDay { get; set; }

		public String CrimeLocation { get; set; }

		public String ForceUsed { get; set; }

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Constructors

		//TODO: make a construction that takes in reportID, goes out to db, and returns a populated instance

		public UcrBurglary()
		{
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Public Methods

		/// <summary>
		/// Update a UCR Burglary offense.
		/// </summary>
		public StatusInfo Update(int reportID, string modifiedBy)
		{			
			return UcrDA.UpdateBurglary(reportID,
										this.TimeOfDay,
										this.CrimeLocation,
										this.ForceUsed,
										modifiedBy);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
				
		#region Private Methods



		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}
