using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.Caching;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using ReportHq.Common;
using ReportHq.IncidentReports.DataAccess;

namespace ReportHq.IncidentReports
{
	public class Vehicle
	{
		#region Properties

		private int _ID;
		private int _reportID;
		private int _statusTypeID;
		private string _status;
		private int _year;
		private int _makeTypeID;
		private string _make;
		private int _modelTypeID;
		private string _model;
		private int _colorTypeID;
		private string _color;
		private int _styleTypeID;
		private string _style;
		private int _value;
		private string _licensePlate;
		private string _licensePlateState;
		private int _licensePlateYear;
		private string _vin;
		private int _recoveredTypeID;
		private string _recoveredType;
		private string _recoveryLocation;
		private int _recoveryDistrict;
		private string _recoveryZone;
		private string _recoverySubZone;
		private string _additionalDescription;
		private int _damageLevelTypeID;
		private string _damageLevel;
		private List<DescriptionItem> _damageZoneItems;
		private List<DescriptionItem> _recoveredVehicleMoItems;

		public int ID
		{
			set { _ID = value; }
			get { return _ID; }
		}

		public int ReportID
		{
			set { _reportID = value; }
			get { return _reportID; }
		}

		public int StatusTypeID
		{
			set { _statusTypeID = value; }
			get { return _statusTypeID; }
		}

		public string Status
		{
			set { _status = value; }
			get { return _status; }
		}

		public int Year
		{
			set { _year = value; }
			get { return _year; }
		}

		public int MakeTypeID
		{
			set { _makeTypeID = value; }
			get { return _makeTypeID; }
		}

		public string Make
		{
			set { _make = value; }
			get { return _make; }
		}

		public int ModelTypeID
		{
			set { _modelTypeID = value; }
			get { return _modelTypeID; }
		}

		public string Model
		{
			set { _model = value; }
			get { return _model; }
		}

		public int ColorTypeID
		{
			set { _colorTypeID = value; }
			get { return _colorTypeID; }
		}

		public string Color
		{
			set { _color = value; }
			get { return _color; }
		}

		public int StyleTypeID
		{
			set { _styleTypeID = value; }
			get { return _styleTypeID; }
		}

		public string Style
		{
			set { _style = value; }
			get { return _style; }
		}

		public int Value
		{
			set { _value = value; }
			get { return _value; }
		}

		public string LicensePlate
		{
			set { _licensePlate = value; }
			get { return _licensePlate; }
		}

		public string LicensePlateState
		{
			set { _licensePlateState = value; }
			get { return _licensePlateState; }
		}

		public int LicensePlateYear
		{
			set { _licensePlateYear = value; }
			get { return _licensePlateYear; }
		}

		public string Vin
		{
			set { _vin = value; }
			get { return _vin; }
		}

		public int RecoveredTypeID
		{
			set { _recoveredTypeID = value; }
			get { return _recoveredTypeID; }
		}

		public string RecoveredType
		{
			set { _recoveredType = value; }
			get { return _recoveredType; }
		}

		public string RecoveryLocation
		{
			set { _recoveryLocation = value; }
			get { return _recoveryLocation; }
		}

		public int RecoveryDistrict
		{
			set { _recoveryDistrict = value; }
			get { return _recoveryDistrict; }
		}

		public string RecoveryZone
		{
			set { _recoveryZone = value; }
			get { return _recoveryZone; }
		}

		public string RecoverySubZone
		{
			set { _recoverySubZone = value; }
			get { return _recoverySubZone; }
		}

		public string AdditionalDescription
		{
			set { _additionalDescription = value; }
			get { return _additionalDescription; }
		}

		public int DamageLevelTypeID
		{
			set { _damageLevelTypeID = value; }
			get { return _damageLevelTypeID; }
		}

		public string DamageLevel
		{
			set { _damageLevel = value; }
			get { return _damageLevel; }
		}

		public List<DescriptionItem> DamageZoneItems
		{
			get
			{
				if (_damageZoneItems == null)
					return GetDamageZonesByVehicle(this.ID);
				else
					return _damageZoneItems;
			}
			set { _damageZoneItems = value; }
		}

		public List<DescriptionItem> RecoveredVehicleMoItems
		{
			get
			{
				if (_recoveredVehicleMoItems == null)
					return GetRecoveredVehicleMosByVehicle(this.ID);
				else
					return _recoveredVehicleMoItems;
			}
			set { _recoveredVehicleMoItems = value; }
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Constructors

		/// <summary>
		/// Use this when filling a new Vehicle from values collected from a Grid. (ex: an insert operation)
		/// </summary>
		public Vehicle(Hashtable newValues)
		{
			this.StatusTypeID = (int)newValues["StatusTypeID"];
			this.Year = (int)newValues["Year"];
			this.MakeTypeID = (int)newValues["MakeTypeID"];
			this.ModelTypeID = (int)newValues["ModelTypeID"];
			this.ColorTypeID = (int)newValues["ColorTypeID"];
			this.StyleTypeID = (int)newValues["StyleTypeID"];
			this.Value = (int)newValues["Value"];
			this.LicensePlate = (string)newValues["LicensePlate"];
			this.LicensePlateState = (string)newValues["LicensePlateState"];
			this.LicensePlateYear = (int)newValues["LicensePlateYear"];
			this.Vin = (string)newValues["Vin"];
			this.RecoveredTypeID = (int)newValues["RecoveredTypeID"];
			this.RecoveryLocation = (string)newValues["RecoveryLocation"];
			this.RecoveryDistrict = (int)newValues["RecoveryDistrict"];
			this.RecoveryZone = (string)newValues["RecoveryZone"];
			this.RecoverySubZone = (string)newValues["RecoverySubZone"];
			this.AdditionalDescription = (string)newValues["AdditionalDescription"];
			this.DamageLevelTypeID = (int)newValues["DamageLevelTypeID"];
			this.DamageZoneItems = (List<DescriptionItem>)newValues["DamageZonesTypes"];
			this.RecoveredVehicleMoItems = (List<DescriptionItem>)newValues["RecoveredVehicleMoTypes"];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Use this when filling a new Vehicle from datarow. (ex: record from db)
		/// </summary>
		public Vehicle(DataRow row)
		{
			FillObject(this, row);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Public Methods

		public StatusInfo Add(string createdBy)
		{
			return VehicleDA.InsertVehicle(this.ReportID,
										   this.StatusTypeID,
										   this.Year,
										   this.MakeTypeID,
										   this.ModelTypeID,
										   this.ColorTypeID,
										   this.StyleTypeID,
										   this.Value,
										   this.LicensePlate,
										   this.LicensePlateState,
										   this.LicensePlateYear,
										   this.Vin,
										   this.RecoveredTypeID,
										   this.RecoveryLocation,
										   this.RecoveryDistrict,
										   this.RecoveryZone,
										   this.RecoverySubZone,
										   this.AdditionalDescription,
										   this.DamageLevelTypeID,
										   Common.GetXmlStringOfIds(this.DamageZoneItems),
										   Common.GetXmlStringOfIds(this.RecoveredVehicleMoItems),
										   createdBy);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public StatusInfo Update(string modifiedBy)
		{
			return VehicleDA.UpdateVehicle(this.ID,
										   this.StatusTypeID,
										   this.Year,
										   this.MakeTypeID,
										   this.ModelTypeID,
										   this.ColorTypeID,
										   this.StyleTypeID,
										   this.Value,
										   this.LicensePlate,
										   this.LicensePlateState,
										   this.LicensePlateYear,
										   this.Vin,
										   this.RecoveredTypeID,
										   this.RecoveryLocation,
										   this.RecoveryDistrict,
										   this.RecoveryZone,
										   this.RecoverySubZone,
										   this.AdditionalDescription,
										   this.DamageLevelTypeID,
										   Common.GetXmlStringOfIds(this.DamageZoneItems),
										   Common.GetXmlStringOfIds(this.RecoveredVehicleMoItems),
										   modifiedBy);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Public Static Methods

		public static StatusInfo DeleteVehicle(int vehicleID)
		{
			return VehicleDA.DeleteVehicle(vehicleID);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetVehiclesByReport(int reportID)
		{
			return VehicleDA.GetVehiclesByReport(reportID);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetVehicleStatusTypes()
		{
			const string KEYNAME = "dtVehicleStatusTypes";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in session, create
			{
				DataTable results = VehicleDA.GetVehicleStatusTypes();

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetVehicleRecoveredTypes()
		{
			const string KEYNAME = "dtVehicleRecoveredTypes";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in session, create
			{
				DataTable results = VehicleDA.GetVehicleRecoveredTypes();

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetVehicleDamageLevelTypes()
		{
			const string KEYNAME = "dtVehicleDamageLevelTypes";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in session, create
			{
				DataTable results = VehicleDA.GetVehicleDamageLevelTypes();

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetVehicleDamageZoneTypes()
		{
			const string KEYNAME = "dtVehicleDamageZoneTypes";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in session, create
			{
				DataTable results = VehicleDA.GetVehicleDamageZoneTypes();

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetRecoveredVehicleMoTypes()
		{
			const string KEYNAME = "dtRecoveredVehicleMoTypes";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in session, create
			{
				DataTable results = VehicleDA.GetRecoveredVehicleMoTypes();

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static List<DescriptionItem> GetDamageZonesByVehicle(int vehicleID)
		{
			List<DescriptionItem> items = new List<DescriptionItem>();

			DescriptionItem newItem;
			foreach (DataRow row in VehicleDA.GetDamageZonesByVehicle(vehicleID).Rows)
			{
				newItem = new DescriptionItem();
				newItem.ID = (int)row["VehicleDamageZoneTypeID"];
			
				items.Add(newItem);
			}

			return items;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static List<DescriptionItem> GetRecoveredVehicleMosByVehicle(int vehicleID)
		{
			List<DescriptionItem> items = new List<DescriptionItem>();

			DescriptionItem newItem;
			foreach (DataRow row in VehicleDA.GetRecoveredVehicleMosByVehicle(vehicleID).Rows)
			{
				newItem = new DescriptionItem();
				newItem.ID = (int)row["RecoveredVehicleMoTypeID"];
				newItem.Description = (string)row["Description"];

				items.Add(newItem);
			}

			return items;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		
		#endregion

		#region Private Methods

		/// <summary>
		/// Fills this object for a specific Vehicle, via constructor.
		/// </summary>
		private void FillObject(Vehicle vehicle, DataRow row)
		{
			//required
			vehicle.ID = (int)row["VehicleID"];
			vehicle.ReportID = (int)row["ReportID"];
			vehicle.StatusTypeID = (int)row["VehicleStatusTypeID"];
			vehicle.Status = (string)row["Status"];

			if (row["Year"] != DBNull.Value)
				vehicle.Year = (int)row["Year"];

			if (row["VehicleMakeTypeID"] != DBNull.Value)
				vehicle.MakeTypeID = (int)row["VehicleMakeTypeID"];

			if (row["Make"] != DBNull.Value)
				vehicle.Make = (string)row["Make"];

			if (row["VehicleModelTypeID"] != DBNull.Value)
				vehicle.ModelTypeID = (int)row["VehicleModelTypeID"];

			if (row["Model"] != DBNull.Value)
				vehicle.Model = (string)row["Model"];

			if (row["VehicleColorTypeID"] != DBNull.Value)
				vehicle.ColorTypeID = (int)row["VehicleColorTypeID"];

			if (row["Color"] != DBNull.Value)
				vehicle.Color = (string)row["Color"];

			if (row["VehicleStyleTypeID"] != DBNull.Value)
				vehicle.StyleTypeID = (int)row["VehicleStyleTypeID"];

			if (row["Style"] != DBNull.Value)
				vehicle.Style = (string)row["Style"];

			if (row["Value"] != DBNull.Value)
				vehicle.Value = (int)row["Value"];

			if (row["LicensePlate"] != DBNull.Value)
				vehicle.LicensePlate = (string)row["LicensePlate"];

			if (row["LicensePlateState"] != DBNull.Value)
				vehicle.LicensePlateState = (string)row["LicensePlateState"];

			if (row["LicensePlateYear"] != DBNull.Value)
				vehicle.LicensePlateYear = (int)row["LicensePlateYear"];

			if (row["VIN"] != DBNull.Value)
				vehicle.Vin = (string)row["VIN"];

			if (row["VehicleRecoveredTypeID"] != DBNull.Value)
				vehicle.RecoveredTypeID = (int)row["VehicleRecoveredTypeID"];

			if (row["RecoveredType"] != DBNull.Value)
				vehicle.RecoveredType = (string)row["RecoveredType"];

			if (row["RecoveryLocation"] != DBNull.Value)
				vehicle.RecoveryLocation = (string)row["RecoveryLocation"];

			if (row["RecoveryDistrict"] != DBNull.Value)
				vehicle.RecoveryDistrict = (int)row["RecoveryDistrict"];

			if (row["RecoveryZone"] != DBNull.Value)
				vehicle.RecoveryZone = (string)row["RecoveryZone"];

			if (row["RecoverySubZone"] != DBNull.Value)
				vehicle.RecoverySubZone = (string)row["RecoverySubZone"];

			if (row["AdditionalDescription"] != DBNull.Value)
				vehicle.AdditionalDescription = (string)row["AdditionalDescription"];

			if (row["VehicleDamageLevelTypeID"] != DBNull.Value)
				vehicle.DamageLevelTypeID = (int)row["VehicleDamageLevelTypeID"];

			if (row["DamageLevel"] != DBNull.Value)
				vehicle.DamageLevel = (string)row["DamageLevel"];

		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}
