using System;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using System.Web;
using System.Web.Security;

using ReportHq.Common;
using ReportHq.IncidentReports.DataAccess;

namespace ReportHq.IncidentReports
{
	/// <summary>
	/// Organizational reporting tasks -- user stats, dashboards, charts, etc.
	/// </summary>
	public class Reporting
	{
		#region Enums

		public enum UcrPeriods
		{
			ThisMonth = 1,
			LastMonth = 2,
			LastThreeMonths = 3,
			ThisYear = 4,
			LastYear = 5,
			LastThreeYears = 6
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Public Static Methods

		/// <summary>
		/// Gets the user's stats for Rejected, Incomplete, Pending, Approved.
		/// </summary>
		public static DataTable DashboardStats(string username)
		{
			return ReportingDA.DashboardStats(username);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		
		/// <summary>
		/// Tallies pending Reports, per-district.
		/// </summary>
		public static DataTable PendingDistrictReports()
		{
			return ReportingDA.DistrictReportsByStatusType((int)Report.ApprovalStatusTypes.Pending);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Tallies pending Reports, per-district.
		/// </summary>
		public static DataTable RejectedDistrictReports()
		{
			return ReportingDA.DistrictReportsByStatusType((int)Report.ApprovalStatusTypes.Rejected);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Get tallies of approved Reports, per-district, by date range.
		/// </summary>
		public static DataTable ApprovedReportsByDistrict(DateTime startDate, DateTime endDate)
		{
			return ReportingDA.ApprovedReportsByDistrict(startDate, endDate);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Tallies all approved district Reports, returns percentages by district.
		/// </summary>
		public static DataTable DistrictReportsPercentage()
		{
			return ReportingDA.DistrictReportsPercentage();
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		///// <summary>
		///// Tallies reports by District.
		///// </summary>
		//public static DataTable ReportsByDistrict(int reportTypeID, DateTime startDate, DateTime endDate)
		//{
		//	DateTime emptyDate = new DateTime(1900, 1, 1);

		//	//if user entered startdate but no enddate, assume today is enddate
		//	if (startDate != emptyDate && endDate == emptyDate)
		//		endDate = DateTime.Now;
		//
		//	return ReportingDA.ReportsByDistrict(reportTypeID, startDate, endDate);
		//}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		///// <summary>
		///// Finds approved reports within a date range..
		///// </summary>
		//public static DataTable ApprovedReports(DateTime startDate, DateTime endDate)
		//{
		//	DateTime emptyDate = new DateTime(1900, 1, 1);

		//	//if user entered startdate but no enddate, assume today is enddate
		//	if (startDate != emptyDate && endDate == emptyDate)
		//		endDate = DateTime.Now;

		//	return ReportingDA.ApprovedReports(startDate, endDate);
		//}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		
		/// <summary>
		/// Tallies all approved district Reports, returns percentages by district.
		/// </summary>
		public static DataSet UcrTotals(UcrPeriods reportPeriod)
		{
			DateTime startDate = new DateTime(1900, 1, 1);
			DateTime endDate = new DateTime(1900, 1, 1);

			DateTime today = DateTime.Now;
			DateTime firstDayOfThisMonth = new DateTime(today.Year, today.Month, 1);

			switch (reportPeriod)
			{
				case UcrPeriods.ThisMonth:
					startDate = firstDayOfThisMonth;
					endDate = today;
					break;

				case UcrPeriods.LastMonth:
					startDate = firstDayOfThisMonth.AddMonths(-1);
					endDate = firstDayOfThisMonth.AddDays(-1);
					break;

				case UcrPeriods.LastThreeMonths:
					//what do we mean by last three months? three months from now, or previous three whole months? i believe we mean 3 months from today.
					startDate = today.AddMonths(-3);
					endDate = today;
					break;

				case UcrPeriods.ThisYear:
					startDate = new DateTime(today.Year, 1, 1);
					endDate = today;
					break;

				case UcrPeriods.LastYear:
					int lastYear = today.AddYears(-1).Year;
					startDate = new DateTime(lastYear, 1, 1);
					endDate = new DateTime(lastYear, 12, 31);
					break;

				case UcrPeriods.LastThreeYears:
					startDate = today.AddYears(-3);
					endDate = today;
					break;
			}

			return ReportingDA.UcrTotals(startDate, endDate, (int)reportPeriod);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Tallies all approved district Reports, returns percentages by district.
		/// </summary>
		public static DataTable UcrPeriodComparisons(int ucrOffenseTypeID, UcrPeriods reportPeriod)
		{
			DateTime startDate = new DateTime(1900, 1, 1);
			DateTime endDate = new DateTime(1900, 1, 1);

			DateTime today = DateTime.Now;
			//DateTime firstDayOfThisMonth = new DateTime(today.Year, today.Month, 1);

			switch (reportPeriod)
			{
				case UcrPeriods.LastThreeMonths:
					startDate = today.AddMonths(-2); //-3 returns 4 calendar months since today's month is included
					endDate = today;
					break;

				case UcrPeriods.LastYear:
					startDate = today;
					endDate = today;
					break;
			}

			DataSet ds = ReportingDA.UcrPeriodComparisons(ucrOffenseTypeID, (int)reportPeriod, startDate, endDate);
			//ds.Tables[0] - this year's current months
			//ds.Tables[1] - all last year months

			DataTable lastYear = null;

			if (ds.Tables.Count > 1)
			{
				DataTable thisYear = ds.Tables[0];
				lastYear = ds.Tables[1];

				lastYear.Columns.Add("ThisPeriod", typeof(System.Int32));

				//merge ds tables into one (there better linq for this?)

				DataRowCollection rows = thisYear.Rows;

				//for each row in this year
				int i;
				for (i = 0; i < (rows.Count); i++)
				{
					lastYear.Rows[i]["ThisPeriod"] = (int)rows[i]["ThisPeriod"]; //add it to last year's row
				}
			}

			return lastYear;			
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}
