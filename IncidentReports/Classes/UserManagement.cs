using System;
using System.Data;

using ReportHq.Common;
using ReportHq.IncidentReports.DataAccess;

namespace ReportHq.IncidentReports
{
	/// <summary>
	/// Class for working w/ our users and roles security model.
	/// </summary>
	public class UserManagement
	{
		#region Enums

		//public enum Types
		//{
		//	APB = 1,
		//	MissingPerson = 2
		//}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Properties

		
		
		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Constructors

		/// <summary>
		/// Get a Bulletin from the db.
		/// </summary>
		//public UserManagement()
		//{
		//	//this.ID = bulletinID;

		//	//DataSet ds = BulletinDA.GetBulletinByID(bulletinID);

		//	//if (ds.Tables[0].Rows.Count > 0)
		//	//	FillObject(ds.Tables[0].Rows[0]);
		//}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Public Methods

	
		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Public Static Methods

		public static DataTable GetUserRoles()
		{
			return UserManagementDA.GetUserRoles();
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Private Methods

		///// <summary>
		///// Fills this object for a specific GlobalNotification, via constructor.
		///// </summary>
		//private void FillObject(DataRow row)
		//{
		//	this.TypeID = (int)row["BulletinTypeID"];
		//	this.Type = (string)row["Type"];
		//	this.Title = (string)row["Title"];
		//	this.BulletinContent = (string)row["Bulletin"];
		//	this.CreateDate = (DateTime)row["CreatedDate"];
		//}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}
