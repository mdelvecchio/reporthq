using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using ReportHq.Common;
using ReportHq.IncidentReports.DataAccess;

namespace ReportHq.IncidentReports
{
	public class Attachment
	{
		#region Enums

		public enum FileTypes
		{
			Photo = 1,
			PDF = 2			
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Properties

		public int ID { get; set; }
		public int ReportID { get; set; }
		public FileTypes FileType { get; set; }
		public int FileTypeID { get; set; }
		public string SavedPath { get; set; }
		
		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Constructors

		/// <summary>
		/// Use this when creating a new attachment from an uploader.
		/// </summary>
		public Attachment(int reportID, string savedPath)
		{
			this.ReportID = reportID;
			this.SavedPath = savedPath;

			switch (Path.GetExtension(savedPath))
			{
				case ".jpg":
				case ".jpeg":
					this.FileType = FileTypes.Photo;
					break;

				case ".pdf":
					this.FileType = FileTypes.PDF;
					break;
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Use this when filling a new Notification from datarow. (ex: record from db)
		/// </summary>
		//public Attachment(DataRow row)
		//{
		//	FillObject(this, row);
		//}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Public Methods

		/// <summary>
		/// Record new attachment file to the database. Does not store file in db, only the path where it was stored on the file system.
		/// </summary>
		public void Add()
		{
			AttachmentsDA.InsertAttachment(this.ReportID, this.SavedPath, (int)this.FileType);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Public Static Methods

		public static StatusInfo DeleteAttachment(int attachmentID)
		{
			//1 remove from db
			StatusInfo status = AttachmentsDA.DeleteAttachment(attachmentID);

			if (status.Success)
			{
				//2 remove from file system

				string filesFolder = System.Configuration.ConfigurationManager.AppSettings["FilesFolder"];

				//in this case upon delete the status .Message is populated w/ file's save-path
				File.Delete(Path.Combine(filesFolder, status.Message));
			}

			return status;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


		/// <summary>
		/// Gets Pictures and PDFs tables
		/// </summary>
		public static DataSet GetAttachmentsByReportID(int reportID)
		{
			return AttachmentsDA.GetAttachmentsByReportID(reportID);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Gets the full path to where the files should be stored on the host file system. This is based on the report's CreatedDate and ItemNumber.
		/// Ex: "C:\ReportHqFiles\Attachments\Attachments\2014\7\G-99102-14"
		/// </summary>
		public static string GetSavePath(string itemNumber, DateTime reportCreatedDate)
		{
			string filesFolder = System.Configuration.ConfigurationManager.AppSettings["FilesFolder"];
			
			//build up save path using year, month, incident number
			string savePath = Path.Combine(filesFolder, "ReportAttachments", reportCreatedDate.Year.ToString(), reportCreatedDate.Month.ToString(), itemNumber);

			//if doesnt exist, create
			if (!Directory.Exists(savePath))
				Directory.CreateDirectory(savePath);

			return savePath;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Gets the relative path to where the files should be stored on the host file system. This is based on the report's CreatedDate and ItemNumber. We store 
		/// only this part in the db in order to decouple the server's target folder from the formulaic part (in case it's moved to a different volume, etc).
		/// Ex: "Attachments\2014\7\G-99102-14"
		/// </summary>
		public static string GetRelativePath(string itemNumber, DateTime reportCreatedDate)
		{
			//build up save path using year, month, incident number
			return Path.Combine("ReportAttachments", reportCreatedDate.Year.ToString(), reportCreatedDate.Month.ToString(), itemNumber);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Private Methods

		/// <summary>
		/// Fills this object for a specific GlobalNotification, via constructor.
		/// </summary>
		//private void FillObject(Attachment attachment, DataRow row)
		//{
		//	////required
		//	//notification.ID = (int)row["GlobalNotificationID"];
		//	//notification.Type = (string)row["TypeDescription"];
		//	//notification.Message = (string)row["Message"];

		//	//if (row["RelatedObjectID"] != DBNull.Value)
		//	//	notification.ClickID = (int)row["RelatedObjectID"];

		//	//if (row["ViewedDate"] != DBNull.Value)
		//	//	notification.Viewed = true;
		//}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}
