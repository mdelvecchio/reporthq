using System;
using System.Data;
using System.Collections;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using ReportHq.Common;
using ReportHq.IncidentReports.DataAccess;

namespace ReportHq.IncidentReports
{
	/// <summary>
	/// A simple container object for managing Security-Used checkbox-items. 
	/// </summary>
	public class SecurityUsedDescriptionItem
	{
		#region Properties

		private int _ID;
		private string _mainframeCode;
		private string _description;
		private bool _wasDefeated;
		
		/// <summary>
		/// Our unique SQL Server ID.
		/// </summary>
		public int ID
		{
			set { _ID = value; }
			get { return _ID; }
		}

		/// <summary>
		/// A legacy mainframe value used by the data entry people and for backwards compatibility w/ the official mainframe stats (we will be doing an EPR nightly dump into it).
		/// </summary>
		public string MainframeCode
		{
			set { _mainframeCode = value; }
			get { return _mainframeCode; }
		}

		/// <summary>
		/// Human readble description.
		/// </summary>
		public string Description
		{
			set { _description = value; }
			get { return _description; }
		}

		/// <summary>
		/// Indicates whether this security-item was defeated by the baddie.
		/// </summary>
		public bool WasDefeated
		{
			set { _wasDefeated = value; }
			get { return _wasDefeated; }
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Constructors

		public SecurityUsedDescriptionItem()
		{
			
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Use this when filling a new SecurityUsedDescriptionItem inline.
		/// </summary>
		public SecurityUsedDescriptionItem(int idValue)
		{
			this.ID = idValue;
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Use this when filling a new SecurityUsedDescriptionItem from datarow. (ex: record from db)
		/// </summary>
		public SecurityUsedDescriptionItem(DataRow row)
		{
			FillObject(this, row);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Private Methods

		/// <summary>
		/// Fills this object for a specific SecurityUsedDescriptionItem, via constructor.
		/// </summary>
		private void FillObject(SecurityUsedDescriptionItem item, DataRow row)
		{
			if (row["MeansOfEntryTypeID"] != DBNull.Value)
				item.ID = (int)row["MeansOfEntryTypeID"];

			if (row["MainframeCode"] != DBNull.Value)
				item.MainframeCode = (string)row["MainframeCode"];

			if (row["Description"] != DBNull.Value)
				item.Description = (string)row["Description"];

			if (row["WasDefeated"] != DBNull.Value)
				item.WasDefeated = (bool)row["WasDefeated"];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}
