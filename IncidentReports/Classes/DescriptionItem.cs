using System;
using System.Data;
using System.Collections;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace ReportHq.IncidentReports
{
	/// <summary>
	/// A simple container object for managing the report-form's many checkbox-items. 
	/// </summary>
	public class DescriptionItem
	{
		#region Properties

		private int _ID;
		private string _description;
		
		public int ID
		{
			set { _ID = value; }
			get { return _ID; }
		}

		public string Description
		{
			set { _description = value; }
			get { return _description; }
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Constructors

		public DescriptionItem()
		{
			
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Use this when filling a new DescriptionItem inline.
		/// </summary>
		public DescriptionItem(int idValue)
		{
			this.ID = idValue;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Use this when filling a new DescriptionItem from datarow. (ex: record from db)
		/// </summary>
		public DescriptionItem(DataRow row, int idColumnIndex)
		{
			FillObject(this, row, idColumnIndex);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Private Methods

		/// <summary>
		/// Fills this object for a specific DescriptionItem, via constructor.
		/// </summary>
		private void FillObject(DescriptionItem item, DataRow row, int idColumnIndex)
		{
			//since this obj is used by many types of db items, which all use different ID names, we cant know the ID-column's name -- so use an index int instead
			if (idColumnIndex > -1)
			{
				if (row[idColumnIndex] != DBNull.Value)
				{
					item.ID = (int)row[idColumnIndex];
				}
			}

			if (row["Description"] != DBNull.Value)
			{
				item.Description = (string)row["Description"];
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}
