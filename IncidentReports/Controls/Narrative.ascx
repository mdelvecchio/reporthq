﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Narrative.ascx.cs" Inherits="ReportHq.IncidentReports.Controls.Narrative" %>


<div class="formBox" style="width:640px; padding-bottom:40px;">

	<asp:TextBox ID="tbNarrative" TextMode="multiline" Rows="30" Width="633" Height="450" style="margin-bottom:8px;" onfocus="initializeSaveTimer();" runat="server" /><br />

	<div style="float:left">
		<telerik:RadSpell ID="radSpell" ControlToCheck="tbNarrative" DictionaryPath="~/App_Data/RadSpell/" SupportedLanguages="en-US,English" runat="server" />
	</div>

	<div style="float:right">
		<span id="spanSaveTimestamp"></span> &nbsp;<asp:LinkButton ID="lbSave" Text="Save" OnClick="lbSave_Click" CssClass="flatButton" runat="server" />
	</div>

</div>


<iframe style="display:none" name="formHidden"></iframe>


<Telerik:RadScriptBlock ID="radScriptBlock" runat="server">

	<script type="text/javascript">

		//start our auto-save timer.
		function initializeSaveTimer() {
			setTimeout("saveNarrative()", 300000); //5 minutes. then call saver
			//setTimeout("saveNarrative()", 10000); //10 seconds. then call saver
		}

		function saveNarrative() {

			var lbSave = document.getElementById('<%= lbSave.ClientID %>');
			
			//ensure button isnt disabled
			if (lbSave.className.indexOf('disabled') == -1) {

				//fire button. this works, but user will lose cursor position. it's shockingly difficult to get/set cursor in IE8. forget it for now.
				//lbSave.click();

				var textArea = document.getElementById('<%= tbNarrative.ClientID %>');
				//alert(textArea.value);

				//submit update form
				postToUrl("NarrativeSave.aspx", { 'rid': '<%= this.ReportID %>', 'narrative': textArea.value }, "post");

				var currentTime = new Date();

				var spanTimestamp = document.getElementById('spanSaveTimestamp').innerText = "Autosaved at " + currentTime.toTimeString().replace(' GMT-0500 (Central Daylight Time)', '');

				//restart timer
				initializeSaveTimer();
			}
		}

		//submits a form to another page.
		//call: postToUrl("test.aspx", {'name':'value'}, "post");
		//NOTE: replace this with a jquery webservice call, do proper error handling for save fails
		function postToUrl(path, params, method) {

			method = method || "post"; // Set method to post by default, if not specified.

			var form = document.createElement("form");
			form.setAttribute("method", method);
			form.setAttribute("action", path);
			form.setAttribute("target", "formHidden"); //hidden iframe above. prevents main window from moving to form target

			for (var key in params) {
				if (params.hasOwnProperty(key)) {
					var hiddenField = document.createElement("input");
					hiddenField.setAttribute("type", "hidden");
					hiddenField.setAttribute("name", key);
					hiddenField.setAttribute("value", params[key]);

					form.appendChild(hiddenField);
				}
			}

			document.body.appendChild(form);

			form.submit();
		}
	</script>

</Telerik:RadScriptBlock>