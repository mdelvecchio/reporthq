﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Footer.ascx.cs" Inherits="ReportHq.IncidentReports.Controls.Footer" %>

<footer id="Footer">
	<hr />
	<p>Report HQ, &copy; 2015. All rights reserved.</p>
</footer>