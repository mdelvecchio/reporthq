﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReportHq.IncidentReports;

namespace ReportHq.IncidentReports.Controls
{
	public partial class ReportMainNav : System.Web.UI.UserControl
	{
		#region Enums

		public enum NavSections
		{
			Event,
			People,
			Property,
			ModusOperandi,
			Narrative,
			Attachments,
			None
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Properties

		private NavSections _currentSection;
		private string _targetRedirectUrl;
		private string _validationGroup;
				
		/// <summary>
		/// Which main section link the user is currently at.
		/// </summary>
		public NavSections CurrentSection
		{
			get { return _currentSection; }
			set { _currentSection = value; }
		}

		/// <summary>
		/// Where to bring the user after clicking; used by the consuming .ASPX/ascx page
		/// </summary>
		public string TargetRedirectUrl
		{
			get { return _targetRedirectUrl; }
			set { _targetRedirectUrl = value; }
		}

		public string ValidationGroup
		{
			get { return _validationGroup; }
			set { _validationGroup = value; }
		}

		
		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		
		#endregion

		#region Events

		//public delegate for LinkButton nav-item click
		public delegate void NavItemClick(object sender, EventArgs e);

		//public event for LinkButton click; will be handled by the consuming pages
		public event NavItemClick NavItemClicked;

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
		
		#region Event Handlers

		protected void Page_Load(object sender, EventArgs e)
		{
			//set validation group used by page's validator controls
			if (!string.IsNullOrEmpty(_validationGroup))
			{
				lbEvent.ValidationGroup = _validationGroup;
				lbPeople.ValidationGroup = _validationGroup;
				lbProperty.ValidationGroup = _validationGroup;
				lbMo.ValidationGroup = _validationGroup;
				lbNarrative.ValidationGroup = _validationGroup;
				lbAttachments.ValidationGroup = _validationGroup;
			}


			//set selected-item state
			switch (_currentSection)
			{
				case NavSections.Event:
					liEvent.Attributes.Add("class", "selected");
					litEvent.Visible = true;
					lbEvent.Visible = false;
					break;

				case NavSections.People:
					liPeople.Attributes.Add("class", "selected");
					litPeople.Visible = true;
					lbPeople.Visible = false;
					break;
					
				case NavSections.Property:
					liProperty.Attributes.Add("class", "selected");
					litProperty.Visible = true;
					lbProperty.Visible = false;
					break;

				case NavSections.ModusOperandi:
					liMo.Attributes.Add("class", "selected");
					litMo.Visible = true;
					lbMo.Visible = false;
					break;

				case NavSections.Narrative:
					liNarrative.Attributes.Add("class", "selected");
					litNarrative.Visible = true;
					lbNarrative.Visible = false;
					break;

				case NavSections.Attachments:
					liAttachments.Attributes.Add("class", "selected");
					litAttachments.Visible = true;
					lbAttachments.Visible = false;
					break;
			}

		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void lbEvent_Click(object sender, EventArgs e)
		{
			_targetRedirectUrl = "~/Event/";

			//fire event for consuming page to handler
			NavItemClicked(this, e);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void lbPeople_Click(object sender, EventArgs e)
		{
			_targetRedirectUrl = "~/People/";

			//fire event for consuming page to handler
			NavItemClicked(this, e);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void lbProperty_Click(object sender, EventArgs e)
		{
			_targetRedirectUrl = "~/Property/";

			//fire event for consuming page to handler
			NavItemClicked(this, e);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void lbMo_Click(object sender, EventArgs e)
		{
			_targetRedirectUrl = "~/ModusOperandi/";

			//fire event for consuming page to handler
			NavItemClicked(this, e);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void lbNarrative_Click(object sender, EventArgs e)
		{
			_targetRedirectUrl = "~/Narrative.aspx";

			//fire event for consuming page to handler
			NavItemClicked(this, e);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void lbAttachments_Click(object sender, EventArgs e)
		{
			_targetRedirectUrl = "~/Attachments.aspx";

			//fire event for consuming page to handler
			NavItemClicked(this, e);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Private Methods

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

	}
}