﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReportHq.IncidentReports.Bases;
using ReportHq.IncidentReports.Controls;

namespace ReportHq.IncidentReports.Controls
{
	public partial class Attachments : System.Web.UI.UserControl
	{
		string _finalPath;

		#region Properties

		int _reportID;
		string _itemNumber;
		DateTime _reportCreatedDate;

		public int ReportID
		{
			get { return _reportID; }
			set { _reportID = value; }
		}

		/// <summary>
		/// Report ItemNumber. Used for folder-creation when saving files to file system.
		/// </summary>
		public string ItemNumber
		{
			get { return _itemNumber; }
			set { _itemNumber = value; }
		}

		/// <summary>
		/// Report CreatedDate. Used for folder-creation when saving files to file system.
		/// </summary>
		public DateTime ReportCreatedDate
		{
			get { return _reportCreatedDate; }
			set { _reportCreatedDate = value; }
		}

		/// <summary>
		/// Whether the user can operate certain form elements.
		/// </summary>
		public bool ReadOnly { get; set; }

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Event Handlers
		
		protected void Page_Load(object sender, EventArgs e)
		{
			//TODO: im not sure which way is best...using a .ReadOnly property set by the parent page, or just checking access in the UserControl itself. this may be less overhead than using properties
			//((ReportPage)this.Page).UserHasWriteAccess
			if (this.ReadOnly)
			{
				divUploader.Visible = false;
				radAjaxPanel.EnableAJAX = false;
				radAjaxPanel.Enabled = false;
			}


			if (IsPostBack)
			{
				//if files were uploaded by client
				if (radUpload.UploadedFiles.Count > 0)
				{
					_finalPath = Attachment.GetSavePath(_itemNumber, _reportCreatedDate);

					radUpload.TargetFolder = _finalPath;
				}
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void Page_PreRender(object sender, EventArgs e)
		{
			//bind attachments to UI
			SetAttachmentTables();
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Fires on form postback, once per uploaded file. Can perform additional server-side validations. If valid, files automatically copy to path set to .TargetFolder
		/// </summary>
		protected void radUpload_FileUploaded(object sender, Telerik.Web.UI.FileUploadedEventArgs e)
		{
			//ensure the instantiated report in session is the same ReportID as the one saved in the page's ViewState.
			((ReportPage)this.Page).SyncSessionToViewState();


			//TODO: how to make uploader ensure file uniqueness? rather than over-write, append nameand keep unique..
			
			if (e.IsValid)
			{
				string relativePath = Attachment.GetRelativePath(_itemNumber, _reportCreatedDate);

				//insert record to database
				Attachment attachment = new Attachment(_reportID, Path.Combine(relativePath, e.File.GetName()));
				attachment.Add();
			}			
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void lbDelete_Command(object sender, CommandEventArgs e)
		{
			DeleteAttachment(Convert.ToInt32(e.CommandArgument));

			//if (e.CommandName == "Delete")
			//{
			//	
			//}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion		

		#region Private Methods

		private void SetAttachmentTables()
		{
			//TODO: whats faster -- 1) a dataset of child tables, or 2) a collection of objects and using LINQ to filter sub-groups on bind? ask stackoverflow
			DataSet results = Attachment.GetAttachmentsByReportID(_reportID);

			if (results.Tables.Count > 0)
			{
				DataTable dtPictures = results.Tables[0];
				DataTable dtPdfs = results.Tables[1];

				//pictures
				if (dtPictures.Rows.Count > 0)
				{
					rptPictures.DataSource = dtPictures;
					rptPictures.DataBind();
					rptPictures.Visible = true;

					lightBox.DataImageUrlField = "SavedPath";
					lightBox.DataImageUrlFormatString = @"~\{0}";
					lightBox.DataSource = dtPictures;
					lightBox.DataBind();

					litNonePictures.Visible = false;
				}
				else
				{
					rptPictures.Visible = false;
					litNonePictures.Visible = true;
				}

				//pdfs
				if (dtPdfs.Rows.Count > 0)
				{
					rptPdfs.DataSource = dtPdfs;
					rptPdfs.DataBind();
					rptPdfs.Visible = true;

					litNonePdfs.Visible = false;
				}
				else
				{
					rptPdfs.Visible = false;					
					litNonePdfs.Visible = true;
				}
			}
		}


		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		
		private void DeleteAttachment(int attachmentID)
		{
			Attachment.DeleteAttachment(attachmentID);

			//OPTIONAL: can handle this for exceptions.
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}