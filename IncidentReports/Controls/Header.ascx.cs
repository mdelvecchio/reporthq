﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReportHq.IncidentReports;

namespace ReportHq.IncidentReports.Controls
{
	public partial class Header : System.Web.UI.UserControl
	{
		#region Variables

		public string jsCookieNames;

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Properties

		int _reportID;
		List<BreadCrumb> _breadCrumbs = new List<BreadCrumb>();
		bool _showAdminTools;
		bool _showSupervisorTools;
		bool _showSubmitReportButton;
		bool _showSupervisorButton;
		bool _showSupervisorNotesButton;
		
		public int ReportID
		{
			get { return _reportID; }
			set { _reportID = value; }
		}

		public List<BreadCrumb> BreadCrumbs
		{
			get { return _breadCrumbs; }
			set { _breadCrumbs = value; }
		}

		public string HelpUrl
		{
			set { linkHelp.HRef = value; }
		}

		public string Username
		{
			set { litUsername.Text = value; }
		}

		public bool ShowAdminTools
		{
			get { return _showAdminTools; }
			set { _showAdminTools = value; }
		}

		public bool ShowSupervisorTools
		{
			get { return _showSupervisorTools; }
			set { _showSupervisorTools = value; }
		}

		public string Heading
		{
			set { litHeading.Text = value; }
		}

		public bool ShowSubmitReportButton
		{
			get { return _showSubmitReportButton; }
			set { _showSubmitReportButton = value; }
		}

		public bool ShowSupervisorButton
		{
			get { return _showSupervisorButton; }
			set { _showSupervisorButton = value; }
		}

		public bool ShowSupervisorNotesButton
		{
			get { return _showSupervisorNotesButton; }
			set { _showSupervisorNotesButton = value; }
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		
		#endregion

		#region Event Handlers

		/// <summary>
		/// Turn on/off various header UI elements.
		/// </summary>
		protected void Page_PreRender(object sender, EventArgs e)
		{
			lnkAdminTools.Visible = _showAdminTools;
			lnkSupervisorTools.Visible = _showSupervisorTools;

			//do i need this? or do like other strings in properties only
			liSubmitReportButton.Visible = _showSubmitReportButton;
			liSupervisorButton.Visible = _showSupervisorButton;
			liSupervisorNotesButton.Visible = _showSupervisorNotesButton;

			rptBreadCrumbs.DataSource = _breadCrumbs;
			rptBreadCrumbs.DataBind();

			//these cookie names are used by the HTML's openRadWindow() definition, for rwSupervisorNotes state management in js cookies.
			jsCookieNames = string.Format("'superNotesVis_{0}', 'superNotesCoords_{0}'", _reportID);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}