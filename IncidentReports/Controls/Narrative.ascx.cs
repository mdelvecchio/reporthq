﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReportHq.IncidentReports.Controls;

namespace ReportHq.IncidentReports.Controls
{
	public partial class Narrative : System.Web.UI.UserControl
	{
		#region Events

		//delegate for Save buttons clicks
		public delegate void SaveButtonClick(object sender, System.EventArgs e);

		//event for the same
		public event SaveButtonClick SaveButtonClicked;

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Properties

		int _reportID;

		/// <summary>
		/// Usued by this control to drop the ReportID into the HTML's javascript.
		/// </summary>
		public int ReportID
		{
			get { return _reportID; }
			set { _reportID = value; }
		}

		public string NarrativeText
		{
			get { return tbNarrative.Text; }
			set { tbNarrative.Text = value; }
		}

		/// <summary>
		/// Whether the user can operate certain form elements.
		/// </summary>
		public bool ReadOnly
		{
			set
			{
				if (value == true)
				{
					//radSpell.Enabled = false; //doesnt seem to do anything
					radSpell.Visible = false;
					lbSave.Enabled = false;
					lbSave.CssClass = "flatButton disabled";
				}
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Event Handlers

		protected void Page_Load(object sender, EventArgs e)
		{
			tbNarrative.Focus();
		}
		
		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		
		/// <summary>
		/// Private handler, fires public event for consumption.
		/// </summary>
		protected void lbSave_Click(object sender, EventArgs e)
		{
			SaveButtonClicked(this, e);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		
		#endregion
	}
}