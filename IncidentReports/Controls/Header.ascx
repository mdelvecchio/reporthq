﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Header.ascx.cs" Inherits="ReportHq.IncidentReports.Controls.Header" %>

<div id="MainNav">

	<span id="Logo">
		<a href="/index.html" title="to Report HQ applications">
			Report <span style="color:#ff6a00">HQ<span style="font-size:12px; position:relative; top:-6px;">&trade;</span>
		</a>
	</span>


	<asp:Repeater ID="rptBreadCrumbs" runat="server">
		<ItemTemplate>
			<i class="separator"></i><a href='<%# Eval("Url" )%>' class="breadcrumb" title='<%# Eval("Title") %>'><%# Eval("Text") %></a>
		</ItemTemplate>
	</asp:Repeater>

			
	<div class="navRightSide">

		<ul class="navItems">

			<li class="helpLink">
				<a id="linkHelp" href="#" title="View help for this page" runat="server"><i class="fa fa-question-circle"></i> <span>Help</span></a>
			</li>

			<li class="notificationsLink">
				<a href="#" title="notifications menu" style="text-decoration: none;" onclick="showNotifications();">
					<i id="NotificationsImage" class="fa fa-bell fa-lg"></i><span id="NotificationsCount" class="count"></span>
					<span>Notifications</span>
				</a>

				<div id="Notifications" class="popOverMenu">
					<i class="popoverPointer"></i>


					<div id="ApbNotificationsHeader" class="popoverHeader">APBs</div>
					<div id="ApbNotifications" class="popoverContent"></div>
					<input type="hidden" id="ApbNotificationItemIds" value="" />

					<div id="SystemNotificationsHeader" class="popoverHeader">System Notifications</div>
					<div id="SystemNotifications" class="popoverContent"></div>
					<input type="hidden" id="SystemNotificationItemIds" value="" />

					<div id="ReportNotificationsHeader" class="popoverHeader">Report Notifications</div>
					<div id="ReportNotifications" class="popoverContent"></div>
					<input type="hidden" id="ReportNotificationItemIds" value="" />


					<div id="EmptyNotifications">No new notifications.</div>

					<div class="footerLink">
						<a href="~/YourNotifications.aspx" runat="server">See All</a>
					</div>

				</div>
			</li>
					
			<li class="userLink">
				<a href="#" id="UserLink" title="brings up user menu" style="text-decoration: none;" onclick="toggleMenu('#UserMenu');">
					<i id="UserIcon" class="fa fa-user fa-lg" style="margin-right:6px;"></i> <asp:Literal ID="litUsername" runat="server" /><i class="fa fa-caret-down" style="margin-left:6px;"></i>
				</a>
			</li>

		</ul>

	</div>

	<div id="UserMenu" class="popOverMenu">
		<i class="popoverPointer"></i>
		<ul class="menuItems">
			<li><a href="~/YourDashboard.aspx" runat="server"><i class="fa fa-user fa-fw smallIcon"></i>Your Dashboard</a></li>
			<li><a href="~/RollCall.aspx" runat="server"><i class="fa fa-clock-o fa-fw smallIcon"></i>Roll Call</a></li>
			<li><a href="~/YourReports.aspx" runat="server"><i class="fa fa-copy fa-fw smallIcon"></i>Your Reports</a></li>
			<li><a href="~/DeleteYourReports.aspx" runat="server"><i class="fa fa-trash-o fa-fw smallIcon"></i>Delete Your Reports</a></li>
			<li><a href="~/Tools/FindReports.aspx" runat="server"><i class="fa fa-search fa-fw smallIcon"></i>Find Reports</a></li>
			<li class="divider"></li>
			<li><a href="~/Tools/" class="roleItem" runat="server"><i class="fa fa-cog fa-fw smallIcon"></i>Tools</a></li>
			<li><a id="lnkAdminTools" href="~/Admin/" class="roleItem" visible="false" runat="server"><i class="fa fa-cog fa-fw smallIcon"></i>Admin Tools</a></li>
			<li><a id="lnkSupervisorTools" href="~/Supervisor/" class="roleItem" visible="false" runat="server"><i class="fa fa-cog fa-fw smallIcon"></i>Supervisor Tools</a></li>
		</ul>
	</div>

</div>


<div id="TitleBar">
	<h1><asp:Literal ID="litHeading" runat="server" /></h1>


	<span class="navRightSide">
		
		<ul class="titleBarItems">

			<li id="liSupervisorNotesButton" class="actionButton" Visible="false" runat="server">
				<a href="javascript:openRadWindow('rwSupervisorNotes', <%= jsCookieNames %>);" title="See your supervisor's notes for this Report"><i class="fa fa-pencil-square-o"></i><span>Supervisor Notes</span></a>
			</li>

			<li id="liSupervisorButton" class="actionButton" Visible="false" runat="server">
				<a href="#" id="SupervisorReviewLink" title="Review this Report"><i class="fa fa-check-circle"></i><span>Supervisor Review</span></a>
			</li>

			<li id="liSubmitReportButton" class="actionButton" Visible="false" runat="server">
				<a href="#" id="SubmitLink" title="Submit this Report"><i class="fa fa-check-circle"></i><span>Submit Report</span></a>
			</li>

		</ul>

	</span>

</div>