﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ReportHq.IncidentReports.Controls
{
	public partial class APBs : System.Web.UI.UserControl
	{
		#region Event Handlers

		protected void Page_Load(object sender, EventArgs e)
		{
			DataTable dtApbs = Bulletin.GetAllPointBulletins();

			if (dtApbs.Rows.Count > 0)
			{
				rptApb.DataSource = Bulletin.GetAllPointBulletins();
				rptApb.DataBind();
			}
			else
			{
				litNoneApb.Visible = true;
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

	}
}