﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using Telerik.Web.UI;

namespace ReportHq.IncidentReports
{
	/// <summary>
	/// IncidentReports.Master's class, which exposes some properties. This is sort of an experiment...articles say this is better than using 
	/// this.Master.FindControl("foo") in consuming page classes since that has to iterate thru the controls collection, and these properties dont.
	/// </summary>
	public partial class NormalMasterPage : System.Web.UI.MasterPage
	{
		#region Properties

		/// <summary>
		/// Picked up by bits of javascript.
		/// </summary>
		public string HiddenUsername
		{
			get { return hidUsername.Value; }
			set { hidUsername.Value = value; }
		}

		/// <summary>
		/// Picked up by bits of javascript.
		/// </summary>
		public string HiddenReportID
		{
			get { return hidReportID.Value; }
			set { hidReportID.Value = value; }
		}

		/// <summary>
		/// Used by content pages' RadAjaxManagers.
		/// </summary>
		public Label LabelMessage
		{
			get { return lblMessage; }
			set { lblMessage = value; }
		}

		/// <summary>
		/// Used by content pages' RadAjaxManagers.
		/// </summary>
		public RadAjaxLoadingPanel LoadingPanel
		{
			get { return ajaxLoadingPanel; }
			set { ajaxLoadingPanel = value; }
		}

		#endregion

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void Page_Load(object sender, EventArgs e)
		{

		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	}
}