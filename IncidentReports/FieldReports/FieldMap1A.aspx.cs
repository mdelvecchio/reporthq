﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReportHq.IncidentReports.DataAccess;

namespace ReportHq.IncidentReports.FieldReports
{
	public partial class FieldMap1a : Bases.NormalPage
	{
		#region Event Handlers

		protected void Page_Load(object sender, EventArgs e)
		{
			this.Heading = "Field Map: Lorem";
			this.HelpUrl = "help/YourDashboard.html";
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void Page_PreRender(object sender, EventArgs e)
		{
			List<BreadCrumb> newBreadCrumbs = new List<BreadCrumb>();
			newBreadCrumbs.Add((new BreadCrumb("Field Reports", ResolveUrl("~/FieldReports/"), "to Field Reporting")));
	
			_ucHeader.BreadCrumbs = newBreadCrumbs;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Private Methods

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}