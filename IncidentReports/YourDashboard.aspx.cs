﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReportHq.IncidentReports.DataAccess;

namespace ReportHq.IncidentReports
{
	public partial class YourDashboard : Bases.NormalPage
	{
		#region Event Handlers

		protected void Page_Load(object sender, EventArgs e)
		{
			this.Heading = "Your Dashboard";
			this.HelpUrl = "help/YourDashboard.html";

			SetReportTables();
			SetStats();

			string mode = Request.QueryString["mode"];

			if (Request.QueryString["submitted"] == "1")
				base.RenderUserMessage(Enums.UserMessageTypes.Success, "Your report was submitted.");
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Private Methods

		private void SetReportTables()
		{
			//TODO: whats faster -- 1) a dataset of child tables, or 2) a collection of objects and using LINQ to filter sub-groups on bind? ask stackoverflow
			DataSet results = ReportDA.GetDashboardReportsByUsername(this.Username);

			if (results.Tables.Count > 0)
			{
				DataTable dtRejected = results.Tables[0];
				DataTable dtIncomplete = results.Tables[1];
				DataTable dtPending = results.Tables[2];

				//rejected
				if (dtRejected.Rows.Count > 0)
				{
					rptRejected.DataSource = dtRejected;
					rptRejected.DataBind();
				}
				else
				{
					litNoneRejected.Visible = true;
				}

				//incomplete
				if (dtIncomplete.Rows.Count > 0)
				{
					rptIncomplete.DataSource = dtIncomplete;
					rptIncomplete.DataBind();
				}
				else
				{
					litNoneIncomplete.Visible = true;
				}

				//pending
				if (dtPending.Rows.Count > 0)
				{
					rptPending.DataSource = dtPending;
					rptPending.DataBind();
				}
				else
				{
					litNonePending.Visible = true;
				}

			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		private void SetStats()
		{
			DataTable dt = Reporting.DashboardStats(this.Username);

			radChart.DataSource = dt;

			//set chart headers
			DataRow row = dt.Rows[0];

			litStatRejected.Text = row["Rejected"].ToString();
			litStatIncomplete.Text = row["Incomplete"].ToString();
			litStatPending.Text = row["Pending"].ToString();
			litStatApproved.Text = row["Approved"].ToString();			
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}