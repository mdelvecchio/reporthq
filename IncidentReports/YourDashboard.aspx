﻿<%@ Page Title="Your Dashboard" Language="C#" MasterPageFile="~/IncidentReports.Master" AutoEventWireup="true" CodeBehind="YourDashboard.aspx.cs" Inherits="ReportHq.IncidentReports.YourDashboard" %>
<%@ Register TagPrefix="ReportHq" TagName="Apbs" src="controls/APBs.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<link rel="stylesheet" href="css/boxes.css" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="phContent" runat="server">

	<style type="text/css">
		.statContainer { display:table; width:100%; }

		 .statSum {
			display:table-cell;
			text-align: right;
			/*border: 1px solid black;*/
		 }

		 .statSum.approved { width:60px; padding-right:25px; }
		 .statSum.pending { width:60px; padding-right:24px; }
		 .statSum.incomplete { width:70px; padding-right:24px; }
		 .statSum.rejected { padding-right:14px; }

		 .sLabel {
			font-size:10px;
			font-weight: bold;
			color:gray;
			margin-bottom:-6px;
		 }

		 .sValue { font:400 30px 'Open Sans', sans-serif; }

		 .sValue.rejected { color:#CC3333; }
		 .sValue.incomplete { color:#E68637; }
		 .sValue.pending { color:#C0C0C0; }
		 .sValue.approved { color:#339966; }

		 .sValue.week { color:#599BC6; }
		 /*.sValue.total { color:#599BC6; }*/
	</style>

	<a href="CreateReport.aspx" title="Create a new Report" class="flatButton">
		<i class="fa fa-plus fa-fw"></i> Create Report
	</a>
			
	<div style="margin-top:7px;">
			
		<!-- rejected -->
		<dl class="box subtext fixed">
			<dt>				
				<i class="iconExclaim"></i>
				<div class="description">
					<h3><a href="YourReports.aspx">Rejected</a></h3>
					These reports were not approved and require edits
				</div>
			</dt>
			<dd>
				<asp:Repeater ID="rptRejected" runat="server">
					<HeaderTemplate>
						<table width="100%" cellspacing="0" border="0" class="lightList">
							<thead>
								<tr>
									<th></th>
									<th>INCIDENT NUMBER</th>
									<th>REPORT DATE</th>
								</tr>
							</thead>
							<tbody>
					</HeaderTemplate>
					<ItemTemplate>
						<tr>
							<td><a href='print/ReportPdf.aspx?rid=<%# Eval("ReportID") %>' title="Open PDF"><img src="images/pdf.gif" width="16" height="16"/></a></td>
							<td><a href="event/BasicInfo.aspx?rid=<%#Eval("ReportID")%>" title="Go to Report"><%#Eval("ItemNumber")%></a></td>
							<td><a href="YourReports.aspx" title="See all your Reports for this date"><%# string.Format("{0:MM/dd/yyyy}", Eval("ReportDate"))%></a></td>
						</tr>
					</ItemTemplate>
					<FooterTemplate>
							</tbody>
						</table>
					</FooterTemplate>
				</asp:Repeater>
				<asp:Literal ID="litNoneRejected" Text="None at this time." Visible="false" runat="server" />
			</dd>
		</dl>
				
		<!-- incomplete -->
		<dl class="box subtext fixed">
			<dt>				
				<i class="iconDots"></i>
				<div class="description">
					<h3><a href="YourReports.aspx">Incomplete</a></h3>
					You have not finished these reports
				</div>
			</dt>
			<dd>
				<asp:Repeater ID="rptIncomplete" runat="server">
					<HeaderTemplate>
						<table width="100%" cellspacing="0" border="0" class="lightList">
							<thead>
								<tr>
									<th></th>
									<th>INCIDENT NUMBER</th>
									<th>REPORT DATE</th>
								</tr>
							</thead>
							<tbody>
					</HeaderTemplate>
					<ItemTemplate>
						<tr>
							<td><a href='print/ReportPdf.aspx?rid=<%# Eval("ReportID") %>' title="Open PDF"><img src="images/pdf.gif" width="16" height="16"/></a></td>
							<td><a href="event/BasicInfo.aspx?rid=<%#Eval("ReportID")%>" title="Go to Report"><%#Eval("ItemNumber")%></a></td>
							<td><a href="YourReports.aspx" title="See all your Reports for this date"><%# string.Format("{0:MM/dd/yyyy}", Eval("ReportDate"))%></a></td>
						</tr>
					</ItemTemplate>
					<FooterTemplate>
							</tbody>
						</table>
					</FooterTemplate>
				</asp:Repeater>
				<asp:Literal ID="litNoneIncomplete" Text="None at this time." Visible="false" runat="server" />
			</dd>
		</dl>
							
		<!-- pending -->
		<dl class="box subtext fixed">
			<dt>
				<i class="iconTimer"></i>
				<div class="description">
					<h3><a href="YourReports.aspx">Pending</a></h3>
					Submitted reports pending supervisor review
				</div>
			</dt>
			<dd>
				<asp:Repeater ID="rptPending" runat="server">
					<HeaderTemplate>
						<table width="100%" cellspacing="0" border="0" class="lightList">
							<thead>
								<tr>
									<th></th>
									<th>INCIDENT NUMBER</th>
									<th>REPORT DATE</th>
								</tr>
							</thead>
							<tbody>
					</HeaderTemplate>
					<ItemTemplate>
						<tr>
							<td><a href='print/ReportPdf.aspx?rid=<%# Eval("ReportID") %>' title="Open PDF"><img src="images/pdf.gif" width="16" height="16"/></a></td>
							<td><a href="event/BasicInfo.aspx?rid=<%#Eval("ReportID")%>" title="Go to Report"><%#Eval("ItemNumber")%></a></td>
							<td><a href="YourReports.aspx" title="See all your Reports for this date"><%# string.Format("{0:MM/dd/yyyy}", Eval("ReportDate"))%></a></td>
						</tr>
					</ItemTemplate>
					<FooterTemplate>
							</tbody>
						</table>
					</FooterTemplate>
				</asp:Repeater>
				<asp:Literal ID="litNonePending" Text="None at this time." Visible="false" runat="server" />
			</dd>
		</dl>
				
		<!-- APBs -->
		<ReportHq:Apbs ID="ucApbs" runat="server" />

		<!-- stats -->
		<dl class="box subtext">
			<dt>
				<i class="iconGauge"></i>
				<div class="description">
					<h3><a href="YourAnalytics.aspx">Stats</a></h3>
					Current stats for your reports
				</div>
			</dt>
			<dd>

				<div class="statContainer">

					<div class="statSum rejected">
						<div class="sLabel">REJECTED</div>
						<div class="sValue rejected"><asp:Literal ID="litStatRejected" runat="server" /></div>
					</div>

					<div class="statSum incomplete">
						<div class="sLabel">INCOMPLETE</div>
						<div class="sValue incomplete"><asp:Literal ID="litStatIncomplete" runat="server" /></div>
					</div>

					<div class="statSum pending">
						<div class="sLabel">PENDING</div>
						<div class="sValue pending"><asp:Literal ID="litStatPending" runat="server" /></div>
					</div>

					<div class="statSum approved">
						<div class="sLabel">APPROVED</div>
						<div class="sValue approved"><asp:Literal ID="litStatApproved" runat="server" /></div>
					</div>

				</div>

				<telerik:RadHtmlChart ID="radChart" Width="400" Height="180" Transitions="true" OnClientSeriesClicked="OnClientSeriesClicked" runat="server">
                    <Appearance>
                         <FillStyle BackgroundColor="White"></FillStyle>
                    </Appearance>

                    <Legend>
                         <Appearance BackgroundColor="White" Position="Bottom" Visible="false" />
                    </Legend>

					<PlotArea>
						<%--
						lighter: #EFEFEF
						darker: #CCCCCC
						--%>
						<YAxis Color="white" Visible="true" MajorTickType="None" Step="1">
							<MajorGridLines Color="#E4E4E4" />
							<MinorGridLines Visible="false" />
							<LabelsAppearance Skip="1">
								<TextStyle Color="#CCCCCC" FontSize="10px" />
							</LabelsAppearance>
						</YAxis>

						<XAxis Color="#E4E4E4" MajorTickType="None">
							<MajorGridLines Visible="false" />
							<MinorGridLines Visible="false" />
						</XAxis>						

						<Series>

							<telerik:ColumnSeries Name="Rejected" DataFieldY="Rejected" Gap="1">
								<Appearance>
									<FillStyle BackgroundColor="#CC3333" />
								</Appearance>
								<LabelsAppearance Visible="false" />
								<TooltipsAppearance DataFormatString="{0} reports. Click to view" Color="White" />
							</telerik:ColumnSeries>

							<telerik:ColumnSeries Name="Incomplete" DataFieldY="Incomplete" Gap="1">
								<Appearance>
									<FillStyle BackgroundColor="#E68637" />
								</Appearance>
								<LabelsAppearance Visible="false" />
								<TooltipsAppearance DataFormatString="{0} reports. Click to view" Color="White" />
							</telerik:ColumnSeries>

							<telerik:ColumnSeries Name="Pending" DataFieldY="Pending" Gap="1">
								<Appearance>
									<FillStyle BackgroundColor="#C0C0C0" />
								</Appearance>
								<LabelsAppearance Visible="false" />
								<TooltipsAppearance DataFormatString="{0} reports. Click to view" Color="White" />
							</telerik:ColumnSeries>

							<telerik:ColumnSeries Name="Approved" DataFieldY="Approved" Gap="1">
								<Appearance>
									<FillStyle BackgroundColor="#339966" />
								</Appearance>
								<LabelsAppearance Visible="false" />
								<TooltipsAppearance DataFormatString="{0:N0} reports. Click to view" Color="White" />
							</telerik:ColumnSeries>
						
						</Series>
					</PlotArea>
				</telerik:RadHtmlChart>

				<script type="text/javascript">
					function OnClientSeriesClicked(sender, eventArgs) {
						//alert('value ' + eventArgs.get_value());
						window.location.href = "/YourReports.aspx";
					}
				</script>

			</dd>
		</dl>
		
		<!-- analytics -->
		<dl class="box subtext">
			<dt>
				<i class="iconTrend"></i>
				<div class="description">
					<h3><a href="YourAnalytics.aspx">Analytics</a></h3>
					Your historic trends
				</div>
			</dt>
			<dd>

				<div class="statContainer">

					<div class="statSum ">
						<div class="sLabel">TOTAL</div>
						<div class="sValue">100</div>
					</div>

					<div class="statSum ">
						<div class="sLabel">LAST 7 DAYS</div>
						<div class="sValue week">4</div>
					</div>

					<div class="statSum ">
						<div class="sLabel">AVG. PER WEEK</div>
						<div class="sValue week">3</div>
					</div>


				</div>
				<img src="images/chart2.gif" width="400" height="150" title="Reports per Week over time" />
				<%--<img src="images/chart.gif" width="421" height="226" title="placeholder chart" />--%>
			</dd>
		</dl>

				
		<p style="clear:both;">&nbsp;</p>
		
	</div>

</asp:Content>
