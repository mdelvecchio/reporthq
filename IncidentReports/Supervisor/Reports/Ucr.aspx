﻿<%@ Page Title="UCR Report" Language="C#" MasterPageFile="~/IncidentReports.Master" AutoEventWireup="true" CodeBehind="Ucr.aspx.cs" Inherits="ReportHq.IncidentReports.Supervisor.Reports.Ucr" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<link rel="stylesheet" href="../../css/boxes.css" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="phContent" runat="server">

	<div>
		<span class="label">Period:</span> 
		<asp:DropDownList ID="ddlPeriod" OnSelectedIndexChanged="ddlPeriod_SelectedIndexChanged" AutoPostBack="true" runat="server">
			<asp:ListItem Value="1" Text="This Month" />
			<asp:ListItem Value="2" Text="Last Month" />
			<asp:ListItem Value="3" Text="Last Three Months" />
			<asp:ListItem Value="4" Text="This Year" />
			<asp:ListItem Value="5" Text="Last Year" />
			<asp:ListItem Value="6" Text="Last Three Years" />
		</asp:DropDownList>
	</div>

	
	<!-- totals -->
	<dl class="box subtext" style="width:650px; height:445px;">
		<dt>
			<div class="description">
				<h3>UCR Offense Totals</h3>
				UCR Part I offenses
			</div>
		</dt>
		<dd>
			<telerik:RadHtmlChart ID="rhcUcrTotals" Width="610" Height="320" Transitions="true" runat="server">
				<Legend>
					<%--<Appearance BackgroundColor="White" Position="Bottom" Visible="true">
						<TextStyle Margin="10 0 0 0" />
					</Appearance>--%>
					<Appearance BackgroundColor="White" Position="Right" Visible="true">
						<TextStyle Margin="0 0 100 10" />
					</Appearance>
				</Legend>

				<PlotArea>					
					<XAxis Color="#E4E4E4" MajorTickType="None">
						<MajorGridLines Visible="false" />
						<MinorGridLines Visible="false" />
					</XAxis>
					<YAxis Color="white" Visible="true" MajorTickType="None">
						<MajorGridLines Color="#E4E4E4" />
						<MinorGridLines Visible="false" />
						<LabelsAppearance Skip="1">
							<TextStyle Color="#CCCCCC" FontSize="10px" />
						</LabelsAppearance>
					</YAxis>

					<Series>
						<telerik:ColumnSeries Name="Criminal Homicide" DataFieldY="CriminalHomicide">
							<LabelsAppearance DataFormatString="{0:N0}" />
							<TooltipsAppearance Color="White">
								<ClientTemplate>
									#=series.name.split("|")[0]# - #=kendo.format(\'{0:N0}\', value)# reports
								</ClientTemplate>
							</TooltipsAppearance>
						</telerik:ColumnSeries>

						<telerik:ColumnSeries Name="Forcible Rape" DataFieldY="ForcibleRape">
							<LabelsAppearance DataFormatString="{0:N0}" />
							<TooltipsAppearance Color="White">
								<ClientTemplate>
									#=series.name.split("|")[0]# - #=kendo.format(\'{0:N0}\', value)# reports
								</ClientTemplate>
							</TooltipsAppearance>
						</telerik:ColumnSeries>

						<telerik:ColumnSeries Name="Robbery" DataFieldY="Robbery">
							<LabelsAppearance DataFormatString="{0:N0}" />
							<TooltipsAppearance Color="White">
								<ClientTemplate>
									#=series.name.split("|")[0]# - #=kendo.format(\'{0:N0}\', value)# reports
								</ClientTemplate>
							</TooltipsAppearance>
						</telerik:ColumnSeries>

						<telerik:ColumnSeries Name="Aggravated Assault" DataFieldY="AggravatedAssault">
							<LabelsAppearance DataFormatString="{0:N0}" />
							<TooltipsAppearance Color="White">
								<ClientTemplate>
									#=series.name.split("|")[0]# - #=kendo.format(\'{0:N0}\', value)# reports
								</ClientTemplate>
							</TooltipsAppearance>
						</telerik:ColumnSeries>

						<telerik:ColumnSeries Name="Burglary" DataFieldY="Burglary">
							<LabelsAppearance DataFormatString="{0:N0}" />
							<TooltipsAppearance Color="White">
								<ClientTemplate>
									#=series.name.split("|")[0]# - #=kendo.format(\'{0:N0}\', value)# reports
								</ClientTemplate>
							</TooltipsAppearance>
						</telerik:ColumnSeries>

						<telerik:ColumnSeries Name="Larceny Theft" DataFieldY="LarcenyTheft">
							<LabelsAppearance DataFormatString="{0:N0}" />
							<TooltipsAppearance Color="White">
								<ClientTemplate>
									#=series.name.split("|")[0]# - #=kendo.format(\'{0:N0}\', value)# reports
								</ClientTemplate>
							</TooltipsAppearance>
						</telerik:ColumnSeries>

						<telerik:ColumnSeries Name="Motor Vehicle Theft" DataFieldY="MotorVehicleTheft">
							<LabelsAppearance DataFormatString="{0:N0}" />
							<TooltipsAppearance Color="White">
								<ClientTemplate>
									#=series.name.split("|")[0]# - #=kendo.format(\'{0:N0}\', value)# reports
								</ClientTemplate>
							</TooltipsAppearance>
						</telerik:ColumnSeries>

						<telerik:ColumnSeries Name="Arson" DataFieldY="Arson">
							<LabelsAppearance DataFormatString="{0:N0}" />
							<TooltipsAppearance Color="White">
								<ClientTemplate>
									#=series.name.split("|")[0]# - #=kendo.format(\'{0:N0}\', value)# reports
								</ClientTemplate>
							</TooltipsAppearance>
						</telerik:ColumnSeries>
					</Series>
				</PlotArea>
			</telerik:RadHtmlChart>

			<p class="actionButton">
				<a href="#" title="Click to save chart" onclick="exportRadHtmlChart('rhcUcrTotals', 'ucrTotalsChart');return false;"><i class="fa fa-bar-chart-o fa-fw"></i>Save</a>
			</p>
		</dd>
	</dl>



	<!-- total over time -->
	<dl class="box subtext" style="width: 650px; height: 445px;">
		<dt>
			<div class="description">
				<h3>UCR Offenses Over Time</h3>
				(click legend items to add)
			</div>
		</dt>
		<dd>
			<telerik:RadHtmlChart ID="rhcUcrTotalsOverTime" Width="610" Height="320" runat="server">
				<Legend>
					<Appearance BackgroundColor="White" Position="Right" Visible="true">
						<TextStyle Margin="0 0 100 10" />
					</Appearance>
				</Legend>

				<PlotArea>
					<XAxis MajorTickType="None" DataLabelsField="OccurredDate" Type="Date" BaseUnit="Months" Color="#E4E4E4">
						<MinorGridLines Visible="False" />
						<MajorGridLines Visible="False" />
						<LabelsAppearance RotationAngle="-45" Color="black" />
					</XAxis>

					<YAxis MinValue="1" Visible="True" MajorTickType="None" Color="white" >
						<MajorGridLines Color="#E4E4E4" />
						<MinorGridLines Visible="False" />
						<LabelsAppearance Visible="true" Color="#CCCCCC" />
					</YAxis>

					<Series>
						<telerik:LineSeries Name="Criminal Homicide" DataFieldY="CriminalHomicide">
							<LineAppearance Width="2" LineStyle="Normal" />
							<MarkersAppearance Visible="true" MarkersType="Circle" Size="5"/>
							<TooltipsAppearance Color="White">
								<ClientTemplate>
									#=kendo.format(\'{0:M/d/yyyy}\', dataItem.OccurredDate)# - #=dataItem.CriminalHomicide# reports
								</ClientTemplate>
								<%--									
								<ClientTemplate>
									#=kendo.format(\'{0:M/d/yyyy}\', dataItem.OccurredDate)# - #=kendo.format(\'{0:N}\', dataItem.CriminalHomicide)#
								</ClientTemplate>--%>
							</TooltipsAppearance>
							<LabelsAppearance Visible="false" />
						</telerik:LineSeries>

						<telerik:LineSeries Name="Forcible Rape" DataFieldY="ForcibleRape" Visible="false">
							<LineAppearance Width="2" />
							<MarkersAppearance Visible="true" MarkersType="Circle" Size="5" />
							<TooltipsAppearance Color="White">
								<ClientTemplate>
									#=kendo.format(\'{0:M/d/yyyy}\', dataItem.OccurredDate)# - #=dataItem.ForcibleRape# reports
								</ClientTemplate>
							</TooltipsAppearance>
							<LabelsAppearance Visible="false" />
						</telerik:LineSeries>

						<telerik:LineSeries Name="Robbery" DataFieldY="Robbery" Visible="false">
							<LineAppearance Width="2" />
							<MarkersAppearance Visible="true" MarkersType="Circle" Size="5" />
							<TooltipsAppearance Color="White">
								<ClientTemplate>
									#=kendo.format(\'{0:M/d/yyyy}\', dataItem.OccurredDate)# - #=dataItem.Robbery# reports
								</ClientTemplate>
							</TooltipsAppearance>
							<LabelsAppearance Visible="false" />
						</telerik:LineSeries>

						<telerik:LineSeries Name="Aggravated Assault" DataFieldY="AggravatedAssault" Visible="false">
							<LineAppearance Width="2" />
							<MarkersAppearance Visible="true" MarkersType="Circle" Size="5" />
							<TooltipsAppearance Color="White">
								<ClientTemplate>
									#=kendo.format(\'{0:M/d/yyyy}\', dataItem.OccurredDate)# - #=dataItem.AggravatedAssault# reports
								</ClientTemplate>
							</TooltipsAppearance>
							<LabelsAppearance Visible="false" />
						</telerik:LineSeries>

						<telerik:LineSeries Name="Burglary" DataFieldY="Burglary" Visible="false">
							<LineAppearance Width="2" />
							<MarkersAppearance Visible="true" MarkersType="Circle" Size="5" />
							<TooltipsAppearance Color="White">
								<ClientTemplate>
									#=kendo.format(\'{0:M/d/yyyy}\', dataItem.OccurredDate)# - #=dataItem.Burglary# reports
								</ClientTemplate>
							</TooltipsAppearance>
							<LabelsAppearance Visible="false" />
						</telerik:LineSeries>

						<telerik:LineSeries Name="Larceny Theft" DataFieldY="LarcenyTheft" Visible="false">
							<LineAppearance Width="2" />
							<MarkersAppearance Visible="true" MarkersType="Circle" Size="5" />
							<TooltipsAppearance Color="White">
								<ClientTemplate>
									#=kendo.format(\'{0:M/d/yyyy}\', dataItem.OccurredDate)# - #=dataItem.LarcenyTheft# reports
								</ClientTemplate>
							</TooltipsAppearance>
							<LabelsAppearance Visible="false" />
						</telerik:LineSeries>

						<telerik:LineSeries Name="Motor Vehicle Theft" DataFieldY="MotorVehicleTheft" Visible="false">
							<LineAppearance Width="2" />
							<MarkersAppearance Visible="true" MarkersType="Circle" Size="5" />
							<TooltipsAppearance Color="White">
								<ClientTemplate>
									#=kendo.format(\'{0:M/d/yyyy}\', dataItem.OccurredDate)# - #=dataItem.MotorVehicleTheft# reports
								</ClientTemplate>
							</TooltipsAppearance>
							<LabelsAppearance Visible="false" />
						</telerik:LineSeries>

						<telerik:LineSeries Name="Arson" DataFieldY="Arson" Visible="false">
							<LineAppearance Width="2" />
							<MarkersAppearance Visible="true" MarkersType="Circle" Size="5" />
							<TooltipsAppearance Color="White">
								<ClientTemplate>
									#=kendo.format(\'{0:M/d/yyyy}\', dataItem.OccurredDate)# - #=dataItem.Arson# reports
								</ClientTemplate>
							</TooltipsAppearance>
							<LabelsAppearance Visible="false" />
						</telerik:LineSeries>
					</Series>
				
				</PlotArea>
				
			</telerik:RadHtmlChart>

			<p class="actionButton">
				<a href="#" title="Click to save chart" onclick="exportRadHtmlChart('rhcUcrTotalsOverTime', 'ucrTotalsOverTimeChart');return false;"><i class="fa fa-bar-chart-o fa-fw"></i>Save</a>
			</p>
		</dd>
	</dl>



	<!-- comparison -->
	<dl class="box subtext" style="width: 650px; height: 445px;">
		<dt>
			<div class="description">
				<h3>UCR Period Comparison</h3>
				Compare this year or quarter to the last
			</div>
		</dt>
		<dd>
			<div>
				<span style="margin-right: 10px;">
					<span class="label">Type:</span>
					<asp:DropDownList ID="ddlCompareType" AutoPostBack="true" OnSelectedIndexChanged="ddlCompareType_SelectedIndexChanged" runat="server">
						<asp:ListItem Value="1" Text="Criminal Homicide" />
						<asp:ListItem Value="2" Text="Forcible Rape" />
						<asp:ListItem Value="3" Text="Robbery" />
						<asp:ListItem Value="4" Text="Aggravated Assault" />
						<asp:ListItem Value="5" Text="Burglary" />
						<asp:ListItem Value="6" Text="Larceny Theft" />
						<asp:ListItem Value="7" Text="Motor Vehicle Theft" />
						<asp:ListItem Value="8" Text="Arson" />
					</asp:DropDownList>
				</span>

				<span class="label">Compare:</span>
				<asp:DropDownList ID="ddlComparePeriod" AutoPostBack="true" OnSelectedIndexChanged="ddlComparePeriod_SelectedIndexChanged" runat="server">
					<asp:ListItem Value="5" Text="This Year to Last Year" />
					<asp:ListItem Value="3" Text="This Quarter to Last Quarter" />
				</asp:DropDownList>
			</div>

			<telerik:RadHtmlChart ID="rhcUcrPeriodComparison" Width="610" Height="300" runat="server">
				<Legend>
					<Appearance BackgroundColor="White" Position="Right" Visible="true">
						<TextStyle Margin="0 0 100 10" />
					</Appearance>
				</Legend>

				<PlotArea>
					<XAxis MajorTickType="None" Color="#E4E4E4">
						<MinorGridLines Visible="False" />
						<MajorGridLines Visible="False" />
						<LabelsAppearance RotationAngle="-45" Color="black" />
					</XAxis>

					<YAxis MinValue="1" Visible="True" MajorTickType="None" Color="white">
						<MajorGridLines Color="#E4E4E4" />
						<MinorGridLines Visible="False" />
						<LabelsAppearance Visible="true" Color="#CCCCCC" />
					</YAxis>

					<Series>

						<telerik:LineSeries Name="This Quarter" DataFieldY="ThisPeriod">
							<Appearance>
								<FillStyle BackgroundColor="#4F81BD" />
							</Appearance>
							<LineAppearance Width="2" />
							<MarkersAppearance Visible="true" MarkersType="Circle" Size="5" />
							<TooltipsAppearance Color="White">
								<ClientTemplate>
									#=value# reports
								</ClientTemplate>
							</TooltipsAppearance>
							<LabelsAppearance Visible="false" />
						</telerik:LineSeries>
											
						<telerik:LineSeries Name="Last Quarter" DataFieldY="LastPeriod">
							<Appearance>
								<FillStyle BackgroundColor="#98B954" />
							</Appearance>
							<LineAppearance Width="2" LineStyle="Normal" />
							<MarkersAppearance Visible="true" MarkersType="Circle" Size="5" />
							<TooltipsAppearance Color="White">
								<ClientTemplate>
									#=value# reports
								</ClientTemplate>
							</TooltipsAppearance>
							<LabelsAppearance Visible="false" />
						</telerik:LineSeries>

						
					</Series>

				</PlotArea>

			</telerik:RadHtmlChart>

			<p class="actionButton">
				<a href="#" title="Click to save chart" onclick="exportRadHtmlChart('rhcUcrPeriodComparison', 'ucrPeriodComparisonChart');return false;"><i class="fa fa-bar-chart-o fa-fw"></i>Save</a>
			</p>
		</dd>
	</dl>


	<p style="clear: both;">&nbsp;</p>


	<telerik:RadAjaxManager ID="radAjaxManager" runat="server">
		<AjaxSettings>

			<telerik:AjaxSetting AjaxControlID="ddlPeriod">
				<UpdatedControls>
					<telerik:AjaxUpdatedControl ControlID="rhcUcrTotals" />
					<telerik:AjaxUpdatedControl ControlID="rhcUcrTotalsOverTime" />
				</UpdatedControls>
			</telerik:AjaxSetting>

			<telerik:AjaxSetting AjaxControlID="ddlCompareType">
				<UpdatedControls>
					<telerik:AjaxUpdatedControl ControlID="rhcUcrPeriodComparison" />
				</UpdatedControls>
			</telerik:AjaxSetting>

			<telerik:AjaxSetting AjaxControlID="ddlComparePeriod">
				<UpdatedControls>
					<telerik:AjaxUpdatedControl ControlID="rhcUcrPeriodComparison" />
				</UpdatedControls>
			</telerik:AjaxSetting>

		</AjaxSettings>
	</telerik:RadAjaxManager>

	<telerik:RadClientExportManager ID="radClientExportManager" runat="server">
		<PdfSettings FileName="UcrChart.pdf" />
	</telerik:RadClientExportManager>

	<telerik:RadScriptBlock ID="radScriptBlock" runat="server">
		<script type="text/javascript">

			var proxyUrl = '/Services/TelerikPdfExport.aspx';

			function pageLoad() {
				adjustChartDateLabels();

				setProxyUrl();
			}

			//helper to fix too-many-labels problem w/ charts
			function adjustChartDateLabels() {
				var kendoWidget = $find("<%=rhcUcrTotalsOverTime.ClientID%>").get_kendoWidget(); //newer versions of control
				
				var rowCount = kendoWidget._plotArea.categoryAxis.labels.length; //the number of date-rows in data source
				//alert('count: ' + rowCount); //the number of date-rows. 

				if (rowCount > 15) {
					var newStep = Math.max(1, Math.round(100 / rowCount));
					//alert('newStep: ' + newStep);

					switch (newStep) {
						case 1:
							if (rowCount > 700)
								newStep = 60;
							else if (rowCount > 300)
								newStep = 40;
							else
								newStep = 20;
							break;

						case 2:
							newStep = 15;
							break;
					}
					//alert(newStep);

					kendoWidget.options.categoryAxis.labels.step = newStep;
					kendoWidget.redraw();
				}
			}

			function exportRadHtmlChart(targetName, filename) {
				var actualName = $('div[id$="' + targetName + '"]')[0].id;
				//alert(actualName);

				var radClientExportManager = $find('<%=radClientExportManager.ClientID%>');

				var pdfSettings = {
					fileName: filename + ".pdf",
					proxyURL: proxyUrl
				};

				radClientExportManager.set_pdfSettings(pdfSettings); //uncommenting this causes Safari to reload page instead of get pdf
				radClientExportManager.exportPDF($("#" + actualName));
			}

			function setProxyUrl()
			{
				var parts = window.location.pathname.split('/');
				//alert(parts[1]);

				//check if running as sub-application in IIS (otherwise running as main app in IISExpress
				if (parts[1] == 'IncidentReports')
					proxyUrl = '/IncidentReports' + proxyUrl;
			}

		</script>
	</telerik:RadScriptBlock>

</asp:Content>
