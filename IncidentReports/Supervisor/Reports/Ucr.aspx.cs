﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReportHq.IncidentReports.Controls;
//using Telerik.Web.UI;

namespace ReportHq.IncidentReports.Supervisor.Reports
{
	public partial class Ucr : Bases.NormalPage
	{
		#region Variables

		Reporting.UcrPeriods _ucrPeriod;
		int _comparisonOffenseTypeID;
		Reporting.UcrPeriods _comparisonPeriod;
		
		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Event Handlers

		protected void Page_Load(object sender, EventArgs e)
		{
			this.Heading = "UCR Report";
			this.BreadCrumbs.Add((new BreadCrumb("Supervisor Tools", ResolveUrl("~/Supervisor/"), "to Supervisor Tools")));
			this.HelpUrl = "~/Supervisor/Help/Default.html";

			_ucrPeriod = (Reporting.UcrPeriods)Int32.Parse(ddlPeriod.SelectedValue);
			_comparisonOffenseTypeID = Int32.Parse(ddlCompareType.SelectedValue);
			_comparisonPeriod = (Reporting.UcrPeriods)Int32.Parse(ddlComparePeriod.SelectedValue);

			if (!IsPostBack)
			{
				BindCharts();
				BindComparisonChart();
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void ddlPeriod_SelectedIndexChanged(object sender, EventArgs e)
		{
			_ucrPeriod = (Reporting.UcrPeriods)Int32.Parse(ddlPeriod.SelectedValue);

			BindCharts();
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void ddlComparePeriod_SelectedIndexChanged(object sender, EventArgs e)
		{
			_comparisonPeriod = (Reporting.UcrPeriods)Int32.Parse(ddlComparePeriod.SelectedValue);

			BindComparisonChart();
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void ddlCompareType_SelectedIndexChanged(object sender, EventArgs e)
		{
			_comparisonOffenseTypeID = Int32.Parse(ddlCompareType.SelectedValue);

			BindComparisonChart();
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Private Methods

		private void BindCharts()
		{
			//TODO: come up w/ a better private method to set the below formatting strings

			int i; 

			//adjust chart unit/labels
			switch (_ucrPeriod)
			{
				case Reporting.UcrPeriods.ThisMonth:
				case Reporting.UcrPeriods.LastMonth:
					rhcUcrTotalsOverTime.PlotArea.XAxis.BaseUnit = Telerik.Web.UI.HtmlChart.DateTimeBaseUnit.Days;
					
					for (i = 0; i < 8; i++)
					{
						rhcUcrTotalsOverTime.PlotArea.Series[0].TooltipsAppearance.ClientTemplate = "#=kendo.format(\\'{0:M/d/yyyy}\\', dataItem.OccurredDate)# - #=dataItem.CriminalHomicide# reports";
					}
					break;

				case Reporting.UcrPeriods.LastThreeMonths:
					rhcUcrTotalsOverTime.PlotArea.XAxis.BaseUnit = Telerik.Web.UI.HtmlChart.DateTimeBaseUnit.Weeks;
					
					for (i = 0; i < 8; i++)
					{
						rhcUcrTotalsOverTime.PlotArea.Series[0].TooltipsAppearance.ClientTemplate = "Week #=kendo.format(\\'{0:M/d/yyyy}\\', dataItem.OccurredDate)#, #=dataItem.CriminalHomicide# reports";
					}
					break;

				case Reporting.UcrPeriods.ThisYear:
				case Reporting.UcrPeriods.LastYear:
				case Reporting.UcrPeriods.LastThreeYears:
					rhcUcrTotalsOverTime.PlotArea.XAxis.BaseUnit = Telerik.Web.UI.HtmlChart.DateTimeBaseUnit.Months;
					
					for (i=0; i < 8; i++) {
						rhcUcrTotalsOverTime.PlotArea.Series[i].TooltipsAppearance.ClientTemplate = "#=kendo.format(\\'{0:MMM-yy}\\', dataItem.OccurredDate)#, #=dataItem.CriminalHomicide# reports";
					}
					break;
			}

			//get data
			DataSet ds = Reporting.UcrTotals(_ucrPeriod);

			rhcUcrTotals.DataSource = ds.Tables[0];
			rhcUcrTotalsOverTime.DataSource = ds.Tables[1];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		private void BindComparisonChart()
		{
			Telerik.Web.UI.HtmlChart.PlotArea.HtmlChartPlotArea plotArea = rhcUcrPeriodComparison.PlotArea;

			Telerik.Web.UI.SeriesBase series0 = rhcUcrPeriodComparison.PlotArea.Series[0];
			Telerik.Web.UI.SeriesBase series1 = rhcUcrPeriodComparison.PlotArea.Series[1];

			if (_comparisonPeriod == Reporting.UcrPeriods.LastYear)
			{
				plotArea.XAxis.DataLabelsField = "Month"; //show us month labels

				series0.Name = "This Year";
				series1.Name = "Last Year";

				series0.TooltipsAppearance.ClientTemplate = "#=dataItem.Month#, #=dataItem.ThisPeriod# reports";
				series1.TooltipsAppearance.ClientTemplate = "#=dataItem.Month#, #=dataItem.LastPeriod# reports";
			}
			else //quarter
			{
				series0.Name = "This Quarter";
				series1.Name = "Last Quarter";

				series0.TooltipsAppearance.ClientTemplate = "#=value# reports";
				series1.TooltipsAppearance.ClientTemplate = "#=value# reports";
			}

			rhcUcrPeriodComparison.DataSource = Reporting.UcrPeriodComparisons(_comparisonOffenseTypeID, _comparisonPeriod);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		private void SetClientTemplate(Telerik.Web.UI.HtmlChart.PlotArea.SeriesCollection seriesCollection)
		{
			//seriesCollection[0].TooltipsAppearance

			foreach (Telerik.Web.UI.HtmlChart.PlotArea.Series.SeriesItems.SeriesItemBase series in seriesCollection)
			{
				
			}			
		}

		#endregion

		
	}
}