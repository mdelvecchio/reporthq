﻿<%@ Page Title="Supervisor Tools" Language="C#" MasterPageFile="~/IncidentReports.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ReportHq.IncidentReports.Supervisor.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<link rel="stylesheet" href="../css/boxes.css" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="phContent" runat="server">

	<div style="width:700px; margin-left:auto; margin-right:auto; margin-bottom:20px;">
		<a href="ReviewReports.aspx" title="Review Reports" class="flatButton med"><i class="fa fa-search fa-fw"></i> Review Reports</a>
		<a href="ManageBulletins.aspx" title="Manage Bulletins" class="flatButton med"><i class="fa fa-file-text fa-fw"></i> Manage Bulletins</a>
		<a href="Reports/Ucr.aspx" title="UCR Reports" class="flatButton med"><i class="fa fa-bar-chart-o fa-fw"></i> UCR Report</a>
	</div>

	<!-- pending reports -->
	<dl class="box subtext">
		<dt>
			<div class="description">
				<h3><a href="YourAnalytics.aspx">Pending District Reports</a></h3>
				Reports awaiting supervisor review
			</div>
		</dt>
		<dd>
			<telerik:RadHtmlChart ID="rcPendingReports" Width="400" Height="220" Transitions="true" OnClientSeriesClicked="NavToSearch" runat="server">
				<Legend>
					<Appearance BackgroundColor="White" Position="Bottom" Visible="true"></Appearance>
				</Legend>

				<PlotArea>					
					<XAxis Color="#E4E4E4" MajorTickType="None">
						<MajorGridLines Visible="false" />
						<MinorGridLines Visible="false" />
					</XAxis>
					<YAxis Color="white" Visible="true" MajorTickType="None">
						<MajorGridLines Color="#E4E4E4" />
						<MinorGridLines Visible="false" />
						<LabelsAppearance Skip="1">
							<TextStyle Color="#CCCCCC" FontSize="10px" />
						</LabelsAppearance>
					</YAxis>

					<Series>
						<telerik:ColumnSeries Name="District 1|1|3" DataFieldY="District1">
							<LabelsAppearance Visible="true" />
							<TooltipsAppearance Color="White">
								<ClientTemplate>
									#=series.name.split("|")[0]# - #=value# reports. Click to view
								</ClientTemplate>
							</TooltipsAppearance>
						</telerik:ColumnSeries>

						<telerik:ColumnSeries Name="District 2|2|3" DataFieldY="District2">
							<LabelsAppearance Visible="true" />
							<TooltipsAppearance Color="White">
								<ClientTemplate>
									#=series.name.split("|")[0]# - #=value# reports. Click to view
								</ClientTemplate>
							</TooltipsAppearance>
						</telerik:ColumnSeries>

						<telerik:ColumnSeries Name="District 3|3|3" DataFieldY="District3">
							<LabelsAppearance Visible="true" />
							<TooltipsAppearance Color="White">
								<ClientTemplate>
									#=series.name.split("|")[0]# - #=value# reports. Click to view
								</ClientTemplate>
							</TooltipsAppearance>
						</telerik:ColumnSeries>

						<telerik:ColumnSeries Name="District 4|4|3" DataFieldY="District4">
							<LabelsAppearance Visible="true" />
							<TooltipsAppearance Color="White">
								<ClientTemplate>
									#=series.name.split("|")[0]# - #=value# reports. Click to view
								</ClientTemplate>
							</TooltipsAppearance>
						</telerik:ColumnSeries>

						<telerik:ColumnSeries Name="District 5|5|3" DataFieldY="District5">
							<LabelsAppearance Visible="true" />
							<TooltipsAppearance Color="White">
								<ClientTemplate>
									#=series.name.split("|")[0]# - #=value# reports. Click to view
								</ClientTemplate>
							</TooltipsAppearance>
						</telerik:ColumnSeries>

						<telerik:ColumnSeries Name="District 6|6|3" DataFieldY="District6">
							<LabelsAppearance Visible="true" />
							<TooltipsAppearance Color="White">
								<ClientTemplate>
									#=series.name.split("|")[0]# - #=value# reports. Click to view
								</ClientTemplate>
							</TooltipsAppearance>
						</telerik:ColumnSeries>

						<telerik:ColumnSeries Name="District 7|7|3" DataFieldY="District7">
							<LabelsAppearance Visible="true" />
							<TooltipsAppearance Color="White">
								<ClientTemplate>
									#=series.name.split("|")[0]# - #=value# reports. Click to view
								</ClientTemplate>
							</TooltipsAppearance>
						</telerik:ColumnSeries>

						<telerik:ColumnSeries Name="District 8|8|3" DataFieldY="District8">
							<LabelsAppearance Visible="true" />
							<TooltipsAppearance Color="White">
								<ClientTemplate>
									#=series.name.split("|")[0]# - #=value# reports. Click to view
								</ClientTemplate>
							</TooltipsAppearance>
						</telerik:ColumnSeries>
					</Series>
				</PlotArea>
			</telerik:RadHtmlChart>
		</dd>
	</dl>


	<!-- rejected reports -->
	<dl class="box subtext">
		<dt>
			<div class="description">
				<h3><a href="YourAnalytics.aspx">Rejected District Reports</a></h3>
				Reports currently rejected by supervisors
			</div>
		</dt>
		<dd>
			<telerik:RadHtmlChart ID="rcRejectedReports" Width="400" Height="220" Transitions="true" OnClientSeriesClicked="NavToSearch" runat="server">
				<Legend>
					<Appearance BackgroundColor="White" Position="Bottom" Visible="true" />
				</Legend>

				<PlotArea>					
					<XAxis Color="#E4E4E4" MajorTickType="None">
						<MajorGridLines Visible="false" />
						<MinorGridLines Visible="false" />
					</XAxis>
					<YAxis Color="white" Visible="true" MajorTickType="None">
						<MajorGridLines Color="#E4E4E4" />
						<MinorGridLines Visible="false" />
						<LabelsAppearance Skip="1">
							<TextStyle Color="#CCCCCC" FontSize="10px" />
						</LabelsAppearance>
					</YAxis>

					<Series>
						<telerik:ColumnSeries Name="District 1|1|2" DataFieldY="District1">
							<LabelsAppearance Visible="true" />
							<TooltipsAppearance Color="White">
								<ClientTemplate>
									#=series.name.split("|")[0]# - #=value# reports. Click to view
								</ClientTemplate>
							</TooltipsAppearance>
						</telerik:ColumnSeries>

						<telerik:ColumnSeries Name="District 2|2|2" DataFieldY="District2">
							<LabelsAppearance Visible="true" />
							<TooltipsAppearance Color="White">
								<ClientTemplate>
									#=series.name.split("|")[0]# - #=value# reports. Click to view
								</ClientTemplate>
							</TooltipsAppearance>
						</telerik:ColumnSeries>

						<telerik:ColumnSeries Name="District 3|3|2" DataFieldY="District3">
							<LabelsAppearance Visible="true" />
							<TooltipsAppearance Color="White">
								<ClientTemplate>
									#=series.name.split("|")[0]# - #=value# reports. Click to view
								</ClientTemplate>
							</TooltipsAppearance>
						</telerik:ColumnSeries>

						<telerik:ColumnSeries Name="District 4|4|2" DataFieldY="District4">
							<LabelsAppearance Visible="true" />
							<TooltipsAppearance Color="White">
								<ClientTemplate>
									#=series.name.split("|")[0]# - #=value# reports. Click to view
								</ClientTemplate>
							</TooltipsAppearance>
						</telerik:ColumnSeries>

						<telerik:ColumnSeries Name="District 5|5|2" DataFieldY="District5">
							<LabelsAppearance Visible="true" />
							<TooltipsAppearance Color="White">
								<ClientTemplate>
									#=series.name.split("|")[0]# - #=value# reports. Click to view
								</ClientTemplate>
							</TooltipsAppearance>
						</telerik:ColumnSeries>

						<telerik:ColumnSeries Name="District 6|6|2" DataFieldY="District6">
							<LabelsAppearance Visible="true" />
							<TooltipsAppearance Color="White">
								<ClientTemplate>
									#=series.name.split("|")[0]# - #=value# reports. Click to view
								</ClientTemplate>
							</TooltipsAppearance>
						</telerik:ColumnSeries>

						<telerik:ColumnSeries Name="District 7|7|2" DataFieldY="District7">
							<LabelsAppearance Visible="true" />
							<TooltipsAppearance Color="White">
								<ClientTemplate>
									#=series.name.split("|")[0]# - #=value# reports. Click to view
								</ClientTemplate>
							</TooltipsAppearance>
						</telerik:ColumnSeries>

						<telerik:ColumnSeries Name="District 8|8|2" DataFieldY="District8">
							<LabelsAppearance Visible="true" />
							<TooltipsAppearance Color="White">
								<ClientTemplate>
									#=series.name.split("|")[0]# - #=value# reports. Click to view
								</ClientTemplate>
							</TooltipsAppearance>
						</telerik:ColumnSeries>
					</Series>
				</PlotArea>
			</telerik:RadHtmlChart>
		</dd>
	</dl>


	<!-- approved reports -->
	<dl class="box subtext">
		<dt>
			<div class="description">
				<h3><a href="YourAnalytics.aspx">Approved District Reports</a></h3>
				Reports approved by supervisors in last 90-days
			</div>
		</dt>
		<dd>
			<telerik:RadHtmlChart ID="rcApprovedReports" Width="400" Height="220" Transitions="true" runat="server">
				<Legend>
					<Appearance BackgroundColor="White" Position="Bottom" Visible="true" />
				</Legend>

				<PlotArea>					
					<XAxis Color="#E4E4E4" MajorTickType="None">
						<MajorGridLines Visible="false" />
						<MinorGridLines Visible="false" />
					</XAxis>
					<YAxis Color="white" Visible="true" MajorTickType="None">
						<MajorGridLines Color="#E4E4E4" />
						<MinorGridLines Visible="false" />
						<LabelsAppearance Skip="1">
							<TextStyle Color="#CCCCCC" FontSize="10px" />
						</LabelsAppearance>
					</YAxis>

					<Series>
						<telerik:ColumnSeries Name="District 1" DataFieldY="District1">
							<LabelsAppearance DataFormatString="{0:N0}" />
							<TooltipsAppearance Color="White">
								<ClientTemplate>
									#=series.name# - #=kendo.format(\'{0:N0}\', value)# reports
								</ClientTemplate>
							</TooltipsAppearance>
						</telerik:ColumnSeries>

						<telerik:ColumnSeries Name="District 2" DataFieldY="District2">
							<LabelsAppearance DataFormatString="{0:N0}" />
							<TooltipsAppearance Color="White">
								<ClientTemplate>
									#=series.name# - #=kendo.format(\'{0:N0}\', value)# reports
								</ClientTemplate>
							</TooltipsAppearance>
						</telerik:ColumnSeries>

						<telerik:ColumnSeries Name="District 3" DataFieldY="District3">
							<LabelsAppearance DataFormatString="{0:N0}" />
							<TooltipsAppearance Color="White">
								<ClientTemplate>
									#=series.name# - #=kendo.format(\'{0:N0}\', value)# reports
								</ClientTemplate>
							</TooltipsAppearance>
						</telerik:ColumnSeries>

						<telerik:ColumnSeries Name="District 4" DataFieldY="District4">
							<LabelsAppearance DataFormatString="{0:N0}" />
							<TooltipsAppearance Color="White">
								<ClientTemplate>
									#=series.name# - #=kendo.format(\'{0:N0}\', value)# reports
								</ClientTemplate>
							</TooltipsAppearance>
						</telerik:ColumnSeries>

						<telerik:ColumnSeries Name="District 5" DataFieldY="District5">
							<LabelsAppearance DataFormatString="{0:N0}" />
							<TooltipsAppearance Color="White">
								<ClientTemplate>
									#=series.name# - #=kendo.format(\'{0:N0}\', value)# reports
								</ClientTemplate>
							</TooltipsAppearance>
						</telerik:ColumnSeries>

						<telerik:ColumnSeries Name="District 6" DataFieldY="District6">
							<LabelsAppearance DataFormatString="{0:N0}" />
							<TooltipsAppearance Color="White">
								<ClientTemplate>
									#=series.name# - #=kendo.format(\'{0:N0}\', value)# reports
								</ClientTemplate>
							</TooltipsAppearance>
						</telerik:ColumnSeries>

						<telerik:ColumnSeries Name="District 7" DataFieldY="District7">
							<LabelsAppearance DataFormatString="{0:N0}" />
							<TooltipsAppearance Color="White">
								<ClientTemplate>
									#=series.name# - #=kendo.format(\'{0:N0}\', value)# reports
								</ClientTemplate>
							</TooltipsAppearance>
						</telerik:ColumnSeries>

						<telerik:ColumnSeries Name="District 8" DataFieldY="District8">
							<LabelsAppearance DataFormatString="{0:N0}" />
							<TooltipsAppearance Color="White">
								<ClientTemplate>
									#=series.name# - #=kendo.format(\'{0:N0}\', value)# reports
								</ClientTemplate>
							</TooltipsAppearance>
						</telerik:ColumnSeries>
					</Series>
				</PlotArea>
			</telerik:RadHtmlChart>
		</dd>
	</dl>

	<div style="float: left;">

		<telerik:RadHtmlChart runat="server" ID="rcTotalReports" Width="500" Height="390" Transitions="true">
			<ChartTitle Text="Total District Reports">
				<Appearance Align="Left">
					<TextStyle FontFamily="Open Sans,sans-serif" FontSize="16" Bold="true" Color="#2E3E50" />
				</Appearance>
			</ChartTitle>
			<Legend>
				<Appearance Position="Right" Visible="true" />
			</Legend>

			<PlotArea>
				<Series>
					<telerik:PieSeries NameField="District" DataFieldY="Percentage">
					
						<LabelsAppearance Position="OutsideEnd" Visible="true" Color="Black">
							<ClientTemplate>
								#= kendo.format(\'{0:N0}\', dataItem.Total)#
							</ClientTemplate>
						</LabelsAppearance>

						<TooltipsAppearance Color="White">
							<ClientTemplate>
								#= dataItem.District# - #= dataItem.Percentage#%
							</ClientTemplate>
						</TooltipsAppearance>

					</telerik:PieSeries>
				</Series>
			</PlotArea>
		</telerik:RadHtmlChart>

	</div>



	<p style="clear:both;">&nbsp;</p>

	<script type="text/javascript">
		function pageLoad() {
			var chart = $find('<%= rcPendingReports.ClientID %>');
			chart._chartObject.options.legend.labels.template = "#= series.name.split('|')[0] #"; //"District 7|7|1" - first part is the friendly name, "District 7"
			chart.repaint();

			chart = $find('<%= rcRejectedReports.ClientID %>');
			chart._chartObject.options.legend.labels.template = "#= series.name.split('|')[0] #";
			chart.repaint();
		}
		
		function NavToSearch(sender, args) {
			var parts = args.get_seriesName().split('|'); //"District 7|7|1" - first part is the friendly name, "District 7", second is the district number, third is the status type
			window.location.href = 'ReviewReports.aspx?status=' + +parts[2] + '&district=' + parts[1];
		}
	</script>

</asp:Content>
