﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReportHq.IncidentReports.Controls;
using Telerik.Web.UI;

namespace ReportHq.IncidentReports.Supervisor
{
	public partial class ManageBulletins : Bases.NormalPage
	{
		#region Event Handlers

		protected void Page_Load(object sender, EventArgs e)
		{
			this.BreadCrumbs.Add((new BreadCrumb("Supervisor Tools", ResolveUrl("~/Supervisor/"), "to Supervisor Tools")));
			this.HelpUrl = "http://www.google.com"; //temp
			this.Heading = "Manage Bulletins";

			if (!string.IsNullOrEmpty(Request.QueryString["created"]))
				base.RenderUserMessage(Enums.UserMessageTypes.Success, ("Bulletin created!"));

			if (!string.IsNullOrEmpty(Request.QueryString["saved"]))
				base.RenderUserMessage(Enums.UserMessageTypes.Success, ("Bulletin saved!"));
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Fired when grid needs a datasource. eg, after editing or deleting.
		/// </summary>
		protected void gridBulletins_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
		{
			DataTable results = Bulletin.GetAllBulletins();

			gridBulletins.DataSource = results;

			lblRecordCount.Text = string.Format("There are <b>{0:N0}</b> Bulletins:<br/><br/>", results.Rows.Count);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void gridBulletins_ItemUpdated(object sender, GridUpdatedEventArgs e)
		{
			GridEditableItem item = (GridEditableItem)e.Item;

			int bulletinID = (int)item.GetDataKeyValue("ID");

			//int x = 1;
		}
				
		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		
		#endregion
	}
}