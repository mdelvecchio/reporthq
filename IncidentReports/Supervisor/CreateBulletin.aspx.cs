﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReportHq.Common;
using ReportHq.IncidentReports.Controls;

namespace ReportHq.IncidentReports.Supervisor
{
	public partial class CreateBulletin : Bases.NormalPage
	{
		#region Event Handlers

		protected void Page_Load(object sender, EventArgs e)
		{
			this.BreadCrumbs.Add((new BreadCrumb("Supervisor Tools", ResolveUrl("~/Supervisor/"), "to Supervisor Tools")));
			this.HelpUrl = "http://www.google.com"; //temp
			this.Heading = "Create Bulletin";			
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void lbSend_Click(object sender, EventArgs e)
		{
			Bulletin bulletin = new Bulletin();

			bulletin.TypeID = ucBulletinEditor.TypeID;
			bulletin.IsApb = ucBulletinEditor.IsApb;
			bulletin.Title = ucBulletinEditor.Title;
			bulletin.BulletinContent = ucBulletinEditor.BulletinContent;
			bulletin.RecipientRoleIDs = ucBulletinEditor.RecipientCheckedItems;

			//save it
			StatusInfo status = bulletin.Add(this.Username);

			if (status.Success)
			{
				//base.RenderUserMessage(Enums.UserMessageTypes.Success, ("Bulletin created"));

				Response.Redirect("ManageBulletins.aspx?created=1", false);  //false = prevents a ThreadAbortException from raising (which is expensive)
				HttpContext.Current.ApplicationInstance.CompleteRequest(); //stops all subsquent code from executing
			}
			else
			{
				base.RenderUserMessage(Enums.UserMessageTypes.Warning, ("Sorry, there was a problem saving: " + status.Message));
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}