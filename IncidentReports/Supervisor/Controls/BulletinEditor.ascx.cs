﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReportHq.IncidentReports.Controls;

namespace ReportHq.IncidentReports.Supervisor.Controls
{
	public partial class BulletinEditor : System.Web.UI.UserControl
	{
		#region Properties

		public int TypeID
		{
			get { return Int32.Parse(ddlTypes.SelectedValue); }
			set { ddlTypes.SelectedValue = value.ToString(); }
		}

		public bool IsApb
		{
			get { return cbIsApb.Checked; }
			set { cbIsApb.Checked = value; }
		}

		public String Title
		{
			get { return tbTitle.Text; }
			set { tbTitle.Text = value; }
		}

		public bool IsActive
		{
			get { return cbIsActive.Checked; }
			set { cbIsActive.Checked = value; }
		}

		public String BulletinContent
		{
			get { return radEditor.Content; }
			set { radEditor.Content = value; }
		}

		public List<DescriptionItem> RecipientCheckedItems
		{
			get { return Common.GetCheckedItems(cblUserRoles); }
			set
			{
				if (value.Count > 0)
					Common.SetCheckedItems(value, cblUserRoles);
			}
		}

		/// <summary>
		/// Whether to show the Is Active selector UI. Should not be visible for new Bulletins.
		/// </summary>
		public bool ShowActive
		{
			set { trActive.Visible = value; }
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Event Handlers

		protected void Page_Init(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				ddlTypes.DataSource = Bulletin.GetBulletinTypes();
				ddlTypes.DataBind();

				cblUserRoles.DataSource = UserManagement.GetUserRoles();
				cblUserRoles.DataBind();
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}