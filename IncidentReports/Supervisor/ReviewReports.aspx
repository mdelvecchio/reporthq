﻿<%@ Page Title="Review Reports" Language="C#" MasterPageFile="~/IncidentReports.Master" AutoEventWireup="true" CodeBehind="ReviewReports.aspx.cs" Inherits="ReportHq.IncidentReports.Supervisor.ReviewReports" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<link rel="stylesheet" href="../css/boxes.css" type="text/css" />
	<script type="text/javascript" src="../js/radComboBoxHelpers.js"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="phContent" runat="server">
	
	<!-- used by multiple RadDatePickers -->
	<telerik:RadCalendar ID="sharedCalendar" ShowRowHeaders="false" UseColumnHeadersAsSelectors="false" runat="server" />

	<dl class="box search" style="width:725px; margin-bottom:30px;">
		<dt><span class="label">SEARCH CRITERIA</span></dt>
		<dd>

			<table border="0">
				<tr>
					<td class="label">Approval Status:</td>
					<td>
						<Telerik:RadComboBox ID="rcbApprovalStatusTypes" DataValueField="ApprovalStatusTypeID" DataTextField="Description" AppendDataBoundItems="true" AllowCustomText="False" MarkFirstMatch="true" OnClientFocus="showDropDown" TabIndex="1" runat="server">
							<Items>
								<Telerik:RadComboBoxItem />
								<Telerik:RadComboBoxItem Value="100" Text="Non-Approved" Selected="true" />
							</Items>						
						</Telerik:RadComboBox>
					</td>
					<td style="width:25px;"></td>
					<td class="label">Car # Between:</td>
					<td><Telerik:RadNumericTextBox ID="rntbStartCarNumber" type="Number" MinValue="1" MaxValue="10999" MaxLength="5" NumberFormat-DecimalDigits="0" NumberFormat-GroupSeparator="" Width="75" TabIndex="4" runat="server"/>&nbsp;and &nbsp;<Telerik:RadNumericTextBox ID="rntbEndCarNumber" type="Number" MinValue="1" MaxValue="10999" MaxLength="5" NumberFormat-DecimalDigits="0" NumberFormat-GroupSeparator="" Width="75" TabIndex="5" runat="server"/></td>
				</tr>
				<tr>
					<td class="label">District:</td>
					<td><Telerik:RadComboBox ID="rcbDistrict" DataValueField="District" DataTextField="District" Width="85" AllowCustomText="False" MarkFirstMatch="true" TabIndex="2" runat="server" /></td>
					<td></td>
					<td class="label">Dates Between:</td>
					<td><Telerik:RadDatePicker id="rdpStartDate" Width="101" SharedCalendarID="sharedCalendar" TabIndex="6" Runat="server" />&nbsp;and &nbsp;<Telerik:RadDatePicker id="rdpEndDate" Width="101" SharedCalendarID="sharedCalendar" TabIndex="7" Runat="server" /></td>
				</tr>
				<tr>
					<td class="label">Platoon:</td>
					<td><Telerik:RadComboBox ID="rcbPlatoons" DataValueField="PlatoonType" DataTextField="PlatoonType" Width="85" MarkFirstMatch="true" OnClientFocus="showDropDown" TabIndex="3" runat="server" /></td>
					<td></td>
					<td class="label">Badge Number:</td>
					<td><Telerik:RadNumericTextBox ID="rntbBadgeNumber" type="Number" MinValue="1" MaxValue="10999" MaxLength="5" NumberFormat-DecimalDigits="0" NumberFormat-GroupSeparator="" Width="75" TabIndex="8" runat="server" /></td>
				</tr>
				<tr>
					<td class="label">Item Number:</td>
					<td>
						<Telerik:RadMaskedTextBox id="rmtbItemNumber" Mask="L-#####-##" ClientEvents-OnBlur="padNumberSection" Width="85" TabIndex="9" runat="server" />
						<asp:RegularExpressionValidator 
							id="revItemNumber" 
							ControlToValidate="rmtbItemNumber" 
							ErrorMessage="Invalid" 
							ValidationExpression="[A-L]-[0-9]{5}-[0-9]{2}" 
							Display="Dynamic" CssClass="error"
							runat="server" />
					</td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
			</table><br/>

			<asp:LinkButton ID="lbSubmit" CssClass="flatButton" OnClick="lbSubmit_Click" TabIndex="10" runat="server"><i class="fa fa-search fa-fw"></i> Find Reports</asp:LinkButton>

		</dd>
	</dl>

	
	<div id="divResults" visible="false" runat="server">

		<asp:Label ID="lblResultCount" runat="server" />

		<%--<Telerik:RadAjaxPanel ID="updResults" LoadingPanelID="ajaxLoadingPanel" runat="server">--%>
					
			<telerik:RadGrid ID="gridReports"
				Width="95%"
				AutoGenerateColumns="False"
				AllowPaging="true"
				PageSize="25"
				AllowSorting="true"
				AllowMultiRowSelection="false"
				AllowMultiRowEdit="false"
				EnableHeaderContextMenu="true"
				OnNeedDataSource="gridReports_NeedDataSource"
				OnDetailTableDataBind="gridReports_DetailTableDataBind"
				runat="server">

				<ClientSettings 
					EnableRowHoverStyle="true"
					AllowDragToGroup="false" 
					AllowColumnsReorder="false" 
					AllowKeyboardNavigation="true"
					ReorderColumnsOnClient="false" >
					<Resizing AllowColumnResize="false" AllowRowResize="false" />
					<Selecting AllowRowSelect="false" EnableDragToSelectRows="false" />
				</ClientSettings>

				<ExportSettings FileName="reviewReports" ExportOnlyData="true" IgnorePaging="true" HideStructureColumns="true" OpenInNewWindow="true">
					<Excel Format="ExcelML"></Excel>
				</ExportSettings>
			
				<MasterTableView 
					CommandItemDisplay="Bottom"
					DataKeyNames="ReportID" 
					NoMasterRecordsText="No Reports to display." 
					CommandItemStyle-Font-Bold="true" 
					HeaderStyle-ForeColor="#191970" 
					ItemStyle-CssClass="item" 
					AlternatingItemStyle-CssClass="item">
				
					<PagerStyle PageSizes="25,50,100" Position="TopAndBottom" />
					<CommandItemSettings ShowRefreshButton="false" ShowAddNewRecordButton="false" ShowExportToExcelButton="true" />
				
					<Columns>				
						<Telerik:GridTemplateColumn UniqueName="PdfIcon" AllowFiltering="false" ItemStyle-HorizontalAlign="center">
							<ItemTemplate>
								<a href='../print/ReportPdf.aspx?rid=<%# Eval("ReportID") %>'><img src="../images/pdf.gif" width="16" height="16" border="0" alt="View PDF" /></a>
							</ItemTemplate>
						</Telerik:GridTemplateColumn>
						<Telerik:GridHyperLinkColumn UniqueName="Review" DataNavigateUrlFields="ReportID" DataNavigateUrlFormatString="../Event/BasicInfo.aspx?rid={0}" DataTextField="ReportID" DataTextFormatString="Review" AllowFiltering="false" ItemStyle-HorizontalAlign="center" ItemStyle-Wrap="false" ItemStyle-CssClass="commandItem" />
						<Telerik:GridBoundColumn DataField="ReportID" Visible="false" />
						<Telerik:GridBoundColumn HeaderText="Item #" UniqueName="ItemNumber" DataField="ItemNumber" ShowFilterIcon="false" ItemStyle-Wrap="false" />
						<Telerik:GridBoundColumn HeaderText="Type" UniqueName="ReportType" DataField="ReportType" ItemStyle-Wrap="false" />
						<Telerik:GridBoundColumn HeaderText="Status" UniqueName="Status" DataField="ApprovalStatus" ItemStyle-Wrap="false" />
						<Telerik:GridBoundColumn HeaderText="Signal" UniqueName="Signal" DataField="Signal" HeaderStyle-Wrap="false" />
						<Telerik:GridBoundColumn HeaderText="Report Date" UniqueName="ReportDate" DataField="ReportDate" DataFormatString="{0:MM/dd/yyyy}" HeaderStyle-Wrap="false" />
						<Telerik:GridBoundColumn HeaderText="Last Modified" UniqueName="LastModifiedDate" DataField="LastModifiedDate" DataFormatString="{0:MM/dd/yyyy}" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" />
						<Telerik:GridBoundColumn HeaderText="District" UniqueName="District" DataField="District" HeaderStyle-HorizontalAlign="center" HeaderStyle-Wrap="false" ItemStyle-HorizontalAlign="center" />
						<Telerik:GridBoundColumn HeaderText="Platoon" UniqueName="Platoon" DataField="ReportingCarPlatoon" HeaderStyle-Wrap="false" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="center" />
						<Telerik:GridBoundColumn HeaderText="Officer 1" UniqueName="Officer1" DataField="Officer1Name" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" />
						<Telerik:GridBoundColumn HeaderText="Badge" UniqueName="Officer1Badge" DataField="Officer1Badge" HeaderStyle-Wrap="false" />
					</Columns>
				
					<DetailTables>
		
						<%-- PERSONS --%>
						<Telerik:GridTableView 
							Name="persons" 
							DataKeyNames="VictimPersonID" 
							AutoGenerateColumns="false" 
							AllowSorting="false" 
							Caption="VICTIM/PERSONS" 
							Width="100%" 
							NoDetailRecordsText="No Victims/Persons to display">
						
							<ParentTableRelation>
								<Telerik:GridRelationFields DetailKeyField="ReportID" MasterKeyField="ReportID" />
							</ParentTableRelation>
						
							<Columns>
								<Telerik:GridBoundColumn HeaderText="ReportID" DataField="ReportID" Visible="false" />
								<Telerik:GridBoundColumn HeaderText="PersonID" DataField="PersonID" Visible="false" />
								<Telerik:GridBoundColumn HeaderText="Victim #" DataField="VictimNumber" ItemStyle-Width="70" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="center" />
								<Telerik:GridBoundColumn HeaderText="Last Name" DataField="LastName" />
								<Telerik:GridBoundColumn HeaderText="First Name" DataField="FirstName" />
								<Telerik:GridBoundColumn HeaderText="Race" DataField="Race" />
								<Telerik:GridBoundColumn HeaderText="Gender" DataField="Gender" />
								<Telerik:GridBoundColumn HeaderText="DOB" DataField="DateOfBirth" DataFormatString="{0:MM/dd/yyyy}" />
							</Columns>
						</Telerik:GridTableView>
					
						<%-- OFFENDERS --%>
						<Telerik:GridTableView 
							Name="offenders" 
							DataKeyNames="OffenderID" 
							AutoGenerateColumns="false" 
							AllowSorting="false" 
							Caption="OFFENDERS" 
							Width="100%" 
							NoDetailRecordsText="No Offenders to display" 
							CssClass="setMargin">
						
							<ParentTableRelation>
								<Telerik:GridRelationFields DetailKeyField="ReportID" MasterKeyField="ReportID" />
							</ParentTableRelation>
							<Columns>
								<Telerik:GridBoundColumn HeaderText="ReportID" DataField="ReportID" Visible="false" />
								<Telerik:GridBoundColumn HeaderText="OffenderID" DataField="OffenderID" Visible="false" />
								<Telerik:GridBoundColumn HeaderText="Offender #" DataField="OffenderNumber" ItemStyle-Width="70" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="center" />
								<Telerik:GridBoundColumn HeaderText="Last Name" DataField="LastName" />
								<Telerik:GridBoundColumn HeaderText="First Name" DataField="FirstName" />
								<Telerik:GridBoundColumn HeaderText="Race" DataField="Race" />
								<Telerik:GridBoundColumn HeaderText="Gender" DataField="Gender" />
								<Telerik:GridBoundColumn HeaderText="DOB " DataField="DateOfBirth" DataFormatString="{0:MM/dd/yyyy}" />
							</Columns>
						</Telerik:GridTableView>

					</DetailTables>
				
				</MasterTableView>
			</Telerik:RadGrid>

		<%--</Telerik:RadAjaxPanel>--%>

	</div>

</asp:Content>
