﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReportHq.Common;
using ReportHq.IncidentReports.Controls;

namespace ReportHq.IncidentReports.Supervisor
{
	public partial class EditBulletin : Bases.NormalPage
	{
		#region Variables

		Bulletin _bulletin;

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Event Handlers

		protected void Page_Load(object sender, EventArgs e)
		{
			this.BreadCrumbs.Add((new BreadCrumb("Supervisor Tools", ResolveUrl("~/Supervisor/"), "to Supervisor Tools")));
			this.HelpUrl = "http://www.google.com"; //temp
			this.Heading = "Edit Bulletin";

			_bulletin = new Bulletin(Int32.Parse(Request.QueryString["bid"]));

			if (!IsPostBack)
				 SetFormValues();
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void lbSave_Click(object sender, EventArgs e)
		{
			SaveForm();
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Private Methods

		private void SetFormValues()
		{
			ucBulletinEditor.TypeID = _bulletin.TypeID;
			ucBulletinEditor.IsApb = _bulletin.IsApb;
			ucBulletinEditor.Title = _bulletin.Title;
			ucBulletinEditor.IsActive = _bulletin.IsActive;
			ucBulletinEditor.BulletinContent = _bulletin.BulletinContent;
			ucBulletinEditor.RecipientCheckedItems = _bulletin.RecipientRoleIDs;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		private void SaveForm()
		{
			_bulletin.TypeID = ucBulletinEditor.TypeID;
			_bulletin.IsApb = ucBulletinEditor.IsApb;
			_bulletin.Title = ucBulletinEditor.Title;
			_bulletin.IsActive = ucBulletinEditor.IsActive;
			_bulletin.BulletinContent = ucBulletinEditor.BulletinContent;
			_bulletin.RecipientRoleIDs = ucBulletinEditor.RecipientCheckedItems;

			//save it
			StatusInfo status = _bulletin.Update(this.Username);

			if (status.Success)
			{
				//base.RenderUserMessage(Enums.UserMessageTypes.Success, ("Bulletin saved!"));

				Response.Redirect("ManageBulletins.aspx?saved=1", false);  //false = prevents a ThreadAbortException from raising (which is expensive)
				HttpContext.Current.ApplicationInstance.CompleteRequest(); //stops all subsquent code from executing
			}
			else
			{
				base.RenderUserMessage(Enums.UserMessageTypes.Warning, ("Sorry, there was a problem saving: " + status.Message));
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}