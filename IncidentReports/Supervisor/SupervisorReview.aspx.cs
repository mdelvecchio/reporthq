﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReportHq.Common;
using ReportHq.IncidentReports.Controls;
using Telerik.Web.UI;

namespace ReportHq.IncidentReports.Supervisor
{
	public partial class SupervisorReview : Bases.ReportPopup
	{
		#region Variables

		private Report _report;

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Event Handlers

		protected void Page_Init(object sender, EventArgs e)
		{
			_report = base.Report; //set from base's OnInit
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
				SetFormValues();
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Supervisor saves form.
		/// </summary>
		protected void lbSubmit_Click(object sender, EventArgs e)
		{
			//needed? could do w/ existing cookie technique?
			//Session["isSupervisorOpen"] = false; //used for managing the visibility-state of this window (used by MajorSections.ascx)

			SaveReport();
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
				
		#region Private Methods

		private void SetFormValues()
		{
			//rank list items
			rcbSupervisorRank.DataSource = Report.GetSupervisorRankTypes();
			rcbSupervisorRank.DataBind();
			rcbSupervisorRank.Items.Insert(0, new RadComboBoxItem());


			string userDisplayName = "John Smith"; //use when not inside the NO network and thus no AD access
			//string userDisplayName = (ActiveDirectory.GetAdDisplayName(ActiveDirectory.GetAdEmail()));

			string commentsPrefix = String.Format("{0}, {1}:{2}-------------------------------{2}{2}", _report.SupervisorLastModifiedDate.ToShortDateString(), _report.SupervisorName, Environment.NewLine);

			// 2/22/2011, Joe Supervisor:
			// -------------------------------
			//
			//

			#region depending on ApprovalStatus set form differently

			switch (_report.ApprovalStatusTypeID)
			{
				case 1: //Approved - set values from approving supervisor
					base.RenderUserMessage(Enums.UserMessageTypes.Warning, "This report has already been Approved and cannot be changed.");

					//disable form fields
					rcbSupervisorRank.Enabled = false;
					tbSupervisorName.Enabled = false;
					rntbSupervisorBadge.Enabled = false;
					rblApprovalStatus.Enabled = false;
					tbComments.Enabled = false;
					lbSubmit.Visible = false;

					//name
					if (!string.IsNullOrEmpty(_report.SupervisorName))
						tbSupervisorName.Text = _report.SupervisorName;

					//rank
					if (_report.SupervisorRankTypeID > 0)
						rcbSupervisorRank.SelectedValue = _report.SupervisorRankTypeID.ToString();

					//badge
					if (_report.SupervisorBadge > 0)
						rntbSupervisorBadge.Value = _report.SupervisorBadge;

					//approval
					if (_report.ApprovalStatusTypeID > 0)
						rblApprovalStatus.SelectedValue = _report.ApprovalStatusTypeID.ToString();

					tbComments.Text = _report.SupervisorComments;

					break;

				case 2: //Rejected - keep only notes from prior disapproval
				case 3: //Pending

					tbSupervisorName.Text = userDisplayName;

					if (!string.IsNullOrEmpty(_report.SupervisorComments))
						tbComments.Text = commentsPrefix + _report.SupervisorComments;

					break;

				case 4: //Incomplete

					base.RenderUserMessage(Enums.UserMessageTypes.Warning, "This report is Incomplete and cannot be changed; Officer must submit it first.");
					
					//disable form fields
					rcbSupervisorRank.Enabled = false;
					tbSupervisorName.Enabled = false;
					rntbSupervisorBadge.Enabled = false;
					rblApprovalStatus.Enabled = false;
					tbComments.Enabled = false;
					lbSubmit.Visible = false;

					break;

				case 5: //Data Entry
					base.RenderUserMessage(Enums.UserMessageTypes.Warning, "This is a Data Entry report and cannot be changed.");
					
					//disable form fields
					rcbSupervisorRank.Enabled = false;
					tbSupervisorName.Enabled = false;
					rntbSupervisorBadge.Enabled = false;
					rblApprovalStatus.Enabled = false;
					tbComments.Enabled = false;
					lbSubmit.Visible = false;

					break;
			}

			#endregion
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Saves the Supervisor values, and if approved spits out to DTS archival folder.
		/// </summary>
		private void SaveReport()
		{
			//querystring-to-session reportID sync not needed since this page now loads from qs everytime

			//current ApprovalStatusType is used for validity checks inside UpdateSupervisor
			Report.ApprovalStatusTypes currentApprovalStatusType = _report.ApprovalStatusType;

			//new ApprovalStatusType selected from list
			int newApprovalStatusTypeID = Int32.Parse(rblApprovalStatus.SelectedValue);

			_report.SupervisorName = tbSupervisorName.Text;
			_report.SupervisorBadge = Convert.ToInt32(rntbSupervisorBadge.Value);
			_report.SupervisorRankTypeID = Convert.ToInt32(rcbSupervisorRank.SelectedValue);
			_report.ApprovalStatusTypeID = newApprovalStatusTypeID;
			_report.ApprovalStatusType = (Report.ApprovalStatusTypes)newApprovalStatusTypeID;
			_report.SupervisorComments = tbComments.Text;

			//save it
			StatusInfo status = _report.UpdateSupervisor(currentApprovalStatusType, this.Username);

			#region redirect or handle error

			if (status.Success == true)
			{
				//this page sits & spins while waiting for ReviewReports.aspx to render its results query, which can be slow. so disable the buttons and instead show a status message to sooth our gentle user.
				lbSubmit.Enabled = false;
				lbCancel.Enabled = false;
				litSaving.Visible = true;

				//redirect supervisor to search page
				Session["useLastSupervisorQuery"] = true; //picked up by /Supervisor/FindReports.aspx, which then uses the last-used search criteria
				ClientScript.RegisterClientScriptBlock(GetType(), "x", "redirectParentPage('ReviewReports.aspx')", true);
			}
			else //update failed
			{
				string error = string.Empty;

				if (status.Logs != null) //multiple errors
				{
					StringBuilder sb = new StringBuilder(400);

					sb.Append(status.Message + ":<br/><br/>");
					sb.Append("<ul>");

					foreach (string item in status.SimpleLogs)
						sb.Append(String.Format("<li>{0}</li>", item));

					sb.Append("</ul>");

					error = sb.ToString();
				}
				else
				{
					error = status.Message;
				}

				//render error
				base.RenderUserMessage(Enums.UserMessageTypes.Warning, "Sorry, but there was a problem:" + error);
			}

			#endregion
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}