﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReportHq.Common;
using ReportHq.IncidentReports.Controls;
using Telerik.Web.UI;

namespace ReportHq.IncidentReports.Supervisor
{
	public partial class ReviewReports : Bases.NormalPage
	{
		#region Variables

		private bool _useCookieSearchValues = false;
		private const string _COOKIE_NAME = "supervisorSearch";
		private const string _RESULTS_KEY = "dtSupervisorFindReports";

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Event Handlers

		protected void Page_Load(object sender, EventArgs e)
		{
			this.BreadCrumbs.Add((new BreadCrumb("Supervisor Tools", ResolveUrl("~/Supervisor/"), "to Supervisor Tools")));
			this.HelpUrl = "~/Supervisor/Help/ReviewReports.html";
			this.Heading = "Review Reports";

			//_username = this.Username;

			if (!IsPostBack)
			{
				#region set defaults

				//approval statuses
				rcbApprovalStatusTypes.DataSource = Report.GetApprovalStatusTypes();
				rcbApprovalStatusTypes.DataBind();
				//rcbApprovalStatusTypes.Items.FindItemByText("Pending").Selected = true; //set default

				//districts
				rcbDistrict.DataSource = GenericLookups.GetDistricts();
				rcbDistrict.DataBind();
				rcbDistrict.Items.Insert(0, new RadComboBoxItem());

				//platoons
				rcbPlatoons.DataSource = Report.GetPlatoonTypes();
				rcbPlatoons.DataBind();
				rcbPlatoons.Items.Insert(0, new RadComboBoxItem());

				//default daterange is 1-year
				//rdpStartDate.SelectedDate = DateTime.Now.AddYears(-1);
				//rdpEndDate.SelectedDate = DateTime.Now;

				#endregion

				#region process querystring criteria
				//set querystring-supplied criteria (ex: charts may send to here while passing in certain criteria)

				bool criteriaPassedIn = false;
				string status = Request.QueryString["status"];
				string district = Request.QueryString["district"];
				string start = Request.QueryString["start"];
				string end = Request.QueryString["end"];
				
				if (!string.IsNullOrEmpty(status))
				{
					rcbApprovalStatusTypes.SelectedValue = status;
					criteriaPassedIn = true;
				}					

				if (!string.IsNullOrEmpty(district))
				{
					rcbDistrict.SelectedValue = district;
					criteriaPassedIn = true;
				}

				CultureInfo provider = CultureInfo.InvariantCulture;

				if (!string.IsNullOrEmpty(start))
				{
					rdpStartDate.SelectedDate = DateTime.ParseExact(start, "yyyyMMdd", provider);
					criteriaPassedIn = true;
				}

				if (!string.IsNullOrEmpty(end))
				{
					rdpEndDate.SelectedDate = DateTime.ParseExact(end, "yyyyMMdd", provider);
					criteriaPassedIn = true;
				}
				
				if (criteriaPassedIn)
					DoSearch();

				#endregion


				//check to see if user came here from SupervisorOptions.aspx, which after saving a report status sets a Session variable and
				//redirects back here to re-run the last query (thus saving supervisor from having to click around the search fields UI).
				if (Session["useLastSupervisorQuery"] != null && this.Context.Request.Cookies[_COOKIE_NAME] != null)
					_useCookieSearchValues = (bool)Session["useLastSupervisorQuery"];

				if (_useCookieSearchValues)
				{
					base.RenderUserMessage(Enums.UserMessageTypes.Success, "Report approval-status set.");

					//new - get data here using last set of search criteria from cookie
					Session[_RESULTS_KEY] = GetData(true);

					gridReports.Rebind();

					//we only want to load the last query once (on the redirect here), so after that reset this flag.
					Session["useLastSupervisorQuery"] = false;
				}
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Fired when grid needs a datasource. eg, after editing or deleting.
		/// </summary>
		protected void gridReports_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
		{
			//we cache the user's last-results in Session in whenever search criteria are acted on in lbSubmit_Click; it gets used by grid here.

			//use cached version instead of hitting db
			DataTable results = (DataTable)Session[_RESULTS_KEY];

			gridReports.DataSource = results;

			lblResultCount.Text = string.Format("There are <b>{0:N0}</b> results matching your query:<br/><br/>", results.Rows.Count);

			divResults.Visible = true;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Fired when grid item's child-table icon is expanded.
		/// </summary>
		protected void gridReports_DetailTableDataBind(object source, GridDetailTableDataBindEventArgs e)
		{
			GridDataItem parentItem = e.DetailTableView.ParentItem as GridDataItem;

			//came from sdk example. if parent is in edit-mode dont update this
			if (parentItem.Edit)
				return;

			int reportID = (int)parentItem.GetDataKeyValue("ReportID");

			DataTable dt = null;

			//get appropiate child-table data
			switch (e.DetailTableView.Name)
			{
				case "persons":
					dt = VictimPerson.GetVictimPersonsByReport(reportID);
					break;

				case "offenders":
					dt = Offender.GetOffendersByReport(reportID);
					break;
			}

			e.DetailTableView.DataSource = dt;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void lbSubmit_Click(object sender, EventArgs e)
		{
			DoSearch();
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Private Methods

		/// <summary>
		/// Clear the user's last-search results from Session, and rebind the grid (gets data using form's criteria).
		/// </summary>
		private void DoSearch()
		{
			//cache the user's last-results in Session here whenever search criteria are acted on; it gets used by grid in gridResults_NeedDataSource.

			Session[_RESULTS_KEY] = GetData(false);

			gridReports.Rebind();
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		private DataTable GetData(bool useCookieSearchValues)
		{
			int approvalStatusTypeID = -1;
			int district = -1;			
			int startCarNumber = -1;
			int endCarNumber = -1;
			string platoonType = string.Empty;
			int badgeNumber = -1;
			DateTime startDate = new DateTime(1900, 1, 1);
			DateTime endDate = new DateTime(1900, 1, 1);

			HttpContext context = this.Context;

			//run query off of either cookie values or form values
			if (useCookieSearchValues)
			{
				#region get cookie values

				//the idea is, the Supervisor user doesnt want to constantly re-set his search criteria. so when he hits
				//this page the first time we store his criteria. as the supervisor goes thru the approve/disappove workflow,
				//he eventually comes back here and we use the stored cookie criteria if available.

				//get cookie values. gotta see if they exist first, in case supervisor hasnt run a query yet this session.

				if (!string.IsNullOrEmpty(Cookies.GetCookieItem(context, _COOKIE_NAME, "approvalStatusTypeID")))
					approvalStatusTypeID = Int32.Parse(Cookies.GetCookieItem(context, _COOKIE_NAME, "approvalStatusTypeID"));

				if (!string.IsNullOrEmpty(Cookies.GetCookieItem(context, _COOKIE_NAME, "district")))
					district = Int32.Parse(Cookies.GetCookieItem(context, _COOKIE_NAME, "district"));

				if (!string.IsNullOrEmpty(Cookies.GetCookieItem(context, _COOKIE_NAME, "platoonType")))
					platoonType = Cookies.GetCookieItem(context, _COOKIE_NAME, "platoonType");

				if (!string.IsNullOrEmpty(Cookies.GetCookieItem(context, _COOKIE_NAME, "startCarNumber")))
					startCarNumber = Int32.Parse(Cookies.GetCookieItem(context, _COOKIE_NAME, "startCarNumber"));

				if (!string.IsNullOrEmpty(Cookies.GetCookieItem(context, _COOKIE_NAME, "endCarNumber")))
					endCarNumber = Int32.Parse(Cookies.GetCookieItem(context, _COOKIE_NAME, "endCarNumber"));

				if (!string.IsNullOrEmpty(Cookies.GetCookieItem(context, _COOKIE_NAME, "startDate")))
					startDate = Convert.ToDateTime(Cookies.GetCookieItem(context, _COOKIE_NAME, "startDate"));

				if (!string.IsNullOrEmpty(Cookies.GetCookieItem(context, _COOKIE_NAME, "endDate")))
					endDate = Convert.ToDateTime(Cookies.GetCookieItem(context, _COOKIE_NAME, "endDate"));

				if (!string.IsNullOrEmpty(Cookies.GetCookieItem(context, _COOKIE_NAME, "badgeNumber")))
					badgeNumber = Convert.ToInt32(Cookies.GetCookieItem(context, _COOKIE_NAME, "badgeNumber"));

				#endregion

				#region set them to form UI

				//set em to form
				if (approvalStatusTypeID > 0)
					rcbApprovalStatusTypes.SelectedValue = approvalStatusTypeID.ToString();

				if (district > 0)
					rcbDistrict.SelectedValue = district.ToString();

				if (!string.IsNullOrEmpty(platoonType))
					rcbPlatoons.SelectedValue = platoonType;

				if (startCarNumber > 0)
					rntbStartCarNumber.Value = startCarNumber;

				if (endCarNumber > 0)
					rntbEndCarNumber.Value = endCarNumber;

				if (startDate != new DateTime(1900, 1, 1))
					rdpStartDate.SelectedDate = startDate;

				if (endDate != new DateTime(1900, 1, 1))
					rdpEndDate.SelectedDate = endDate;

				if (badgeNumber > 0)
					rntbBadgeNumber.Text = badgeNumber.ToString();

				#endregion
			}
			else
			{
				//get form values
				if (!string.IsNullOrEmpty(rcbApprovalStatusTypes.SelectedValue))
					approvalStatusTypeID = Int32.Parse(rcbApprovalStatusTypes.SelectedValue);

				if (!string.IsNullOrEmpty(rcbDistrict.SelectedValue))
					district = Int32.Parse(rcbDistrict.SelectedValue);

				if (!string.IsNullOrEmpty(rcbPlatoons.SelectedValue))
					platoonType = rcbPlatoons.SelectedValue;

				if (rntbStartCarNumber.Value > 0)
					startCarNumber = Convert.ToInt32(rntbStartCarNumber.Value);

				if (rntbEndCarNumber.Value > 0)
					endCarNumber = Convert.ToInt32(rntbEndCarNumber.Value);

				if (rdpStartDate.SelectedDate != null)
					startDate = (DateTime)rdpStartDate.SelectedDate;

				if (rdpEndDate.SelectedDate != null)
					endDate = (DateTime)rdpEndDate.SelectedDate;

				if (!string.IsNullOrEmpty(rntbBadgeNumber.Text))
					badgeNumber = Convert.ToInt32(rntbBadgeNumber.Text);											

				//save query for later use
				SaveSearchCriteria(context,
								   approvalStatusTypeID,					
								   district,
								   platoonType,
								   startCarNumber,
								   endCarNumber,
								   startDate,
								   endDate,
								   badgeNumber);
			}

	
			//do it
			return Report.GetReportsByCriteria(string.Empty,
											   approvalStatusTypeID,
											   district,
											   platoonType,
											   startCarNumber,
											   endCarNumber,
											   startDate,
											   endDate,
											   badgeNumber);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		private void SaveSearchCriteria(HttpContext context,
										int approvalStatusTypeID,
										int district,
										string platoonType,
										int startCarNumber,
										int endCarNumber,
										DateTime startDate,
										DateTime endDate,
										int badgeNumber)
		{
			Cookies.CreateNewCookie(context, _COOKIE_NAME, false);

			Cookies.AddCookieItem(context, _COOKIE_NAME, "approvalStatusTypeID", approvalStatusTypeID.ToString());
			Cookies.AddCookieItem(context, _COOKIE_NAME, "district", district.ToString());
			Cookies.AddCookieItem(context, _COOKIE_NAME, "platoonType", platoonType);
			Cookies.AddCookieItem(context, _COOKIE_NAME, "startCarNumber", startCarNumber.ToString());
			Cookies.AddCookieItem(context, _COOKIE_NAME, "endCarNumber", endCarNumber.ToString());
			Cookies.AddCookieItem(context, _COOKIE_NAME, "startDate", startDate.ToString());
			Cookies.AddCookieItem(context, _COOKIE_NAME, "endDate", endDate.ToString());
			Cookies.AddCookieItem(context, _COOKIE_NAME, "badgeNumber", badgeNumber.ToString());
		}
		
			//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}