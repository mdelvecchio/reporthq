﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReportHq.Common;
using Telerik.Web.UI;

namespace ReportHq.IncidentReports
{
	public partial class DeleteYourReports : Bases.NormalPage
	{
		#region Variables

		private string _username;
		//private const string _RESULTS_KEY = "dtYourReports";

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Event Handlers

		protected void Page_Load(object sender, EventArgs e)
		{
			this.Heading = "Delete Your Reports";
			this.HelpUrl = "~/Help/DeleteYourReports.html"; 

			_username = this.Username;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Fired when grid needs a datasource. eg, after editing or deleting.
		/// </summary>
		protected void gridReports_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
		{
			DataTable results = Report.GetDeletableReportsByUsername(_username);

			gridReports.DataSource = results;

			if (results.Rows.Count == 0)
			{
				base.RenderUserMessage(Enums.UserMessageTypes.Warning, "Could not locate any Reports for " + _username);
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Fired when grid item's child-table icon is expanded.
		/// </summary>
		protected void gridReports_DetailTableDataBind(object source, GridDetailTableDataBindEventArgs e)
		{
			GridDataItem parentItem = e.DetailTableView.ParentItem as GridDataItem;

			//came from sdk example. if parent is in edit-mode dont update this
			if (parentItem.Edit)
				return;

			int reportID = (int)parentItem.GetDataKeyValue("ReportID");

			DataTable dt = null;

			//get appropiate child-table data
			switch (e.DetailTableView.Name)
			{
				case "persons":
					dt = VictimPerson.GetVictimPersonsByReport(reportID);
					break;

				case "offenders":
					dt = Offender.GetOffendersByReport(reportID);
					break;
			}

			e.DetailTableView.DataSource = dt;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void lbDeleteDelete_Click(object sender, EventArgs e)
		{
			//get selected reprots
			ArrayList reportIDs = GetSelectedReportIDs();

			if (reportIDs.Count > 0)
			{
				StringCollection errors = new StringCollection();
				StatusInfo status = null;

				//deactivate each
				foreach (Int32 reportID in reportIDs)
				{
					status = Report.DeactivateReport(reportID, _username);
					//status = new StatusInfo(false, "test");

					//log errors
					if (!status.Success)
						errors.Add(status.Message);
				}

				#region render success/fail message

				if (errors.Count == 0)
				{
					base.RenderUserMessage(Enums.UserMessageTypes.Success, "Report(s) deleted");
				}
				else
				{
					string message = Strings.FormatList("Sorry, but the following errors occurred:", errors);

					base.RenderUserMessage(Enums.UserMessageTypes.Warning, message);
				}

				#endregion

				gridReports.Rebind();
			}
			else
			{
				base.RenderUserMessage(Enums.UserMessageTypes.Warning, "No reports selected.");
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Private Methods

		/// <summary>
		/// Gets the ReportIDs for all rows selected by user.
		/// </summary>
		private ArrayList GetSelectedReportIDs()
		{
			ArrayList reportIDs = new ArrayList();

			foreach (GridDataItem item in gridReports.SelectedItems)
			{
				reportIDs.Add((int)item.GetDataKeyValue("ReportID"));
			}

			return reportIDs;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}