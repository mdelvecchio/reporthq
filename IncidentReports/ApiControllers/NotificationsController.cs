﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using ReportHq.Common;
using ReportHq.IncidentReports;
using ReportHq.IncidentReports.ApiModels;
using ReportHq.IncidentReports.DataAccess;

namespace ReportHq.IncidentReports.ApiControllers
{
	public class NotificationsController : ApiController
	{
		#region Methods

		[Route("api/notifications/apb/{username}")] //attribute routing, new in Web API 2
		//[HttpGet] //really only needed if ambiguous actions
		public IEnumerable<Notification> GetNewApbNotifications(string username)
		{
			return Notification.GetNewNotifications(username.Replace("|", @"\"), Notification.Types.APB, true);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		
		[Route("api/notifications/system/{username}")] //attribute routing, new in Web API 2
		//[HttpGet] //really only needed if ambiguous actions
		public IEnumerable<Notification> GetNewSystemNotifications(string username)
		{
			return Notification.GetNewNotifications(username.Replace("|", @"\"), Notification.Types.System, true);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		[Route("api/notifications/report/{username}")] //attribute routing, new in Web API 2
		//[HttpGet] //really only needed if ambiguous actions
		public IEnumerable<Notification> GetNewReportNotifications(string username)
		{
			return Notification.GetNewNotifications(username.Replace("|", @"\"), Notification.Types.Report, false);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		
		/// <summary>
		/// A set-method that updates to the database.
		/// </summary>
		/// <param name="username">The user to update read-status for.</param>
		/// <param name="itemIDs">An XML string of notification IDs to update.</param>
		[Route("api/notifications/setAsRead/{username}")] //attribute routing, new in Web API 2
		[HttpPost]
		public IHttpActionResult SetNotificationAsRead(string username, [FromBody]string itemIDs)
		{
			//for some reason, Web API doesnt map plain text from the Request Body -- it has to be a complex object. but one hack is to prefix the body text w/ a "=" and voila, it maps. so now we remove it.
			//itemIDs.Substring(1); //doesnt seem needed -- body text doesnt have "=" in it from here.

			StatusInfo status = Notification.MarkAsRead(username.Replace("|", @"\"), itemIDs, 1);

			if (status.Success)
			{
				return Ok();

				//return Ok<string>("Custom message.");
				//return type of IHttpActionResult. new to Web API 2, has several items defined. suggested over HttpResponse
			}
			else
			{
				return Ok();

				//TODO: figure out how to send the error back
				//this.ResponseMessage = status.Message;
			}
		}
		
		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''



		/// <summary>
		/// A set-method that updates to the database.
		/// </summary>
		/// <param name="username">The user to update read-status for.</param>
		/// <param name="itemIDs">An XML string of notification IDs to update.</param>
		[Route("api/notifications/setGlobalAsRead/{username}")] //attribute routing, new in Web API 2
		[HttpPost]
		public IHttpActionResult SetGlobalNotificationAsRead(string username, [FromBody]string itemIDs)
		{
			StatusInfo status = Notification.MarkAsRead(username.Replace("|", @"\"), itemIDs, 2);

			if (status.Success)
				return Ok();
			else
				return Ok();
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		
		#endregion
	}
}