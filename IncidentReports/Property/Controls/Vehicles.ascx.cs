﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReportHq.Common;
using ReportHq.IncidentReports.Bases;
using ReportHq.IncidentReports.Controls;
using Telerik.Web.UI;

namespace ReportHq.IncidentReports.Property.Controls
{
	public partial class Vehicles : System.Web.UI.UserControl
	{
		#region Variables

		private int _reportID;
		ReportPage _reportPage;

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Event Handlers

		protected void Page_Load(object sender, EventArgs e)
		{
			_reportPage = (ReportPage)this.Page;
			_reportPage.SyncSessionToViewState();

			_reportID = _reportPage.Report.ID;

			if (!_reportPage.UserHasWriteAccess)
			{
				//change "Edit" to "View"
				((GridEditCommandColumn)gridVehicles.Columns[0]).EditText = "View";

				//hide "Delete"
				gridVehicles.Columns[1].Visible = false;

				//disable Add command
				gridVehicles.MasterTableView.CommandItemDisplay = GridCommandItemDisplay.None;
			}

			//setting definitions in code-behind so we can get the master page's label
			ajaxManager.AjaxSettings.AddAjaxSetting(gridVehicles, gridVehicles); //grid updates itself
			ajaxManager.AjaxSettings.AddAjaxSetting(gridVehicles, _reportPage.Master.FindControl("lblMessage")); //grid updates label
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Fired when grid needs a datasource. eg, after editing or deleting. 
		/// </summary>
		protected void gridVehicles_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
		{
			//get table of current weapons from db
			gridVehicles.DataSource = Vehicle.GetVehiclesByReport(_reportID);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void gridVehicles_InsertCommand(object source, GridCommandEventArgs e)
		{
			//get values into hashtable
			Hashtable newValues = GetFormValues((GridForms.VehicleForm)e.Item.FindControl(GridEditFormItem.EditFormUserControlID));

			Vehicle vehicle = new Vehicle(newValues);
			vehicle.ReportID = _reportID;

			StatusInfo status = vehicle.Add(HttpContext.Current.User.Identity.Name);

			if (!status.Success)
				_reportPage.RenderUserMessage(Enums.UserMessageTypes.Warning, ("Sorry, there was a problem saving: " + status.Message));
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void gridVehicles_UpdateCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
		{
			GridEditableItem editableItem = (GridEditableItem)e.Item;
			int vehicleID = (int)editableItem.GetDataKeyValue("VehicleID");

			//get values into hashtable
			Hashtable newValues = GetFormValues((GridForms.VehicleForm)e.Item.FindControl(GridEditFormItem.EditFormUserControlID));

			Vehicle vehicle = new Vehicle(newValues);
			vehicle.ID = vehicleID;

			StatusInfo status = vehicle.Update(HttpContext.Current.User.Identity.Name);

			if (!status.Success)
				_reportPage.RenderUserMessage(Enums.UserMessageTypes.Warning, ("Sorry, there was a problem saving: " + status.Message));
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void gridVehicles_DeleteCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
		{
			GridEditableItem editableItem = (GridEditableItem)e.Item;
			int vehicleID = (int)editableItem.GetDataKeyValue("VehicleID");

			StatusInfo status = Vehicle.DeleteVehicle(vehicleID);

			if (!status.Success)
				_reportPage.RenderUserMessage(Enums.UserMessageTypes.Warning, ("Sorry, there was a problem saving: " + status.Message));
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void ucReportSubNav_NavItemClicked(object sender, EventArgs e)
		{
			_reportPage.ProcessNavClick((sender as ReportSubNav).TargetRedirectUrl, false);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Private Methods

		private Hashtable GetFormValues(GridForms.VehicleForm userControl)
		{
			Hashtable newValues = new Hashtable();
			string userStringValue;
			double? userDoubleValue;

			newValues["StatusTypeID"] = Int32.Parse((userControl.FindControl("rcbStatusTypes") as RadComboBox).SelectedValue);

			//year
			userDoubleValue = (userControl.FindControl("ntbYear") as RadNumericTextBox).Value;
			if (userDoubleValue > 0)
				newValues["Year"] = Convert.ToInt32(userDoubleValue);
			else
				newValues["Year"] = -1;

			//make
			userStringValue = (userControl.FindControl("rcbVehicleMakes") as RadComboBox).SelectedValue;
			if (!string.IsNullOrEmpty(userStringValue))
				newValues["MakeTypeID"] = Int32.Parse(userStringValue);
			else
				newValues["MakeTypeID"] = -1;

			//model
			//RadComboBox box = (userControl.FindControl("rcbVehicleModels") as RadComboBox);

			userStringValue = (userControl.FindControl("rcbVehicleModels") as RadComboBox).SelectedValue;

			//if (box.SelectedItem != null) //this list is null if no make selection has been made.
			//if (!string.IsNullOrEmpty(box.SelectedValue))
			if (!string.IsNullOrEmpty(userStringValue))
				newValues["ModelTypeID"] = Int32.Parse((userControl.FindControl("rcbVehicleModels") as RadComboBox).SelectedValue);
			else
				newValues["ModelTypeID"] = -1;

			//color
			userStringValue = (userControl.FindControl("rcbVehicleColors") as RadComboBox).SelectedValue;
			if (!string.IsNullOrEmpty(userStringValue))
				newValues["ColorTypeID"] = Int32.Parse(userStringValue);
			else
				newValues["ColorTypeID"] = -1;

			//style
			userStringValue = (userControl.FindControl("rcbVehicleStyles") as RadComboBox).SelectedValue;
			if (!string.IsNullOrEmpty(userStringValue))
				newValues["StyleTypeID"] = Int32.Parse(userStringValue);
			else
				newValues["StyleTypeID"] = -1;


			//value
			userDoubleValue = (userControl.FindControl("rntbValue") as RadNumericTextBox).Value;
			if (userDoubleValue > 0)
				newValues["Value"] = Convert.ToInt32(userDoubleValue);
			else
				newValues["Value"] = -1;

			newValues["LicensePlate"] = (userControl.FindControl("tbLicensePlate") as TextBox).Text;
			newValues["LicensePlateState"] = (userControl.FindControl("rcbLicensePlateState") as RadComboBox).SelectedValue;

			//license plate year
			userDoubleValue = (userControl.FindControl("rntbLicensePlateYear") as RadNumericTextBox).Value;
			if (userDoubleValue > 0)
				newValues["LicensePlateYear"] = Convert.ToInt32(userDoubleValue);
			else
				newValues["LicensePlateYear"] = -1;

			newValues["Vin"] = (userControl.FindControl("tbVin") as TextBox).Text;

			//recovery type
			userStringValue = (userControl.FindControl("rcbRecoveredTypes") as RadComboBox).SelectedValue;
			if (!string.IsNullOrEmpty(userStringValue))
				newValues["RecoveredTypeID"] = Int32.Parse(userStringValue);
			else
				newValues["RecoveredTypeID"] = -1;

			newValues["RecoveryLocation"] = (userControl.FindControl("tbRecoveryLocation") as TextBox).Text;

			//recovery district
			userStringValue = (userControl.FindControl("rcbDistricts") as RadComboBox).SelectedValue;
			if (!string.IsNullOrEmpty(userStringValue))
				newValues["RecoveryDistrict"] = Int32.Parse(userStringValue);
			else
				newValues["RecoveryDistrict"] = -1;

			newValues["RecoveryZone"] = (userControl.FindControl("rcbZones") as RadComboBox).SelectedValue;
			newValues["RecoverySubZone"] = (userControl.FindControl("rcbSubZones") as RadComboBox).SelectedValue;

			newValues["AdditionalDescription"] = (userControl.FindControl("tbAdditionalDesc") as TextBox).Text;

			//damage desc
			userStringValue = (userControl.FindControl("rcbDamgeLevelTypes") as RadComboBox).SelectedValue;
			if (!string.IsNullOrEmpty(userStringValue))
				newValues["DamageLevelTypeID"] = Int32.Parse(userStringValue);
			else
				newValues["DamageLevelTypeID"] = -1;

			//damage zones
			newValues["DamageZonesTypes"] = Common.GetCheckedItems((CheckBoxList)userControl.FindControl("cblDamageZones"));

			//recovered vehicle MOs
			newValues["RecoveredVehicleMoTypes"] = Common.GetCheckedItems((CheckBoxList)userControl.FindControl("cblRecoveredVehicleMos"));


			return newValues;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}