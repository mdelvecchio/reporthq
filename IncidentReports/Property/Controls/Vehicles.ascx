﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Vehicles.ascx.cs" Inherits="ReportHq.IncidentReports.Property.Controls.Vehicles" %>
<%@ Register TagPrefix="ReportHq" TagName="ReportSubNav" src="ReportSubNav.ascx" %>

<ReportHq:ReportSubNav ID="ucReportSubNav" CurrentSection="Vehicles" OnNavItemClicked="ucReportSubNav_NavItemClicked" runat="server" />
	
<div class="formBox" style="width:700px;">

	<Telerik:RadGrid ID="gridVehicles" 
		AllowPaging="false"
		AllowSorting="true" 				
		AutoGenerateColumns="False"
		AllowMultiRowSelection="true"
		OnNeedDataSource="gridVehicles_NeedDataSource" 
		OnInsertCommand="gridVehicles_InsertCommand"
		OnUpdateCommand="gridVehicles_UpdateCommand" 
		OnDeleteCommand="gridVehicles_DeleteCommand"
		runat="server">

		<ClientSettings 
			EnableRowHoverStyle="true"
			AllowDragToGroup="false" 
			AllowColumnsReorder="false" 
			AllowKeyboardNavigation="true"
			ReorderColumnsOnClient="false" >
			<Resizing AllowColumnResize="false" AllowRowResize="false" />
			<Selecting AllowRowSelect="false" />
		</ClientSettings>
			
		<MasterTableView 
			CommandItemDisplay="Top" 
			DataKeyNames="VehicleID" 
			EditMode="EditForms" 
			NoMasterRecordsText="No Vehicles to display." 
			HeaderStyle-ForeColor="#191970" 
			ItemStyle-ForeColor="black" 
			AlternatingItemStyle-ForeColor="black">
				
			<CommandItemSettings AddNewRecordText="Add Vehicle" ShowRefreshButton="false" />
			<CommandItemStyle CssClass="commandItem" />
				
			<Columns>
				<Telerik:GridEditCommandColumn ButtonType="LinkButton" UniqueName="editColumn" ItemStyle-HorizontalAlign="center" ItemStyle-CssClass="commandItem" />
				<Telerik:GridButtonColumn CommandName="Delete" ButtonType="LinkButton" Text="Delete" UniqueName="deleteColumn" ConfirmText="Delete Vehicle?" ConfirmTitle="Delete" ItemStyle-HorizontalAlign="center" ItemStyle-CssClass="commandItem" />
				<Telerik:GridBoundColumn HeaderText="VehicleID" UniqueName="VehicleID" DataField="VehicleID" ReadOnly="true" Visible="false"/>
				<Telerik:GridBoundColumn HeaderText="Status" UniqueName="Status" DataField="Status" />
				<Telerik:GridBoundColumn HeaderText="Year" UniqueName="Year" DataField="Year" />
				<Telerik:GridBoundColumn HeaderText="Make" UniqueName="Make" DataField="Make" />
				<Telerik:GridBoundColumn HeaderText="Model" UniqueName="Model" DataField="Model" />
				<Telerik:GridBoundColumn HeaderText="Color" UniqueName="Color" DataField="Color" HeaderStyle-Wrap="false"/>
				<Telerik:GridBoundColumn HeaderText="Style" UniqueName="Style" DataField="Style" />
			</Columns>
				
			<EditFormSettings EditFormType="WebUserControl" UserControlName="controls/GridForms/VehicleForm.ascx" />
				
		</MasterTableView>
	</Telerik:RadGrid>

</div>

<telerik:RadAjaxManager ID="ajaxManager" DefaultLoadingPanelID="ajaxLoadingPanel" runat="server" />