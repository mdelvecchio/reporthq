﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReportSubNav.ascx.cs" Inherits="ReportHq.IncidentReports.Property.Controls.ReportSubNav" %>

<!--
<div id="ReportSubNav">
	<a href="#">Area 1</a>
	<a href="#">Area 2</a>
	<span class="selected">Area 3</span>
	<a href="#">Area 4</a>				
</div>
-->


<div id="ReportSubNav">
	<asp:Label ID="lblPropertyEvidence" Text="Property / Evidence" CssClass="selected" Visible="false" runat="server" /><asp:LinkButton ID="lbPropertyEvidence" Text="Property / Evidence" ToolTip="Save your work and move here" OnClick="lbPropertyEvidence_Click" runat="server" />
	<asp:Label ID="lblVehicles" Text="Vehicles" CssClass="selected" Visible="false" runat="server" /><asp:LinkButton ID="lbVehicles" Text="Vehicles" ToolTip="Save your work and move here" OnClick="lbVehicles_Click" runat="server" />
	<asp:Label ID="lblWeapons" Text="Weapons" CssClass="selected" Visible="false" runat="server" /><asp:LinkButton ID="lbWeapons" Text="Weapons" ToolTip="Save your work and move here" OnClick="lbWeapons_Click" runat="server" />
</div>