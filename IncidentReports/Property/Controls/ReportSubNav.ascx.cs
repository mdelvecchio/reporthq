﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReportHq.IncidentReports;

namespace ReportHq.IncidentReports.Property.Controls
{
	public partial class ReportSubNav : System.Web.UI.UserControl
	{
		#region Enums

		public enum NavSections
		{
			Weapons,
			Vehicles,
			PropertyEvidence
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Properties

		private NavSections _currentSection;
		private string _targetRedirectUrl;
		private string _validationGroup;
				
		/// <summary>
		/// Which main section link the user is currently at.
		/// </summary>
		public NavSections CurrentSection
		{
			get { return _currentSection; }
			set { _currentSection = value; }
		}

		/// <summary>
		/// Where to bring the user after clicking; used by the consuming .ASPX/ascx page
		/// </summary>
		public string TargetRedirectUrl
		{
			get { return _targetRedirectUrl; }
			set { _targetRedirectUrl = value; }
		}

		public string ValidationGroup
		{
			get { return _validationGroup; }
			set { _validationGroup = value; }
		}

		
		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		
		#endregion

		#region Events

		//public delegate for LinkButton nav-item click
		public delegate void NavItemClick(object sender, EventArgs e);

		//public event for LinkButton click; will be handled by the consuming pages
		public event NavItemClick NavItemClicked;

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
		
		#region Event Handlers

		protected void Page_Load(object sender, EventArgs e)
		{
			//set validation group used by page's validator controls
			if (!string.IsNullOrEmpty(_validationGroup))
			{
				lbWeapons.ValidationGroup = _validationGroup;
				lbVehicles.ValidationGroup = _validationGroup;
				lbPropertyEvidence.ValidationGroup = _validationGroup;
			}

			//set selected-item state
			switch (_currentSection)
			{
				case NavSections.Weapons:
					lblWeapons.Visible = true;
					lbWeapons.Visible = false;
					break;

				case NavSections.Vehicles:
					lblVehicles.Visible = true;
					lbVehicles.Visible = false;
					break;

				case NavSections.PropertyEvidence:
					lblPropertyEvidence.Visible = true;
					lbPropertyEvidence.Visible = false;
					break;
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void lbWeapons_Click(object sender, EventArgs e)
		{
			_targetRedirectUrl = "Weapons.aspx";

			//fire event for consuming page to handler
			NavItemClicked(this, e);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void lbVehicles_Click(object sender, EventArgs e)
		{
			_targetRedirectUrl = "Vehicles.aspx";

			//fire event for consuming page to handler
			NavItemClicked(this, e);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void lbPropertyEvidence_Click(object sender, EventArgs e)
		{
			_targetRedirectUrl = "PropertyEvidence.aspx";

			//fire event for consuming page to handler
			NavItemClicked(this, e);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Private Methods

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

	}
}