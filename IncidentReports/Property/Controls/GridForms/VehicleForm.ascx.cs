﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReportHq.Common;
using ReportHq.IncidentReports.Bases;
using Telerik.Web.UI;

namespace ReportHq.IncidentReports.Property.Controls.GridForms
{
	public partial class VehicleForm : System.Web.UI.UserControl
	{
		#region Properties

		private object _dataItem;

		public object DataItem
		{
			get { return _dataItem; }
			set { _dataItem = value; }
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Event Handlers

		override protected void OnInit(EventArgs e)
		{
			//needed to initiate special event handler for dropdownlists bindings
			this.DataBinding += new EventHandler(VehicleForm_DataBinding);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void Page_Load(object sender, EventArgs e)
		{
			//rntbVictimNumber.Focus();
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		private void Page_PreRender(object sender, EventArgs e)
		{
			//on nav-click postbacks there is no data
			if (this.DataItem != null) 
			{ 
				//if not a new vehicle, set the checkboxlists
				if (!string.IsNullOrEmpty(DataBinder.Eval(this.DataItem, "VehicleID").ToString()))
				{
					int vehicleID = (int)DataBinder.Eval(this.DataItem, "VehicleID");

					Common.SetCheckedItems(Vehicle.GetDamageZonesByVehicle(vehicleID), cblDamageZones);
					Common.SetCheckedItems(Vehicle.GetRecoveredVehicleMosByVehicle(vehicleID), cblRecoveredVehicleMos);
				}

				if (!(this.Page as ReportPage).UserHasWriteAccess)
				{
					lbInsert.Visible = false;
					lbUpdate.Visible = false;
				}
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Was told by Telerik that any list-binding for this control must happen in this custom event handler.
		/// </summary>
		private void VehicleForm_DataBinding(object sender, System.EventArgs e)
		{
			//vehicle statuses
			rcbStatusTypes.DataSource = Vehicle.GetVehicleStatusTypes();
			rcbStatusTypes.DataBind();
			rcbStatusTypes.Items.Insert(0, new RadComboBoxItem());

			//vehicle makes
			rcbVehicleMakes.DataSource = GenericLookups.GetVehicleMakes();
			rcbVehicleMakes.DataBind();
			rcbVehicleMakes.Items.Insert(0, new RadComboBoxItem());

			//vehicle models
			//(handled by SetVehicleModel(), which is called by rcbVehicleModels_ItemsRequested, which is called by  rcbVehicleMakes's OnClientSelectedIndexChanging="LoadModels")

			//vehicle colors
			rcbVehicleColors.DataSource = GenericLookups.GetVehicleColors();
			rcbVehicleColors.DataBind();
			rcbVehicleColors.Items.Insert(0, new RadComboBoxItem());

			//vehicle styles
			rcbVehicleStyles.DataSource = GenericLookups.GetVehicleStyles();
			rcbVehicleStyles.DataBind();
			rcbVehicleStyles.Items.Insert(0, new RadComboBoxItem());

			//license plate state
			rcbLicensePlateState.DataSource = UsStates.GetStateNamesAndAbbreviations(true);
			rcbLicensePlateState.DataBind();
			rcbLicensePlateState.Items.Insert(0, new RadComboBoxItem());

			//recovered types
			rcbRecoveredTypes.DataSource = Vehicle.GetVehicleRecoveredTypes();
			rcbRecoveredTypes.DataBind();
			rcbRecoveredTypes.Items.Insert(0, new RadComboBoxItem());

			//districts
			rcbDistricts.DataSource = GenericLookups.GetDistricts();
			rcbDistricts.DataBind();
			rcbDistricts.Items.Insert(0, new RadComboBoxItem());

			//zones
			rcbZones.DataSource = GenericLookups.GetZones();
			rcbZones.DataBind();
			rcbZones.Items.Insert(0, new RadComboBoxItem());

			//subzones
			rcbSubZones.DataSource = GenericLookups.GetSubZones();
			rcbSubZones.DataBind();
			rcbSubZones.Items.Insert(0, new RadComboBoxItem());

			//damage levels
			rcbDamgeLevelTypes.DataSource = Vehicle.GetVehicleDamageLevelTypes();
			rcbDamgeLevelTypes.DataBind();
			rcbDamgeLevelTypes.Items.Insert(0, new RadComboBoxItem());

			//damage zones
			cblDamageZones.DataSource = Vehicle.GetVehicleDamageZoneTypes();
			cblDamageZones.DataBind();

			//recovered vehicle mos (actual checkmarks set in Page_PreRender)
			cblRecoveredVehicleMos.DataSource = Vehicle.GetRecoveredVehicleMoTypes();
			cblRecoveredVehicleMos.DataBind();


			//set grid item's values
			rcbStatusTypes.Items.FindItemByValue(DataBinder.Eval(this.DataItem, "VehicleStatusTypeID").ToString()).Selected = true;
			rcbVehicleMakes.Items.FindItemByValue(DataBinder.Eval(this.DataItem, "VehicleMakeTypeID").ToString()).Selected = true;
			rcbVehicleColors.Items.FindItemByValue(DataBinder.Eval(this.DataItem, "VehicleColorTypeID").ToString()).Selected = true;
			rcbVehicleStyles.Items.FindItemByValue(DataBinder.Eval(this.DataItem, "VehicleStyleTypeID").ToString()).Selected = true;
			rcbLicensePlateState.Items.FindItemByValue(DataBinder.Eval(this.DataItem, "LicensePlateState").ToString()).Selected = true;
			rcbRecoveredTypes.Items.FindItemByValue(DataBinder.Eval(this.DataItem, "VehicleRecoveredTypeID").ToString()).Selected = true;
			rcbDistricts.Items.FindItemByValue(DataBinder.Eval(this.DataItem, "RecoveryDistrict").ToString()).Selected = true;
			rcbZones.Items.FindItemByValue(DataBinder.Eval(this.DataItem, "RecoveryZone").ToString()).Selected = true;
			rcbSubZones.Items.FindItemByValue(DataBinder.Eval(this.DataItem, "RecoverySubzone").ToString()).Selected = true;
			rcbDamgeLevelTypes.Items.FindItemByValue(DataBinder.Eval(this.DataItem, "VehicleDamageLevelTypeID").ToString()).Selected = true;

			//set vehicle model - have to do after make is set by the above
			if (!string.IsNullOrEmpty(rcbVehicleMakes.SelectedValue))
			{
				rcbVehicleModels.DataSource = GenericLookups.GetVehicleModelsByMake(Int32.Parse(rcbVehicleMakes.SelectedValue));
				rcbVehicleModels.DataBind();
				rcbVehicleModels.Items.Insert(0, new RadComboBoxItem());

				rcbVehicleModels.Items.FindItemByValue(DataBinder.Eval(this.DataItem, "VehicleModelTypeID").ToString()).Selected = true;
			}
			
			//deselects the first item in the dropdown lists (keeps blank)
			rcbStatusTypes.DataSource = null;
			rcbVehicleMakes.DataSource = null;
			rcbVehicleModels.DataSource = null;
			rcbVehicleColors.DataSource = null;
			rcbVehicleStyles.DataSource = null;
			rcbLicensePlateState.DataSource = null;
			rcbRecoveredTypes.DataSource = null;
			rcbDistricts.DataSource = null;
			rcbZones.DataSource = null;
			rcbSubZones.DataSource = null;
			rcbDamgeLevelTypes.DataSource = null;
			//http://www.telerik.com/help/aspnet-ajax/grdcustomeditforms.html


		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Sets the Models for a given Make. Called by ASPX's javascript's callback method.
		/// </summary>
		protected void rcbVehicleModels_ItemsRequested(object o, RadComboBoxItemsRequestedEventArgs e)
		{
			SetVehicleModel(e.Text);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Private Methods

		private void SetVehicleModel(string sMakeTypeID)
		{
			int makeTypeID = Int32.Parse(sMakeTypeID);

			rcbVehicleModels.DataSource = GenericLookups.GetVehicleModelsByMake(makeTypeID);
			rcbVehicleModels.DataBind();
			rcbVehicleModels.Items.Insert(0, new RadComboBoxItem());
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}