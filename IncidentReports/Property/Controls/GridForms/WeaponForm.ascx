﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WeaponForm.ascx.cs" Inherits="ReportHq.IncidentReports.Property.Controls.GridForms.WeaponForm" %>

<Telerik:RadFormDecorator id="radFormDecorator" DecoratedControls="RadioButtons" Skin="Metro" runat="server" />

<div class="gridForm">

	<table border="0" style="margin-bottom:10px;">
		<tr>
			<td class="label">Type: <span class="required">*</span></td>
			<td>
				<Telerik:RadComboBox ID="rcbFirearmTypes" DataValueField="FirearmTypeID" DataTextField="Description" AllowCustomText="False" MarkFirstMatch="true" width="180" DropDownWidth="300" TabIndex="1" runat="server" />
				<asp:RequiredFieldValidator ID="rfvType" ControlToValidate="rcbFirearmTypes" ValidationGroup="weapon" ErrorMessage="Type" Display="none" runat="server" />
			</td>
			<td style="width:25px;"></td>
			<td class="label">NCIC:</td>
			<td>
				<asp:RadioButtonList ID="rblNcicIsStolen" RepeatDirection="horizontal" RepeatLayout="Flow" TabIndex="6" runat="server">
					<asp:ListItem Value="True">Stolen</asp:ListItem>
					<asp:ListItem Value="False">Not Stolen</asp:ListItem>
				</asp:RadioButtonList>
			</td>
		</tr>		
		<tr>
			<td class="label">Make:</td>
			<td><Telerik:RadComboBox ID="rcbFirearmMakeTypes" DataValueField="FirearmMakeTypeID" DataTextField="Description" ShowToggleImage="false" AllowCustomText="False" MarkFirstMatch="true" Width="180" DropDownWidth="250" TabIndex="2" runat="server" /></td>
			<td></td>
			<td class="label">Pawnshop:</td>
			<td>
				<asp:RadioButtonList ID="rblPawnshopRecord" RepeatDirection="horizontal" RepeatLayout="Flow" TabIndex="7" runat="server">
					<asp:ListItem Value="True">Record</asp:ListItem>
					<asp:ListItem Value="False">No Record</asp:ListItem>
				</asp:RadioButtonList>
			</td>
		</tr>		
		<tr>
			<td class="label">Model:</td>
			<td><asp:TextBox ID="tbModel" MaxLength="50" Text='<%# Eval( "Model") %>' onblur="upperCaseIt(this);" Width="96" TabIndex="3" runat="server" /></td>
			<td></td>
			<td class="label">NCIC Contact:</td>
			<td><asp:TextBox ID="tbNcicContactName" Text='<%# Eval( "NcicContactName") %>' onblur="upperCaseIt(this);" Width="176" TabIndex="8" runat="server" /></td>
		</tr>		
		<tr>
			<td class="label">Caliber:</td>
			<td><Telerik:RadComboBox ID="rcbFirearmCaliberTypes" DataValueField="FirearmCaliberTypeID" DataTextField="Description" ShowToggleImage="false" AllowCustomText="False" MarkFirstMatch="true" OnClientFocus="showDropDown" Width="100" DropDownWidth="125" TabIndex="4" runat="server" /></td>
			<td></td>
			<td class="label">Pawnshop Contact:</td>
			<td><asp:TextBox ID="tbPawnshopContactName" Text='<%# Eval( "PawnshopContactName") %>' onblur="upperCaseIt(this);" Width="176" TabIndex="9" runat="server" /></td>
		</tr>		
		<tr>
			<td class="label">Serial:</td>
			<td><asp:TextBox ID="tbSerialNumber" Text='<%# Eval( "SerialNumber") %>' onblur="upperCaseIt(this);" Width="96" TabIndex="5" runat="server" /></td>
			<td></td>
			<td class="label">Additional Info:</td>
			<td><asp:TextBox ID="tbAdditionalInfo" MaxLength="100" Text='<%# Eval( "AdditionalInfo") %>' onblur="upperCaseIt(this);" Width="176" TabIndex="10" runat="server" /></td>
		</tr>
	</table>

	<asp:ValidationSummary ID="validationSummary" ValidationGroup="weapon" DisplayMode="bulletList" CssClass="validationSummary" HeaderText="Problems:" EnableClientScript="true" runat="server" />
		
	<asp:LinkButton ID="lbInsert" CommandName="PerformInsert" Text="Save Weapon" ValidationGroup="weapon" Visible='<%# (DataItem is Telerik.Web.UI.GridInsertionObject) %>' TabIndex="27" CssClass="flatButton" runat="server" />
	<asp:LinkButton ID="lbUpdate" CommandName="Update" Text="Update" ValidationGroup="weapon" Visible='<%# !(DataItem is Telerik.Web.UI.GridInsertionObject) %>' TabIndex="27" CssClass="flatButton" runat="server" />
	<asp:LinkButton ID="lbCancel" CommandName="Cancel" Text="Cancel" CausesValidation="false" TabIndex="28" CssClass="flatButton" runat="server" />

</div>