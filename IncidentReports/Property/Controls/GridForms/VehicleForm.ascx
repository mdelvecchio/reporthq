﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VehicleForm.ascx.cs" Inherits="ReportHq.IncidentReports.Property.Controls.GridForms.VehicleForm" %>

<Telerik:RadFormDecorator id="radFormDecorator" DecoratedControls="CheckBoxes" Skin="Metro" runat="server" />

<div class="gridForm">

	<table border="0" width="100%" style="margin-bottom:10px;">
		<tr>
			<td class="label">Status: <span class="required">*</span></td>
			<td>
				<Telerik:RadComboBox ID="rcbStatusTypes" DataValueField="VehicleStatusTypeID" DataTextField="Description" AllowCustomText="False" MarkFirstMatch="true" Width="125" TabIndex="1" runat="server" />
				<asp:RequiredFieldValidator ID="rfvStatusType" ControlToValidate="rcbStatusTypes" ValidationGroup="vehicle" ErrorMessage="Status" Display="none" runat="server" />
			</td>
			<td style="width:20px;"></td>
			<td class="label">Additional Desc:</td>
			<td><asp:TextBox ID="tbAdditionalDesc" Text='<%# Eval("AdditionalDescription") %>' MaxLength="50" onblur="upperCaseIt(this);" Width="171" TabIndex="12" runat="server" /></td>
		</tr>
		<tr>
			<td class="label">Year:</td>
			<td><Telerik:RadNumericTextBox ID="ntbYear" Text='<%# Eval("Year") %>' Type="Number" MaxLength="4" NumberFormat-DecimalDigits="0" NumberFormat-GroupSeparator="" MinValue="1900" MaxValue="2020" width="50" TabIndex="2" runat="server"/></td>
			<td></td>
			<td class="label">Recovered:</td>
			<td><Telerik:RadComboBox ID="rcbRecoveredTypes" DataValueField="VehicleRecoveredTypeID" DataTextField="Description" ShowToggleImage="true" AllowCustomText="False" MarkFirstMatch="true" OnClientFocus="showDropDown" Width="175" TabIndex="13" runat="server" /></td>
		</tr>
		<tr>
			<td class="label">Make:</td>
			<td>
				<Telerik:RadComboBox ID="rcbVehicleMakes" 
					DataValueField="VehicleMakeTypeID" 
					DataTextField="Description" 
					Width="175" 
					DropDownWidth="220" 
					AllowCustomText="False" 
					MarkFirstMatch="true" 
					ShowToggleImage="true"
					OnClientSelectedIndexChanging="LoadModels"
					TabIndex="3"
					runat="server"/>
			</td>
			<td></td>
			<td class="label">Recovery Locaction:</td>
			<td><asp:TextBox ID="tbRecoveryLocation" Text='<%# Eval("RecoveryLocation") %>' MaxLength="50" onblur="upperCaseIt(this);" Width="171" TabIndex="14" runat="server" /></td>
		</tr>
		<tr>
			<td class="label">Model:</td>
			<td>
				<Telerik:RadComboBox ID="rcbVehicleModels" 
					DataValueField="VehicleModelTypeID" 
					DataTextField="Description" 
					Width="175" 					
					AllowCustomText="False" 
					MarkFirstMatch="true" 
					ShowToggleImage="true"
					OnItemsRequested="rcbVehicleModels_ItemsRequested"
					OnClientItemsRequested="ItemsLoaded" 
					TabIndex="4" 
					runat="server" />
			</td>
			<td></td>
			<td class="label">District:</td>
			<td><Telerik:RadComboBox ID="rcbDistricts" DataValueField="District" DataTextField="District" ShowToggleImage="true" AllowCustomText="False" MarkFirstMatch="true" Width="75" TabIndex="15" runat="server" /></td>
		</tr>
		<tr>
			<td class="label">Color:</td>
			<td><Telerik:RadComboBox ID="rcbVehicleColors" DataValueField="VehicleColorTypeID" DataTextField="Description" DropDownWidth="145" ShowToggleImage="true" AllowCustomText="False" MarkFirstMatch="true" Width="175" TabIndex="5" runat="server" /></td>
			<td></td>
			<td class="label">Zone:</td>
			<td><Telerik:RadComboBox ID="rcbZones" DataValueField="Zone" DataTextField="Zone" ShowToggleImage="true" AllowCustomText="False" MarkFirstMatch="true" Width="75" TabIndex="16" runat="server" /></td>
		</tr>
		<tr>
			<td class="label">Style:</td>
			<td><Telerik:RadComboBox ID="rcbVehicleStyles" DataValueField="VehicleStyleTypeID" DataTextField="Description" ShowToggleImage="true" AllowCustomText="False" MarkFirstMatch="true" OnClientFocus="showDropDown" Width="175" TabIndex="6" runat="server" /></td>
			<td></td>
			<td class="label">Subzone:</td>
			<td><Telerik:RadComboBox ID="rcbSubZones" DataValueField="SubZone" DataTextField="SubZone" ShowToggleImage="true" AllowCustomText="False" MarkFirstMatch="true" Width="75" TabIndex="17" runat="server" /></td>
		</tr>
		<tr>
			<td class="label">Value:</td>
			<td><Telerik:RadNumericTextBox id="rntbValue" Text='<%# Eval("Value") %>' Type="Currency" NumberFormat-DecimalDigits="2" NumberFormat-GroupSeparator="," Width="75" TabIndex="7" runat="server" /></td>
			<td></td>
			<td class="label">Damage Level:</td>
			<td><Telerik:RadComboBox ID="rcbDamgeLevelTypes" DataValueField="VehicleDamageLevelTypeID" DataTextField="Description" ShowToggleImage="true" AllowCustomText="False" MarkFirstMatch="true" OnClientFocus="showDropDown" Width="75" TabIndex="18" runat="server" /></td>
		</tr>
		<tr>
			<td nowrap="nowrap" class="label">Lic Plate:</td>
			<td><asp:TextBox ID="tbLicensePlate" Text='<%# Eval("LicensePlate") %>' MaxLength="10" onblur="upperCaseIt(this);" Width="71" TabIndex="8" runat="server" /></td>
			<td></td>
			<td colspan="2" rowspan="5" valign="top">
				
				<div id="CarDamageMap">				
					<img src="images/carDamageMap.png" width="200" height="80"/>
					<div>
						<!-- <label style="top:-150px; left:40px;"><input type="checkbox" />01</label>
						<label style="top:-150px; left:80px;"><input type="checkbox" />02</label>
						<label style="top:-120px; left:120px;"><input type="checkbox" />03</label>
						<label style="top:-60px; left:78px;"><input type="checkbox" />04</label>
						<label style="top:-20px; left:-46px;"><input type="checkbox" />05</label>
						<label style="top:-20px; left:-170px;"><input type="checkbox" />06</label>
						<label style="top:-60px; left:-295px;"><input type="checkbox" />07</label>
						<label style="top:-120px; left:-335px;"><input type="checkbox" />08</label>
						<label style="top:-90px; left:-300px;"><input type="checkbox" />09</label>
						<label style="top:-90px; left:-280px;"><input type="checkbox" />10</label>
						<label style="top:-90px; left:-260px;"><input type="checkbox" />11</label> -->
					
						<label style="top:-102px; left:40px;"><input type="checkbox" /></label>
						<label style="top:-102px; left:70px;"><input type="checkbox" /></label>
						<label style="top:-102px; left:105px;"><input type="checkbox" /></label>
						
						<label style="top:-82px; left:130px;"><input type="checkbox" /></label>
						<label style="top:-27px; left:107px;"><input type="checkbox" /></label>
						
						<label style="top:-6px; left:36px;"><input type="checkbox" /></label>
						<label style="top:-6px; left:-45px;"><input type="checkbox" /></label>
						<label style="top:-6px; left:-121px;"><input type="checkbox" /></label>
						
						<label style="top:-27px; left:-205px;"><input type="checkbox" /></label>
						<label style="top:-82px; left:-228px;"><input type="checkbox" /></label>

						<label style="top:-54px; left:-187px;"><input type="checkbox" /></label>
						<label style="top:-54px; left:-150px;"><input type="checkbox" /></label>
						<label style="top:-54px; left:-115px;"><input type="checkbox" /></label>
					</div>
				</div>
				
				<asp:CheckBoxList ID="cblDamageZones" DataValueField="VehicleDamageZoneTypeID" DataTextField="MainframeCode" RepeatColumns="8" RepeatLayout="Flow" TabIndex="20" runat="server" Visible="false" />
			</td>
		</tr>
		<tr>
			<td class="label">State:</td>

			<td><Telerik:RadComboBox ID="rcbLicensePlateState" DataValueField="Abbreviation" DataTextField="State" ShowToggleImage="true" AllowCustomText="False" MarkFirstMatch="true" Width="175" TabIndex="9" runat="server" /></td>
			<td></td>
		</tr>
		<tr>
			<td class="label">Year:</td>
			<td><Telerik:RadNumericTextBox ID="rntbLicensePlateYear" Text='<%# Eval("LicensePlateYear") %>' Type="Number" MaxLength="4" NumberFormat-DecimalDigits="0" NumberFormat-GroupSeparator="" MinValue="1900" MaxValue="2020" width="50" TabIndex="10" runat="server"/></td>
			<td></td>
		</tr>
		<tr>
			<td class="label">VIN:</td>
			<td><asp:TextBox ID="tbVin" Text='<%# Eval("Vin") %>' MaxLength="50" onblur="upperCaseIt(this);" Width="175" TabIndex="11" runat="server" /></td>
			<td></td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
			<td></td>
		</tr>
		<tr>
			<td width="100%" colspan="5" style="padding:8px 0 8px 0;">
				<div class="checkBoxListLabel">Recovered Vehicle M.O.</div>
				<asp:CheckBoxList ID="cblRecoveredVehicleMos" DataValueField="RecoveredVehicleMoTypeID" DataTextField="Description" RepeatColumns="5" Width="100%" CellPadding="2" CellSpacing="0" TabIndex="20" runat="server" />
			</td>
		</tr>
	</table>

	<asp:ValidationSummary ID="validationSummary" ValidationGroup="vehicle" DisplayMode="bulletList" CssClass="validationSummary" HeaderText="Problems:" EnableClientScript="true" runat="server" />

	<asp:LinkButton ID="lbInsert" CommandName="PerformInsert" Text="Save Vehicle" ValidationGroup="vehicle" Visible='<%# (DataItem is Telerik.Web.UI.GridInsertionObject) %>' TabIndex="27" CssClass="flatButton" runat="server" />
	<asp:LinkButton ID="lbUpdate" CommandName="Update" Text="Update" ValidationGroup="vehicle" Visible='<%# !(DataItem is Telerik.Web.UI.GridInsertionObject) %>' TabIndex="27" CssClass="flatButton" runat="server" />
	<asp:LinkButton ID="lbCancel" CommandName="Cancel" Text="Cancel" CausesValidation="false" TabIndex="28" CssClass="flatButton" runat="server" />

</div>

<Telerik:RadScriptBlock ID="radScriptBlock" runat="server">
	<script type="text/javascript">
		//called when VehicleMakes changes
		function LoadModels(combo, eventargs)
		{
			var item = eventargs.get_item();

			var rcbVehicleModels = $find('<%= rcbVehicleModels.ClientID %>');

			if (item.get_index() > 0) //Make selected, get Models
			{
				rcbVehicleModels.set_text("Loading...");			
				rcbVehicleModels.requestItems(item.get_value(), false);                                
			}
			else //empty item selected, reset Models
			{
				rcbVehicleModels.set_text(" ");
				rcbVehicleModels.clearItems();
			}
		}
		
		//called when VehicleModels loads its items 
		function ItemsLoaded(combo, eventarqs)
		{
			if (combo.get_items().get_count() > 0)
			{
				combo.set_text(combo.get_items().getItem(0).get_text());
				combo.get_items().getItem(0).highlight();
			}
			
			combo.showDropDown();
		}
	</script>
</Telerik:RadScriptBlock>