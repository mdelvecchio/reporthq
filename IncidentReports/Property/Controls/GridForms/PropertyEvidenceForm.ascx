﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PropertyEvidenceForm.ascx.cs" Inherits="ReportHq.IncidentReports.Property.Controls.GridForms.PropertyEvidenceForm" %>

<div class="gridForm">

	<table border="0" style="margin-bottom:10px;">
		<tr>
			<td class="label">Loss Type: <span class="required">*</span></td>
			<td>
				<Telerik:RadComboBox ID="rcbLossTypes" DataValueField="PropertyEvidenceLossTypeID" DataTextField="Description" AllowCustomText="False" MarkFirstMatch="true" Width="175" TabIndex="1" runat="server" />
				<asp:RequiredFieldValidator ID="rfvLossType" ControlToValidate="rcbLossTypes" ValidationGroup="propEvItem" ErrorMessage="Loss Type" Display="none" runat="server" />
			</td>
			<td style="width:25px;"></td>
			<td class="label">Description:</td>
			<td><asp:TextBox ID="tbDescription" Text='<%# Eval( "Description") %>' MaxLength="250" onblur="upperCaseIt(this);" Width="171" TabIndex="6" runat="server" /></td>
		</tr>
		<tr>
			<td class="label">Type:</td>
			<td><Telerik:RadComboBox ID="rcbPropertyTypes" DataValueField="PropertyTypeID" DataTextField="Description" DropDownWidth="180" AllowCustomText="False" MarkFirstMatch="true" Width="175" TabIndex="2" runat="server" /></td>
			<td></td>
			<td class="label">Brand:</td>
			<td><asp:TextBox ID="tbBrand" Text='<%# Eval( "Brand") %>' MaxLength="50" onblur="upperCaseIt(this);" Width="171" TabIndex="7" runat="server" /></td>
		</tr>
		<tr>
			<td class="label">Quantity:</td>
			<td><Telerik:RadNumericTextBox id="rntbQuantity" Type="Number" MaxLength="6" NumberFormat-DecimalDigits="1" NumberFormat-GroupSeparator="," Text='<%# Eval("Quantity") %>' Width="75" TabIndex="3" runat="server" /></td>
			<td></td>
			<td class="label">Serial Number:</td>
			<td><asp:TextBox ID="tbSerialNumber" Text='<%# Eval( "SerialNumber") %>' MaxLength="50" onblur="upperCaseIt(this);" Width="171" TabIndex="8" runat="server" /></td>
		</tr>
		<tr>
			<td class="label">Narc Weight:</td>
			<td><Telerik:RadComboBox ID="rcbNarcWeightTypes" DataValueField="NarcoticWeightTypeID" DataTextField="Description" ShowToggleImage="true" AllowCustomText="False" MarkFirstMatch="true" OnClientFocus="showDropDown" Width="175" TabIndex="4" runat="server" /></td>
			<td></td>
			<td class="label">Value:</td>
			<td><Telerik:RadNumericTextBox id="rntbValue" Type="Currency" NumberFormat-DecimalDigits="2" NumberFormat-GroupSeparator="," Text='<%# Eval("Value") %>' Width="75" TabIndex="9" runat="server" /></td>
		</tr>
		<tr>
			<td class="label">Narc Type:</td>
			<td><Telerik:RadComboBox ID="rcbNarcTypes" DataValueField="NarcoticTypeID" DataTextField="Description" DropDownWidth="170" ShowToggleImage="true" AllowCustomText="False" MarkFirstMatch="true" OnClientFocus="showDropDown" Width="175" TabIndex="5" runat="server" /></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
	</table>

	<asp:ValidationSummary ID="validationSummary" ValidationGroup="property" DisplayMode="bulletList" CssClass="validationSummary" HeaderText="Problems:" EnableClientScript="true" runat="server" />
		
	<asp:LinkButton ID="lbInsert" CommandName="PerformInsert" Text="Save Item" ValidationGroup="property" Visible='<%# (DataItem is Telerik.Web.UI.GridInsertionObject) %>' TabIndex="27" CssClass="flatButton" runat="server" />
	<asp:LinkButton ID="lbUpdate" CommandName="Update" Text="Update" ValidationGroup="property" Visible='<%# !(DataItem is Telerik.Web.UI.GridInsertionObject) %>' TabIndex="27" CssClass="flatButton" runat="server" />	
	<asp:LinkButton ID="lbCancel" CommandName="Cancel" Text="Cancel" CausesValidation="false" TabIndex="28" CssClass="flatButton" runat="server" />

</div>