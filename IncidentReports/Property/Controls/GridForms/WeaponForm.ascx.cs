﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReportHq.Common;
using ReportHq.IncidentReports.Bases;
using Telerik.Web.UI;

namespace ReportHq.IncidentReports.Property.Controls.GridForms
{
	public partial class WeaponForm : System.Web.UI.UserControl
	{
		#region Properties

		private object _dataItem;

		public object DataItem
		{
			get { return _dataItem; }
			set { _dataItem = value; }
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Event Handlers

		override protected void OnInit(EventArgs e)
		{
			//needed to initiate special event handler for dropdownlists bindings
			this.DataBinding += new EventHandler(WeaponForm_DataBinding);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void Page_Load(object sender, EventArgs e)
		{
			//rntbVictimNumber.Focus();
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		private void Page_PreRender(object sender, EventArgs e)
		{
			if (!(this.Page as ReportPage).UserHasWriteAccess)
			{
				lbInsert.Visible = false;
				lbUpdate.Visible = false;
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Was told by Telerik that any list-binding for this control must happen in this custom event handler.
		/// </summary>
		private void WeaponForm_DataBinding(object sender, System.EventArgs e)
		{
			//firearm types
			rcbFirearmTypes.DataSource = Weapon.GetFirearmTypes();
			rcbFirearmTypes.DataBind();
			rcbFirearmTypes.Items.Insert(0, new RadComboBoxItem());

			//firearm makes
			rcbFirearmMakeTypes.DataSource = Weapon.GetFirearmMakeTypes();
			rcbFirearmMakeTypes.DataBind();
			rcbFirearmMakeTypes.Items.Insert(0, new RadComboBoxItem());

			//firearm calibers
			rcbFirearmCaliberTypes.DataSource = Weapon.GetFirearmCaliberTypes();
			rcbFirearmCaliberTypes.DataBind();
			rcbFirearmCaliberTypes.Items.Insert(0, new RadComboBoxItem());


			//set grid item's values
			rcbFirearmTypes.Items.FindItemByValue(DataBinder.Eval(this.DataItem, "FirearmTypeID").ToString()).Selected = true;
			rcbFirearmMakeTypes.Items.FindItemByValue(DataBinder.Eval(this.DataItem, "FirearmMakeTypeID").ToString()).Selected = true;
			rcbFirearmCaliberTypes.Items.FindItemByValue(DataBinder.Eval(this.DataItem, "FirearmCaliberTypeID").ToString()).Selected = true;

			
			//deselects the first item in the dropdown lists (keeps blank)
			rcbFirearmTypes.DataSource = null;
			rcbFirearmMakeTypes.DataSource = null;
			rcbFirearmCaliberTypes.DataSource = null;
			//http://www.telerik.com/help/aspnet-ajax/grdcustomeditforms.html


			//set radio button list values
			string theValue = DataBinder.Eval(this.DataItem, "NcicIsStolen").ToString();
			if (!string.IsNullOrEmpty(theValue))
				rblNcicIsStolen.SelectedValue = theValue;

			theValue = DataBinder.Eval(this.DataItem, "HasPawnshopRecord").ToString();
			if (!string.IsNullOrEmpty(theValue))
				rblPawnshopRecord.SelectedValue = theValue;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}