﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReportHq.Common;
using ReportHq.IncidentReports.Bases;
using Telerik.Web.UI;

namespace ReportHq.IncidentReports.Property.Controls.GridForms
{
	public partial class PropertyEvidenceForm : System.Web.UI.UserControl
	{
		#region Properties

		private object _dataItem;

		public object DataItem
		{
			get { return _dataItem; }
			set { _dataItem = value; }
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Event Handlers

		override protected void OnInit(EventArgs e)
		{
			//needed to initiate special event handler for dropdownlists bindings
			this.DataBinding += new EventHandler(PropertyEvidenceForm_DataBinding);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void Page_Load(object sender, EventArgs e)
		{
			//rntbVictimNumber.Focus();
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		private void Page_PreRender(object sender, EventArgs e)
		{
			if (!(this.Page as ReportPage).UserHasWriteAccess)
			{
				lbInsert.Visible = false;
				lbUpdate.Visible = false;
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Was told by Telerik that any list-binding for this control must happen in this custom event handler.
		/// </summary>
		private void PropertyEvidenceForm_DataBinding(object sender, System.EventArgs e)
		{
			//loss types
			rcbLossTypes.DataSource = Bases.PropertyEvidenceItem.GetPropertyEvidenceLossTypes();
			rcbLossTypes.DataBind();
			rcbLossTypes.Items.Insert(0, new RadComboBoxItem());

			//property types
			rcbPropertyTypes.DataSource = Bases.PropertyEvidenceItem.GetPropertyTypes();
			rcbPropertyTypes.DataBind();
			rcbPropertyTypes.Items.Insert(0, new RadComboBoxItem());

			//narc weights
			rcbNarcWeightTypes.DataSource = Bases.PropertyEvidenceItem.GetNarcoticWeightTypes();
			rcbNarcWeightTypes.DataBind();
			rcbNarcWeightTypes.Items.Insert(0, new RadComboBoxItem());

			//narc types
			rcbNarcTypes.DataSource = Bases.PropertyEvidenceItem.GetNarcoticTypes();
			rcbNarcTypes.DataBind();
			rcbNarcTypes.Items.Insert(0, new RadComboBoxItem());


			//set grid item's values
			rcbLossTypes.Items.FindItemByValue(DataBinder.Eval(this.DataItem, "PropertyEvidenceLossTypeID").ToString()).Selected = true;
			rcbPropertyTypes.Items.FindItemByValue(DataBinder.Eval(this.DataItem, "PropertyTypeID").ToString()).Selected = true;
			rcbNarcWeightTypes.Items.FindItemByValue(DataBinder.Eval(this.DataItem, "NarcoticWeightTypeID").ToString()).Selected = true;
			rcbNarcTypes.Items.FindItemByValue(DataBinder.Eval(this.DataItem, "NarcoticTypeID").ToString()).Selected = true;
			
			//deselects the first item in the dropdown lists (keeps blank)
			rcbLossTypes.DataSource = null;
			rcbPropertyTypes.DataSource = null;
			rcbNarcWeightTypes.DataSource = null;
			rcbNarcTypes.DataSource = null;
			//http://www.telerik.com/help/aspnet-ajax/grdcustomeditforms.html


		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}