﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PropertyEvidence.ascx.cs" Inherits="ReportHq.IncidentReports.Property.Controls.PropertyEvidence" %>
<%@ Register TagPrefix="ReportHq" TagName="ReportSubNav" src="ReportSubNav.ascx" %>

<ReportHq:ReportSubNav ID="ucReportSubNav" CurrentSection="PropertyEvidence" OnNavItemClicked="ucReportSubNav_NavItemClicked" runat="server" />
	
<div class="formBox" style="width:740px;">

	<Telerik:RadGrid ID="gridProperty" 
		AllowPaging="false"
		AllowSorting="true" 				
		AutoGenerateColumns="False"
		AllowMultiRowSelection="true"
		OnNeedDataSource="gridProperty_NeedDataSource"
		OnInsertCommand="gridProperty_InsertCommand"
		OnUpdateCommand="gridProperty_UpdateCommand" 
		OnDeleteCommand="gridProperty_DeleteCommand"
		runat="server">

		<ClientSettings 
			EnableRowHoverStyle="true"
			AllowDragToGroup="false" 
			AllowColumnsReorder="false" 
			AllowKeyboardNavigation="true"
			ReorderColumnsOnClient="false" >
			<Resizing AllowColumnResize="false" AllowRowResize="false" />
			<Selecting AllowRowSelect="false" />
		</ClientSettings>
			
		<MasterTableView 
			CommandItemDisplay="Top" 
			DataKeyNames="PropertyID" 
			EditMode="EditForms" 
			NoMasterRecordsText="No Property to display." 
			HeaderStyle-ForeColor="#191970" 
			ItemStyle-ForeColor="black" 
			AlternatingItemStyle-ForeColor="black">
				
			<CommandItemSettings AddNewRecordText="Add Property" ShowRefreshButton="false" />
			<CommandItemStyle CssClass="commandItem" />
				
			<Columns>
				<Telerik:GridEditCommandColumn ButtonType="LinkButton" UniqueName="editColumn" ItemStyle-HorizontalAlign="center" ItemStyle-CssClass="commandItem" />
				<Telerik:GridButtonColumn CommandName="Delete" ButtonType="LinkButton" Text="Delete" UniqueName="deleteColumn" ConfirmText="Delete Property?" ConfirmTitle="Delete" ItemStyle-HorizontalAlign="center" ItemStyle-CssClass="commandItem" />
				<Telerik:GridBoundColumn HeaderText="PropertyID" UniqueName="PropertyID" DataField="PropertyID" ReadOnly="true" Visible="false"/>
				<Telerik:GridBoundColumn HeaderText="Loss Type" UniqueName="LossType" DataField="LossType" />
				<Telerik:GridBoundColumn HeaderText="Type" UniqueName="Type" DataField="Type" HeaderStyle-Wrap="false" />
				<Telerik:GridBoundColumn HeaderText="Quantity" UniqueName="Quantity" DataField="Quantity" />
				<telerik:GridBoundColumn HeaderText="Value" UniqueName="Value" DataField="Value" DataFormatString="{0:c}" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
				<Telerik:GridBoundColumn HeaderText="Narc. Weight" UniqueName="WeightUnit" DataField="NarcoticWeightUnit" HeaderStyle-Wrap="false" />
				<Telerik:GridBoundColumn HeaderText="Narc. Type" UniqueName="NarcoticType" DataField="NarcoticType" HeaderStyle-Wrap="false" />
				<Telerik:GridBoundColumn HeaderText="Description" UniqueName="Description" DataField="Description" />
				<Telerik:GridBoundColumn HeaderText="Brand" UniqueName="Brand" DataField="Brand" />
			</Columns>
				
			<EditFormSettings EditFormType="WebUserControl" UserControlName="controls/GridForms/PropertyEvidenceForm.ascx" />
				
		</MasterTableView>
	</Telerik:RadGrid>
		
	<br /><br />
		
	<Telerik:RadGrid ID="gridEvidence" 
		AllowPaging="false"
		AllowSorting="true" 				
		AutoGenerateColumns="False"
		AllowMultiRowSelection="true"
		OnNeedDataSource="gridEvidence_NeedDataSource"
		OnInsertCommand="gridEvidence_InsertCommand"
		OnUpdateCommand="gridEvidence_UpdateCommand" 
		OnDeleteCommand="gridEvidence_DeleteCommand"
		runat="server">

		<ClientSettings 
			EnableRowHoverStyle="true"
			AllowDragToGroup="false" 
			AllowColumnsReorder="false" 
			AllowKeyboardNavigation="true"
			ReorderColumnsOnClient="false" >
			<Resizing AllowColumnResize="false" AllowRowResize="false" />
			<Selecting AllowRowSelect="false" />
		</ClientSettings>
			
		<MasterTableView 
			CommandItemDisplay="Top" 
			DataKeyNames="EvidenceID" 
			EditMode="EditForms" 
			NoMasterRecordsText="No Evidence to display." 
			HeaderStyle-ForeColor="#191970" 
			ItemStyle-ForeColor="black" 
			AlternatingItemStyle-ForeColor="black">
				
			<CommandItemSettings AddNewRecordText="Add Evidence" ShowRefreshButton="false" />
			<CommandItemStyle CssClass="commandItem" />
				
			<Columns>
				<Telerik:GridEditCommandColumn ButtonType="LinkButton" UniqueName="editColumn" ItemStyle-HorizontalAlign="center" ItemStyle-CssClass="commandItem" />
				<Telerik:GridButtonColumn CommandName="Delete" ButtonType="LinkButton" Text="Delete" UniqueName="deleteColumn" ConfirmText="Delete Evidence?" ConfirmTitle="Delete" ItemStyle-HorizontalAlign="center" ItemStyle-CssClass="commandItem" />
				<Telerik:GridBoundColumn HeaderText="EvidenceID" UniqueName="EvidenceID" DataField="EvidenceID" ReadOnly="true" Visible="false"/>
				<Telerik:GridBoundColumn HeaderText="Loss Type" UniqueName="LossType" DataField="LossType" />
				<Telerik:GridBoundColumn HeaderText="Type" UniqueName="Type" DataField="Type" HeaderStyle-Wrap="false" />
				<Telerik:GridBoundColumn HeaderText="Quantity" UniqueName="Quantity" DataField="Quantity" />
				<telerik:GridBoundColumn HeaderText="Value" UniqueName="Value" DataField="Value" DataFormatString="{0:c}" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
				<Telerik:GridBoundColumn HeaderText="Narc. Weight" UniqueName="WeightUnit" DataField="NarcoticWeightUnit" HeaderStyle-Wrap="false" />
				<Telerik:GridBoundColumn HeaderText="Narc. Type" UniqueName="NarcoticType" DataField="NarcoticType" HeaderStyle-Wrap="false" />
				<Telerik:GridBoundColumn HeaderText="Description" UniqueName="Description" DataField="Description" />
				<Telerik:GridBoundColumn HeaderText="Brand" UniqueName="Brand" DataField="Brand" />
			</Columns>
				
			<EditFormSettings EditFormType="WebUserControl" UserControlName="controls/GridForms/PropertyEvidenceForm.ascx" />
				
		</MasterTableView>
	</Telerik:RadGrid>

	

	<div class="label" style="margin-top:33px;">
		Property/Evidence Receipt: <asp:TextBox ID="tbReceiptNumber" onblur="upperCaseIt(this);" runat="server"/>
	</div>

</div>

<telerik:RadAjaxManager ID="ajaxManager" DefaultLoadingPanelID="ajaxLoadingPanel" runat="server" />