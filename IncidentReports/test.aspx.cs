﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReportHq.IncidentReports.DataAccess;
using ReportHq.Common;

namespace ReportHq.IncidentReports
{
	public partial class test : System.Web.UI.Page
	{
		
		//CURRENTLY: generating dummy UCR data for UCR report screen

		protected void Page_Load(object sender, EventArgs e)
		{
			string query = string.Empty;
			Random random = new Random();
			string _connStr = ConfigurationManager.ConnectionStrings["ReportHq"].ConnectionString;
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = null;
			
						
			//DateTime endDate = DateTime.Now;
			//DateTime startDate = endDate.AddYears(-1);

			DateTime startDate = new DateTime(2012, 1, 1);
			DateTime endDate = new DateTime(2014, 6, 4);
			
			foreach (DateTime day in DateBuddy.GetDateRange(startDate, endDate))
			{
				query = String.Format("INSERT INTO DummyDataForUcrReport([OccurredDate],[UcrOffenseTypeId]) VALUES('{0}', {1})", day.ToString(), random.Next(1, 9));

				litTemp.Text += query + "<br/>";

				command = new SqlCommand(query, conn);
				command.CommandType = CommandType.Text;

				try
				{
					conn.Open();
					command.ExecuteNonQuery();
				}
				catch (Exception ex)
				{
					string x = ex.Message;
				}
				finally
				{
					conn.Close();
				}				
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

	}

}