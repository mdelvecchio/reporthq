﻿<%@ Page Title="Supervisor Notes" Language="C#" MasterPageFile="~/Popup.Master" AutoEventWireup="true" CodeBehind="SupervisorNotes.aspx.cs" Inherits="ReportHq.IncidentReports.SupervisorNotes" %>

<asp:Content ID="Content2" ContentPlaceHolderID="phContent" runat="server">

	<h1>Supervisor Notes:</h1>

	<asp:Label ID="lblNotes" runat="server" />	

</asp:Content>