﻿<%@ Page Title="Submit Report" Language="C#" MasterPageFile="~/Popup.Master" AutoEventWireup="true" CodeBehind="SubmitReport.aspx.cs" Inherits="ReportHq.IncidentReports.SubmitReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<script src="js/radWindowHelpers.js" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="phContent" runat="server">

	<asp:LinkButton ID="lbSubmit" OnClick="lbSubmit_Click" CssClass="flatButton" visible="false" runat="server"><i class="fa fa-check fa-fw"></i> Submit Report</asp:LinkButton> &nbsp;
	<asp:LinkButton ID="lbCancel" OnClientClick="closeRadWindow();" CssClass="flatButton" runat="server">Cancel</asp:LinkButton>

</asp:Content>