# README #

This repo is a demo of code for a Visual Studio solution Matt Del Vecchio built and supported, based on an earlier solution I also created. It is an incident records management system (RMS) for police reports, originally built for the NOPD. It contains four projects: 

* Common - shared helper classes
* IncidentReports - main web client/UI. BAL  inside "/Classes"
* IncidentReports.DataAcces - DAL
* IncidentReports.Entities - base entity objects inherited by BAL, and used as data-transfer objects between DAL & BAL. The DAL uses DataReaders to populate the entities, which is faster than old way of using DataSets to return DataTables between layers. See its _entity usage.txt for more details.

### What is this repository for? ###

* sample of my coding and organization