using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;

using ReportHq.Common;

namespace ReportHq.IncidentReports.DataAccess
{
	/// <summary>
	/// DA layer for the GlobalNofitication and ReportNotification objects.
	/// </summary>
	public class AttachmentsDA
	{
		#region Variables

		static private string _connStr = ConfigurationManager.ConnectionStrings["ReportHq"].ConnectionString;

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Public Methods

		public static StatusInfo InsertAttachment(int reportID, string savedPath, int fileTypeID)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("InsertAttachment", conn);
			command.CommandType = CommandType.StoredProcedure;

			#region params

			command.Parameters.Add("@reportID", SqlDbType.Int).Value = reportID;
			command.Parameters.Add("@savedPath", SqlDbType.NVarChar, 100).Value = savedPath;
			command.Parameters.Add("@attachmentTypeID", SqlDbType.Int).Value = fileTypeID;

			#endregion

			//do it
			try
			{
				conn.Open();
				command.ExecuteNonQuery();

				return new StatusInfo(true);
			}
			catch (Exception ex)
			{
				//send back error
				return new StatusInfo(false, ex.Message);
			}
			finally
			{
				conn.Close();
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataSet GetAttachmentsByReportID(int reportID)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetAttachmentsByReportID", conn);
			command.CommandType = CommandType.StoredProcedure;

			//params
			command.Parameters.Add("@reportID", SqlDbType.Int).Value = reportID;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static StatusInfo DeleteAttachment(int attachmentID)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("DeleteAttachment", conn);
			command.CommandType = CommandType.StoredProcedure;

			#region params

			command.Parameters.Add("@ID", SqlDbType.Int).Value = attachmentID;

			//output - file's saved path
			SqlParameter param = new SqlParameter("@savedPath", SqlDbType.NVarChar, 100);
			param.Direction = ParameterDirection.Output;
			command.Parameters.Add(param);

			#endregion

			//do it
			try
			{
				conn.Open();
				command.ExecuteNonQuery();

				return new StatusInfo(true, (string)param.Value);
			}
			catch (Exception ex)
			{
				//send back error
				return new StatusInfo(false, ex.Message);
			}
			finally
			{
				conn.Close();
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}	
}
