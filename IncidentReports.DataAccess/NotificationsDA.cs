using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;

using ReportHq.Common;

namespace ReportHq.IncidentReports.DataAccess
{
	/// <summary>
	/// DA layer for the GlobalNofitication and ReportNotification objects.
	/// </summary>
	public class NotificationsDA
	{
		#region Variables

		static private string _connStr = ConfigurationManager.ConnectionStrings["ReportHq"].ConnectionString;

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Public Methods

		/// <summary>
		///Gets notifications addresse to a single user.
		/// </summary>
		/// <param name="username"></param>
		/// <param name="notificationTypeID"></param>
		/// <param name="includeNewish">If true, notifications viewed less-than 24-hours ago will be included (ex for APBs).</param>
		public static DataTable GetNewNotificationsByType(string username, int notificationTypeID, bool includeNewish)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetNewNotificationsByType", conn);
			command.CommandType = CommandType.StoredProcedure;

			#region params

			command.Parameters.Add("@recipient", SqlDbType.NVarChar, 50).Value = username;
			command.Parameters.Add("@notificationTypeID", SqlDbType.Int).Value = notificationTypeID;

			if (includeNewish)
				command.Parameters.Add("@includeNewish", SqlDbType.Bit).Value = true;

			#endregion

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetNewGlobalNotificationsByType(string username, int notificationTypeID, bool includeNewish)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetNewGlobalNotificationsByType", conn);
			command.CommandType = CommandType.StoredProcedure;

			#region params

			command.Parameters.Add("@recipient", SqlDbType.NVarChar, 50).Value = username;
			command.Parameters.Add("@notificationTypeID", SqlDbType.Int).Value = notificationTypeID;

			if (includeNewish)
				command.Parameters.Add("@includeNewish", SqlDbType.Bit).Value = true;

			#endregion

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataSet GetNotificationsByRecipient(string username)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetNotificationsByRecipient", conn);
			command.CommandType = CommandType.StoredProcedure;

			//params
			command.Parameters.Add("@recipient", SqlDbType.NVarChar, 50).Value = username;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetNotificationsByType(int notificationTypeID)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetNotificationsByType", conn);
			command.CommandType = CommandType.StoredProcedure;

			//params
			command.Parameters.Add("@notificationTypeID", SqlDbType.Int).Value = notificationTypeID;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		
		public static StatusInfo SetNotificationAsRead(string username, string itemIDs, int mode)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("SetNotificationsAsRead", conn);
			command.CommandType = CommandType.StoredProcedure;

			#region params

			command.Parameters.Add("@recipient", SqlDbType.NVarChar, 50).Value = username;
			command.Parameters.Add("@itemIDs", SqlDbType.Xml).Value = itemIDs;
			command.Parameters.Add("@mode", SqlDbType.Int).Value = mode;

			#endregion

			//do it
			try
			{
				conn.Open();
				command.ExecuteNonQuery();

				return new StatusInfo(true);
			}
			catch (Exception ex)
			{
				//send back error
				return new StatusInfo(false, ex.Message);
			}
			finally
			{
				conn.Close();
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static StatusInfo InsertNotification(int typeID, int relatedObjectID, string message, string url, string recipient, string createdBy)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("InsertNotification", conn);
			command.CommandType = CommandType.StoredProcedure;

			#region params

			command.Parameters.Add("@notificationTypeID", SqlDbType.Int).Value = typeID;

			if (relatedObjectID > -1)
				command.Parameters.Add("@relatedObjectID", SqlDbType.Int).Value = relatedObjectID;

			command.Parameters.Add("@message", SqlDbType.NVarChar, 100).Value = message;

			if (!String.IsNullOrEmpty(url))
				command.Parameters.Add("@url", SqlDbType.NVarChar, 50).Value = url;

			command.Parameters.Add("@recipient", SqlDbType.NVarChar, 50).Value = recipient;
			command.Parameters.Add("@createdBy", SqlDbType.NVarChar, 50).Value = createdBy;

			#endregion

			//do it
			try
			{
				conn.Open();
				command.ExecuteNonQuery();

				return new StatusInfo(true);
			}
			catch (Exception ex)
			{
				//send back error
				return new StatusInfo(false, ex.Message);
			}
			finally
			{
				conn.Close();
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static StatusInfo InsertGlobalNotification(int typeID, int relatedObjectID, string message, string url, string createdBy)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("InsertNotificationGlobal", conn);
			command.CommandType = CommandType.StoredProcedure;

			#region params

			command.Parameters.Add("@notificationTypeID", SqlDbType.Int).Value = typeID;

			if (relatedObjectID > -1)
				command.Parameters.Add("@relatedObjectID", SqlDbType.Int).Value = relatedObjectID;

			command.Parameters.Add("@message", SqlDbType.NVarChar, 100).Value = message;

			if (!String.IsNullOrEmpty(url))
				command.Parameters.Add("@url", SqlDbType.NVarChar, 50).Value = url;

			command.Parameters.Add("@createdBy", SqlDbType.NVarChar, 50).Value = createdBy;

			#endregion

			//do it
			try
			{
				conn.Open();
				command.ExecuteNonQuery();

				return new StatusInfo(true);
			}
			catch (Exception ex)
			{
				//send back error
				return new StatusInfo(false, ex.Message);
			}
			finally
			{
				conn.Close();
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		
		public static StatusInfo UpdateNotification(int notificationID, string message)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("UpdateNotification", conn);
			command.CommandType = CommandType.StoredProcedure;

			#region params

			command.Parameters.Add("@notificationID", SqlDbType.Int).Value = notificationID;
			command.Parameters.Add("@message", SqlDbType.NVarChar, 100).Value = message;

			#endregion

			//do it
			try
			{
				conn.Open();
				command.ExecuteNonQuery();

				return new StatusInfo(true);
			}
			catch (Exception ex)
			{
				//send back error
				return new StatusInfo(false, ex.Message);
			}
			finally
			{
				conn.Close();
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static StatusInfo DeleteNotification(int notificationID)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("DeleteNotification", conn);
			command.CommandType = CommandType.StoredProcedure;

			//params
			command.Parameters.Add("@notificationID", SqlDbType.Int).Value = notificationID;

			//do it
			try
			{
				conn.Open();
				command.ExecuteNonQuery();

				return new StatusInfo(true);
			}
			catch (Exception ex)
			{
				//send back error
				return new StatusInfo(false, ex.Message);
			}
			finally
			{
				conn.Close();
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''







		/// <summary>
		/// TEMP only. reset notification read-states in order to have un-read notification badges show up in UI.
		/// </summary>
		public static StatusInfo TempResetNotifications()
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("dev.ResetNotifications", conn);
			command.CommandType = CommandType.StoredProcedure;

			//do it
			try
			{
				conn.Open();
				command.ExecuteNonQuery();

				return new StatusInfo(true);
			}
			catch (Exception ex)
			{
				//send back error
				return new StatusInfo(false, ex.Message);
			}
			finally
			{
				conn.Close();
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
			
		#endregion



























		#region old code

		//public static DataTable GetNewGlobalNotificationsByRecipient(string username)
		//{
		//	//conn & command
		//	SqlConnection conn = new SqlConnection(_connStr);
		//	SqlCommand command = new SqlCommand("GetNewGlobalNotificationsByRecipient", conn);
		//	command.CommandType = CommandType.StoredProcedure;

		//	//params
		//	command.Parameters.Add("@recipient", SqlDbType.NVarChar, 50).Value = username;

		//	//do it
		//	DataSet ds = new DataSet();
		//	SqlDataAdapter da = new SqlDataAdapter(command);
		//	da.Fill(ds);

		//	return ds.Tables[0];
		//}

		////'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		//public static StatusInfo SetGlobalNotificationAsRead(string username, string itemIDs)
		//{
		//	//conn & command
		//	SqlConnection conn = new SqlConnection(_connStr);
		//	SqlCommand command = new SqlCommand("SetGlobalNotificationsAsRead", conn);
		//	command.CommandType = CommandType.StoredProcedure;

		//	#region params

		//	command.Parameters.Add("@recipient", SqlDbType.NVarChar, 50).Value = username;
		//	command.Parameters.Add("@itemIDs", SqlDbType.Xml).Value = itemIDs;

		//	#endregion

		//	//do it
		//	try
		//	{
		//		conn.Open();
		//		command.ExecuteNonQuery();

		//		return new StatusInfo(true);
		//	}
		//	catch (Exception ex)
		//	{
		//		//send back error
		//		return new StatusInfo(false, ex.Message);
		//	}
		//	finally
		//	{
		//		conn.Close();
		//	}
		//}

		////'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		//public static DataTable GetNewReportNotificationsByRecipient(string username)
		//{
		//	//conn & command
		//	SqlConnection conn = new SqlConnection(_connStr);
		//	SqlCommand command = new SqlCommand("GetNewReportNotificationsByRecipient", conn);
		//	command.CommandType = CommandType.StoredProcedure;

		//	//params
		//	command.Parameters.Add("@recipient", SqlDbType.NVarChar, 50).Value = username;

		//	//do it
		//	DataSet ds = new DataSet();
		//	SqlDataAdapter da = new SqlDataAdapter(command);
		//	da.Fill(ds);

		//	return ds.Tables[0];
		//}

		////'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		//public static StatusInfo SetReportNotificationAsRead(string username, string itemIDs)
		//{
		//	//conn & command
		//	SqlConnection conn = new SqlConnection(_connStr);
		//	SqlCommand command = new SqlCommand("SetReportNotificationsAsRead", conn);
		//	command.CommandType = CommandType.StoredProcedure;

		//	#region params

		//	command.Parameters.Add("@recipient", SqlDbType.NVarChar, 50).Value = username;
		//	command.Parameters.Add("@itemIDs", SqlDbType.Xml).Value = itemIDs;

		//	#endregion

		//	//do it
		//	try
		//	{
		//		conn.Open();
		//		command.ExecuteNonQuery();

		//		return new StatusInfo(true);
		//	}
		//	catch (Exception ex)
		//	{
		//		//send back error
		//		return new StatusInfo(false, ex.Message);
		//	}
		//	finally
		//	{
		//		conn.Close();
		//	}
		//}

		////'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		//public static StatusInfo InsertGlobalNotification(int typeID, int objectID, string message, string url, string username)
		//{
		//	//conn & command
		//	SqlConnection conn = new SqlConnection(_connStr);
		//	SqlCommand command = new SqlCommand("InsertGlobalNotification", conn);
		//	command.CommandType = CommandType.StoredProcedure;

		//	#region params

		//	command.Parameters.Add("@globalNotificationTypeID", SqlDbType.Int).Value = typeID;
		//	command.Parameters.Add("@relatedObjectID", SqlDbType.Int).Value = objectID;
		//	command.Parameters.Add("@message", SqlDbType.NVarChar, 100).Value = message;
		//	command.Parameters.Add("@url", SqlDbType.NVarChar, 50).Value = url;
		//	command.Parameters.Add("@createdBy", SqlDbType.NVarChar, 50).Value = username;

		//	#endregion

		//	//do it
		//	try
		//	{
		//		conn.Open();
		//		command.ExecuteNonQuery();

		//		return new StatusInfo(true);
		//	}
		//	catch (Exception ex)
		//	{
		//		//send back error
		//		return new StatusInfo(false, ex.Message);
		//	}
		//	finally
		//	{
		//		conn.Close();
		//	}
		//}

		////'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		//public static StatusInfo InsertReportNotification(int reportID, string recipient, string message)
		//{
		//	//conn & command
		//	SqlConnection conn = new SqlConnection(_connStr);
		//	SqlCommand command = new SqlCommand("InsertReportNotification", conn);
		//	command.CommandType = CommandType.StoredProcedure;

		//	#region params

		//	command.Parameters.Add("@reportID", SqlDbType.Int).Value = reportID;
		//	command.Parameters.Add("@recipient", SqlDbType.NVarChar, 50).Value = recipient;
		//	command.Parameters.Add("@message", SqlDbType.NVarChar, 100).Value = message;

		//	#endregion

		//	//do it
		//	try
		//	{
		//		conn.Open();
		//		command.ExecuteNonQuery();

		//		return new StatusInfo(true);
		//	}
		//	catch (Exception ex)
		//	{
		//		//send back error
		//		return new StatusInfo(false, ex.Message);
		//	}
		//	finally
		//	{
		//		conn.Close();
		//	}
		//}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion


		
	}	
}
