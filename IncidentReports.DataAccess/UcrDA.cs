using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;

using ReportHq.Common;

namespace ReportHq.IncidentReports.DataAccess
{
	/// <summary>
	/// DA layer for the Bulletin objects.
	/// </summary>
	public class UcrDA
	{
		#region Variables

		static private string _connStr = ConfigurationManager.ConnectionStrings["ReportHq"].ConnectionString;

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Public Methods

		/// <summary>
		/// Not sure whether to use one bigass method for all like this, or individual methods.
		/// </summary>
		public static StatusInfo UpdateOffense(int reportID,
											   string burglaryTimeOfDay,
											   string burglaryCrimeLocation,
											   string burglaryForceUsed,
											   string larcenyNature,
											   string modifiedBy)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("UpdateUcrOffensePartOne", conn);
			command.CommandType = CommandType.StoredProcedure;

			#region params

			//required
			command.Parameters.Add("@reportID", SqlDbType.Int).Value = reportID;
			command.Parameters.Add("@modifiedBy", SqlDbType.NVarChar, 50).Value = modifiedBy;

			//optional - per offense type

			//burglary
			if (!string.IsNullOrEmpty(burglaryTimeOfDay))
				command.Parameters.Add("@burglaryTimeOfDay", SqlDbType.NVarChar, 5).Value = burglaryTimeOfDay;

			if (!string.IsNullOrEmpty(burglaryCrimeLocation))
				command.Parameters.Add("@burglaryCrimeLocation", SqlDbType.NVarChar, 15).Value = burglaryCrimeLocation;

			if (!string.IsNullOrEmpty(burglaryForceUsed))
				command.Parameters.Add("@burglaryForceUsed", SqlDbType.NVarChar, 15).Value = burglaryForceUsed;
			
			//larceny
			if (!string.IsNullOrEmpty(larcenyNature))
				command.Parameters.Add("@larcenyNature", SqlDbType.NVarChar, 50).Value = larcenyNature;

			
			//TODO: additional sections
			

			#endregion

			//do it
			try
			{
				conn.Open();
				command.ExecuteNonQuery();

				return new StatusInfo(true);
			}
			catch (Exception ex)
			{
				//send back error
				return new StatusInfo(false, ex.Message);
			}
			finally
			{
				conn.Close();
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Not sure whether to use individual methods like this one, or one bigass method for all.
		/// </summary>
		public static StatusInfo UpdateBurglary(int reportID, 
												string timeOfDay, 
												string crimeLocation, 
												string forceUsed, 
												string modifiedBy)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("UpdateUcrBurglary", conn);
			command.CommandType = CommandType.StoredProcedure;

			#region params

			command.Parameters.Add("@reportID", SqlDbType.Int).Value = reportID;
			command.Parameters.Add("@timeOfDay", SqlDbType.NVarChar, 5).Value = timeOfDay;
			command.Parameters.Add("@crimeLocation", SqlDbType.NVarChar, 10).Value = crimeLocation;
			command.Parameters.Add("@forceUsed", SqlDbType.NVarChar, 20).Value = forceUsed;
			command.Parameters.Add("@modifiedBy", SqlDbType.NVarChar, 50).Value = modifiedBy;

			#endregion

			//do it
			try
			{
				conn.Open();
				command.ExecuteNonQuery();

				return new StatusInfo(true);
			}
			catch (Exception ex)
			{
				//send back error
				return new StatusInfo(false, ex.Message);
			}
			finally
			{
				conn.Close();
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataSet GetUcrOffenseByReportID(int reportID)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetUcrOffenseByReportID", conn);
			command.CommandType = CommandType.StoredProcedure;

			command.Parameters.Add("@reportID", SqlDbType.Int).Value = reportID;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetOffenseCategories()
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetUcrOffenseCategories", conn);
			command.CommandType = CommandType.StoredProcedure;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetOffenseTypesByCategoryID(int categoryID)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetUcrOffenseTypesByCategoryID", conn);
			command.CommandType = CommandType.StoredProcedure;

			//params
			command.Parameters.Add("@categoryID", SqlDbType.Int).Value = categoryID;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static int GetOffenseTypeBySignalID_old(int signalTypeID)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetUcrOffenseTypeBySignalTypeID", conn);
			command.CommandType = CommandType.StoredProcedure;

			#region params

			command.Parameters.Add("@signalTypeID", SqlDbType.Int).Value = signalTypeID;

			//return
			SqlParameter returnValue = command.Parameters.Add("@RETURN_VALUE", SqlDbType.Int);
			returnValue.Direction = ParameterDirection.ReturnValue;

			#endregion

			int myReturnValue = -1;

			try
			{
				conn.Open();
				command.ExecuteNonQuery();

				myReturnValue = (int)returnValue.Value;
			}
			//catch (Exception ex)
			catch 
			{
				//send back error
				//return new StatusInfo(false, ex.Message);
			}
			finally
			{
				conn.Close();
			}

			return myReturnValue;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static object[] GetOffenseTypeBySignalID(int signalTypeID)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetUcrOffenseBySignalTypeID", conn);
			command.CommandType = CommandType.StoredProcedure;

			command.Parameters.Add("@signalTypeID", SqlDbType.Int).Value = signalTypeID;

			SqlDataReader reader = null;

			try
			{
				object[] returnValues = null;

				conn.Open();
				reader = command.ExecuteReader();

				while (reader.Read())
				{
					returnValues = new object[reader.FieldCount];
					reader.GetValues(returnValues);
				}

				return returnValues;
			}
			catch (Exception ex)
			{
				//send back error
				//return new StatusInfo(false, ex.Message);
				return null;
			}
			finally
			{
				if (reader != null)
					reader.Close();

				//if (conn.State == ConnectionState.Open)
				conn.Close();
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetAllOffenseTypes()
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetUcrOffenseTypes", conn);
			command.CommandType = CommandType.StoredProcedure;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion		
	}
}
