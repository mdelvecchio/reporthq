using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;

using ReportHq.Common;

namespace ReportHq.IncidentReports.DataAccess
{
	/// <summary>
	/// DA layer for the users and roles management in the application.
	/// </summary>
	public class UserManagementDA
	{
		#region Variables

		static private string _connStr = ConfigurationManager.ConnectionStrings["ReportHq"].ConnectionString;

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Public Methods

		public static DataTable GetUserRoles()
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetUserRoles", conn);
			command.CommandType = CommandType.StoredProcedure;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		//public static StatusInfo DeleteBulletin(int bulletinID)
		//{
		//	//conn & command
		//	SqlConnection conn = new SqlConnection(_connStr);
		//	SqlCommand command = new SqlCommand("DeleteBulletin", conn);
		//	command.CommandType = CommandType.StoredProcedure;

		//	//params
		//	command.Parameters.Add("@ID", SqlDbType.Int).Value = bulletinID;

		//	//do it
		//	try
		//	{
		//		conn.Open();
		//		command.ExecuteNonQuery();

		//		return new StatusInfo(true);
		//	}
		//	catch (Exception ex)
		//	{
		//		//send back error
		//		return new StatusInfo(false, ex.Message);
		//	}
		//	finally
		//	{
		//		conn.Close();
		//	}
		//}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}	
}
