using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;

using ReportHq.Common;

namespace ReportHq.IncidentReports.DataAccess
{
	/// <summary>
	/// DA layer for the VictimPerson object.
	/// </summary>
	public class VictimPersonDA
	{
		#region Variables

		static private string _connStr = ConfigurationManager.ConnectionStrings["ReportHq"].ConnectionString;

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Public Methods

		public static StatusInfo InsertVictimPerson(int reportID,
													int victimNumber,
													int victimPersonTypeID,
													int victimTypeID,
													string lastName,
													string firstName,
													int raceTypeID,
													int genderTypeID,
													DateTime dateOfBirth,
													string streetAddress,
													string city,
													string state,
													int zipCode,
													string phoneNumber,
													string socialSecurityNumber,
													string driversLicenseNumber,
													int sobrietyTypeID,
													int injurtyTypeID,
													int treatedTypeID,
													string occupation,
													string workStreetAddress,
													string workCity,
													string workState,
													int workZipCode,
													string workPhoneNumber,
													string emailAddress,
													string createdBy)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("InsertVictimPerson", conn);
			command.CommandType = CommandType.StoredProcedure;

			#region params

			command.Parameters.Add("@reportID", SqlDbType.Int).Value = reportID;
			command.Parameters.Add("@victimNumber", SqlDbType.Int).Value = victimNumber;
			command.Parameters.Add("@createdBy", SqlDbType.NVarChar, 50).Value = createdBy;

			//optional
			if (victimPersonTypeID > 0)
				command.Parameters.Add("@victimPersonTypeID", SqlDbType.Int).Value = victimPersonTypeID;

			if (victimTypeID > 0)
				command.Parameters.Add("@victimTypeID", SqlDbType.Int).Value = victimTypeID;

			if (!string.IsNullOrEmpty(lastName))
				command.Parameters.Add("@lastName", SqlDbType.NVarChar, 50).Value = lastName;

			if (!string.IsNullOrEmpty(firstName))
				command.Parameters.Add("@firstName", SqlDbType.NVarChar, 50).Value = firstName;

			if (raceTypeID > 0)
				command.Parameters.Add("@raceTypeID", SqlDbType.Int).Value = raceTypeID;

			if (genderTypeID > 0)
				command.Parameters.Add("@genderTypeID", SqlDbType.Int).Value = genderTypeID;

			if (dateOfBirth > new DateTime(1900, 1, 1))
				command.Parameters.Add("@dateOfBirth", SqlDbType.Date).Value = dateOfBirth;

			if (!string.IsNullOrEmpty(streetAddress))
				command.Parameters.Add("@streetAddress", SqlDbType.NVarChar, 50).Value = streetAddress;

			if (!string.IsNullOrEmpty(city))
				command.Parameters.Add("@city", SqlDbType.NVarChar, 50).Value = city;

			if (!string.IsNullOrEmpty(state))
				command.Parameters.Add("@state", SqlDbType.NVarChar, 2).Value = state;

			if (zipCode > 0)
				command.Parameters.Add("@zipCode", SqlDbType.Int).Value = zipCode;

			if (!string.IsNullOrEmpty(phoneNumber))
				command.Parameters.Add("@phoneNumber", SqlDbType.NVarChar, 20).Value = phoneNumber;

			if (!string.IsNullOrEmpty(socialSecurityNumber))
				command.Parameters.Add("@socialSecurityNumber", SqlDbType.NVarChar, 9).Value = socialSecurityNumber;

			if (!string.IsNullOrEmpty(driversLicenseNumber))
				command.Parameters.Add("@driversLicenseNumber", SqlDbType.NVarChar, 20).Value = driversLicenseNumber;

			if (sobrietyTypeID > 0)
				command.Parameters.Add("@sobrietyTypeID", SqlDbType.Int).Value = sobrietyTypeID;

			if (injurtyTypeID > 0)
				command.Parameters.Add("@injuryTypeID", SqlDbType.Int).Value = injurtyTypeID;

			if (treatedTypeID > 0)
				command.Parameters.Add("@treatedTypeID", SqlDbType.Int).Value = treatedTypeID;

			if (!string.IsNullOrEmpty(occupation))
				command.Parameters.Add("@occupation", SqlDbType.NVarChar, 50).Value = occupation;

			if (!string.IsNullOrEmpty(workStreetAddress))
				command.Parameters.Add("@workStreetAddress", SqlDbType.NVarChar, 50).Value = workStreetAddress;

			if (!string.IsNullOrEmpty(workCity))
				command.Parameters.Add("@workCity", SqlDbType.NVarChar, 50).Value = workCity;

			if (!string.IsNullOrEmpty(workState))
				command.Parameters.Add("@workState", SqlDbType.NVarChar, 2).Value = workState;

			if (workZipCode > 0)
				command.Parameters.Add("@workZipCode", SqlDbType.Int).Value = workZipCode;

			if (!string.IsNullOrEmpty(workPhoneNumber))
				command.Parameters.Add("@workPhoneNumber", SqlDbType.NVarChar, 20).Value = workPhoneNumber;

			if (!string.IsNullOrEmpty(emailAddress))
				command.Parameters.Add("@emailAddress", SqlDbType.NVarChar, 254).Value = emailAddress;

			//output - new VictimPersonID
			SqlParameter param = new SqlParameter("@victimPersonID", SqlDbType.Int);
			param.Direction = ParameterDirection.Output;
			command.Parameters.Add(param);

			#endregion

			//do it
			try
			{
				conn.Open();
				command.ExecuteNonQuery();

				//send back new ID
				return new StatusInfo(true, (int)param.Value);
			}
			catch (Exception ex)
			{
				//send back error
				return new StatusInfo(false, ex.Message);
			}
			finally
			{
				conn.Close();
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static StatusInfo UpdateVictimPerson(int victimPersonID,
													int victimNumber,
													int victimPersonTypeID,
													int victimTypeID,
													string lastName,
													string firstName,
													int raceTypeID,
													int genderTypeID,
													DateTime dateOfBirth,
													string streetAddress,
													string city,
													string state,
													int zipCode,
													string phoneNumber,
													string socialSecurityNumber,
													string driversLicenseNumber,
													int sobrietyTypeID,
													int injurtyTypeID,
													int treatedTypeID,
													string occupation,
													string workStreetAddress,
													string workCity,
													string workState,
													int workZipCode,
													string workPhoneNumber,
													string emailAddress,
													string modifiedBy)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("UpdateVictimPerson", conn);
			command.CommandType = CommandType.StoredProcedure;

			#region params

			command.Parameters.Add("@victimPersonID", SqlDbType.Int).Value = victimPersonID;
			command.Parameters.Add("@victimNumber", SqlDbType.Int).Value = victimNumber;
			command.Parameters.Add("@modifiedBy", SqlDbType.NVarChar, 50).Value = modifiedBy;

			//optional
			if (victimPersonTypeID > 0)
				command.Parameters.Add("@victimPersonTypeID", SqlDbType.Int).Value = victimPersonTypeID;

			if (victimTypeID > 0)
				command.Parameters.Add("@victimTypeID", SqlDbType.Int).Value = victimTypeID;

			if (!string.IsNullOrEmpty(lastName))
				command.Parameters.Add("@lastName", SqlDbType.NVarChar, 50).Value = lastName;

			if (!string.IsNullOrEmpty(firstName))
				command.Parameters.Add("@firstName", SqlDbType.NVarChar, 50).Value = firstName;

			if (raceTypeID > 0)
				command.Parameters.Add("@raceTypeID", SqlDbType.Int).Value = raceTypeID;

			if (genderTypeID > 0)
				command.Parameters.Add("@genderTypeID", SqlDbType.Int).Value = genderTypeID;

			if (dateOfBirth > new DateTime(1900, 1, 1))
				command.Parameters.Add("@dateOfBirth", SqlDbType.Date).Value = dateOfBirth;

			if (!string.IsNullOrEmpty(streetAddress))
				command.Parameters.Add("@streetAddress", SqlDbType.NVarChar, 50).Value = streetAddress;

			if (!string.IsNullOrEmpty(city))
				command.Parameters.Add("@city", SqlDbType.NVarChar, 50).Value = city;

			if (!string.IsNullOrEmpty(state))
				command.Parameters.Add("@state", SqlDbType.NVarChar, 2).Value = state;

			if (zipCode > 0)
				command.Parameters.Add("@zipCode", SqlDbType.Int).Value = zipCode;

			if (!string.IsNullOrEmpty(phoneNumber))
				command.Parameters.Add("@phoneNumber", SqlDbType.NVarChar, 20).Value = phoneNumber;

			if (!string.IsNullOrEmpty(socialSecurityNumber))
				command.Parameters.Add("@socialSecurityNumber", SqlDbType.NVarChar, 9).Value = socialSecurityNumber;

			if (!string.IsNullOrEmpty(driversLicenseNumber))
				command.Parameters.Add("@driversLicenseNumber", SqlDbType.NVarChar, 20).Value = driversLicenseNumber;

			if (sobrietyTypeID > 0)
				command.Parameters.Add("@sobrietyTypeID", SqlDbType.Int).Value = sobrietyTypeID;

			if (injurtyTypeID > 0)
				command.Parameters.Add("@injuryTypeID", SqlDbType.Int).Value = injurtyTypeID;

			if (treatedTypeID > 0)
				command.Parameters.Add("@treatedTypeID", SqlDbType.Int).Value = treatedTypeID;

			if (!string.IsNullOrEmpty(occupation))
				command.Parameters.Add("@occupation", SqlDbType.NVarChar, 50).Value = occupation;

			if (!string.IsNullOrEmpty(workStreetAddress))
				command.Parameters.Add("@workStreetAddress", SqlDbType.NVarChar, 50).Value = workStreetAddress;

			if (!string.IsNullOrEmpty(workCity))
				command.Parameters.Add("@workCity", SqlDbType.NVarChar, 50).Value = workCity;

			if (!string.IsNullOrEmpty(workState))
				command.Parameters.Add("@workState", SqlDbType.NVarChar, 2).Value = workState;

			if (workZipCode > 0)
				command.Parameters.Add("@workZipCode", SqlDbType.Int).Value = workZipCode;

			if (!string.IsNullOrEmpty(workPhoneNumber))
				command.Parameters.Add("@workPhoneNumber", SqlDbType.NVarChar, 20).Value = workPhoneNumber;

			if (!string.IsNullOrEmpty(emailAddress))
				command.Parameters.Add("@emailAddress", SqlDbType.NVarChar, 254).Value = emailAddress;

			#endregion

			//do it
			try
			{
				conn.Open();
				command.ExecuteNonQuery();
		
				return new StatusInfo(true);
			}
			catch (Exception ex)
			{
				//send back error
				return new StatusInfo(false, ex.Message);
			}
			finally
			{
				conn.Close();
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static StatusInfo DeleteVictimPerson(int victimPersonID)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("DeleteVictimPerson", conn);
			command.CommandType = CommandType.StoredProcedure;

			command.Parameters.Add("@victimPersonID", SqlDbType.Int).Value = victimPersonID;

			//do it
			try
			{
				conn.Open();
				command.ExecuteNonQuery();
			
				return new StatusInfo(true);
			}
			catch (Exception ex)
			{
				//send back error
				return new StatusInfo(false, ex.Message);
			}
			finally
			{
				conn.Close();
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static StatusInfo InsertDomesticViolenceVictim(int reportID,
															  int victimPersonID,
															  int medicalTreatmentTypeID,
															  string medicalUnitNumber,
															  string medicalAddress,
															  bool infoSheetGiven,
															  bool itemNumberGiven,
															  string descriptorsXml,
															  string injuriesXml,
															  string createdBy)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("InsertDomesticViolenceVictim", conn);
			command.CommandType = CommandType.StoredProcedure;

			#region params

			command.Parameters.Add("@reportID", SqlDbType.Int).Value = reportID;
			command.Parameters.Add("@victimPersonID", SqlDbType.Int).Value = victimPersonID;
			command.Parameters.Add("@createdBy", SqlDbType.NVarChar, 50).Value = createdBy;

			//optional
			if (medicalTreatmentTypeID > 0)
				command.Parameters.Add("@medicalTreatmentTypeID", SqlDbType.Int).Value = medicalTreatmentTypeID;

			if (!string.IsNullOrEmpty(medicalUnitNumber))
				command.Parameters.Add("@medicalUnitNumber", SqlDbType.NVarChar, 10).Value = medicalUnitNumber;

			if (!string.IsNullOrEmpty(medicalAddress))
				command.Parameters.Add("@medicalAddress", SqlDbType.NVarChar, 200).Value = medicalAddress;
			
			command.Parameters.Add("@infoSheetGiven", SqlDbType.Bit).Value = infoSheetGiven;
			command.Parameters.Add("@itemNumberGiven", SqlDbType.Bit).Value = itemNumberGiven;
			command.Parameters.Add("@descriptorItemIds", SqlDbType.Xml).Value = descriptorsXml;
			command.Parameters.Add("@injuryItemIds", SqlDbType.Xml).Value = injuriesXml;

			#endregion

			//do it
			try
			{
				conn.Open();
				command.ExecuteNonQuery();

				//send back new ID
				//return new StatusInfo(true, (int)param.Value);
				return new StatusInfo(true);
			}
			catch (Exception ex)
			{
				//send back error
				return new StatusInfo(false, ex.Message);
			}
			finally
			{
				conn.Close();
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static StatusInfo UpdateDomesticViolenceVictim(int victimPersonID,
															  int medicalTreatmentTypeID,
															  string medicalUnitNumber,
															  string medicalAddress,
															  bool infoSheetGiven,
															  bool itemNumberGiven,
															  string descriptorsXml,
															  string injuriesXml,
															  string modifiedBy)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("UpdateDomesticViolenceVictim", conn);
			command.CommandType = CommandType.StoredProcedure;

			#region params

			command.Parameters.Add("@victimPersonID", SqlDbType.Int).Value = victimPersonID;
			command.Parameters.Add("@lastModifiedBy", SqlDbType.NVarChar, 50).Value = modifiedBy;

			//optional
			if (medicalTreatmentTypeID > 0)
				command.Parameters.Add("@medicalTreatmentTypeID", SqlDbType.Int).Value = medicalTreatmentTypeID;

			if (!string.IsNullOrEmpty(medicalUnitNumber))
				command.Parameters.Add("@medicalUnitNumber", SqlDbType.NVarChar, 10).Value = medicalUnitNumber;

			if (!string.IsNullOrEmpty(medicalAddress))
				command.Parameters.Add("@medicalAddress", SqlDbType.NVarChar, 200).Value = medicalAddress;

			command.Parameters.Add("@infoSheetGiven", SqlDbType.Bit).Value = infoSheetGiven;
			command.Parameters.Add("@itemNumberGiven", SqlDbType.Bit).Value = itemNumberGiven;
			command.Parameters.Add("@descriptorItemIds", SqlDbType.Xml).Value = descriptorsXml;
			command.Parameters.Add("@injuryItemIds", SqlDbType.Xml).Value = injuriesXml;

			#endregion

			//do it
			try
			{
				conn.Open();
				command.ExecuteNonQuery();

				//send back new ID
				//return new StatusInfo(true, (int)param.Value);
				return new StatusInfo(true);
			}
			catch (Exception ex)
			{
				//send back error
				return new StatusInfo(false, ex.Message);
			}
			finally
			{
				conn.Close();
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static StatusInfo DeleteDomesticViolenceVictim(int victimPersonID)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("DeleteDomesticViolenceVictim", conn);
			command.CommandType = CommandType.StoredProcedure;

			command.Parameters.Add("@victimPersonID", SqlDbType.Int).Value = victimPersonID;

			//do it
			try
			{
				conn.Open();
				command.ExecuteNonQuery();

				return new StatusInfo(true);
			}
			catch (Exception ex)
			{
				//send back error
				return new StatusInfo(false, ex.Message);
			}
			finally
			{
				conn.Close();
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static StatusInfo InsertDomesticViolenceRelationship(int reportID,
																	int victimPersonID,
																	int offenderID,
																	int victimPersonAsOffenderID,
																	int relationshipTypeID,
																	int lengthMonths,
																	DateTime endedDate,
																	bool hadPriorHistory,
																	bool priorHistoryDocumented,
																	int restrainingOrderTypeID,
																	string issuingCourt,
																	string docketNumber)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("InsertDomesticViolenceRelationship", conn);
			command.CommandType = CommandType.StoredProcedure;

			#region params

			command.Parameters.Add("@reportID", SqlDbType.Int).Value = reportID;
			command.Parameters.Add("@victimPersonID", SqlDbType.Int).Value = victimPersonID;
			command.Parameters.Add("@relationshipTypeID", SqlDbType.Int).Value = relationshipTypeID;

			//optional

			if (offenderID > 0) //record can be VictimPerson-Offender combo
				command.Parameters.Add("@offenderID", SqlDbType.Int).Value = offenderID;

			if (victimPersonAsOffenderID > 0) //record can be VictimPerson-VictimPerson combo
				command.Parameters.Add("@victimPersonAsOffenderID", SqlDbType.Int).Value = victimPersonAsOffenderID;

			if (lengthMonths > 0)
				command.Parameters.Add("@lengthMonths", SqlDbType.Int).Value = lengthMonths;

			if (endedDate > new DateTime(1900, 1, 1))
				command.Parameters.Add("@endedDate", SqlDbType.DateTime).Value = endedDate;

			command.Parameters.Add("@hadPriorHistory", SqlDbType.Bit).Value = hadPriorHistory;
			command.Parameters.Add("@priorHistoryDocumented", SqlDbType.Bit).Value = priorHistoryDocumented;

			if (restrainingOrderTypeID > 0)
				command.Parameters.Add("@restrainingOrderTypeID", SqlDbType.Int).Value = restrainingOrderTypeID;

			if (!string.IsNullOrEmpty(issuingCourt))
				command.Parameters.Add("@issuingCourt", SqlDbType.NVarChar, 50).Value = issuingCourt;

			if (!string.IsNullOrEmpty(docketNumber))
				command.Parameters.Add("@docketNumber", SqlDbType.NVarChar, 10).Value = docketNumber;

			#endregion

			//do it
			try
			{
				conn.Open();
				command.ExecuteNonQuery();

				return new StatusInfo(true);
			}
			catch (Exception ex)
			{
				//send back error
				return new StatusInfo(false, ex.Message);
			}
			finally
			{
				conn.Close();
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static StatusInfo UpdateDomesticViolenceRelationship(int victimPersonID,
																	int offenderID,
																	int victimPersonAsOffenderID,
																	int relationshipTypeID,
																	int lengthMonths,
																	DateTime endedDate,
																	bool hadPriorHistory,
																	bool priorHistoryDocumented,
																	int restrainingOrderTypeID,
																	string issuingCourt,
																	string docketNumber)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("UpdateDomesticViolenceRelationship", conn);
			command.CommandType = CommandType.StoredProcedure;

			#region params

			command.Parameters.Add("@victimPersonID", SqlDbType.Int).Value = victimPersonID;
			command.Parameters.Add("@relationshipTypeID", SqlDbType.Int).Value = relationshipTypeID;

			//optional

			if (offenderID > 0) //record can be VictimPerson-Offender combo
				command.Parameters.Add("@offenderID", SqlDbType.Int).Value = offenderID;

			if (victimPersonAsOffenderID > 0) //record can be VictimPerson-VictimPerson combo
				command.Parameters.Add("@victimPersonAsOffenderID", SqlDbType.Int).Value = victimPersonAsOffenderID;

			if (lengthMonths > 0)
				command.Parameters.Add("@lengthMonths", SqlDbType.Int).Value = lengthMonths;

			if (endedDate > new DateTime(1900, 1, 1))
				command.Parameters.Add("@endedDate", SqlDbType.DateTime).Value = endedDate;

			command.Parameters.Add("@hadPriorHistory", SqlDbType.Bit).Value = hadPriorHistory;
			command.Parameters.Add("@priorHistoryDocumented", SqlDbType.Bit).Value = priorHistoryDocumented;

			if (restrainingOrderTypeID > 0)
				command.Parameters.Add("@restrainingOrderTypeID", SqlDbType.Int).Value = restrainingOrderTypeID;

			if (!string.IsNullOrEmpty(issuingCourt))
				command.Parameters.Add("@issuingCourt", SqlDbType.NVarChar, 50).Value = issuingCourt;

			if (!string.IsNullOrEmpty(docketNumber))
				command.Parameters.Add("@docketNumber", SqlDbType.NVarChar, 10).Value = docketNumber;

			#endregion

			//do it
			try
			{
				conn.Open();
				command.ExecuteNonQuery();

				return new StatusInfo(true);
			}
			catch (Exception ex)
			{
				//send back error
				return new StatusInfo(false, ex.Message);
			}
			finally
			{
				conn.Close();
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static StatusInfo DeleteDomesticViolenceRelationship(int victimPersonID, int offenderID, int victimPersonAsOffenderID)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("DeleteDomesticViolenceRelationship", conn);
			command.CommandType = CommandType.StoredProcedure;

			command.Parameters.Add("@victimPersonID", SqlDbType.Int).Value = victimPersonID;

			if (offenderID > 0) //record can be VictimPerson-Offender combo
				command.Parameters.Add("@offenderID", SqlDbType.Int).Value = offenderID;

			if (victimPersonAsOffenderID > 0) //record can be VictimPerson-VictimPerson combo
				command.Parameters.Add("@victimPersonAsOffenderID", SqlDbType.Int).Value = victimPersonAsOffenderID;

			//do it
			try
			{
				conn.Open();
				command.ExecuteNonQuery();

				return new StatusInfo(true);
			}
			catch (Exception ex)
			{
				//send back error
				return new StatusInfo(false, ex.Message);
			}
			finally
			{
				conn.Close();
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		
		public static DataSet GetVictimPersonByID(int victimPersonID)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetVictimPersonByID", conn);
			command.CommandType = CommandType.StoredProcedure;

			command.Parameters.Add("@victimPersonID", SqlDbType.Int).Value = victimPersonID;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		
		public static DataTable GetVictimPersonTypes()
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetVictimPersonTypes", conn);
			command.CommandType = CommandType.StoredProcedure;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetVictimTypes()
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetVictimTypes", conn);
			command.CommandType = CommandType.StoredProcedure;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetVictimsByReport(int reportID)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetVictimsByReportID", conn);
			command.CommandType = CommandType.StoredProcedure;

			//params
			command.Parameters.Add("@reportID", SqlDbType.Int).Value = reportID;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetVictimOffenderRelationshipTypes()
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetVictimOffenderRelationshipTypes", conn);
			command.CommandType = CommandType.StoredProcedure;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetVictimPersonsByReport(int reportID)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetVictimPersonsByReportID", conn);
			command.CommandType = CommandType.StoredProcedure;

			command.Parameters.Add("@reportID", SqlDbType.Int).Value = reportID;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetDomesticViolenceVictimsByReport(int reportID)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetDomesticViolenceVictimsByReportID", conn);
			command.CommandType = CommandType.StoredProcedure;

			command.Parameters.Add("@reportID", SqlDbType.Int).Value = reportID;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetDomesticViolenceVictimDescriptors(int victimPersonID)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetDomesticViolenceDescriptorsByVictimID", conn);
			command.CommandType = CommandType.StoredProcedure;

			command.Parameters.Add("@victimPersonID", SqlDbType.Int).Value = victimPersonID;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetDomesticViolenceVictimInjuries(int victimPersonID)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetDomesticViolenceInjuriesByVictimID", conn);
			command.CommandType = CommandType.StoredProcedure;

			command.Parameters.Add("@victimPersonID", SqlDbType.Int).Value = victimPersonID;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}	
}
