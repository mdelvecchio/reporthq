using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;

using ReportHq.Common;

namespace ReportHq.IncidentReports.DataAccess
{
	/// <summary>
	/// DA layer for Reporting -- dashboards, stats, etc. 
	/// </summary>
	public class ReportingDA
	{
		#region Variables

		static private string _connStr = ConfigurationManager.ConnectionStrings["ReportHq"].ConnectionString;

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Public Methods

		//public static DataTable ReportsByDistrict(int reportTypeID, DateTime startDate, DateTime endDate)
		//{
		//	//conn & command
		//	SqlConnection conn = new SqlConnection(_connStr);
		//	SqlCommand command = new SqlCommand("Rpt_IncidentsByDistrict", conn);
		//	command.CommandType = CommandType.StoredProcedure;

		//	#region params

		//	if (reportTypeID > 0)
		//		command.Parameters.Add("@reportTypeID", SqlDbType.Int).Value = reportTypeID;

		//	if (startDate != new DateTime(1900, 1, 1))
		//		command.Parameters.Add("@startDate", SqlDbType.DateTime).Value = startDate;

		//	if (endDate != new DateTime(1900, 1, 1))
		//		command.Parameters.Add("@endDate", SqlDbType.DateTime).Value = endDate;

		//	#endregion

		//	//do it
		//	DataSet ds = new DataSet();
		//	SqlDataAdapter da = new SqlDataAdapter(command);
		//	da.Fill(ds);

		//	return ds.Tables[0];
		//}

		////'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		//public static DataTable ApprovedReports(DateTime startDate, DateTime endDate)
		//{
		//	//conn & command
		//	SqlConnection conn = new SqlConnection(_connStr);
		//	SqlCommand command = new SqlCommand("Rpt_ApprovedReports", conn);
		//	command.CommandType = CommandType.StoredProcedure;

		//	#region params

		//	if (startDate != new DateTime(1900, 1, 1))
		//		command.Parameters.Add("@startDate", SqlDbType.DateTime).Value = startDate;

		//	if (endDate != new DateTime(1900, 1, 1))
		//		command.Parameters.Add("@endDate", SqlDbType.DateTime).Value = endDate;

		//	#endregion

		//	//do it
		//	DataSet ds = new DataSet();
		//	SqlDataAdapter da = new SqlDataAdapter(command);
		//	da.Fill(ds);

		//	return ds.Tables[0];
		//}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable DashboardStats(string username)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetDashboardReportStatsByUsername", conn);
			command.CommandType = CommandType.StoredProcedure;

			command.Parameters.Add("@username", SqlDbType.NVarChar, 50).Value = username;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable DistrictReportsByStatusType(int approvalStatusTypeID)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("rpt.DistrictReportsByStatusType", conn);
			command.CommandType = CommandType.StoredProcedure;

			command.Parameters.Add("@approvalStatusTypeID", SqlDbType.Int).Value = approvalStatusTypeID;
			
			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable ApprovedReportsByDistrict(DateTime startDate, DateTime endDate)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("rpt.ApprovedDistrictReports", conn);
			command.CommandType = CommandType.StoredProcedure;

			command.Parameters.Add("@startDate", SqlDbType.DateTime).Value = startDate;
			command.Parameters.Add("@endDate", SqlDbType.DateTime).Value = endDate;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable DistrictReportsPercentage()
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("rpt.DistrictReportsPercentage", conn);
			command.CommandType = CommandType.StoredProcedure;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataSet UcrTotals(DateTime startDate, DateTime endDate, int reportPeriod)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("rpt.UcrTotals", conn);
			command.CommandType = CommandType.StoredProcedure;

			command.Parameters.Add("@startDate", SqlDbType.DateTime).Value = startDate;
			command.Parameters.Add("@endDate", SqlDbType.DateTime).Value = endDate;
			command.Parameters.Add("@reportPeriod", SqlDbType.Int).Value = reportPeriod;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataSet UcrPeriodComparisons(int ucrOffenseTypeID, int reportPeriod, DateTime startDate, DateTime endDate)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("rpt.UcrPeriodComparison", conn);
			command.CommandType = CommandType.StoredProcedure;

			command.Parameters.Add("@ucrOffenseTypeID", SqlDbType.Int).Value = ucrOffenseTypeID;
			command.Parameters.Add("@reportPeriod", SqlDbType.Int).Value = reportPeriod;
			command.Parameters.Add("@startDate", SqlDbType.DateTime).Value = startDate;
			command.Parameters.Add("@endDate", SqlDbType.DateTime).Value = endDate;
			
			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}	
}
