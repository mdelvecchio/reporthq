using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;

using ReportHq.Common;
using ReportHq.IncidentReports.Entities;

namespace ReportHq.IncidentReports.DataAccess
{
	/// <summary>
	/// DA layer for the OffenderCharge object.
	/// </summary>
	public class OffenderChargeDA
	{
		#region Variables

		static private string _connStr = ConfigurationManager.ConnectionStrings["ReportHq"].ConnectionString;

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Public Methods

		public static StatusInfo InsertOffenderCharge(int offenderID,
													  int chargeTypeID,
													  string chargeDetails,
													  int victimPersonID,
													  int victimOffenderRelationshipTypeID,
													  string createdBy)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("InsertOffenderCharge", conn);
			command.CommandType = CommandType.StoredProcedure;

			#region params

			command.Parameters.Add("@offenderID", SqlDbType.Int).Value = offenderID;
			command.Parameters.Add("@chargeTypeID", SqlDbType.Int).Value = chargeTypeID;
			command.Parameters.Add("@createdBy", SqlDbType.NVarChar, 50).Value = createdBy;

			//optional
			if (!string.IsNullOrEmpty(chargeDetails))
				command.Parameters.Add("@chargeDetails", SqlDbType.NVarChar, 200).Value = chargeDetails;
				
			if (victimPersonID > 0)
				command.Parameters.Add("@victimPersonID", SqlDbType.Int).Value = victimPersonID;

			if (victimOffenderRelationshipTypeID > 0)
				command.Parameters.Add("@victimOffenderRelationshipTypeID", SqlDbType.Int).Value = victimOffenderRelationshipTypeID;

			#endregion

			//do it
			try
			{
				conn.Open();
				command.ExecuteNonQuery();

				//send back new ID
				//return new StatusInfo(true, (int)param.Value);
				return new StatusInfo(true);
			}
			catch (Exception ex)
			{
				//send back error
				return new StatusInfo(false, ex.Message);
			}
			finally
			{
				conn.Close();
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static StatusInfo UpdateOffenderCharge(int offenderChargeID,
													  int chargeTypeID,
													  string chargeDetails,
													  int victimPersonID,
													  int victimOffenderRelationshipTypeID,
													  string modifiedBy)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("UpdateOffenderCharge", conn);
			command.CommandType = CommandType.StoredProcedure;

			#region params

			command.Parameters.Add("@offenderChargeID", SqlDbType.Int).Value = offenderChargeID;
			command.Parameters.Add("@chargeTypeID", SqlDbType.Int).Value = chargeTypeID;
			command.Parameters.Add("@modifiedBy", SqlDbType.NVarChar, 50).Value = modifiedBy;

			//optional

			if (!string.IsNullOrEmpty(chargeDetails))
				command.Parameters.Add("@chargeDetails", SqlDbType.NVarChar, 200).Value = chargeDetails;
			
			if (victimPersonID > 0)
				command.Parameters.Add("@victimPersonID", SqlDbType.Int).Value = victimPersonID;

			if (victimOffenderRelationshipTypeID > 0)
				command.Parameters.Add("@victimOffenderRelationshipTypeID", SqlDbType.Int).Value = victimOffenderRelationshipTypeID;

			#endregion

			//do it
			try
			{
				conn.Open();
				command.ExecuteNonQuery();
			
				return new StatusInfo(true);
			}
			catch (Exception ex)
			{
				//send back error
				return new StatusInfo(false, ex.Message);
			}
			finally
			{
				conn.Close();
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static StatusInfo DeleteOffenderCharge(int offenderChargeID)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("DeleteOffenderCharge", conn);
			command.CommandType = CommandType.StoredProcedure;

			command.Parameters.Add("@offenderChargeID", SqlDbType.Int).Value = offenderChargeID;

			//do it
			try
			{
				conn.Open();
				command.ExecuteNonQuery();
			
				return new StatusInfo(true);
			}
			catch (Exception ex)
			{
				//send back error
				return new StatusInfo(false, ex.Message);
			}
			finally
			{
				conn.Close();
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Old way.
		/// </summary>
		//public static DataTable GetOffenderChargesByOffender(int offenderID)
		//{
		//		//conn & command
		//	SqlConnection conn = new SqlConnection(_connStr);
		//	SqlCommand command = new SqlCommand("GetOffenderChargesByOffenderID", conn);
		//	command.CommandType = CommandType.StoredProcedure;

		//	command.Parameters.Add("@offenderID", SqlDbType.Int).Value = offenderID;

		//	//do it
		//	DataSet ds = new DataSet();
		//	SqlDataAdapter da = new SqlDataAdapter(command);
		//	da.Fill(ds);

		//	return ds.Tables[0];
		//}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


		/// <summary>
		/// New way. More code, but uses a DataReader which is faster than DataSet/DataTable, is typed, and works well w/ Web API/JSON.
		/// </summary>
		public static List<OffenderChargeEntity> GetOffenderChargesByOffender(int offenderID)
		{
			List<OffenderChargeEntity> returnObjects = new List<OffenderChargeEntity>();

			//when using an IDisposable object, use using statement. ensures disposal of resources such as .Dispose() and .Close()
			using (SqlConnection conn = new SqlConnection(_connStr))
			using (SqlCommand command = new SqlCommand("GetOffenderChargesByOffenderID", conn))
			{
				command.CommandType = CommandType.StoredProcedure;

				//params here
				command.Parameters.Add("@offenderID", SqlDbType.Int).Value = offenderID;

				conn.Open();

				using (SqlDataReader reader = command.ExecuteReader())
				{
					if (reader.HasRows)
					{
						//looping thru results
						while (reader.Read())
						{
							OffenderChargeEntity offenderCharge = new OffenderChargeEntity();

							offenderCharge.ID = (int)reader["OffenderChargeID"];

							if (reader["OffenderID"] != DBNull.Value)
								offenderCharge.OffenderID = (int)reader["OffenderID"];

							if (reader["ChargePrefixTypeID"] != DBNull.Value) //nullable to support early EPR-Web release records
								offenderCharge.ChargePrefixTypeID = (int)reader["ChargePrefixTypeID"];

							if (reader["Prefix"] != DBNull.Value) //nullable to support early EPR-Web release records
								offenderCharge.Prefix = (string)reader["Prefix"];

							if (reader["ChargeTypeID"] != DBNull.Value)
								offenderCharge.ChargeTypeID = (int)reader["ChargeTypeID"];

							if (reader["Code"] != DBNull.Value) //nullable to support early EPR-Web release records
								offenderCharge.Code = (string)reader["Code"];

							if (reader["Charge"] != DBNull.Value)
								offenderCharge.Charge = (string)reader["Charge"];

							if (reader["ChargeDetails"] != DBNull.Value)
								offenderCharge.ChargeDetails = (string)reader["ChargeDetails"];

							if (reader["VictimPersonID"] != DBNull.Value)
								offenderCharge.VictimPersonID = (int)reader["VictimPersonID"];

							if (reader["VictimNumber"] != DBNull.Value)
								offenderCharge.VictimNumber = (int)reader["VictimNumber"];

							if (reader["VictimOffenderRelationshipTypeID"] != DBNull.Value)
								offenderCharge.VictimOffenderRelationshipTypeID = (int)reader["VictimOffenderRelationshipTypeID"];

							if (reader["VictimsRelationship"] != DBNull.Value)
								offenderCharge.VictimsRelationship = (string)reader["VictimsRelationship"];

							//add filled object to return list
							returnObjects.Add(offenderCharge);
						}
					}
				}
			}

			return returnObjects;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''




		public static DataTable GetChargePrefixTypes(bool getActiveOnly)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetChargePrefixTypes", conn);
			command.CommandType = CommandType.StoredProcedure;

			#region params

			command.Parameters.Add("@getActiveOnly", SqlDbType.Bit).Value = getActiveOnly;

			#endregion

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static StatusInfo InsertChargePrefixType(string name, string description, bool isActive)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("InsertChargePrefixType", conn);
			command.CommandType = CommandType.StoredProcedure;

			#region params

			command.Parameters.Add("@name", SqlDbType.NVarChar, 10).Value = name;
			command.Parameters.Add("@description", SqlDbType.NVarChar, 100).Value = description;
			command.Parameters.Add("@isActive", SqlDbType.Bit).Value = isActive;

			#endregion

			//do it
			try
			{
				conn.Open();
				command.ExecuteNonQuery();

				return new StatusInfo(true);
			}
			catch (Exception ex)
			{
				//send back error
				return new StatusInfo(false, ex.Message);
			}
			finally
			{
				conn.Close();
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static StatusInfo UpdateChargePrefixType(int chargePrefixTypeID, string name, string description, bool isActive)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("UpdateChargePrefixType", conn);
			command.CommandType = CommandType.StoredProcedure;

			#region params

			command.Parameters.Add("@chargePrefixTypeID", SqlDbType.Int).Value = chargePrefixTypeID;
			command.Parameters.Add("@name", SqlDbType.NVarChar, 10).Value = name;
			command.Parameters.Add("@description", SqlDbType.NVarChar, 100).Value = description;
			command.Parameters.Add("@isActive", SqlDbType.Bit).Value = isActive;

			#endregion

			//do it
			try
			{
				conn.Open();
				command.ExecuteNonQuery();

				return new StatusInfo(true);
			}
			catch (Exception ex)
			{
				//send back error
				return new StatusInfo(false, ex.Message);
			}
			finally
			{
				conn.Close();
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static StatusInfo DeactivateChargePrefixType(int chargeTypeID)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("DeactivateChargePrefixType", conn);
			command.CommandType = CommandType.StoredProcedure;

			command.Parameters.Add("@chargePrefixTypeID", SqlDbType.Int).Value = chargeTypeID;

			//do it
			try
			{
				conn.Open();
				command.ExecuteNonQuery();

				return new StatusInfo(true);
			}
			catch (Exception ex)
			{
				//send back error
				return new StatusInfo(false, ex.Message);
			}
			finally
			{
				conn.Close();
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetChargeTypesByChargePrefix(int chargePrefixTypeID, bool getActiveOnly)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetChargeTypesByChargePrefixTypeID", conn);
			command.CommandType = CommandType.StoredProcedure;

			#region params

			command.Parameters.Add("@chargePrefixTypeID", SqlDbType.Int).Value = chargePrefixTypeID;
			command.Parameters.Add("@getActiveOnly", SqlDbType.Bit).Value = getActiveOnly;

			#endregion

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static StatusInfo InsertChargeType(int chargePrefixTypeID,
												  string code,
												  string description,
												  string ucrCode,
												  string nibrsCode,
												  string librsCode,
												  bool isActive)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("InsertChargeType", conn);
			command.CommandType = CommandType.StoredProcedure;

			#region params

			command.Parameters.Add("@chargePrefixTypeID", SqlDbType.Int).Value = chargePrefixTypeID;
			command.Parameters.Add("@code", SqlDbType.NVarChar, 20).Value = code;
			command.Parameters.Add("@description", SqlDbType.NVarChar, 100).Value = description;
			command.Parameters.Add("@ucrCode", SqlDbType.NVarChar, 2).Value = ucrCode;
			command.Parameters.Add("@nibrsCode", SqlDbType.NVarChar, 3).Value = nibrsCode;
			command.Parameters.Add("@isActive", SqlDbType.Bit).Value = isActive;

			//optional

			if (!string.IsNullOrEmpty(librsCode))
				command.Parameters.Add("@librsCode", SqlDbType.NVarChar, 20).Value = librsCode;

			#endregion

			//do it
			try
			{
				conn.Open();
				command.ExecuteNonQuery();

				return new StatusInfo(true);
			}
			catch (Exception ex)
			{
				//send back error
				return new StatusInfo(false, ex.Message);
			}
			finally
			{
				conn.Close();
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static StatusInfo UpdateChargeType(int chargeTypeID,											  
												  int chargePrefixTypeID,
												  string code,
												  string description,
												  string ucrCode,
												  string nibrsCode,
												  string librsCode,
												  bool isActive)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("UpdateChargeType", conn);
			command.CommandType = CommandType.StoredProcedure;

			#region params

			command.Parameters.Add("@chargeTypeID", SqlDbType.Int).Value = chargeTypeID;
			command.Parameters.Add("@chargePrefixTypeID", SqlDbType.Int).Value = chargePrefixTypeID;
			command.Parameters.Add("@code", SqlDbType.NVarChar, 20).Value = code;
			command.Parameters.Add("@description", SqlDbType.NVarChar, 100).Value = description;
			command.Parameters.Add("@ucrCode", SqlDbType.NVarChar, 2).Value = ucrCode;
			command.Parameters.Add("@nibrsCode", SqlDbType.NVarChar, 3).Value = nibrsCode;
			command.Parameters.Add("@isActive", SqlDbType.Bit).Value = isActive;

			//optional

			if (!string.IsNullOrEmpty(librsCode))
				command.Parameters.Add("@librsCode", SqlDbType.NVarChar, 20).Value = librsCode;

			#endregion

			//do it
			try
			{
				conn.Open();
				command.ExecuteNonQuery();

				return new StatusInfo(true);
			}
			catch (Exception ex)
			{
				//send back error
				return new StatusInfo(false, ex.Message);
			}
			finally
			{
				conn.Close();
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static StatusInfo DeactivateChargeType(int chargeTypeID)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("DeactivateChargeType", conn);
			command.CommandType = CommandType.StoredProcedure;

			command.Parameters.Add("@chargeTypeID", SqlDbType.Int).Value = chargeTypeID;
			
			//do it
			try
			{
				conn.Open();
				command.ExecuteNonQuery();

				return new StatusInfo(true);
			}
			catch (Exception ex)
			{
				//send back error
				return new StatusInfo(false, ex.Message);
			}
			finally
			{
				conn.Close();
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetChargeType(int chargeTypeID)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetChargeTypeByID", conn);
			command.CommandType = CommandType.StoredProcedure;

			#region params

			command.Parameters.Add("@chargeTypeID", SqlDbType.Int).Value = chargeTypeID;
			
			#endregion

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		#endregion
	}	
}
