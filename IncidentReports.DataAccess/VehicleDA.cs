using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;

using ReportHq.Common;

namespace ReportHq.IncidentReports.DataAccess
{
	/// <summary>
	/// DA layer for the Vehicle object.
	/// </summary>
	public class VehicleDA
	{
		#region Variables

		static private string _connStr = ConfigurationManager.ConnectionStrings["ReportHq"].ConnectionString;

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Public Methods

		public static StatusInfo InsertVehicle(int reportID,
											   int statusTypeID,
											   int year,
											   int vehicleMakeTypeID,
											   int vehicleModelTypeID,
											   int vehicleColorTypeID,
											   int vehicleStyleTypeID,
											   int value,
											   string licensePlate,
											   string licensePlateState,
											   int licensePlateYear,
											   string vin,
											   int vehicleRecoveredTypeID,
											   string recoveryLocation,
											   int recoveryDistrict,
											   string recoveryZone,
											   string recoverySubZone,
											   string additionalDescription,
											   int vehicleDamageLevelTypeID,
											   string damageZonesXml,
											   string recoveredVehicleMosXml,
											   string createdBy)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("InsertVehicle", conn);
			command.CommandType = CommandType.StoredProcedure;

			#region params

			command.Parameters.Add("@reportID", SqlDbType.Int).Value = reportID;
			command.Parameters.Add("@vehicleStatusTypeID", SqlDbType.Int).Value = statusTypeID;
			
			command.Parameters.Add("@damageZoneItemIds", SqlDbType.Xml).Value = damageZonesXml;
			command.Parameters.Add("@recoveredVehicleMoItemIds", SqlDbType.Xml).Value = recoveredVehicleMosXml;

			command.Parameters.Add("@createdBy", SqlDbType.NVarChar, 50).Value = createdBy;

			//optional
			if (year > 0)
				command.Parameters.Add("@year", SqlDbType.Int).Value = year;

			if (vehicleMakeTypeID > 0)
				command.Parameters.Add("@vehicleMakeTypeID", SqlDbType.Int).Value = vehicleMakeTypeID;

			if (vehicleModelTypeID > 0)
				command.Parameters.Add("@vehicleModelTypeID", SqlDbType.Int).Value = vehicleModelTypeID;

			if (vehicleColorTypeID > 0)
				command.Parameters.Add("@vehicleColorTypeID", SqlDbType.Int).Value = vehicleColorTypeID;

			if (vehicleStyleTypeID > 0)
				command.Parameters.Add("@vehicleStyleTypeID", SqlDbType.Int).Value = vehicleStyleTypeID;
			
			if (value > 0)
				command.Parameters.Add("@value", SqlDbType.Int).Value = value;

			if (!string.IsNullOrEmpty(licensePlate))
				command.Parameters.Add("@licensePlate", SqlDbType.NVarChar, 10).Value = licensePlate;

			if (!string.IsNullOrEmpty(licensePlateState))
				command.Parameters.Add("@licensePlateState", SqlDbType.NChar, 2).Value = licensePlateState;

			if (licensePlateYear > 0)
				command.Parameters.Add("@licensePlateYear", SqlDbType.Int).Value = licensePlateYear;

			if (!string.IsNullOrEmpty(vin))
				command.Parameters.Add("@vin", SqlDbType.NVarChar, 50).Value = vin;
			
			if (vehicleRecoveredTypeID > 0)
				command.Parameters.Add("@vehicleRecoveredTypeID", SqlDbType.Int).Value = vehicleRecoveredTypeID;

			if (!string.IsNullOrEmpty(recoveryLocation))
				command.Parameters.Add("@recoveryLocation", SqlDbType.NVarChar, 50).Value = recoveryLocation;

			if (recoveryDistrict > 0)
				command.Parameters.Add("@recoveryDistrict", SqlDbType.Int).Value = recoveryDistrict;

			if (!string.IsNullOrEmpty(recoveryZone))
				command.Parameters.Add("@recoveryZone", SqlDbType.NVarChar, 1).Value = recoveryZone;

			if (!string.IsNullOrEmpty(recoverySubZone))
				command.Parameters.Add("@recoverySubZone", SqlDbType.NVarChar, 2).Value = recoverySubZone;
			
			if (!string.IsNullOrEmpty(additionalDescription))
				command.Parameters.Add("@additionalDescription", SqlDbType.NVarChar, 200).Value = additionalDescription;

			if (vehicleDamageLevelTypeID > 0)
				command.Parameters.Add("@vehicleDamageLevelTypeID", SqlDbType.Int).Value = vehicleDamageLevelTypeID;

			#endregion

			//do it
			try
			{
				conn.Open();
				command.ExecuteNonQuery();

				//send back new ID
				//return new StatusInfo(true, (int)param.Value);
				return new StatusInfo(true);
			}
			catch (Exception ex)
			{
				//send back error
				return new StatusInfo(false, ex.Message);
			}
			finally
			{
				conn.Close();
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static StatusInfo UpdateVehicle(int vehicleID,
											   int statusTypeID,
											   int year,
											   int vehicleMakeTypeID,
											   int vehicleModelTypeID,
											   int vehicleColorTypeID,
											   int vehicleStyleTypeID,
											   int value,
											   string licensePlate,
											   string licensePlateState,
											   int licensePlateYear,
											   string vin,
											   int vehicleRecoveredTypeID,
											   string recoveryLocation,
											   int recoveryDistrict,
											   string recoveryZone,
											   string recoverySubZone,
											   string additionalDescription,
											   int vehicleDamageLevelTypeID,
											   string damageZonesXml,
											   string recoveredVehicleMosXml,
											   string modifiedBy)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("UpdateVehicle", conn);
			command.CommandType = CommandType.StoredProcedure;

			#region params

			command.Parameters.Add("@vehicleID", SqlDbType.Int).Value = vehicleID;
			command.Parameters.Add("@vehicleStatusTypeID", SqlDbType.Int).Value = statusTypeID;

			command.Parameters.Add("@damageZoneItemIds", SqlDbType.Xml).Value = damageZonesXml;
			command.Parameters.Add("@recoveredVehicleMoItemIds", SqlDbType.Xml).Value = recoveredVehicleMosXml;

			command.Parameters.Add("@lastModifiedBy", SqlDbType.NVarChar, 50).Value = modifiedBy;

			//optional
			if (year > 0)
				command.Parameters.Add("@year", SqlDbType.Int).Value = year;

			if (vehicleMakeTypeID > 0)
				command.Parameters.Add("@vehicleMakeTypeID", SqlDbType.Int).Value = vehicleMakeTypeID;

			if (vehicleModelTypeID > 0)
				command.Parameters.Add("@vehicleModelTypeID", SqlDbType.Int).Value = vehicleModelTypeID;

			if (vehicleColorTypeID > 0)
				command.Parameters.Add("@vehicleColorTypeID", SqlDbType.Int).Value = vehicleColorTypeID;

			if (vehicleStyleTypeID > 0)
				command.Parameters.Add("@vehicleStyleTypeID", SqlDbType.Int).Value = vehicleStyleTypeID;

			if (value > 0)
				command.Parameters.Add("@value", SqlDbType.Int).Value = value;

			if (!string.IsNullOrEmpty(licensePlate))
				command.Parameters.Add("@licensePlate", SqlDbType.NVarChar, 10).Value = licensePlate;

			if (!string.IsNullOrEmpty(licensePlateState))
				command.Parameters.Add("@licensePlateState", SqlDbType.NChar, 2).Value = licensePlateState;

			if (licensePlateYear > 0)
				command.Parameters.Add("@licensePlateYear", SqlDbType.Int).Value = licensePlateYear;

			if (!string.IsNullOrEmpty(vin))
				command.Parameters.Add("@vin", SqlDbType.NVarChar, 50).Value = vin;

			if (vehicleRecoveredTypeID > 0)
				command.Parameters.Add("@vehicleRecoveredTypeID", SqlDbType.Int).Value = vehicleRecoveredTypeID;

			if (!string.IsNullOrEmpty(recoveryLocation))
				command.Parameters.Add("@recoveryLocation", SqlDbType.NVarChar, 50).Value = recoveryLocation;

			if (recoveryDistrict > 0)
				command.Parameters.Add("@recoveryDistrict", SqlDbType.Int).Value = recoveryDistrict;

			if (!string.IsNullOrEmpty(recoveryZone))
				command.Parameters.Add("@recoveryZone", SqlDbType.NVarChar, 1).Value = recoveryZone;

			if (!string.IsNullOrEmpty(recoverySubZone))
				command.Parameters.Add("@recoverySubZone", SqlDbType.NVarChar, 2).Value = recoverySubZone;

			if (!string.IsNullOrEmpty(additionalDescription))
				command.Parameters.Add("@additionalDescription", SqlDbType.NVarChar, 200).Value = additionalDescription;

			if (vehicleDamageLevelTypeID > 0)
				command.Parameters.Add("@vehicleDamageLevelTypeID", SqlDbType.Int).Value = vehicleDamageLevelTypeID;

			#endregion

			//do it
			try
			{
				conn.Open();
				command.ExecuteNonQuery();

				return new StatusInfo(true);
			}
			catch (Exception ex)
			{
				//send back error
				return new StatusInfo(false, ex.Message);
			}
			finally
			{
				conn.Close();
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static StatusInfo DeleteVehicle(int vehicleID)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("DeleteVehicle", conn);
			command.CommandType = CommandType.StoredProcedure;

			command.Parameters.Add("@vehicleID", SqlDbType.Int).Value = vehicleID;

			//do it
			try
			{
				conn.Open();
				command.ExecuteNonQuery();

				return new StatusInfo(true);
			}
			catch (Exception ex)
			{
				//send back error
				return new StatusInfo(false, ex.Message);
			}
			finally
			{
				conn.Close();
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetVehiclesByReport(int reportID)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetVehiclesByReportID", conn);
			command.CommandType = CommandType.StoredProcedure;

			//params
			command.Parameters.Add("@reportID", SqlDbType.Int).Value = reportID;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetDamageZonesByVehicle(int vehicleID)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetVehicleDamageZonesByVehicleID", conn);
			command.CommandType = CommandType.StoredProcedure;

			//params
			command.Parameters.Add("@vehicleID", SqlDbType.Int).Value = vehicleID;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetRecoveredVehicleMosByVehicle(int vehicleID)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetRecoveredVehicleMosVehicleID", conn);
			command.CommandType = CommandType.StoredProcedure;

			//params
			command.Parameters.Add("@vehicleID", SqlDbType.Int).Value = vehicleID;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetVehicleStatusTypes()
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetVehicleStatusTypes", conn);
			command.CommandType = CommandType.StoredProcedure;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetVehicleRecoveredTypes()
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetVehicleRecoveredTypes", conn);
			command.CommandType = CommandType.StoredProcedure;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetVehicleDamageLevelTypes()
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetVehicleDamageLevelTypes", conn);
			command.CommandType = CommandType.StoredProcedure;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetVehicleDamageZoneTypes()
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetVehicleDamageZoneTypes", conn);
			command.CommandType = CommandType.StoredProcedure;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetRecoveredVehicleMoTypes()
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetRecoveredVehicleMoTypes", conn);
			command.CommandType = CommandType.StoredProcedure;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}	
}
