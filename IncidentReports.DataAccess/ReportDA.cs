using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;

using ReportHq.Common;

namespace ReportHq.IncidentReports.DataAccess
{
	/// <summary>
	/// DA layer for the Report object.
	/// </summary>
	public class ReportDA
	{
		#region Variables

		static private string _connStr = ConfigurationManager.ConnectionStrings["ReportHq"].ConnectionString;

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
		
		#region Public Methods

		/// <summary>
		/// Creates a new Report.
		/// </summary>
		public static StatusInfo InsertReport(int reportTypeID, string itemNumber, string createdBy)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("InsertReport", conn);
			command.CommandType = CommandType.StoredProcedure;

			#region params

			command.Parameters.Add("@reportTypeID", SqlDbType.Int).Value = reportTypeID;
			command.Parameters.Add("@itemNumber", SqlDbType.NVarChar, 10).Value = itemNumber;
			command.Parameters.Add("@createdBy", SqlDbType.NVarChar, 50).Value = createdBy;

			//output - new ReportID
			SqlParameter param = new SqlParameter("@reportID", SqlDbType.Int);
			param.Direction = ParameterDirection.Output;
			command.Parameters.Add(param);

			#endregion

			//do it
			try
			{
				conn.Open();
				command.ExecuteNonQuery();

				//send back new ID
				return new StatusInfo(true, (int)param.Value);
			}
			catch (Exception ex)
			{
				//send back error
				return new StatusInfo(false, ex.Message);
			}
			finally
			{
				conn.Close();
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Updates the Facesheet's Event section.
		/// </summary>
		public static StatusInfo UpdateEvent(int reportID,
											 int signalTypeID,
											 string incidentDescription,
											 string location,
											 int district,
											 string zone,
											 string subZone,
											 DateTime occurredDateTime,
											 DateTime reportDateTime,
											 int lightingConditionTypeID,
											 int reportStatusTypeID,
											 bool bulletinRequired,
											 int bulletinNumber,
											 int ucrOffenseTypeID,
											 string modifiedBy)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("UpdateReportEvent", conn);
			command.CommandType = CommandType.StoredProcedure;

			#region params

			command.Parameters.Add("@reportID", SqlDbType.Int).Value = reportID;
			command.Parameters.Add("@bulletinRequired", SqlDbType.Bit).Value = bulletinRequired;
			command.Parameters.Add("@modifiedBy", SqlDbType.NVarChar, 50).Value = modifiedBy;
			
			//optional
			if (signalTypeID > 0)
				command.Parameters.Add("@signalTypeID", SqlDbType.Int).Value = signalTypeID;

			if (!string.IsNullOrEmpty(incidentDescription))
				command.Parameters.Add("@incidentDescription", SqlDbType.NVarChar, 100).Value = incidentDescription;

			if (!string.IsNullOrEmpty(location))
				command.Parameters.Add("@location", SqlDbType.NVarChar, 100).Value = location;

			if (district > 0)
				command.Parameters.Add("@district", SqlDbType.Int).Value = district;

			if (!string.IsNullOrEmpty(zone))
				command.Parameters.Add("@zone", SqlDbType.NChar, 1).Value = zone;

			if (!string.IsNullOrEmpty(subZone))
				command.Parameters.Add("@subZone", SqlDbType.NChar, 2).Value = subZone;
			
			if (occurredDateTime > new DateTime(1900, 1, 1))
				command.Parameters.Add("@occurredDate", SqlDbType.DateTime).Value = occurredDateTime;

			if (reportDateTime > new DateTime(1900, 1, 1))
				command.Parameters.Add("@reportDate", SqlDbType.DateTime).Value = reportDateTime;

			if (lightingConditionTypeID > 0)
				command.Parameters.Add("@lightingConditionTypeID", SqlDbType.Int).Value = lightingConditionTypeID;

			if (reportStatusTypeID > 0)
				command.Parameters.Add("@reportStatusTypeID", SqlDbType.Int).Value = reportStatusTypeID;

			if (bulletinNumber > 0)
				command.Parameters.Add("@bulletinNumber", SqlDbType.Int).Value = bulletinNumber;

			if (ucrOffenseTypeID > 0)
				command.Parameters.Add("@ucrOffenseTypeID", SqlDbType.Int).Value = ucrOffenseTypeID;

			#endregion

			//do it
			try
			{
				conn.Open();
				command.ExecuteNonQuery();
		
				return new StatusInfo(true);
			}
			catch (Exception ex)
			{
				//send back error
				return new StatusInfo(false, ex.Message);
			}
			finally
			{
				conn.Close();
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Updates the Facesheet's Admin section.
		/// </summary>
		public static StatusInfo UpdateAdmin(int reportID,
											 int officer1EmployeeID,
											 string officer1Name, 
										     int officer1Badge,
											 int officer1RankTypeID,
											 string officer2Name,
											 int officer2Badge,
											 int officer2RankTypeID,
											 int reportingCarNumber,
											 string reportingCarPlatoon,
											 int reportingAssignmentUnitTypeID,
											 string detectiveName,
											 string crimeLabName,
											 string otherName,
											 string modifiedBy) 
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("UpdateReportAdmin", conn);
			command.CommandType = CommandType.StoredProcedure;

			#region params

			command.Parameters.Add("@reportID", SqlDbType.Int).Value = reportID;
			command.Parameters.Add("@officer1EmployeeID", SqlDbType.Int).Value = officer1EmployeeID;
			command.Parameters.Add("@officer1Name", SqlDbType.NVarChar, 50).Value = officer1Name;
			command.Parameters.Add("@officer1Badge", SqlDbType.Int).Value = officer1Badge;
			command.Parameters.Add("@officer1RankTypeID", SqlDbType.Int).Value = officer1RankTypeID;
			command.Parameters.Add("@modifiedBy", SqlDbType.NVarChar, 50).Value = modifiedBy;

			//optional
			if (!string.IsNullOrEmpty(officer2Name))
				command.Parameters.Add("@officer2Name", SqlDbType.NVarChar, 50).Value = officer2Name;

			if (officer2Badge > 0)
				command.Parameters.Add("@officer2Badge", SqlDbType.Int).Value = officer2Badge;

			if (officer2RankTypeID > 0)
				command.Parameters.Add("@officer2RankTypeID", SqlDbType.Int).Value = officer2RankTypeID;

			if (reportingCarNumber > 0)
				command.Parameters.Add("@reportingCarNumber", SqlDbType.Int).Value = reportingCarNumber;

			if (!string.IsNullOrEmpty(reportingCarPlatoon))
				command.Parameters.Add("@reportingCarPlatoon", SqlDbType.NVarChar, 2).Value = reportingCarPlatoon;

			if (reportingAssignmentUnitTypeID > 0)
				command.Parameters.Add("@reportingAssignmentUnitTypeID", SqlDbType.Int).Value = reportingAssignmentUnitTypeID;
			
			if (!string.IsNullOrEmpty(detectiveName))
				command.Parameters.Add("@detectiveName", SqlDbType.NVarChar, 50).Value = detectiveName;

			if (!string.IsNullOrEmpty(crimeLabName))
				command.Parameters.Add("@crimeLabName", SqlDbType.NVarChar, 50).Value = crimeLabName;

			if (!string.IsNullOrEmpty(otherName))
				command.Parameters.Add("@otherName", SqlDbType.NVarChar, 50).Value = otherName;

			#endregion

			//do it
			try
			{
				conn.Open();
				command.ExecuteNonQuery();
				
				return new StatusInfo(true);
			}
			catch (Exception ex)
			{
				//send back error
				return new StatusInfo(false, ex.Message);
			}
			finally
			{
				conn.Close();
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		///// <summary>
		///// Updates the Facesheet's UCR section.
		///// </summary>
		//public static StatusInfo UpdateUcrDummy(int reportID,
		//										int ucrOffenseTypeID,
		//										string modifiedBy)
		//{
		//	//conn & command
		//	SqlConnection conn = new SqlConnection(_connStr);
		//	SqlCommand command = new SqlCommand("UpdateUcrDummy", conn);
		//	command.CommandType = CommandType.StoredProcedure;

		//	#region params

		//	command.Parameters.Add("@reportID", SqlDbType.Int).Value = reportID;
		//	command.Parameters.Add("@ucrOffenseTypeID", SqlDbType.Int).Value = ucrOffenseTypeID;
		//	command.Parameters.Add("@modifiedBy", SqlDbType.NVarChar, 50).Value = modifiedBy;

		//	#endregion

		//	//do it
		//	try
		//	{
		//		conn.Open();
		//		command.ExecuteNonQuery();

		//		return new StatusInfo(true);
		//	}
		//	catch (Exception ex)
		//	{
		//		//send back error
		//		return new StatusInfo(false, ex.Message);
		//	}
		//	finally
		//	{
		//		conn.Close();
		//	}
		//}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Updates the M.O.'s Motivation section.
		/// </summary>
		public static StatusInfo UpdateMotivation(int reportID, 
												  string itemIdsXml, 
												  string motivationDescription, 
												  string modifiedBy)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("UpdateMotivations", conn);
			command.CommandType = CommandType.StoredProcedure;

			//params
			command.Parameters.Add("@reportID", SqlDbType.Int).Value = reportID;
			command.Parameters.Add("@itemIds", SqlDbType.Xml).Value = itemIdsXml;
			command.Parameters.Add("@modifiedBy", SqlDbType.NVarChar, 50).Value = modifiedBy;

			if (!string.IsNullOrEmpty(motivationDescription))
				command.Parameters.Add("@motivationDescription", SqlDbType.NVarChar, 500).Value = motivationDescription;

		
			//do it
			try
			{
				conn.Open();
				command.ExecuteNonQuery();

				return new StatusInfo(true);
			}
			catch (Exception ex)
			{
				//send back error
				return new StatusInfo(false, ex.Message);
			}
			finally
			{
				conn.Close();
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Updates the M.O.'s Means Of Entry section.
		/// </summary>
		public static StatusInfo UpdateMeansOfEntry(int reportID, 
													string itemIdsXml, 
													string securityUsedIdsXml, 
													string meansOfEntryDescription, 
													string modifiedBy)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("UpdateMeansOfEntry", conn);
			command.CommandType = CommandType.StoredProcedure;

			//params
			command.Parameters.Add("@reportID", SqlDbType.Int).Value = reportID;
			command.Parameters.Add("@itemIds", SqlDbType.Xml).Value = itemIdsXml;
			command.Parameters.Add("@securityUsedItemIds", SqlDbType.Xml).Value = securityUsedIdsXml; //put here, or in new proc call?
			command.Parameters.Add("@modifiedBy", SqlDbType.NVarChar, 50).Value = modifiedBy;

			if (!string.IsNullOrEmpty(meansOfEntryDescription))
				command.Parameters.Add("@meansOfEntryDescription", SqlDbType.NVarChar, 500).Value = meansOfEntryDescription;

			//do it
			try
			{
				conn.Open();
				command.ExecuteNonQuery();
		
				return new StatusInfo(true);
			}
			catch (Exception ex)
			{
				//send back error
				return new StatusInfo(false, ex.Message);
			}
			finally
			{
				conn.Close();
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Updates the M.O.'s Crime Location section.
		/// </summary>
		public static StatusInfo UpdateCrimeLocation(int reportID, 
													 string itemIdsXml, 
													 string crimeLocationDescription, 
													 string modifiedBy)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("UpdateCrimeLocations", conn);
			command.CommandType = CommandType.StoredProcedure;

			//params
			command.Parameters.Add("@reportID", SqlDbType.Int).Value = reportID;
			command.Parameters.Add("@itemIds", SqlDbType.Xml).Value = itemIdsXml;
			command.Parameters.Add("@modifiedBy", SqlDbType.NVarChar, 50).Value = modifiedBy;

			if (!string.IsNullOrEmpty(crimeLocationDescription))
				command.Parameters.Add("@crimeLocationDescription", SqlDbType.NVarChar, 500).Value = crimeLocationDescription;


			//do it
			try
			{
				conn.Open();
				command.ExecuteNonQuery();
		
				return new StatusInfo(true);
			}
			catch (Exception ex)
			{
				//send back error
				return new StatusInfo(false, ex.Message);
			}
			finally
			{
				conn.Close();
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Updates the M.O.'s Offender's Actions section.
		/// </summary>
		public static StatusInfo UpdateOffendersActions(int reportID, 
														string itemIdsXml, 
														string offendersActionsDescription, 
														string modifiedBy)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("UpdateOffendersActions", conn);
			command.CommandType = CommandType.StoredProcedure;

			//params
			command.Parameters.Add("@reportID", SqlDbType.Int).Value = reportID;
			command.Parameters.Add("@itemIds", SqlDbType.Xml).Value = itemIdsXml;
			command.Parameters.Add("@modifiedBy", SqlDbType.NVarChar, 50).Value = modifiedBy;

			if (!string.IsNullOrEmpty(offendersActionsDescription))
				command.Parameters.Add("@offendersActionsDescription", SqlDbType.NVarChar, 500).Value = offendersActionsDescription;

			//do it
			try
			{
				conn.Open();
				command.ExecuteNonQuery();
		
				return new StatusInfo(true);
			}
			catch (Exception ex)
			{
				//send back error
				return new StatusInfo(false, ex.Message);
			}
			finally
			{
				conn.Close();
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Updates the Narrative section.
		/// </summary>
		public static StatusInfo UpdateNarrative(int reportID, string narrative, string modifiedBy)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("UpdateReportNarrative", conn);
			command.CommandType = CommandType.StoredProcedure;

			//params
			command.Parameters.Add("@reportID", SqlDbType.Int).Value = reportID;
			command.Parameters.Add("@modifiedBy", SqlDbType.NVarChar, 50).Value = modifiedBy;

			//if its empty, db will use NULL
			if (!string.IsNullOrEmpty(narrative))
				command.Parameters.Add("@narrative", SqlDbType.NVarChar, -1).Value = narrative;
			
		
			//do it
			try
			{
				conn.Open();
				command.ExecuteNonQuery();
		
				return new StatusInfo(true);
			}
			catch (Exception ex)
			{
				//send back error
				return new StatusInfo(false, ex.Message);
			}
			finally
			{
				conn.Close();
			}
		}
		
		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static StatusInfo UpdatePropertyEvidence(int reportID, string propertyEvidenceReceiptNumber, string modifiedBy)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("UpdateReportPropertyEvidence", conn);
			command.CommandType = CommandType.StoredProcedure;

			//params
			command.Parameters.Add("@reportID", SqlDbType.Int).Value = reportID;
			command.Parameters.Add("@modifiedBy", SqlDbType.NVarChar, 50).Value = modifiedBy;

			//if its empty, db will use NULL
			if (!string.IsNullOrEmpty(propertyEvidenceReceiptNumber))
				command.Parameters.Add("@propertyEvidenceReceiptNumber", SqlDbType.NVarChar, 50).Value = propertyEvidenceReceiptNumber;


			//do it
			try
			{
				conn.Open();
				command.ExecuteNonQuery();

				return new StatusInfo(true);
			}
			catch (Exception ex)
			{
				//send back error
				return new StatusInfo(false, ex.Message);
			}
			finally
			{
				conn.Close();
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Updates the Supervisor section.
		/// </summary>
		public static StatusInfo UpdateSupervisor(int reportID,
												  string supervisorName,
												  int supervisorBadgeNumber,
												  int supervisorRankTypeID,
												  int approvalStatusTypeID,
												  string supervisorComments,
												  string modifiedBy)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("UpdateReportSupervisor", conn);
			command.CommandType = CommandType.StoredProcedure;

			//params
			command.Parameters.Add("@reportID", SqlDbType.Int).Value = reportID;
			command.Parameters.Add("@supervisorName", SqlDbType.NVarChar, 50).Value = supervisorName;
			command.Parameters.Add("@supervisorBadge", SqlDbType.Int).Value = supervisorBadgeNumber;
			command.Parameters.Add("@supervisorRankTypeID", SqlDbType.Int).Value = supervisorRankTypeID;
			command.Parameters.Add("@approvalStatusTypeID", SqlDbType.Int).Value = approvalStatusTypeID;
			command.Parameters.Add("@modifiedBy", SqlDbType.NVarChar, 50).Value = modifiedBy;

			if (!string.IsNullOrEmpty(supervisorComments))
				command.Parameters.Add("@supervisorComments", SqlDbType.NVarChar, -1).Value = supervisorComments;


			//do it
			try
			{
				conn.Open();
				command.ExecuteNonQuery();

				return new StatusInfo(true);
			}
			catch (Exception ex)
			{
				//send back error
				return new StatusInfo(false, ex.Message);
			}
			finally
			{
				conn.Close();
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static StatusInfo UpdateApproval(int reportID, int approvalStatusTypeID, string modifiedBy)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("UpdateReportApproval", conn);
			command.CommandType = CommandType.StoredProcedure;

			//params
			command.Parameters.Add("@reportID", SqlDbType.Int).Value = reportID;
			command.Parameters.Add("@approvalStatusTypeID", SqlDbType.Int).Value = approvalStatusTypeID;
			command.Parameters.Add("@lastModifiedBy", SqlDbType.NVarChar, 50).Value = modifiedBy;
			

			//do it
			try
			{
				conn.Open();
				command.ExecuteNonQuery();

				return new StatusInfo(true);
			}
			catch (Exception ex)
			{
				//send back error
				return new StatusInfo(false, ex.Message);
			}
			finally
			{
				conn.Close();
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Updates the Incident Number only.
		/// </summary>
		public static StatusInfo UpdateItemNumber(int reportID, string itemNumber, string modifiedBy)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("UpdateItemNumber", conn);
			command.CommandType = CommandType.StoredProcedure;

			//params
			command.Parameters.Add("@reportID", SqlDbType.Int).Value = reportID;
			command.Parameters.Add("@itemNumber", SqlDbType.NChar, 10).Value = itemNumber;
			command.Parameters.Add("@modifiedBy", SqlDbType.NVarChar, 50).Value = modifiedBy;

			//do it
			try
			{
				conn.Open();
				command.ExecuteNonQuery();

				return new StatusInfo(true);
			}
			catch (Exception ex)
			{
				//send back error
				return new StatusInfo(false, ex.Message);
			}
			finally
			{
				conn.Close();
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataSet GetReportByID(int reportID)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetReportByID", conn);
			command.CommandType = CommandType.StoredProcedure;

			command.Parameters.Add("@reportID", SqlDbType.Int).Value = reportID;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetReportsByUsername(string username)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetReportsByUsername", conn);
			command.CommandType = CommandType.StoredProcedure;
			command.CommandTimeout = 60;

			command.Parameters.Add("@username", SqlDbType.NVarChar, 50).Value = username;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataSet GetDashboardReportsByUsername(string username)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetDashboardReportsByUsername", conn);
			command.CommandType = CommandType.StoredProcedure;
			command.CommandTimeout = 60;

			command.Parameters.Add("@username", SqlDbType.NVarChar, 50).Value = username;

			//do it - may contain three tables: rejected, incomplete, pending
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetReportsByUsernameAndApprovalStatus(string username, int approvalStatusTypeID)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetReportsByApprovalStatusTypeID", conn);
			command.CommandType = CommandType.StoredProcedure;

			command.Parameters.Add("@username", SqlDbType.NVarChar, 50).Value = username;
			command.Parameters.Add("@approvalStatusTypeID", SqlDbType.Int).Value = approvalStatusTypeID;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetDeletableReportsByUsername(string username)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetDeletableReportsByUsername", conn);
			command.CommandType = CommandType.StoredProcedure;
			command.CommandTimeout = 60;

			command.Parameters.Add("@username", SqlDbType.NVarChar, 50).Value = username;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetReportsByCriteria(string itemNumber,
													 int approvalStatusTypeID,
													 int district,
													 string platoon,
													 int startCarNumber,
													 int endCarNumber,
													 DateTime startDate,
													 DateTime endDate,
													 int badgeNumber)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetReportsByCriteria", conn);
			command.CommandType = CommandType.StoredProcedure;
			command.CommandTimeout = 120;

			#region params

			if (!string.IsNullOrEmpty(itemNumber))
				command.Parameters.Add("@itemNumber", SqlDbType.NChar, 10).Value = itemNumber;

			if (approvalStatusTypeID > 0)
				command.Parameters.Add("@approvalStatusTypeID", SqlDbType.Int).Value = approvalStatusTypeID;

			if (district > 0)
				command.Parameters.Add("@district", SqlDbType.Int).Value = district;

			if (!string.IsNullOrEmpty(platoon))
				command.Parameters.Add("@platoon", SqlDbType.NVarChar, 2).Value = platoon;

			if (startCarNumber > 0)
				command.Parameters.Add("@startCarNumber", SqlDbType.Int).Value = startCarNumber;

			if (endCarNumber > 0)
				command.Parameters.Add("@endCarNumber", SqlDbType.Int).Value = endCarNumber;
			
			if (startDate != new DateTime(1900, 1, 1))
				command.Parameters.Add("@startDate", SqlDbType.DateTime).Value = startDate;

			if (endDate != new DateTime(1900, 1, 1))
				command.Parameters.Add("@endDate", SqlDbType.DateTime).Value = endDate;

			if (badgeNumber > 0)
				command.Parameters.Add("@badgeNumber", SqlDbType.Int).Value = badgeNumber;

			#endregion

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Gets reports by optional criteria.
		/// </summary>
		public static DataTable GetPlotReportsByCriteria(string itemNumber,
														 int district,
														 string zone,
														 string subzone,
														 string platoon,
														 int startCarNumber,
														 int endCarNumber,
														 DateTime startDate,
														 DateTime endDate,
														 int badgeNumber)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetPlotReportsByCriteria", conn);
			command.CommandType = CommandType.StoredProcedure;
			command.CommandTimeout = 120;

			#region params

			if (!string.IsNullOrEmpty(itemNumber))
				command.Parameters.Add("@itemNumber", SqlDbType.NChar, 10).Value = itemNumber;

			if (district > 0)
				command.Parameters.Add("@district", SqlDbType.Int).Value = district;
			
			if (!string.IsNullOrEmpty(zone))
				command.Parameters.Add("@zone", SqlDbType.NVarChar, 1).Value = zone;

			if (!string.IsNullOrEmpty(subzone))
				command.Parameters.Add("@subzone", SqlDbType.NVarChar, 2).Value = subzone;
			
			if (!string.IsNullOrEmpty(platoon))
				command.Parameters.Add("@platoon", SqlDbType.NVarChar, 2).Value = platoon;

			if (startCarNumber > 0)
				command.Parameters.Add("@startCarNumber", SqlDbType.Int).Value = startCarNumber;

			if (endCarNumber > 0)
				command.Parameters.Add("@endCarNumber", SqlDbType.Int).Value = endCarNumber;

			if (startDate != new DateTime(1900, 1, 1))
				command.Parameters.Add("@startDate", SqlDbType.DateTime).Value = startDate;

			if (endDate != new DateTime(1900, 1, 1))
				command.Parameters.Add("@endDate", SqlDbType.DateTime).Value = endDate;

			if (badgeNumber > 0)
				command.Parameters.Add("@badgeNumber", SqlDbType.Int).Value = badgeNumber;

			#endregion

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetVictimPersonsAndOffenders(int reportID)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetVictimPersonsAndOffendersByReportID", conn);
			command.CommandType = CommandType.StoredProcedure;

			command.Parameters.Add("@reportID", SqlDbType.Int).Value = reportID;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetReportStatusTypes()
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetReportStatusTypes", conn);
			command.CommandType = CommandType.StoredProcedure;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetLightingTypes()
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetLightingConditionTypes", conn);
			command.CommandType = CommandType.StoredProcedure;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetReportTypes()
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetReportTypes", conn);
			command.CommandType = CommandType.StoredProcedure;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetMotivationTypesByCategory(int categoryID)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetMotivationTypesByCategoryID", conn);
			command.CommandType = CommandType.StoredProcedure;

			command.Parameters.Add("@categoryID", SqlDbType.Int).Value = categoryID;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetMeansOfEntryTypesByCategory(int categoryID)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetMeansOfEntryTypesByCategoryID", conn);
			command.CommandType = CommandType.StoredProcedure;

			command.Parameters.Add("@categoryID", SqlDbType.Int).Value = categoryID;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetCrimeLocationTypesByCategory(int categoryID)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetCrimeLocationTypesByCategoryID", conn);
			command.CommandType = CommandType.StoredProcedure;

			command.Parameters.Add("@categoryID", SqlDbType.Int).Value = categoryID;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetOffenderActionTypesByCategory(int categoryID)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetOffenderActionTypesByCategoryID", conn);
			command.CommandType = CommandType.StoredProcedure;

			command.Parameters.Add("@categoryID", SqlDbType.Int).Value = categoryID;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetReportMotivationsByCategory(int reportID, int categoryID)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetReportMotivationsByCategoryID", conn);
			command.CommandType = CommandType.StoredProcedure;

			command.Parameters.Add("@reportID", SqlDbType.Int).Value = reportID;
			command.Parameters.Add("@categoryID", SqlDbType.Int).Value = categoryID;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetReportMeansOfEntryByCategory(int reportID, int categoryID)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetReportMeansOfEntryByCategoryID", conn);
			command.CommandType = CommandType.StoredProcedure;

			command.Parameters.Add("@reportID", SqlDbType.Int).Value = reportID;
			command.Parameters.Add("@categoryID", SqlDbType.Int).Value = categoryID;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetReportCrimeLocationsByCategory(int reportID, int categoryID)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetReportCrimeLocationsByCategoryID", conn);
			command.CommandType = CommandType.StoredProcedure;

			command.Parameters.Add("@reportID", SqlDbType.Int).Value = reportID;
			command.Parameters.Add("@categoryID", SqlDbType.Int).Value = categoryID;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetReportOffenderActionsByCategory(int reportID, int categoryID)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetReportOffendersActionsByCategoryID", conn);
			command.CommandType = CommandType.StoredProcedure;

			command.Parameters.Add("@reportID", SqlDbType.Int).Value = reportID;
			command.Parameters.Add("@categoryID", SqlDbType.Int).Value = categoryID;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetSecurityTypesUsedByReport(int reportID)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetSecurityTypesUsedByReportID", conn);
			command.CommandType = CommandType.StoredProcedure;

			command.Parameters.Add("@reportID", SqlDbType.Int).Value = reportID;
			
			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetApprovalStatusTypes()
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetApprovalStatusTypes", conn);
			command.CommandType = CommandType.StoredProcedure;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetPlatoonTypes()
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetPlatoonTypes", conn);
			command.CommandType = CommandType.StoredProcedure;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetRankTypes()
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetRankTypes", conn);
			command.CommandType = CommandType.StoredProcedure;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetAssignmentUnitTypes()
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetAssignmentUnitTypes", conn);
			command.CommandType = CommandType.StoredProcedure;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Sets a Report's Active flag to false, preventing it from being used anywhere.
		/// </summary>
		public static StatusInfo DeactivateReport(int reportID, string modifiedBy)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("DeactivateReport", conn);
			command.CommandType = CommandType.StoredProcedure;

			#region params

			command.Parameters.Add("@reportID", SqlDbType.Int).Value = reportID;
			command.Parameters.Add("@modifiedBy", SqlDbType.NVarChar, 50).Value = modifiedBy;
			
			#endregion

			//do it
			try
			{
				conn.Open();
				command.ExecuteNonQuery();

				return new StatusInfo(true);
			}
			catch (Exception ex)
			{
				//send back error
				return new StatusInfo(false, ex.Message);
			}
			finally
			{
				conn.Close();
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Inserts a new report workflow status notification.
		/// </summary>
		public static StatusInfo InsertReportNotification(int reportID, string username, string message)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("InsertReportNotification", conn);
			command.CommandType = CommandType.StoredProcedure;

			#region params

			command.Parameters.Add("@reportID", SqlDbType.Int).Value = reportID;
			command.Parameters.Add("@username", SqlDbType.NVarChar, 50).Value = username;
			command.Parameters.Add("@message", SqlDbType.NVarChar, 100).Value = message;

			#endregion

			//do it
			try
			{
				conn.Open();
				command.ExecuteNonQuery();

				//send back new ID
				return new StatusInfo(true);
			}
			catch (Exception ex)
			{
				//send back error
				return new StatusInfo(false, ex.Message);
			}
			finally
			{
				conn.Close();
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Gets all non-delivered ReportNotifications for the supplied username.
		/// </summary>
		public static DataTable GetNewReportNotifications(string username)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetNewReportNotificationsByUsername", conn);
			command.CommandType = CommandType.StoredProcedure;

			//TODO: where is the username param?

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}	
}
