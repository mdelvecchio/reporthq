using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;

using ReportHq.Common;

namespace ReportHq.IncidentReports.DataAccess
{
	/// <summary>
	/// DA layer for the Signal Type objects.
	/// </summary>
	public class SignalTypeDA
	{
		#region Variables

		static private string _connStr = ConfigurationManager.ConnectionStrings["ReportHq"].ConnectionString;

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Public Methods

		public static DataTable GetSignalTypes(bool getActiveOnly)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetSignalTypes", conn);
			command.CommandType = CommandType.StoredProcedure;

			command.Parameters.Add("@getActiveOnly", SqlDbType.Bit).Value = getActiveOnly;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static StatusInfo InsertSignalType(string code,
												  string description,
												  bool isDomesticViolence,
												  bool isCriminal,
												  int ucrOffenseTypeID,
												  bool isActive)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("InsertSignalType", conn);
			command.CommandType = CommandType.StoredProcedure;

			#region params

			command.Parameters.Add("@code", SqlDbType.NVarChar, 20).Value = code;
			command.Parameters.Add("@description", SqlDbType.NVarChar, 100).Value = description;
			command.Parameters.Add("@isDomesticViolence", SqlDbType.Bit).Value = isDomesticViolence;
			command.Parameters.Add("@isCriminal", SqlDbType.Bit).Value = isCriminal;

			//optional
			if (ucrOffenseTypeID > 0)
				command.Parameters.Add("@ucrOffenseTypeID", SqlDbType.Int).Value = ucrOffenseTypeID;

			command.Parameters.Add("@isActive", SqlDbType.Bit).Value = isActive;

			#endregion

			//do it
			try
			{
				conn.Open();
				command.ExecuteNonQuery();

				return new StatusInfo(true);
			}
			catch (Exception ex)
			{
				//send back error
				return new StatusInfo(false, ex.Message);
			}
			finally
			{
				conn.Close();
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static StatusInfo UpdateSignalType(int signalTypeID,
												  string code,
												  string description,
												  bool isDomesticViolence,
												  bool isCriminal,
												  int ucrOffenseTypeID,
												  bool isActive)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("UpdateSignalType", conn);
			command.CommandType = CommandType.StoredProcedure;

			#region params

			command.Parameters.Add("@signalTypeID", SqlDbType.Int).Value = signalTypeID;
			command.Parameters.Add("@code", SqlDbType.NVarChar, 20).Value = code;
			command.Parameters.Add("@description", SqlDbType.NVarChar, 100).Value = description;
			command.Parameters.Add("@isDomesticViolence", SqlDbType.Bit).Value = isDomesticViolence;
			command.Parameters.Add("@isCriminal", SqlDbType.Bit).Value = isCriminal;

			//optional
			if (ucrOffenseTypeID > 0)
				command.Parameters.Add("@ucrOffenseTypeID", SqlDbType.Int).Value = ucrOffenseTypeID;

			command.Parameters.Add("@isActive", SqlDbType.Bit).Value = isActive;

			#endregion

			//do it
			try
			{
				conn.Open();
				command.ExecuteNonQuery();

				return new StatusInfo(true);
			}
			catch (Exception ex)
			{
				//send back error
				return new StatusInfo(false, ex.Message);
			}
			finally
			{
				conn.Close();
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static StatusInfo DeactivateSignalType(int signalTypeID)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("DeactivateSignalType", conn);
			command.CommandType = CommandType.StoredProcedure;

			command.Parameters.Add("@signalTypeID", SqlDbType.Int).Value = signalTypeID;

			//do it
			try
			{
				conn.Open();
				command.ExecuteNonQuery();

				return new StatusInfo(true);
			}
			catch (Exception ex)
			{
				//send back error
				return new StatusInfo(false, ex.Message);
			}
			finally
			{
				conn.Close();
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}
