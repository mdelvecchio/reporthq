using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;

using ReportHq.Common;

namespace ReportHq.IncidentReports.DataAccess
{
	/// <summary>
	/// DA layer for the PropertyEvidence object.
	/// </summary>
	public class PropertyEvidenceDA
	{
		#region Variables

		static private string _connStr = ConfigurationManager.ConnectionStrings["ReportHq"].ConnectionString;

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Public Methods

		public static StatusInfo InsertProperty(int reportID,
												int lossTypeID,
												decimal quantity,
												int typeID,
												int narcoticWeightTypeID,
												int narcoticTypeID,
												string description,
												string brand,
												string serialNumber,
												int value,
												string createdBy)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("InsertProperty", conn);
			command.CommandType = CommandType.StoredProcedure;

			#region params

			command.Parameters.Add("@reportID", SqlDbType.Int).Value = reportID;
			command.Parameters.Add("@createdBy", SqlDbType.NVarChar, 50).Value = createdBy;

			//optional
			if (lossTypeID > 0)
				command.Parameters.Add("@propertyEvidenceLossTypeID", SqlDbType.Int).Value = lossTypeID;

			if (quantity > 0)
				command.Parameters.Add("@quantity", SqlDbType.Decimal).Value = quantity;

			if (typeID > 0)
				command.Parameters.Add("@propertyTypeID", SqlDbType.Int).Value = typeID;

			if (narcoticWeightTypeID > 0)
				command.Parameters.Add("@narcoticWeightTypeID", SqlDbType.Int).Value = narcoticWeightTypeID;

			if (narcoticTypeID > 0)
				command.Parameters.Add("@narcoticTypeID", SqlDbType.Int).Value = narcoticTypeID;
			
			if (!string.IsNullOrEmpty(description))
				command.Parameters.Add("@description", SqlDbType.NVarChar, 250).Value = description;

			if (!string.IsNullOrEmpty(brand))
				command.Parameters.Add("@brand", SqlDbType.NVarChar, 50).Value = brand;

			if (!string.IsNullOrEmpty(serialNumber))
				command.Parameters.Add("@serialNumber", SqlDbType.NVarChar, 50).Value = serialNumber;

			if (value > 0)
				command.Parameters.Add("@value", SqlDbType.Int).Value = value;

			#endregion

			//do it
			try
			{
				conn.Open();
				command.ExecuteNonQuery();

				//send back new ID
				//return new StatusInfo(true, (int)param.Value);
				return new StatusInfo(true);
			}
			catch (Exception ex)
			{
				//send back error
				return new StatusInfo(false, ex.Message);
			}
			finally
			{
				conn.Close();
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static StatusInfo UpdateProperty(int propertyID,
												int lossTypeID,
												decimal quantity,
												int typeID,
												int narcoticWeightTypeID,
												int narcoticTypeID,
												string description,
												string brand,
												string serialNumber,
												int value,
												string modifiedBy)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("UpdateProperty", conn);
			command.CommandType = CommandType.StoredProcedure;

			#region params

			command.Parameters.Add("@propertyID", SqlDbType.Int).Value = propertyID;
			command.Parameters.Add("@lastModifiedBy", SqlDbType.NVarChar, 50).Value = modifiedBy;

			//optional
			if (lossTypeID > 0)
				command.Parameters.Add("@propertyEvidenceLossTypeID", SqlDbType.Int).Value = lossTypeID;

			if (quantity > 0)
				command.Parameters.Add("@quantity", SqlDbType.Decimal).Value = quantity;

			if (typeID > 0)
				command.Parameters.Add("@propertyTypeID", SqlDbType.Int).Value = typeID;

			if (narcoticWeightTypeID > 0)
				command.Parameters.Add("@narcoticWeightTypeID", SqlDbType.Int).Value = narcoticWeightTypeID;

			if (narcoticTypeID > 0)
				command.Parameters.Add("@narcoticTypeID", SqlDbType.Int).Value = narcoticTypeID;

			if (!string.IsNullOrEmpty(description))
				command.Parameters.Add("@description", SqlDbType.NVarChar, 250).Value = description;

			if (!string.IsNullOrEmpty(brand))
				command.Parameters.Add("@brand", SqlDbType.NVarChar, 50).Value = brand;

			if (!string.IsNullOrEmpty(serialNumber))
				command.Parameters.Add("@serialNumber", SqlDbType.NVarChar, 50).Value = serialNumber;

			if (value > 0)
				command.Parameters.Add("@value", SqlDbType.Int).Value = value;

			#endregion

			//do it
			try
			{
				conn.Open();
				command.ExecuteNonQuery();

				return new StatusInfo(true);
			}
			catch (Exception ex)
			{
				//send back error
				return new StatusInfo(false, ex.Message);
			}
			finally
			{
				conn.Close();
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static StatusInfo InsertEvidence(int reportID,
												int lossTypeID,
												decimal quantity,
												int typeID,
												int narcoticWeightTypeID,
												int narcoticTypeID,
												string description,
												string brand,
												string serialNumber,
												int value,
												string createdBy)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("InsertEvidence", conn);
			command.CommandType = CommandType.StoredProcedure;

			#region params

			command.Parameters.Add("@reportID", SqlDbType.Int).Value = reportID;
			command.Parameters.Add("@createdBy", SqlDbType.NVarChar, 50).Value = createdBy;

			//optional
			if (lossTypeID > 0)
				command.Parameters.Add("@propertyEvidenceLossTypeID", SqlDbType.Int).Value = lossTypeID;

			if (quantity > 0)
				command.Parameters.Add("@quantity", SqlDbType.Decimal).Value = quantity;

			if (typeID > 0)
				command.Parameters.Add("@propertyTypeID", SqlDbType.Int).Value = typeID;

			if (narcoticWeightTypeID > 0)
				command.Parameters.Add("@narcoticWeightTypeID", SqlDbType.Int).Value = narcoticWeightTypeID;

			if (narcoticTypeID > 0)
				command.Parameters.Add("@narcoticTypeID", SqlDbType.Int).Value = narcoticTypeID;

			if (!string.IsNullOrEmpty(description))
				command.Parameters.Add("@description", SqlDbType.NVarChar, 250).Value = description;

			if (!string.IsNullOrEmpty(brand))
				command.Parameters.Add("@brand", SqlDbType.NVarChar, 50).Value = brand;

			if (!string.IsNullOrEmpty(serialNumber))
				command.Parameters.Add("@serialNumber", SqlDbType.NVarChar, 50).Value = serialNumber;

			if (value > 0)
				command.Parameters.Add("@value", SqlDbType.Int).Value = value;

			#endregion

			//do it
			try
			{
				conn.Open();
				command.ExecuteNonQuery();

				//send back new ID
				//return new StatusInfo(true, (int)param.Value);
				return new StatusInfo(true);
			}
			catch (Exception ex)
			{
				//send back error
				return new StatusInfo(false, ex.Message);
			}
			finally
			{
				conn.Close();
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static StatusInfo UpdateEvidence(int evidenceID,
												int lossTypeID,
												decimal quantity,
												int typeID,
												int narcoticWeightTypeID,
												int narcoticTypeID,
												string description,
												string brand,
												string serialNumber,
												int value,
												string modifiedBy)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("UpdateEvidence", conn);
			command.CommandType = CommandType.StoredProcedure;

			#region params

			command.Parameters.Add("@evidenceID", SqlDbType.Int).Value = evidenceID;
			command.Parameters.Add("@lastModifiedBy", SqlDbType.NVarChar, 50).Value = modifiedBy;

			//optional
			if (lossTypeID > 0)
				command.Parameters.Add("@propertyEvidenceLossTypeID", SqlDbType.Int).Value = lossTypeID;

			if (quantity > 0)
				command.Parameters.Add("@quantity", SqlDbType.Decimal).Value = quantity;

			if (typeID > 0)
				command.Parameters.Add("@propertyTypeID", SqlDbType.Int).Value = typeID;

			if (narcoticWeightTypeID > 0)
				command.Parameters.Add("@narcoticWeightTypeID", SqlDbType.Int).Value = narcoticWeightTypeID;

			if (narcoticTypeID > 0)
				command.Parameters.Add("@narcoticTypeID", SqlDbType.Int).Value = narcoticTypeID;

			if (!string.IsNullOrEmpty(description))
				command.Parameters.Add("@description", SqlDbType.NVarChar, 250).Value = description;

			if (!string.IsNullOrEmpty(brand))
				command.Parameters.Add("@brand", SqlDbType.NVarChar, 50).Value = brand;

			if (!string.IsNullOrEmpty(serialNumber))
				command.Parameters.Add("@serialNumber", SqlDbType.NVarChar, 50).Value = serialNumber;

			if (value > 0)
				command.Parameters.Add("@value", SqlDbType.Int).Value = value;

			#endregion

			//do it
			try
			{
				conn.Open();
				command.ExecuteNonQuery();

				return new StatusInfo(true);
			}
			catch (Exception ex)
			{
				//send back error
				return new StatusInfo(false, ex.Message);
			}
			finally
			{
				conn.Close();
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static StatusInfo DeleteProperty(int propertyID)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("DeleteProperty", conn);
			command.CommandType = CommandType.StoredProcedure;

			command.Parameters.Add("@propertyID", SqlDbType.Int).Value = propertyID;

			//do it
			try
			{
				conn.Open();
				command.ExecuteNonQuery();

				return new StatusInfo(true);
			}
			catch (Exception ex)
			{
				//send back error
				return new StatusInfo(false, ex.Message);
			}
			finally
			{
				conn.Close();
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static StatusInfo DeleteEvidence(int evidenceID)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("DeleteEvidence", conn);
			command.CommandType = CommandType.StoredProcedure;

			command.Parameters.Add("@evidenceID", SqlDbType.Int).Value = evidenceID;

			//do it
			try
			{
				conn.Open();
				command.ExecuteNonQuery();

				return new StatusInfo(true);
			}
			catch (Exception ex)
			{
				//send back error
				return new StatusInfo(false, ex.Message);
			}
			finally
			{
				conn.Close();
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetPropertyByReport(int reportID)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetPropertyByReportID", conn);
			command.CommandType = CommandType.StoredProcedure;

			//params
			command.Parameters.Add("@reportID", SqlDbType.Int).Value = reportID;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetEvidenceByReport(int reportID)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetEvidenceByReportID", conn);
			command.CommandType = CommandType.StoredProcedure;

			//params
			command.Parameters.Add("@reportID", SqlDbType.Int).Value = reportID;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetPropertyTypes()
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetPropertyTypes", conn);
			command.CommandType = CommandType.StoredProcedure;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetPropertyEvidenceLossTypes()
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetPropertyEvidenceLossTypes", conn);
			command.CommandType = CommandType.StoredProcedure;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		
		public static DataTable GetNarcoticTypes()
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetNarcoticTypes", conn);
			command.CommandType = CommandType.StoredProcedure;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		
		public static DataTable GetNarcoticWeightTypes()
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetNarcoticWeightTypes", conn);
			command.CommandType = CommandType.StoredProcedure;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetPropertySumsByReport(int reportID)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetPropertySumsByReportID", conn);
			command.CommandType = CommandType.StoredProcedure;

			//params
			command.Parameters.Add("@reportID", SqlDbType.Int).Value = reportID;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}	
}
