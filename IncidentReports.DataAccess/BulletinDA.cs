using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;

using ReportHq.Common;

namespace ReportHq.IncidentReports.DataAccess
{
	/// <summary>
	/// DA layer for the Bulletin objects.
	/// </summary>
	public class BulletinDA
	{
		#region Variables

		static private string _connStr = ConfigurationManager.ConnectionStrings["ReportHq"].ConnectionString;

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Public Methods

		public static StatusInfo InsertBulletin(int bulletinTypeID, 
												bool isAllPoints,
												string title, 
												string bulletin,
												string useRoleIds, 
												string createdBy)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("InsertBulletin", conn);
			command.CommandType = CommandType.StoredProcedure;

			#region params

			command.Parameters.Add("@bulletinTypeID", SqlDbType.Int).Value = bulletinTypeID;
			command.Parameters.Add("@isAllPoints", SqlDbType.Bit).Value = isAllPoints;
			command.Parameters.Add("@title", SqlDbType.NVarChar, 50).Value = title;
			command.Parameters.Add("@bulletin", SqlDbType.NVarChar, -1).Value = bulletin;
			command.Parameters.Add("@userRoleIds", SqlDbType.Xml).Value = useRoleIds;
			command.Parameters.Add("@createdBy", SqlDbType.NVarChar, 50).Value = createdBy;

			//output - new ReportID
			SqlParameter param = new SqlParameter("@bulletinID", SqlDbType.Int);
			param.Direction = ParameterDirection.Output;
			command.Parameters.Add(param);
		
			#endregion

			//do it
			try
			{
				conn.Open();
				command.ExecuteNonQuery();

				//send back new ID
				return new StatusInfo(true, (int)param.Value);
			}
			catch (Exception ex)
			{
				//send back error
				return new StatusInfo(false, ex.Message);
			}
			finally
			{
				conn.Close();
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static StatusInfo UpdateBulletin(int bulletinID,
												int bulletinTypeID,
												bool isAllPoints,
												string title,
												bool isActive,
												string bulletin,
												string useRoleIds,
												string lastModifiedBy)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("UpdateBulletin", conn);
			command.CommandType = CommandType.StoredProcedure;

			#region params

			command.Parameters.Add("@bulletinID", SqlDbType.Int).Value = bulletinID;
			command.Parameters.Add("@bulletinTypeID", SqlDbType.Int).Value = bulletinTypeID;
			command.Parameters.Add("@isAllPoints", SqlDbType.Bit).Value = isAllPoints;
			command.Parameters.Add("@title", SqlDbType.NVarChar, 50).Value = title;
			command.Parameters.Add("@isActive", SqlDbType.Bit).Value = isActive;
			command.Parameters.Add("@bulletin", SqlDbType.NVarChar, -1).Value = bulletin;
			command.Parameters.Add("@userRoleIds", SqlDbType.Xml).Value = useRoleIds;
			command.Parameters.Add("@lastModifiedBy", SqlDbType.NVarChar, 50).Value = lastModifiedBy;

			#endregion

			//do it
			try
			{
				conn.Open();
				command.ExecuteNonQuery();

				return new StatusInfo(true);
			}
			catch (Exception ex)
			{
				//send back error
				return new StatusInfo(false, ex.Message);
			}
			finally
			{
				conn.Close();
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataSet GetBulletinByID(int bulletinID)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetBulletinByID", conn);
			command.CommandType = CommandType.StoredProcedure;

			//params
			command.Parameters.Add("@ID", SqlDbType.Int).Value = bulletinID;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetAllBulletins()
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetAllBulletins", conn);
			command.CommandType = CommandType.StoredProcedure;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetAllPointBulletins()
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetAllPointBulletins", conn);
			command.CommandType = CommandType.StoredProcedure;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataSet GetBulletinsByUsername(string username)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetBulletinsByUsername", conn);
			command.CommandType = CommandType.StoredProcedure;

			//params
			command.Parameters.Add("@username", SqlDbType.NVarChar, 50).Value = username;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetBulletinTypes()
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("GetBulletinTypes", conn);
			command.CommandType = CommandType.StoredProcedure;

			//do it
			DataSet ds = new DataSet();
			SqlDataAdapter da = new SqlDataAdapter(command);
			da.Fill(ds);

			return ds.Tables[0];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static StatusInfo DeleteBulletin(int bulletinID)
		{
			//conn & command
			SqlConnection conn = new SqlConnection(_connStr);
			SqlCommand command = new SqlCommand("DeleteBulletin", conn);
			command.CommandType = CommandType.StoredProcedure;

			//params
			command.Parameters.Add("@ID", SqlDbType.Int).Value = bulletinID;

			//do it
			try
			{
				conn.Open();
				command.ExecuteNonQuery();

				return new StatusInfo(true);
			}
			catch (Exception ex)
			{
				//send back error
				return new StatusInfo(false, ex.Message);
			}
			finally
			{
				conn.Close();
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}
