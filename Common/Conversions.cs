﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportHq.Common
{
    public class Conversions
    {
		#region Public Methods

		/// <summary>
		/// Converts an imperial height into inches-only.
		/// </summary>
		/// <param name="height">The imperial height. Ex: "6'1"</param>
		/// <returns>The total number of inches.</returns>
		public static int GetInchesFromHeight(string height)
		{
			string[] parts = height.Split("'".ToCharArray());

			int feet = 0;
			int inches = 0;

			//get feet
			if (!string.IsNullOrEmpty(parts[0]))
			{
				feet = Int32.Parse(parts[0]);
			}

			//get inches
			if (parts.Length > 1)
			{
				if (!string.IsNullOrEmpty(parts[1]))
				{
					inches = Int32.Parse(parts[1]);
				}
			}

			int feetInInches = feet * 12;

			return feetInInches + inches;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Converts an inches into an imperial height.
		/// </summary>
		/// <param name="numberOfInches">The total number of inches. Ex: 73</param>
		/// <returns>The imperial height. Ex: "6'1"</returns>
		public static string GetHeightFromInches(int heightInInches)
		{
			string returnValue = string.Empty;

			int feet = heightInInches / 12;

			int inches = heightInInches - (feet * 12);

			//return String.Format("{0}'{1}", feet, inches) // fails to remove 0 if an even-foot amount. ex: 6'0

			if (heightInInches > 0)
			{
				returnValue = feet + "'";

				if (inches > 0)
					returnValue += inches; // +@"""";
			}

			return returnValue;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}		
}
