﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportHq.Common
{
	/// <summary>
	/// An object for containing status codes, states, and messages. Used to pass between methods on transacation attempts, etc.
	/// </summary>
	public class StatusInfo
	{
		#region Variables



		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Properties

		bool _success = false;
		string _message = string.Empty;
		int _code = -1;
		DataSet _logs = null;
		ArrayList _simpleLogs = null;

		public bool Success
		{
			get { return _success; }
			set { _success = value; }
		}

		public string Message
		{
			get { return _message; }
			set { _message = value; }
		}

		public int Code
		{
			get { return _code; }
			set { _code = value; }
		}

		public DataSet Logs
		{
			get { return _logs; }
			set { _logs = value; }
		}

		public ArrayList SimpleLogs
		{
			get { return _simpleLogs; }
			set { _simpleLogs = value; }
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Constructors

		public StatusInfo()
		{

		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public StatusInfo(bool success)
		{
			_success = success;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public StatusInfo(bool success, string message)
		{
			_success = success;
			_message = message;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public StatusInfo(bool success, int code)
		{
			_success = success;
			_code = code;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public StatusInfo(bool success, decimal code)
		{
			_success = success;
			_code = Convert.ToInt32(code);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public StatusInfo(bool success, string message, int code)
		{
			_success = success;
			_message = message;
			_code = code;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public StatusInfo(bool success, string message, DataSet logs)
		{
			_success = success;
			_message = message;
			_logs = logs;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public StatusInfo(bool success, string message, int code, DataSet logs)
		{
			_success = success;
			_message = message;
			_code = code;
			_logs = logs;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public StatusInfo(bool success, string message, ArrayList logs)
		{
			_success = success;
			_message = message;
			_simpleLogs = logs;
		}

		#endregion		
	}
}
