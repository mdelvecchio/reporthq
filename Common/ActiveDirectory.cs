﻿using System;
using System.Collections.Generic;
using System.Data;
using System.DirectoryServices;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ReportHq.Common
{
	/// <summary>
	/// Our helper class for performing AD tasks.
	/// </summary>
	public class ActiveDirectory
	{
		#region Public Methods

		/// <summary>
		/// Get the user's account name from ASP.NET's User principle. Trims out domain. ex: "mdelvecchio"
		/// </summary>
		public static string GetUserName()
		{
			//get username, create array based on "Domain\User" format
			string username = HttpContext.Current.User.Identity.Name;
			string[] nameParts = username.Split(@"\".ToCharArray());

			//if this is a with-domain username, get just the name portion only
			if (nameParts.Length == 2)
			{
				return nameParts[1];
			}
			else
			{
				return username;
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Get the user's account name from ASP.NET's User principle, appends domain.com. ex: "mdelvecchio@someDomain.com".
		/// </summary>
		public static string GetAdEmail()
		{
			return GetUserName() + "@someDomain.com";
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Get a user's full-name from Active Directory. Ex: "Matt Del Vecchio".
		/// </summary>
		/// <param name="username">The user's AD username; format is "mdelvecchio@someDomain.com".</param>
		public static String GetAdDisplayName(string username)
		{
			return GetAdAttributeValue(username, "DisplayName");
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Get a user's unique Employee ID from Acitive Directory. Ex: 12345.
		/// </summary>
		/// <param name="username">The user's AD username; format is "jsmith@someDomain.com".</param>
		public static String GetAdEmployeeID(string username)
		{
			return GetAdAttributeValue(username, "EmployeeID");
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Gets table of users matching supplied name-part.
		/// </summary>
		/// <param name="lastName"></param>
		/// <returns></returns>
		public static DataTable GetActiveDirectoryUsers(string lastName)
		{
			DataTable dt = new DataTable();
			dt.Columns.Add("DisplayName", System.Type.GetType("System.String"));
			dt.Columns.Add("Username", System.Type.GetType("System.String"));

			//the base root to start from
			DirectoryEntry root = new DirectoryEntry("LDAP://DC=someDomain,DC=com"); //TODO: pull from config
			//DirectoryEntry root = new DirectoryEntry("LDAP://DC=someDomain,DC=com", "user@someDomain.com", "password"); //dont seem to need our service account

			DirectorySearcher searcher = new DirectorySearcher(root);
			searcher.PropertiesToLoad.Add("DisplayName"); //ex: Matt Del Vecchio
			searcher.PropertiesToLoad.Add("UserPrincipalName"); //ex: "DOMAIN\mldelvecchio" or "mldelvecchio@someDomain.com"

			#region AD fields
			/*
			"initials"
			telephoneNumber"
			"physicalDeliveryOfficeName"
			"givenName" - first name
			"sn" - last name
			"UserPrincipalName" - mldelvecchio@someDomain.com
			"SamAccountName" - mldelvecchio
			"DisplayName" - Matt Del Vecchio
			"name" - username
			"department"
			"description"
			*/
			#endregion

			//filter by last name
			searcher.Filter = string.Format("(&(objectCategory=user)(sn={0}))", lastName);

			using (SearchResultCollection results = searcher.FindAll())
			{
				foreach (SearchResult result in results)
				{
					if (result.Properties.Contains("DisplayName"))
					{
						string commonName = string.Empty;
						string username = string.Empty;

						if (result.Properties["DisplayName"].Count > 0)
							commonName = (string)result.Properties["DisplayName"][0];

						if (result.Properties["UserPrincipalName"].Count > 0)
							username = (string)result.Properties["UserPrincipalName"][0];

						dt.Rows.Add(new Object[2] { commonName, username });
					}
				}
			}

			return dt;
		}

		#endregion

		#region Private Methods

		/// <summary>
		/// Get the supplier AD attribute value from our AD server.
		/// </summary>
		/// <param name="username">A user's AD username; format is "jsmith@someDomain.com".</param>
		/// <param name="attributeName">The desired AD attribute. Ex: "DisplayName".</param>
		private static String GetAdAttributeValue(string username, string attributeName)
		{
			string returnValue = string.Empty;

			//the base root to start from
			DirectoryEntry root = new DirectoryEntry("LDAP://DC=someDomain,DC=com"); //TODO: pull from config
			//DirectoryEntry root = new DirectoryEntry("LDAP://DC=someDomain,DC=com", "user@someDomain.com", "password"); //dont seem to need our service account

			DirectorySearcher searcher = new DirectorySearcher(root);
			searcher.PropertiesToLoad.Add(attributeName); //ex: DisplayName for "Matt Del Vecchio"
			searcher.Filter = string.Format("(&(objectCategory=user)(UserPrincipalName={0}))", username); //AD filter by username; ex: "jsmith@someDomain.com"

			#region other AD fields
			/*
			"initials"
			telephoneNumber"
			"physicalDeliveryOfficeName"
			"givenName" - first name
			"sn" - last name
			"UserPrincipalName" - mldelvecchio@someDomain.com
			"SamAccountName" - mldelvecchio
			"DisplayName" - Matt Del Vecchio
			"name" - username
			"department"
			"description"
			*/
			#endregion

			SearchResult result = null;

			try
			{
				//do it
				result = searcher.FindOne();

				if (result != null) //got a hit
				{
					if (result.Properties[attributeName].Count > 0)
						returnValue = (string)result.Properties[attributeName][0];
				}
			}
			catch
			{
			}

			return returnValue;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}

}
