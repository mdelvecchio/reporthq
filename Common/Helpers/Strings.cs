using System;
using System.IO;
using System.Web;
using System.Collections.Specialized;
using System.Text;

namespace ReportHq.Common
{
	/// <summary>
	/// Static helper class for working with strings.
	/// </summary>
	public class Strings
	{
		#region Public Methods

		public static bool IsNumeric(string theString)
		{
			//doing a char-by-char eval may be better; but this is easy.
			//TODO: actually, i wrote a better function for this at LPL. see if i can get it... (guess i never found it!)

			bool status = false;

			if (!string.IsNullOrEmpty(theString))
			{
				if (Microsoft.VisualBasic.Information.IsNumeric(theString) == true)
				{
					return true;
				}
			}
			
			return status;
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Capitalizes the first letter of each word passed in.
		/// </summary>
		public static string CapitalizeFirstChars(string theString)
		{
			theString = theString.Trim().ToLower();

			//get array of words
			string[] arrString = theString.Split( new char[]{' '});

			theString = string.Empty;

			//loop each word
			for (int i=0; i < arrString.Length; i++)
			{
				string item = arrString[i];

				//get & capitalize first char
				string firstChar = item.Substring(0, 1).ToUpper();

				//replace lowercase first char w/ new
				item = item.Remove(0, 1);
				item = item.Insert(0, firstChar);

				//add to new return value
				theString += item + " ";
			}

			return theString.TrimEnd();
		}
		
		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Quick way to see if a string has a value. Depreciating since .NET now includes string.IsNullOrEmpty() method
		/// </summary>
		/// <param name="theString">The string to check</param>
		/// <returns>Bool</returns>
		//public static bool Exists(string theString)
		//{
		//	if (theString != null && theString.Length > 0){
		//		return true;
		//	} else {
		//		return false;
		//	}
		//}
				
		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Truncates a string, including a visual indicator of truncation.
		/// </summary>
		/// <param name="theString">Original string</param>
		/// <param name="newLength">Length to shorten it to</param>
		/// <returns>Shortened string w/ dots</returns>
		public static string TruncateWithDots(string theString, int newLength)
		{
			if (theString.Length > newLength)
			{
				theString = theString.Substring(0, newLength) + "...";
			}

			return theString;
		}
			
		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Replaces normal carriage returns w/ br tags for the web.
		/// </summary>
		/// <param name="theString">Original string</param>
		/// <returns>string</returns>
		public static string InsertBreaks(string theString)
		{
			if (!string.IsNullOrWhiteSpace(theString))
				return theString.Replace("\n", "<br/>");
			else
				return string.Empty;
		}
			
		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Takes in a shorthand URL such as "~/images/xxx.gif" and returns it w/ the appropiate app path if running under one. (ex: "/IncidentReports/images/xxx.gif")
		/// </summary>
		/// <param name="context">Pass in the HttpContext; handy if calling this many times from a single page.</param>
		/// <param name="url">The shorthand URL.</param>
		/// <returns>Full app path of the given URL.</returns>
		public static string GetAppPathUrl(HttpContext context, String url)
		{
			string returnValue;

			if (url.StartsWith("~"))
			{
				returnValue = url.Substring(1);

				if (context.Request.ApplicationPath != "/")
				{
					returnValue = context.Request.ApplicationPath + returnValue.Replace("//", "/");
				}
			}
			else
			{
				returnValue = url;
			}

			return returnValue;
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Takes in a collection of strings and formats them in am HTML unordered list format.
		/// </summary>
		/// <param name="header">Text to put above the list items</param>
		/// <param name="items">Collectioin of string items</param>
		public static string FormatList(string header, StringCollection items)
		{
			StringBuilder sb = new StringBuilder(200);
			sb.Append(header + "<ul>");

			foreach (String item in items)
			{
				sb.Append("<li>" + item + "</li>");
			}

			sb.Append("</ul>");

			return sb.ToString();
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Formats a 9-character SSN value into XXX-XX-XXXX.
		/// </summary>
		/// <param name="theValue"></param>
		/// <returns></returns>
		public static string FormatSsn(string theValue)
		{
			string returnValue = string.Empty;

			if (!string.IsNullOrEmpty(theValue))
			{
				if (theValue.Length == 9)
				{
					returnValue = theValue.Insert(5, "-").Insert(3, "-");
					//returnValue = string.Format("{0}-{1}-{2}", theValue.Substring(0, 3), theValue.Substring(3, 2), theValue.Substring(5));
				}
				else
				{
					returnValue = theValue;
				}
			}			

			return returnValue;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}
