using System;
using System.IO;
using System.Web;
using System.Collections.Specialized;
using System.Text;

namespace ReportHq.Common
{
	/// <summary>
	/// Static helper class for working with files.
	/// </summary>
	public class FileBuddy
	{
		#region Public Methods

		/// <summary>
		/// Opens a file from the local file system and returns as a byte array for pushing elsewhere.
		/// </summary>
		/// <param name="fullPath"></param>
		/// <returns></returns>
		public static byte[] GetFile(string fullPath)
		{
			byte[] returnValue;

			Stream file = new FileStream(fullPath, FileMode.Open);

			using (var memoryStream = new MemoryStream())
			{
				file.CopyTo(memoryStream);
				returnValue = memoryStream.ToArray();
			}

			file.Close();

			return returnValue;

			//byte[] buffer;  // byte array
			//Stream fileStream = new FileStream(fullPath, FileMode.Open);
			//buffer = new byte[fileStream.Length];  //declare arraysize
			//fileStream.Read(buffer, 0, buffer.Length); // read from stream to byte array
		}
				
		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}
