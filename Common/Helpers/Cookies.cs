using System;
using System.Web;

namespace ReportHq.Common
{
	/// <summary>
	/// Static helper class for cookie tasks.
	/// </summary>
	public class Cookies
	{
		#region Constructors

		public Cookies()
		{
			
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Methods

		/// <summary>
		/// Creates a new cookie to the current context.
		/// </summary>
		public static void CreateNewCookie(HttpContext context, string cookieName, bool rememberMe)
		{
			HttpCookie cookie = new HttpCookie(cookieName);

			context.Response.Cookies.Add(cookie);

			//optional cookie persist
			if (rememberMe == true)
				context.Response.Cookies[cookieName].Expires = DateTime.Now.AddMonths(1);
		}
		
		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Adds a name-value item to a cookie.
		/// </summary>
		public static void AddCookieItem(HttpContext context, string cookieName, string itemName, string itemValue)
		{
			HttpCookie cookie = context.Request.Cookies[cookieName];

			//if this cookie doesnt exist, create it first
			if (cookie == null)
			{
				CreateNewCookie(context, cookieName, false);
				cookie = context.Request.Cookies[cookieName];
			}
			
			//add value
			cookie.Values[itemName] = itemValue;

			//save it
			context.Response.Cookies.Add(cookie);

			/*
			if (cookie != null)
			{
				cookie.Values[itemName] = itemValue;

				context.Response.Cookies.Add(cookie);
			}
			*/
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Gets a name-value item to a cookie.
		/// </summary>
		public static string GetCookieItem(HttpContext context, string cookieName, string itemName)
		{
			string value = string.Empty;

			HttpCookie cookie = context.Request.Cookies[cookieName];

			if (cookie != null)
				value = cookie.Values[itemName];

			return value;
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Deletes a cookie from the current context.
		/// </summary>
		public static void RemoveCookie(HttpContext context, string cookieName)
		{
			context.Response.Cookies[cookieName].Expires = DateTime.Now.AddMonths(-1);
			context.Request.Cookies.Remove(cookieName);	
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}
