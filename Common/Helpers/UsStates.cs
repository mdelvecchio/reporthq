using System;
using System.Data;
using System.Web;

namespace ReportHq.Common
{
	/// <summary>Enumeration for US States.</summary>
	public enum UsState
	{
		/// <summary> Default </summary>
		None,
		/// <summary> Alabama </summary>
		AL,
		/// <summary> Alaska </summary>
		AK,
		/// <summary> Arizona </summary>
		AZ,
		/// <summary> Arkansas </summary>
		AR,
		/// <summary> California </summary>
		CA,
		/// <summary> Colorado </summary>
		CO,
		/// <summary> Connecticut </summary>
		CT,
		/// <summary> Delaware </summary>
		DE,
		/// <summary> District of Columbia </summary>
		DC,
		/// <summary> Florida </summary>
		FL,
		/// <summary> Georgia </summary>
		GA,
		/// <summary> Hawaii </summary>
		HI,
		/// <summary> Iowa </summary>
		IA,
		/// <summary> Idaho </summary>
		ID,
		/// <summary> Illinois </summary>
		IL,
		/// <summary> Indiana </summary>
		IN,
		/// <summary> Kansas </summary>
		KS,
		/// <summary> Kentucky </summary>
		KY,
		/// <summary> Louisiana </summary>
		LA,
		/// <summary> Massachusetts </summary>
		MA,
		/// <summary> Maryland </summary>
		MD,
		/// <summary> Maine </summary>
		ME,
		/// <summary> Michigan </summary>
		MI,
		/// <summary> Minnesota </summary>
		MN,
		/// <summary> Missouri </summary>
		MO,
		/// <summary> Mississippi </summary>
		MS,
		/// <summary> Montana </summary>
		MT,
		/// <summary> North Carolina </summary>
		NC,
		/// <summary> North Dakota </summary>
		ND,
		/// <summary> Nebraska </summary>
		NE,
		/// <summary> New Hampshire </summary>
		NH,
		/// <summary> New Jersey </summary>
		NJ,
		/// <summary> New Mexico </summary>
		NM,
		/// <summary> Nevada </summary>
		NV,
		/// <summary> New York </summary>
		NY,
		/// <summary> Ohio </summary>
		OH,
		/// <summary> Oklahoma </summary>
		OK,
		/// <summary> Oregon </summary>
		OR,
		/// <summary> Pennsylvania </summary>
		PA,
		/// <summary> Rhode Island </summary>
		RI,
		/// <summary> South Carolina </summary>
		SC,
		/// <summary> South Dakota </summary>
		SD,
		/// <summary> Tennessee </summary>
		TN,
		/// <summary> Texas </summary>
		TX,
		/// <summary> Utah </summary>
		UT,
		/// <summary> Virginia </summary>
		VA,
		/// <summary> Vermont </summary>
		VT,
		/// <summary> Washington </summary>
		WA,
		/// <summary> Wisconsin </summary>
		WI,
		/// <summary> West Virginia </summary>
		WV,
		/// <summary> Wyoming </summary>
		WY
	}


	/// <summary>
	/// Helper class which contains collection of static helper methods for
	/// working with UsState enumeration
	/// </summary>
	public class UsStates
	{
		#region Constructors

		private UsStates()
		{

		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Public Methods

		/// <summary>
		/// Gets state name based on passed abbreviation.
		/// </summary>
		/// <param name="state">Enumerated US state</param>
		/// <returns>String of state name</returns>
		public static string GetName(string stateAbbreviation)
		{
			UsState state = GetState(stateAbbreviation);

			return GetName(state);
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Gets state name based on passed UsState enum.
		/// </summary>
		/// <param name="state">Enumerated US state</param>
		/// <returns>String of state name</returns>
		public static string GetName(UsState state)
		{
			switch (state)
			{
				case (UsState.AK): return ("Alaska");
				case (UsState.AL): return ("Alabama");
				case (UsState.AR): return ("Arkansas");
				case (UsState.AZ): return ("Arizona");
				case (UsState.CA): return ("California");
				case (UsState.CO): return ("Colorado");
				case (UsState.CT): return ("Connecticut");
				case (UsState.DC): return ("District of Columbia");
				case (UsState.DE): return ("Delaware");
				case (UsState.FL): return ("Florida");
				case (UsState.GA): return ("Georgia");
				case (UsState.HI): return ("Hawaii");
				case (UsState.IA): return ("Iowa");
				case (UsState.ID): return ("Idaho");
				case (UsState.IL): return ("Illinois");
				case (UsState.IN): return ("Indiana");
				case (UsState.KS): return ("Kansas");
				case (UsState.KY): return ("Kentucky");
				case (UsState.LA): return ("Louisiana");
				case (UsState.MA): return ("Massachusetts");
				case (UsState.MD): return ("Maryland");
				case (UsState.ME): return ("Maine");
				case (UsState.MI): return ("Michigan");
				case (UsState.MN): return ("Minnesota");
				case (UsState.MO): return ("Missouri");
				case (UsState.MS): return ("Mississippi");
				case (UsState.MT): return ("Montana");
				case (UsState.NC): return ("North Carolina");
				case (UsState.ND): return ("North Dakota");
				case (UsState.NE): return ("Nebraska");
				case (UsState.NJ): return ("New Jersey");
				case (UsState.NH): return ("New Hampshire");
				case (UsState.NM): return ("New Mexico");
				case (UsState.NV): return ("Nevada");
				case (UsState.NY): return ("New York");
				case (UsState.OH): return ("Ohio");
				case (UsState.OK): return ("Oklahoma");
				case (UsState.OR): return ("Oregon");
				case (UsState.PA): return ("Pennsylvania");
				case (UsState.RI): return ("Rhode Island");
				case (UsState.SC): return ("South Carolina");
				case (UsState.SD): return ("South Dakota");
				case (UsState.TN): return ("Tennessee");
				case (UsState.TX): return ("Texas");
				case (UsState.UT): return ("Utah");
				case (UsState.VA): return ("Virginia");
				case (UsState.VT): return ("Vermont");
				case (UsState.WA): return ("Washington");
				case (UsState.WI): return ("Wisconsin");
				case (UsState.WV): return ("West Virginia");
				case (UsState.WY): return ("Wyoming");
			}

			return String.Empty;
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Gets state abbreviation based on passed state name. 
		/// </summary>
		/// <param name="state">state name</param>
		/// <returns>String of state abbreviation</returns>
		public static string GetAbbreviation(string state)
		{
			string abbrev = string.Empty;

			foreach (UsState item in GetStates())
			{
				string stateName = GetName(item);

				if (state.ToLower() == stateName.ToLower())
					abbrev = item.ToString();
			}

			return abbrev;
		}
		
		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Gets UsState enum based on state abbreviation string.
		/// </summary>
		/// <param name="state">state abbriviation to convert to enum</param>
		/// <returns>UsState enum</returns>
		public static UsState GetState(string state)
		{
			foreach (UsState item in GetStates())
			{
				if (item.ToString() == state.ToUpper())
					return item;
			}

			return UsState.None;
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Gets all US states (enum is abbreviations; bindable to lists).
		/// </summary>
		/// <returns>Array of all US states</returns>
		public static UsState[] GetStates()
		{
			UsState[] states = 
			{
				UsState.AL, UsState.AK, UsState.AZ, UsState.AR, UsState.CA, UsState.CO, UsState.CT, UsState.DE, UsState.DC, UsState.FL,
				UsState.GA, UsState.HI, UsState.IA, UsState.ID, UsState.IL, UsState.IN, UsState.KS, UsState.KY, UsState.LA, UsState.MA,
				UsState.MD, UsState.ME, UsState.MI, UsState.MN, UsState.MO, UsState.MS, UsState.MT, UsState.NC, UsState.ND, UsState.NE,
				UsState.NH, UsState.NJ, UsState.NM, UsState.NV, UsState.NY,	UsState.OH,	UsState.OK,	UsState.OR,	UsState.PA,	UsState.RI,
				UsState.SC, UsState.SD, UsState.TN,	UsState.TX,	UsState.UT,	UsState.VA,	UsState.VT,	UsState.WA,	UsState.WI,	UsState.WV,	UsState.WY
			};
			return states;
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Gets a cached table of all states and abbreviations.
		/// </summary>
		/// <returns>DataTable of two string columns</returns>
		public static DataTable GetStateNamesAndAbbreviations(bool upperCase)
		{
			const string KEYNAME = "dtUsStates";

			HttpContext context = HttpContext.Current;

			if (context.Cache[KEYNAME] == null) //not in cache, create
			{
				DataTable states = new DataTable();
				states.Columns.Add("State");
				states.Columns.Add("Abbreviation");

				foreach (UsState state in GetStates())
				{
					DataRow row = states.NewRow();

					if (upperCase)
						row["State"] = GetName(state).ToUpper();
					else
						row["State"] = GetName(state);

					row["Abbreviation"] = state.ToString();
					states.Rows.Add(row);
				}

				context.Cache.Insert(KEYNAME, states, null, DateTime.Now.AddDays(7), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)context.Cache[KEYNAME];
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}
