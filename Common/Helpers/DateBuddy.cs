using System;
using System.Collections.Generic;

namespace ReportHq.Common
{
	/// <summary>
	/// Static helper class for working with dates & times.
	/// </summary>
	public class DateBuddy
	{
		#region Enums

		public enum DayOrNight
		{
			Day,
			Night
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion 

		#region Public Methods

		/// <summary>
		/// Takes in the time of day and returns an enum for day/night.
		/// </summary>
		/// <param name="theTime"></param>
		/// <returns>DayOrNight enum of Day or Night</returns>
		public static DayOrNight GetDayOrNight(TimeSpan theTime)
		{
			//note: TimeSpan is the amount of time since midnight. so 6am is 6 hours, 6pm is 18 hours.

			//TimeSpan sixAM = new TimeSpan(6, 0, 0);
			//TimeSpan sixPM = new TimeSpan(18, 0, 0);

			if (theTime >= new TimeSpan(6, 0, 0) && theTime < new TimeSpan(18, 0, 0))
			{
				return DayOrNight.Day;
			}
			else
			{
				return DayOrNight.Night;
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Takes in DateTime and returns an enum for day/night.
		/// </summary>
		/// <param name="theTime"></param>
		/// <returns>DayOrNight enum of Day or Night</returns>
		public static DayOrNight GetDayOrNight(DateTime theDate)
		{
			//note: TimeSpan is the amount of time since midnight. so 6am is 6 hours, 6pm is 18 hours.

			TimeSpan theTime = theDate.TimeOfDay;
			
			if (theTime >= new TimeSpan(6, 0, 0) && theTime < new TimeSpan(18, 0, 0))
			{
				return DayOrNight.Day;
			}
			else
			{
				return DayOrNight.Night;
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Returns an enumerable collection of DateTimes for the supplied start/end dates.
		/// </summary>
		/// <param name="from"></param>
		/// <param name="thru"></param>
		/// <returns>Enumerable list of DateTimes</returns>
		/// http://stackoverflow.com/questions/1847580/how-do-i-loop-through-a-date-range
		public static IEnumerable<DateTime> GetDateRange(DateTime from, DateTime to)
		{
			for (var days = from.Date; days.Date <= to.Date; days = days.AddDays(1)) //.AddDays(1) adds the date to the return list. optionally: could perform other checks, like skip days, or weekdays only, etc.
			{
				yield return days;
			}
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}
