﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportHq.Common
{
	/// <summary>
	/// Helper methods for working with the Telerik RadGrid asp.net control.
	/// </summary>
	public class RadGrid
	{
		#region Public Methods

		/// <summary>
		/// Used when binding grid to UserControl form. Turns optional/null bool into false.
		/// </summary>
		/// <param name="dataBindValue"></param>
		/// <returns></returns>
		public static bool SetOptionalBool(object dataBindValue)
		{
			//if this is an Insert, databinder value will be DBNull, so set to false. otherwise, evaluate
			if (dataBindValue == DBNull.Value)
			{
				return false;
			}
			else
			{
				return Convert.ToBoolean(dataBindValue);
			}
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Used when binding grid to UserControl form. Turns optional/null inches-height into imperial height.
		/// </summary>
		/// <param name="dataBindValue"></param>
		/// <returns></returns>
		public static string ConvertHeight(object dataBindValue)
		{
			//if this is an Insert, databinder value will be DBNull. so only perform conversion if not.
			if (dataBindValue == DBNull.Value)
			{
				return string.Empty;
			}
			else
			{
				return Conversions.GetHeightFromInches(Convert.ToInt32(dataBindValue));
			}
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}
